	<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'NewUserCreationForm',
	'enableClientValidation'=>true,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
	'validateOnSubmit'=>true,
	),
)); 	?>
    
	<script type="text/javascript">		
	function Clear(){
		$("#NewUserCreationForm_fullName").val('');
		$("#NewUserCreationForm_userName").val('');
		$("#NewUserCreationForm_passWord").val('');
		$("#NewUserCreationForm_confirmPassword").val('');
		$("#NewUserCreationForm_emailId").val('');
		$("#NewUserCreationForm_department").val('');
		$("#NewUserCreationForm_referredFromDept").val('');
		$("#NewUserCreationForm_referredFromVcd").val('');
		
       }
	</script>
	<div class="signin-wrapper">
	<div class="widget">
    <div>
	<div id="title"><h4><u><?php echo $form->labelEx($model,'Title',array('style'=>'font-weight: bold;font-size:20px;')); ?></u>
	</h4></div></div>	
    <div align="center"><?php echo $msg; ?></div>
	<table width="100%" border="0" cellpadding="1">
    <tr>
	<td>
	<tr>
	<td>
	<?php echo $form->labelEx($model,'fullName');?> 
	</td>
	<td width="150px;">	<div>
	<?php echo $form->textField($model,'fullName',array('style'=>'width:215px','onkeydown'=>"return alphaonly('NewUserCreationForm_fullName')",'maxlength'=>'100'));
	echo $form->error($model,'fullName');?>
	</div>
	</td>
	</tr>
	<tr>
	<td>
	<?php echo $form->labelEx($model,'userName');?> 
	</td>
	<td>	<div>
	<?php echo $form->textField($model,'userName',array('style'=>'width:215px','onkeydown'=>"return alphanumeric('NewUserCreationForm_userName')",'maxlength'=>'50'));
	echo $form->error ($model,'userName');?>
	</div>
	</td>
	</tr>	
	<tr>
	<td>
	<?php echo $form->labelEx($model,'passWord');?> 
	</td>
	<td width="150px;"><div>
	
	<?php echo $form->passwordField($model,'passWord',array('style'=>'width:215px','onkeydown'=>"return alphanumeric('NewUserCreationForm_passWord')",'maxlength'=>'50'));
	echo $form->error    ($model,'passWord');?>
	</div>
	</td>
	</tr>
	<tr>	
	<td>
	<?php echo $form->labelEx($model,'confirmPassword');?> 
	</td>
	<td><div>	
	<?php echo $form->passwordField($model,'confirmPassword',array('style'=>'width:215px','onkeydown'=>"return alphanumeric('NewUserCreationForm_confirmPassword')",'maxlength'=>'50'));
	echo $form->error ($model,'confirmPassword');?>
	</div>

	</td>
	</tr>
	<tr>
	<td>
	<?php echo $form->labelEx($model,'emailId');?> 
	</td>
	<td width="150px;"><div>
	
	<?php echo $form->textField($model,'emailId',array('style'=>'width:215px','onkeydown'=>"return emailonly('NewUserCreationForm_emailId')",'maxlength'=>'50'));
	echo $form->error($model,'emailId');?>
	</div>

	</td>
	</tr>
	<tr>
	<td>
	<?php echo $form->labelEx($model,'department');?> 
	</td>
	<td width="150px;"><div>
	
	<?php echo $form->dropDownList($model,'department',CHtml::listData($department,'Department_Code','Department'),array('prompt'=>'-- Select--')); echo $form->error($model,'department'); ?>
</div>
	</td>
	</tr>
	<tr>
	<td>
	<?php echo $form->labelEx($model,'referredFromDept');?> 
	</td>
	<td width="150px;"><div>
	
	<?php echo $form->textField($model,'referredFromDept',array('style'=>'width:215px','onkeydown'=>"return alphaonly('NewUserCreationForm_referredFromDept')",'maxlength'=>'50'));
	echo $form->error    ($model,'referredFromDept');?>
	</div>
	</td>
	</tr>
	
	<tr>
	<td>
	<?php echo $form->labelEx($model,'referredFromVcd');?> 
	</td>
	<td width="150px;"><div>
	
	<?php echo $form->textField($model,'referredFromVcd',array('style'=>'width:215px','onkeydown'=>"return alphaonly('NewUserCreationForm_referredFromVcd')",'maxlength'=>'50'));
	echo $form->error    ($model,'referredFromVcd');?>
	</div>
	</td>
	</tr>
		
	</td>
	</tr>
	</table>	
	<table>
	<tr> 	
	<td><?php echo CHtml::submitButton('Submit',array('class'=>'btn btn-primary','name'=>'n%7*#')) ?>
	<a href="../"><?php echo CHtml::Button('Back',array('class'=>'btn btn-primary')) ?></a>
	</td>
	</tr></table></div></div>
</div>
	<?php $this->endWidget(); ?>
