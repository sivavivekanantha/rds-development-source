<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'htmlOptions'=>array('class'=>'signin-wrapper'),
	'enableClientValidation'=>true,
	'clientOptions'=>array(
	'validateOnSubmit'=>true,
	),
)); 	?>  


<div>
<img alt="full screen background image" src="../images/bg.jpg"/>
            <div style="padding-left:180px;" class="errorMessage">
			<div align="left"><?php echo $message;?></div>
			</div>
              <div class="content">
     			<?php echo $form->textField($model,'username',array('class'=>'span12','placeholder'=>'Username',
				'title'=>'Fill your Username')); ?> 
                    <?php echo $form->error($model,'username'); ?>
                    <?php echo $form->passwordField($model,'Password',array('class'=>'input input-block-level','placeholder'=>'Password',
					'title'=>'Fill your Password')); ?>
                    <?php echo $form->error($model,'Password'); ?>
              </div>
              <div>
			 <table align="center"><tr><td><input class="btn btn-primary" name="l#g*v" type="submit" value="Sign In"></td></tr></table>
			 <br>		 
			    <table align="center"><tr><td>
				<a href="<?php echo Yii::app()->request->baseUrl?>/index.php/Site/NewUserCreation"><?php echo CHtml::Button('NewUser-Clickhere',array('class'=>'btn btn-primary')) ?></a>
				</td></tr></table>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<table align="center"><tr><td>
				<a href="<?php echo Yii::app()->request->baseUrl?>/index.php/Site/ForgetPassword"><span style="color: blue;"><font size="2"><u>I can't access my account</u></font></span></a>				
			 </td></tr></table>
			    <div class="clearfix"></div>
              </div></div>	

                
  <?php $this->endWidget();?>


