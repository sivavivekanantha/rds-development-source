	<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ForgetPasswordForm',
	'enableClientValidation'=>true,
	'htmlOptions'=>array('class'=>'signin-wrapper'),
	'enableClientValidation'=>true,
	'clientOptions'=>array(
	'validateOnSubmit'=>true,
	),
)); 	?>
    
	
	<div style="padding:10px; margin:7px;>
    <div class="widget-header">
    <div class="title">
    <div><h4><u><?php echo $form->labelEx($model,'Header',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4></div>
    </div></div>
	<div align="center"><?php echo $msg; ?></div>
	<table width="100%" border="0" cellpadding="1">    
	<tr>
		<td>
			<?php echo $form->labelEx($model,'fullName');?> 
		</td>
	    <td>	
	    <div>
	<?php echo $form->textField($model,'fullName',array('style'=>'width:215px','onkeydown'=>"return alphaonly('ForgetPasswordForm_fullName')",'maxlength'=>'100'));
	echo $form->error($model,'fullName');?>
	   </div>
	  </td>
	</tr>	
	<tr>
	    <td>
	        <?php echo $form->labelEx($model,'emailId');?> 
	   </td>
	   <td><div>	
	<?php echo $form->textField($model,'emailId',array('style'=>'width:215px','onkeydown'=>"return emailonly('ForgetPasswordForm_emailId')",'maxlength'=>'50'));
	echo $form->error    ($model,'emailId');?>
	   <div align="left" style="padding:0px";>
       <span style="color:#aa0000"> Hint: Pls provide mail id that you have already given in RDS</span></div>
	   </div>
	   </td>
	</tr>
	
	<tr> 	
	<td><?php echo CHtml::submitButton('Submit',array('class'=>'btn btn-primary','name'=>'f%3*w')) ?></td>
	<td><a href="../"><?php echo CHtml::Button('Back',array('class'=>'btn btn-primary')) ?></a></td>
	</tr>
	
	 <tr>
	 <td colspan="2">
	 <span style="color:#aa0000">Hint: If you are unable to proceed for any reason,click here. </span>
			 	<a href="<?php echo Yii::app()->request->baseUrl?>/index.php/Site/Helpdesk"><img src="../../images/contactus.png"/></a>
	</td>
	</tr>
	
	</table>	

	<?php $this->endWidget(); ?>
