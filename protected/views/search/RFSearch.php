
	<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'RFSearch',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); 
 ?>
 <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Isha Check in Check Out List</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
 /* Pusher.log = function(message) {
      if (window.console && window.console.log) {
        window.console.log(message);
      }
    };

    var pusher = new Pusher('df55cae3d661e7a25656');
    var channel = pusher.subscribe('test_channel');
    channel.bind('my_event', function(data) {
      alert('test');
    });
*/
 $(document).ready(function(){
        var callAjax = function(){
          $.ajax({
            method:'get',
            url:'<?php echo Yii::app()->baseUrl;?>/index.php/Search/RFshowauth?monitor='+$("#monitor").val(),
            success:function(data){
				if(jQuery.parseJSON(data).Status==1)
            		$("#checkin").prepend(jQuery.parseJSON(data).msg);
				else
					$("#checkout").prepend(jQuery.parseJSON(data).msg);
					
            }
          });
        }
       setInterval(callAjax,1000);
      }); 
	  
</script>
</head>
<body>
<input type='hidden' id='monitor' value='<?php echo $monitor; ?>' />
	<div id="header">
Isha Check In / Out Info</div>
<div id="tit">
<div id="in_tit">Recent check In list</div>
<div id="out_tit">Recent check Out list</div>
</div>
<div id="container">
<div id="checkin">

</div>
  <div id="checkout">
   
  </div>
</div>
</body>
</html>

		
<?php
 $this->endWidget(); ?>
