	<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'SearchForm',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); 
 ?>
 <style type="text/css" >
 th {
    background-color: rgb(54, 54, 54);
    color: rgb(255, 255, 255);
    font-size: 12px;
    font-weight: bold;
    height: 20px;
    padding: 2px;
    text-align: center;
}
.row1
{
background:#E4E4E4;
}
.row2
{
background:white;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
    $('#example').dataTable( {
        "sScrollY": "400px",
        "bPaginate": false,
		"fnServerData":getRows
    } );
} );
function ajaxDate(obj2)
{  
	var obj=$("#SearchForm_center").val();	
	var obj1=$("#SearchForm_program").val();	
	if(obj==''||obj1=='') return false;
	var url='<?php echo Yii::app()->baseUrl;?>/index.php/search/AjaxDate1?programCode='+obj1+'&centerCode='+obj+'&eventId='+obj2;
	$("#fromdate").load(url);
		//$("#per_State").load(url);
	
}
</script>
        <div class="pull-left">
          <h2>Resident's Information</h2>
	      </div>

          <div class="row-fluid">
          
          
			<table width="100%" border="0" cellpadding="1" style="padding:10px; margin:7px; border:dashed 1px #666666;">
<?php echo $msg; ?>
  <tr>
    <td>
	<table width="100%" border="0" cellpadding="1" >  
	<tr>
		<td colspan="6" style="font-weight:bold;">
			SearchBy
		</td>
	</tr>	
  	<tr>
  	  <td>
	  	<?php echo $form->labelEx($model,'uid');?>      
	 </td>
     <td colspan="5">
    	<div class="">
			<?php echo $form->textField($model,'uid'); ?> 
         </div>    
	</td>
  </tr>
  <tr>
		<td colspan="6" style="font-weight:bold;">
			Personal Info
		</td>
 </tr>
  <tr>
   <td>
	  	<?php echo $form->labelEx($model,'name');?>      
	 </td>
     <td >
    	<div class="">
			<?php echo $form->textField($model,'name'); ?> 
         </div>    
	</td>
	 <td>
	  	<?php echo $form->labelEx($model,'dob');?>      
	 </td>
     <td >
    	<div class="">
        <a href="javascript:NewCal('dob','ddmmyyyy')">               
          
			<?php echo $form->textField($model,'dob',array('id'=>'dob')); ?>
                <img src="../../images/cal.gif" width="16" height="16" border="0" alt="Pick a date"> </a>
           
	</td>
	 <td>
	  	<?php echo $form->labelEx($model,'gender');?>      
	 </td>
     <td >
    	<div class="">
			<?php echo $form->dropDownList($model,'gender',array(''=>'Select','1'=>'Male','2'=>'Female'),array('options'=>array('gender'=>array('Selected'=>TRUE)))); ?> 
         </div>    
	</td>
  </tr>
  <tr>
  	 <td>
	  	<?php echo $form->labelEx($model,'emailid');?>        
	 </td>
     <td >
    	<div class="">
			<?php echo $form->textField($model,'emailid'); ?> 
         </div>    
	</td>
	 <td>
	  	<?php echo $form->labelEx($model,'anytext');?>      
	 </td>
     <td >
    	<div class="">
			<?php echo $form->textField($model,'anytext'); ?> 
         </div>    
	</td>
	 <td>
	  	<?php echo $form->labelEx($model,'mobile');?>      
	 </td>
     <td >
    	<div class="">
			<?php echo $form->textField($model,'mobile'); ?> 
         </div>    
	</td>
  </tr>
  <tr>
		<td colspan="6" style="font-weight:bold;">
			Program Info
		</td>
 </tr>
 <tr>
  <td>
	  	<?php echo $form->labelEx($model,'program');?>      
	 </td>
 <td>
 <div class="">
		 	<?php echo $form->dropDownList($model,'program',CHtml::listData($program,'Program_Code','Program_Name'),array('prompt'=>'-- Select--','onchange'=>'ajaxDate();'),array('id'=>'program'));			 
			 echo $form->error($model,'program');			
			 ?>
         </div>   
</td>
<td>
	  	<?php echo $form->labelEx($model,'center');?>      
	 </td>
 <td>
 <div class="">
		 	<?php echo $form->dropDownList($model,'center',CHtml::listData($center,'Center_Code','Center_Name'),array('prompt'=>'-- Select--','onchange'=>'ajaxDate();')); ?>
         </div>   
		  
</td>
<td>
	  	<?php echo $form->labelEx($model,'teacher');?>      
	 </td>
 <td>
 <div class="">
		 	<?php echo $form->dropDownList($model,'teacher',array(''=>'Select'));			 
			 echo $form->error($model,'teacher');			
			 ?>
         </div>   
</td>
 </tr>
 <tr>
 	 <td>
	  	<?php echo $form->labelEx($model,'fromdate');?> 
	</td>
     <td >
			<?php echo $form->dropDownList($model,'fromdate',array(''=>'Select'),array('id'=>'fromdate'));			 
			 echo $form->error($model,'fromdate'); ?>
   
      
           </td>
		   
      
 </tr>
   
</table>
</td>
  </tr>

</table>

<table align="right" >  
	 <tr>
    	<td><?php echo CHtml::submitButton('Search',array('size'=>50,'class'=>'btn btn-primary')) ?></td>
    </tr>
	</table>
			</div>  
<br />




<table width="100%" border="1" cellspacing="5" cellpadding="5" style="padding:10px; margin:7px; border:dashed 1px #666666;" id="example">

	<?php 
	if(isset($_POST['SearchForm']))
		{?>
			<thead>
		
  		<th>S.No</th>
		 <th>UID</th>
		 <th>Photo</th>
		 <th>Name</th>
         <th>Address</th>
         <th>City</th>
		 <th>State</th>  
		 <th>Country</th>         
 
		 </thead>
		<tbody>	
			<?php if(mssql_num_rows($row)>0)
			{		
			$i=0;
			$colorflag=0;
			 while($field=mssql_fetch_array($row)) 
			{ $i++;			
				$colorflag++; ?>
	<tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?>>
					
					<td align="center"><?php echo $i; ?></td>
					<td align="center"><?php echo $field['IVC_Code']; ?></td>	
					<td align="center">test</td>	
					<td align="center"><?php echo $field['First_Name']; ?></td>	
					<td align="center"><?php echo $field['Res_Address1'].'</br>'
					.$field['Res_Address2']; ?></td>	
					
					<td align="center"><?php echo $field['City_Name']; ?></td>	
					<td align="center"><?php echo $field['State_Name']; ?></td>	
					<td align="center"><?php echo $field['Country_Name']; ?></td>	
				</tr>
			<?php } 
		}		
	else {?>		
			<tr>
			<td align="center" colspan="8">No Records Found.......</td>
			</tr>
		<?php }
		}?>
		</tbody>
		</table>
<script>ajaxDate(<?php echo $model->fromdate ?>)</script>
<?php $this->endWidget(); ?>
