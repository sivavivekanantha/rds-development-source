<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'NewUsersShowForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
					
                    )); 
                    ?>
					

<script type="text/javascript" src="http://gettopup.com/releases/latest/top_up-min.js"></script>

<script>

$( document ).ready(function() {
    $("#data-table span").html('');
});
	
function adminPermission(obj,obj1)
{	
	var st='N';
	if($("#admindata"+obj).is(':checked')) 	st='Y';	
	var msg="Are you sure you want to approve this User?";
  	if(st=='Y') {
		if(!confirm(msg)){
			$("#admindata"+obj).attr('checked',false);
			return false;
     	} else {
			   url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/UpdateAdminPermission?Header_Code='+obj1;
			   $("#fill").load(url);
		alert('User has been created Successfully and login credential has been sent to that user.');
			  $("#NewUsersShowForm").submit();		
	 		}
		}
}	
 	
</script>
 
 <div class="widget"> 
 
 <div id="fill"></div>
 
                   <div id="formTitle">
                   <div class="widget-header">
                   <div class="title"> <h4><u><?php echo $form->labelEx($model,'newUserShow',array('style'=>'font-weight: bold;font-size:20px;')); ?></u></h4></div></div></div>		
				  
				  <div class="widget-body">	
                 <div id="dt_example" class="example_alt_pagination">
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">                        
                        <thead><th><?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'select',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'fullName',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'userName',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'emailId',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'department',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'departmentCo',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'vcdMembers',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
	                    </thead>
                        <tbody>
						 <?php $i=0;?>
						<?php 
					
						  if(mssql_num_rows($dataReader)>0) {
                          while($field = mssql_fetch_array($dataReader))
						   { $i++;?>
						   <tr class="gradeX">
						   <td><?php echo $i; ?></td>		
 <td><input  type="checkbox" id="admindata<?php echo $i  ?>"  style="width:20px;" value="Y" onclick="adminPermission(<?php echo $i ;?>,
 <?php echo $field['Header_Code']?>)", <?php if($field['Active']=='Y') echo "checked";?> /></td>
 

                           <td><?php echo $field['First_Name']; ?></td>		
							  <td><?php echo $field['User_Name']; ?></td>		
							   <td><?php echo $field['Emailid']; ?></td>		
							    <td><?php echo $field['Department']; ?></td>		
								 <td><?php echo $field['ReferredDept']; ?></td>		
								  <td><?php echo $field['ReferredVCD']; ?></td>		
                            
                           </tr>
						   
						   
							<?php  } 
							}?>
	</tbody>

                    </table></div></div>
			
 		<div class="clearfix">	</div>
     </div>    
				 
 <?php $this->endWidget(); ?>
 
 
