<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'LanguageForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
<script>

	function popupValue(obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8)
	{
		
				//$("#errinfo1").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
				//$(".errorMessage").html('');   // Yii Error msg is empty after close, 
		if(obj6!=3) // Coming Add & Edit
		{
			if(obj7!=0) {  // If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings").show();			
				$('#accSettings').removeClass('modal hide fade').addClass('modal hide fade in');
			} else {
				$("#errinfo").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
				$(".errorMessage").html('');   // Yii Error msg is empty after close, 
			}
			if(obj2=='' && obj6!=1) obj2='D';
				if(obj3=='' && obj6!=1) obj3='D';
					if(obj4=='' && obj6!=1) obj4='D';
					if(obj8=='' && obj6!=1) obj8='D';
		$("#LanguageForm_languageName").val(obj1);
		$("#LanguageForm_speaking").val(obj2);
		$("#LanguageForm_reading").val(obj3);
		$("#LanguageForm_writing").val(obj4);
		$("#LanguageForm_ivcLanguageCode").val(obj5);
		$("#LanguageForm_action").val(obj6);
		$("#LanguageForm_typing").val(obj8);
		//$("#LanguageForm_Olanguage").val(obj8);
		
		}
		if(obj6==3) {  // Delete
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
			  //  $("#LanguageForm_Olanguage").val(obj8);
				$("#LanguageForm_action").val(obj6);
				$("#LanguageForm_languageName").val(obj1);
				$("#LanguageForm_speaking").val(obj2);
				$("#LanguageForm_reading").val(obj3);
				$("#LanguageForm_writing").val(obj4);
				$("#LanguageForm_ivcLanguageCode").val(obj5);
				$("#LanguageForm_typing").val(obj8);
				$("#LanguageForm").submit();
			}
		}
		  
		if(obj1>0)
		{      
			other_Language(obj1);
		}
		
	}
	function popupHide()
	{		

			$("#errinfo").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');   // Yii Error msg is empty after close, 
			$('#edPopup').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
			$("#accSettings").hide();	 // Popup Hide
	}
	
	function saveMotherTongue(obj)
	{

	        var msg="Are you sure you want to save this language as your Mother Tongue?";
			if(!confirm(msg)){
				return false;
			}
		
						
			else {		
		url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/Mother?&mother='+obj;	
		
				$("#test").load(url);
		//$("#MedicalForm").submit();
			}
		}
	
	
	
	function other_Language(obj)              
	{
		$("#Olanguage").hide();
		$("#Mlanguage").hide();	
		if(obj==8888) 
			$("#Olanguage").show();										
		if(obj==9999)
			$("#Mlanguage").show();	
	}
	$( document ).ready(function() {
    $("#data-table span").html('');
});
</script>

	
	<div class="widget">
	<div id="formTitle">
	<div class="widget-header">
    <div class="title" style="width: 300px">
	<h4><u><?php echo $form->labelEx($model,'language',array('style'=>'font-weight: bold;font-size:18px;')); ?> 	
    </u></h4>
    </div>
    </div>
    </div>	
	 <div id="test"></div>
	 <div class="widget-body">	
     <div align="center"> <?php echo $msg1; ?></div></div>
	 <div class="widget-body">	
	 <div id="dt_example" class="example_alt_pagination"> 
		  
				  <table align="right"><tr>			
				  <td><div><a href="#accSettings" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue('','','','','',1,0,'','')">Add New</a></div></td></tr></table>
				  
				
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">                
                        <thead>
						<th><?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'languageName',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'speaking',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'reading',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'writing',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'typing',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                        <th><?php echo $form->labelEx($model,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                        </thead>
                        <tbody>
                          <?php $i=0;?>
                          <?php while($field=mssql_fetch_array($row1))						  
						  { $i++;
						  ?>
                      <tr class="gradeX">
							<td align="center"><?php echo $i; ?></td>
                            <td><?php echo $field['Language']; ?></td>							
							<td><?php if($field['Speaking'] =='B') echo "Beginner"; else if($field['Speaking'] =='I')  echo "Intermediate"; 
							else if($field['Speaking'] =='F')  echo "Fluent"; else if($field['Speaking'] =='D' or $field['Speaking']=='' )  echo "Don't Know"; ?></td>
		                    <td><?php if($field['Reading'] =='B') echo "Beginner"; else if($field['Reading'] =='I')  echo "Intermediate"; 
							else if($field['Reading'] =='F')  echo "Fluent"; else if($field['Reading'] =='D' or $field['Reading']=='' )  echo "Don't Know"; ?></td>
						    <td><?php if($field['Writing'] =='B') echo "Beginner"; 
							else if($field['Writing'] =='I' )  echo "Intermediate"; 
							else if($field['Writing'] =='F')  echo "Fluent"; 
							else if($field['Writing'] =='D' or $field['Writing']=='' )  echo "Don't Know"; ?>
							
						</td>
					    	<td><?php if($field['Typing'] =='S') echo "Slow"; 
							else if($field['Typing'] =='M' )  echo "Medium"; 
							else if($field['Typing'] =='F')  echo "Fast"; 
							else if($field['Typing'] =='D' or $field['Typing']=='' )  echo "Don't Know"; ?>
								
						</td>
		                    <td style="width:20px;" class="hidden-phone">
							<a href="#accSettings" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue('<?php echo $field['Language_Code'] ?>','<?php echo $field['Speaking'] ?>','<?php echo $field['Reading'] ?>','<?php echo $field['Writing'];?>','<?php echo $field['IVC_Language_Code'] ?>',2,0,'<?php echo $field['Typing'];?>')">&#x270E</a>
						<?php if(Yii::app()->session['Freeze']=="N") { ?>
							<a class="btn btn-success btn-small hidden-phone"  data-original-title="Delete" onclick="popupValue('<?php echo $field['Language_Code'] ?>','<?php echo $field['Speaking'] ?>','<?php echo $field['Reading'] ?>','<?php echo $field['Writing'];?>','<?php echo $field['IVC_Language_Code'] ?>',3,0,'<?php echo $field['Typing'];?>')">&#x2717</a>
						<?php } ?>
		                </td>
		              </tr>
				<?php } ?>
	    	</tbody>
        </table>
		
										

		<div id="accSettings" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 	<div id="errinfo" class="error" align="center" > <?php echo $msg1; ?> </div>
		<div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide()"> x </button>
         	<h4><u><?php echo $form->labelEx($model,'language',array('style'=>'font-weight: bold;font-size:18px;')); ?> 	
 </u></h4>
    	</div> 
        <div class="modal-body">
   	      <div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
             <?php echo $form->hiddenField($model,'ivcLanguageCode'); ?>
			 
			   <?php echo $form->hiddenField($model,'action'); ?>
			
             </div>
			    <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
						  <?php echo $form->labelEx($model,'languageName'); ?>
                </div><div> 
				 <?php echo $form->dropDownList($model,'languageName',CHtml::listData($lan_Language,'Language_Code','Language'),array('prompt'=>'Select','onchange'=>'other_Language(this.value);'));
			 	echo $form->error($model,'languageName'); ?>
				</div>
		 	</div>
				
			  <div class="row-fluid"> 
			  <div id='Olanguage' style="display: none;">                                
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'Olanguage'); ?>
                </div><div >
				<?php echo $form->textField($model,'Olanguage',array('onkeydown'=>"return alphaonly('LanguageForm_Olanguage')"));
				 	echo $form->error($model,'Olanguage'); ?></div>	
		     </div>	
			 </div>	 
             
        </div> 
		
       <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'speaking'); ?>
                </div><div> 
				<?php echo $form->dropDownList($model,'speaking',array('B'=>'Beginner','I'=>'Intermediate','F'=>'Fluent','D'=>"Don't Know"),
				array('prompt'=>'Select'),
				array('options' => array('speaking'=>array('selected'=>true))));
				 echo $form->error($model,'speaking');?>
                 </div>
         </div>
		 
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'reading'); ?>
                </div><div> 
				<?php echo $form->dropDownList($model,'reading',array('B'=>'Beginner','I'=>'Intermediate','F'=>'Fluent','D'=>"Don't Know"),
				array('prompt'=>'Select'),
				array('options' => array('reading'=>array('selected'=>true))));
				 echo $form->error($model,'reading');?>
                 </div>
         </div>
		 
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'writing'); ?>
                </div><div> 
				<?php echo $form->dropDownList($model,'writing',array('B'=>'Beginner','I'=>'Intermediate','F'=>'Fluent','D'=>"Don't Know"),
				array('prompt'=>'Select'),
				array('options' => array('writing'=>array('selected'=>true))));
				 echo $form->error($model,'writing');?>
                 </div>
         </div>

		 <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'typing'); ?>
                </div><div> 
				<?php echo $form->dropDownList($model,'typing',array('S'=>'Slow','M'=>'Medium','F'=>'Fast','D'=>"Don't Know"),
				array('prompt'=>'Select'),
				array('options' => array('typing'=>array('selected'=>true))));
				 echo $form->error($model,'typing');?>
		         </div>
         </div>
		        
   </div>
                        
    
    <div class="modal-footer">
  
            <?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'l65!e','class'=>'btn btn-primary')); ?> 
             <button type="button" class="btn" aria-hidden="true" data-dismiss="modal" onclick="popupHide()"> Close </button>
        	
   </div>
                    
</div>

<?php if($model->languageName==8888 ){
		echo "<script>other_Language('".$model->languageName."')</script>"; 
	  } 

 if($model->action>0 and $model->action<>3 and (strlen($msg1)>3 or $model->errflag==1)){
				 echo "<div id='edPopup' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
				 echo "<script>popupValue('".$model->languageName."','".$model->speaking."','".$model->reading."','".$model->writing."','".$model->ivcLanguageCode."','".$model->action."',1,'".$model->typing."')</script>"; } ?>                    
 		<div class="clearfix">	</div>
     </div>    
	 </div>
	 </div>
	
	<div class="widget-body" >
	<div class="widget">
	<table align="center">
	<tr>
		<td><div><?php echo $form->labelEx($model,'motherTongue',array('style'=>'font-weight: bold; : font-weight: normal;'));?></div></td>
		<td>
		<table><tr><td><div>
		<?php echo $form->dropDownList($model,'motherTongue',CHtml::listData($language,'Language_Code','Language'),array('prompt'=>'Select', 'onchange'=>'saveMotherTongue(this.value)'));
 echo $form->error($model,'motherTongue');	 ?>			 
			 </div></td>
			</tr></table>
		</td>
	</tr>
</table>
</div>
</div>
 <?php $this->endWidget(); ?>
