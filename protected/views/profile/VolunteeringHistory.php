<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'VolunteeringIshaForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
<script>
$( document ).ready(function() {
    $("#data-table span").html('');
});


	function popupValue(obj1,obj2,obj3,obj4,obj5,obj6,obj7)
	{		
	if(obj4!=3) // Coming Add & Edit
	{
		if(obj5!=0) {  // If u click edit background is fade
			$("#accSettings").show();			
			$("#fade_form").attr("class", "modal-backdrop fade in");
			$('#accSettings').removeClass('modal hide fade').addClass('modal hide fade in');
		}
		$("#VolunteeringIshaForm_volunteeringCode").val(obj1);	
		$("#VolunteeringIshaForm_programName").val(obj2);		
		$("#VolunteeringIshaForm_typeOfVolunteering").val(obj3);
		$("#VolunteeringIshaForm_action").val(obj4);
		$("#VolunteeringIshaForm_Others").val(obj6);
		if(obj2=='8888') Other_Program(obj2);
			
		}
		if(obj4==3) {  // Delete
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
		$("#VolunteeringIshaForm_volunteeringCode").val(obj1);
		$("#VolunteeringIshaForm_programName").val(obj2);
		$("#VolunteeringIshaForm_typeOfVolunteering").val(obj3);
		$("#VolunteeringIshaForm_action").val(obj4);
		$("#VolunteeringIshaForm_Other").val(obj6);	
		$("#VolunteeringIshaForm").submit();
			}
		}

		
	}
	function popupHide()
	{		
	  		$("#errinfo").html('');
	        $('#errinfo').hide();
	         // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');   // Yii Error msg is empty afterFca, 
			$('#edPopup').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
			$("#accSettings").hide();	 // Popup Hide	
	}	
	
	function Other_Program(obj)              
	{
		if(obj==8888) $("#thers").show();	
		else
		{
		 $("#thers").hide();
		 $("#VolunteeringIshaForm_Others").val('');
		}
	}
	
	
	function showdove(obj,obj1)
	{
	if(obj==1)	
			$('#showdiv').show();	
	
	if(obj==2 && obj1==1){

		var msg="Are you sure you want to save You did not Volunteer Isha Center?";
			if(!confirm(msg)){
			return false;
			}
		else 
		{					
		url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/SaveVolunteerIshaFormValid?&FormValid='+obj;
		$("#test").load(url);	
		//$("#MedicalForm_action").val('0');
		$('#showdiv').hide();
		}
	}
	
}
			
	
			
</script>
<div class="widget">
<div id="test" style="display: none;"></div>
 
<div class="widget-header">
<div class="title">
<div><h4><u><?php echo $form->labelEx($model,'VolunteeringA',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4></div>
</div></div>
<div class="widget-body">	
<div id="errinfo3" class="error" align="center" > <?php echo $msg3; ?> </div></div>
              <div class="widget-body">
		      <div id="dt_example" class="example_alt_pagination">
			  <h4><u><?php echo $form->labelEx($model,'VolunteeringB',array('style'=>'font-weight: bold;font-size:15px;')); ?></u></h4>
			  <div class="widget-body">
			  <table>
				  <?php if(mssql_num_rows($row1)==0)  { 		if($model->formValid<>2) $model->formValid='';	 ?>
				<tr><td><?php echo $form->labelEx($model,'formValid')?></td>
				<td><?php echo $form->dropDownList($model,'formValid',array(''=>'Select','1'=>'Yes','2'=>'No'),
				array('onchange'=>'showdove(this.value,1);'))?></td>
				</tr>
<?php } ?>		</table></div>
			    	 
                   <div id="showdiv" style="display: none;"> <table align="right" ><tr>
			
				  <td><div><a href="#accSettings" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue('','','',1,0,'')">Add New</a></div></td></tr></table>
				  
				 
				
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">                        
                        <thead><th><?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>		    
                               <th><?php echo $form->labelEx($model,'programName',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
					<th><?php echo $form->labelEx($model,'typeOfVolunteering',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							  <th><?php echo $form->labelEx($model,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                               </thead>
						 <tbody>
						 <?php $i=0;?>
						<?php 
					
						  if(mssql_num_rows($row1)>0) {
                          while($field = mssql_fetch_array($row1))
						   { $i++;?>
                      		<tr class="gradeX">
							<td align="center"><?php echo $i; ?></td>
                             <td><?php if($field['Program_Code']<>'8888') echo $field['Program_Name'];
							else  echo $field['Others'];?></td>
							<td><?php echo $field['Type_Of_Volunteering'];?></td>		
							<td style="width:20px;" class="hidden-phone">
							
							<a href="#accSettings" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue('<?php echo $field['IVC_VolunteerHistory_Code'] ?>','<?php echo $field['Program_Code'] ?>','<?php echo $field['Type_Of_Volunteering'] ?>',2,0,'<?php echo $field['Others'] ?>')">&#x270E</a>
							
			
							<?php if(Yii::app()->session['Freeze']=="N") { ?>
                            <a class="btn btn-success btn-small hidden-phone"  id="delbtn1"   data-original-title="Delete" onclick="popupValue('<?php echo $field['IVC_VolunteerHistory_Code'] ?>','<?php echo $field['Program_Code'] ?>','<?php echo $field['Type_Of_Volunteering'] ?>',3,0,'<?php echo $field['Others'] ?>')">&#x2717</a>
							<?php } ?>
                            </td>
		
                        </tr>
														
					<?php } 
					}?>
                	</tbody>


                    </table>
					
			<div id="accSettings" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 		<div id="errinfo" class="error" align="center" > <?php echo $msg1; ?> </div>
		<div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide()"> x </button>
         	<h4><u><?php echo $form->labelEx($model,'VolunteeringPopup',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
    	</div> 
   <div class="modal-body">
   		<div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
             <?php echo $form->hiddenField($model,'volunteeringCode'); ?>
			   <?php echo $form->hiddenField($model,'action'); ?>
			 <?php echo $form->labelEx($model,'programName'); ?>
             </div><div>
			 <?php echo $form->dropDownList($model,'programName',CHtml::listData($program,'Program_Code','Program_Name'),array('prompt'=>'Select','onchange'=>'Other_Program(this.value)'));

			 echo $form->error($model,'programName');
			 ?>		
			 
             </div>
        </div>
		
		 <div class="row-fluid" id='thers' style="display: none;">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'Others'); ?>
                </div>
				<div>
				<?php echo $form->textField($model,'Others',array('onkeydown'=>"return alphaonly('VolunteeringIshaForm_Others')",
				'maxlength'=>'100')); echo $form->error($model,'Others'); ?>
				</div>	
		     </div>
	   
			   
			   <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'typeOfVolunteering'); ?>
                </div><div>
				<?php echo $form->textField($model,'typeOfVolunteering',array('onkeydown'=>"return alphaonly('VolunteeringIshaForm_typeOfVolunteering')",'maxlength'=>'50')); ?> 
				<?php echo $form->error($model,'typeOfVolunteering'); ?>      
                 </div>		 
		       </div>
			  
   </div>
                    
    
    <div class="modal-footer">
  
            <?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'i%$v#','class'=>'btn btn-primary')); ?> 
             <button type="button" class="btn" aria-hidden="true" data-dismiss="modal"
			  onclick="popupHide()"> Close </button>
        	
   </div>
                    
</div>

<?php if($model->action<>3 and (strlen($msg1)>3 or $model->errflag==1)) {
				 echo "<div id='edPopup' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
				 echo "<script>popupValue('".$model->volunteeringCode."','".$model->programName."','".$model->typeOfVolunteering."','".$model->action."',1,'".$model->Others."')</script>"; } ?>                    
 		<div class="clearfix">	</div>
     </div>    
	 </div>
	</div></div>
	 <?php 
	 if($model->formValid>0)	echo "<script>showdove('".$model->formValid."',0)</script>"; 
	 		elseif($model->errflag==1) echo "<script>showdove('1',0)</script>";  ?>
	 <?php if($model->programName=='8888'){
						 echo "<script>Other_Program('".$model->programName."')</script>"; 
						 } ?>

 <?php $this->endWidget(); ?>
 
 
 <?php /////////////////////////////////////////////////////////////////////////Third Form///////////////////////////////////////////?>
 <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'VolunteeringTextAreaForm',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
));  ?>
<div class="widget">
<div class="row-fluid">
<table width="98%" border="0" cellpadding="1">

<?php echo $form->hiddenField($model2,'action2'); ?>
    <tr>
    <td><?php echo $form->labelEx($model2,'stayedIsha',array('style'=>'margin-bottom:80px;'));?></td>
     <td><?php echo $form->textArea($model2,'stayedIsha',array('placeholder'=>'max. Length 500 chars.','style'=>'width: 450px; height: 103px;','maxlength'=>'500'));?></td><td align="right"><?php echo CHtml::submitButton('Save',array('id'=>'saveform1','name'=>'9hj%@','size'=>75,'class'=>'btn btn-primary')) ?></td>
   </tr>
   </table>
   <table >  <tr>
    	
    </tr></table>
 </div></div>
 <?php $this->endWidget(); ?>
 
 
 <?php /////////////////////////////second form//////////////////////////////?>

 
 
 
 <?php /////////////////////////////////////////////////////////////////////////Third Form///////////////////////////////////////////?>
 
 
 <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'VolunteeringLocalForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
<script>
$( document ).ready(function() {
    $("#data-table5 span").html('');
});


	function popupValue5(obj1,obj2,obj3,obj4,obj5,obj6,obj7)
	{		
		if(obj5!=3) // Coming Add & Edit
		{
			if(obj6!=0) {  // If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings5").show();			
				$('#accSettings5').removeClass('modal hide fade').addClass('modal hide fade in');
			}
		$("#VolunteeringLocalForm_volunteeringCode1").val(obj1);
		$("#VolunteeringLocalForm_programName1").val(obj2);
		$("#VolunteeringLocalForm_centerName1").val(obj3);		
		$("#VolunteeringLocalForm_typeOfVolunteering1").val(obj4);			
		$("#VolunteeringLocalForm_action1").val(obj5);	
		$("#VolunteeringLocalForm_Others1").val(obj7);

		if(obj2=='8888')
					Other_Program1(obj2);				
		}
		if(obj5==3) {  // Delete
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
		$("#VolunteeringLocalForm_volunteeringCode1").val(obj1);
	    $("#VolunteeringLocalForm_programName1").val(obj2);
		$("#VolunteeringLocalForm_centerName1").val(obj3);		
		$("#VolunteeringLocalForm_typeOfVolunteering1").val(obj4);	
		$("#VolunteeringLocalForm_action1").val(obj5);	
		$("#VolunteeringLocalForm_Others1").val(obj6);					
			
		$("#VolunteeringLocalForm").submit();
			}
		}
		
		
	}
	function popupHide5()
	{		$("#errinfo5").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');   // Yii Error msg is empty after close, 
			$('#edPopup5').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
			$("#accSettings5").hide();	 // Popup Hide	
	}	
	
	function Other_Program1(obj)              
			{

			  if(obj==8888) $("#Others1").show();	
			  						
			  else
			  {
			  	 $("#Others1").hide();
				 // $("#VolunteeringLocalForm_Others1").val('');
				 
				 }
		
			}

function showdove1(obj,obj1)
{
	if(obj==1)	
		$('#showdiv1').show();	
	if(obj==2 && obj1==1){
		var msg="Are you sure you want to save  You did not Volunteer any Loacal Center?";
			if(!confirm(msg)){
			return false;
			}
		else {					
		url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/saveVolunteerLocalFormValid?&FormValid='+obj;	
		$("#test1").load(url);	
		$('#showdiv1').hide();
	
	
		}
	}
	
}
			
	
			
</script>
<div class="widget">
<div id="test1" style="display: none;"> </div>
 <div style="padding:10px; margin:7px;">
 <div id="errinfo5" class="error" align="center" > </div>

		       <div id="dt_example5" class="example_alt_pagination"> 
			  <div class="title" style="margin-left: 10px;"><h4><u><?php echo $form->labelEx($model1,'VolunteeringC',array('style'=>'font-weight: bold;font-size:15px;')); ?></u></h4></div>
			  
			    	<div class="widget-body"> 
                  <table style="margin-left:0px;">				   
				  <?php  if(mssql_num_rows($row2)==0)  { 
				  if($model1->formValid1<>2) $model1->formValid1='';?>
				<tr><td colspan="1"><?php echo $form->labelEx($model1,'formValid1')?></td>
				<td><?php echo $form->dropDownList($model1,'formValid1',array(''=>'Select','1'=>'Yes','2'=>'No'),
				array('onchange'=>'showdove1(this.value,1);'))?></td>
				</tr>
<?php } ?>		</table></div>				 
                    <div id="showdiv1" style="display: none;"><table align="right"><tr>
			
				  <td><div><a href="#accSettings5" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue5('','','','',1,0,'')">Add New</a></div></td></tr></table>
				  
				 
				
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table5">                        
					<thead>						
					<th><?php echo $form->labelEx($model1,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
					<th><?php echo $form->labelEx($model1,'programName1',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
					<th><?php echo $form->labelEx($model1,'centerName1',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
					<th><?php echo $form->labelEx($model1,'typeOfVolunteering1',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
					<th><?php echo $form->labelEx($model1,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
					</thead>
						
						 <tbody>
						 <?php $i=0;?>
						<?php 
					
						  if(mssql_num_rows($row2)>0) {
                          while($field5 = mssql_fetch_array($row2))
						   { $i++;?>
                      		<tr class="gradeX">
							<td align="center"><?php echo $i; ?></td>
                            <td><?php if($field5['Program_Code']<>'8888') echo $field5['Program_Name'];
							else  echo $field5['Others'];?></td>
							<td><?php echo $field5['Center_Name'];?></td>		
							<td><?php echo $field5['Type_Of_Volunteering'];?></td>		
							
                            <td style="width:20px;" class="hidden-phone">
							
							<a href="#accSettings5" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue5('<?php echo $field5['IVC_VolunteerHistory_Code'] ?>','<?php echo $field5['Program_Code'] ?>','<?php echo $field5['Center_Code'] ?>','<?php echo $field5['Type_Of_Volunteering'] ?>',2,0,'<?php echo $field5['Others'] ?>')">&#x270E</a>
							
			
							<?php if(Yii::app()->session['Freeze']=="N") { ?>
                            <a class="btn btn-success btn-small hidden-phone"  id="delbtn"   data-original-title="Delete" onclick="popupValue5('<?php echo $field5['IVC_VolunteerHistory_Code'] ?>','<?php echo $field5['Program_Code'] ?>','<?php echo $field5['Center_Code'] ?>','<?php echo $field5['Type_Of_Volunteering'] ?>',3,0,'<?php echo $field5['Others']?>')">&#x2717</a>
							<?php } ?>
                            </td>
		
                        </tr>
														
					<?php } 
					}?>
                	</tbody>
                    </table>
					
			<div id="accSettings5" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 		<div id="errinfo5" class="error" align="center" > <?php echo $msg2; ?> </div>
		<div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide5()"> x </button>
         	<h4><u><?php echo $form->labelEx($model1,'VolunteeringLPopup',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
    	</div> 
   <div class="modal-body">
   		<div class="row-fluid">  
									 <div style="float:left; overflow: hidden; width:200px"> 									
									 <?php echo $form->hiddenField($model1,'action1'); ?>	
									 <?php echo $form->hiddenField($model1,'volunteeringCode1'); ?>			  
									 <?php echo $form->labelEx($model1,'programName1'); ?>
									 </div><div>
									 <?php echo $form->dropDownList($model1,'programName1',CHtml::listData($program,'Program_Code','Program_Name'),array('prompt'=>'Select','onchange'=>'Other_Program1(this.value)')); echo $form->error($model1,'programName1'); ?>
									 
									 </div>
									 </div>	 
									 
									 <div class="row-fluid" id='Others1' style="display: none;">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model1,'Others1'); ?>
                </div>
				<div>
				<?php echo $form->textField($model1,'Others1',array('onkeydown'=>"return alphaonly('VolunteeringLocalForm_Others1')",
				'maxlength'=>'100')); echo $form->error($model1,'Others1'); ?>
				</div>	
		     </div>  
			   
			   
			 <div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
             <?php echo $form->labelEx($model1,'centerName1'); ?>
             </div><div>
			 <?php echo $form->dropDownList($model1,'centerName1',CHtml::listData($center,'Center_Code','Center_Name'),array('prompt'=>'Select'));
			 echo $form->error($model1,'centerName1');
			 ?>
			 
             </div>
			 </div>  		
			   
			    
			   <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model1,'typeOfVolunteering1'); ?>
                </div><div>
				<?php echo $form->textField($model1,'typeOfVolunteering1',array('onkeydown'=>"return alphaonly('VolunteeringLocalForm_typeOfVolunteering1')",'maxlength'=>'50')); ?>  
				<?php echo $form->error($model1,'typeOfVolunteering1'); ?>      
                 </div>		 
		       </div>
   </div>
                    
    
    <div class="modal-footer">
  
            <?php echo CHtml::submitButton('Save',array('id'=>'saveform2','name'=>'&j*#h','class'=>'btn btn-primary')); ?> 
             <button type="button" class="btn" aria-hidden="true" data-dismiss="modal"
			  onclick="popupHide5()"> Close </button>
        	
   </div>
                    
</div>

<?php  if($model1->action1<>3 and (strlen($msg2)>3 or $model1->errflag==1)) {
				 echo "<div id='edPopup5' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
				 echo "<script>popupValue5('".$model1->volunteeringCode1."','".$model1->programName1."','".$model1->centerName1."','".$model1->typeOfVolunteering1."','".$model1->action1."',1,'".$model1->Others1."')</script>"; } ?>                    
 		<div class="clearfix">	</div>
     </div>    
	 </div>
	</div>
	
	
	<?php  if($model1->formValid1>0) echo "<script>showdove1('".$model1->formValid1."',0)</script>"; 
	elseif($model1->errflag==1) echo "<script>showdove1('1',0)</script>"; ?>
	  <?php if($model1->programName1=='8888'){
						 echo "<script>Other_Program1('".$model1->programName1."')</script>"; 
						 } ?>

 <?php $this->endWidget(); ?>