<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'EnclosureForm',
                    'enableClientValidation'=>true,
					'htmlOptions' => array(
   					'enctype' => 'multipart/form-data',    ),
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
					<script type="text/javascript">
    function fileCheck(obj) {
	
            var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
           	 if ($.inArray($(obj).val().split('.').pop().toLowerCase(), fileExtension) == -1){
			 	 alert("Only '.jpeg','.jpg', '.png', '.gif', '.bmp' formats are allowed.");
				 
			 	$('#EnclosureForm_documents').val('');
			 }
               
    }
</script>
<script>
$( document ).ready(function() {
    $("#data-table span").html('');
});
	$(document).ready(function() {
$("#EnclosureForm_documents").change(function () 
   { 
     var iSize = ($("#EnclosureForm_documents")[0].files[0].size / 1024); 

    
     if(201>iSize) 
     {
        iSize = (Math.round(iSize * 100) / 100)
        $("#lblSize").html("File size is "+ iSize  + "kb"); 
     }  
	 else
	 {
		  alert('File size too large') ; 
		  $('#EnclosureForm_documents').val('');
	 }
	     
}); 

}); 
	function popupValue(obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9)
	{	
	
		if(obj6!=3) // Coming Add & Edit
		{
			if(obj7!=0) {  // If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings").show();			
				$('#accSettings').removeClass('modal hide fade').addClass('modal hide fade in');
			}
		$("#EnclosureForm_enClosureName").val(obj1);
		$("#EnclosureForm_iycAddress").val(obj2);
		$("#EnclosureForm_cardNo").val(obj3);
		$("#cardExpDate").val(obj4);
		$("#EnclosureForm_Others").val(obj8);
		$("#EnclosureForm_documents").val(obj9);
		$("#EnclosureForm_ivcEnclosureCode").val(obj5);
		$("#EnclosureForm_action").val(obj6);
		
		}
		if(obj6==3) {  // Delete
		
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
			   $("#EnclosureForm_documents").val(obj9);
			    $("#EnclosureForm_Others").val(obj8);
			    $("#EnclosureForm_action").val(obj6);
				$("#EnclosureForm_ivcEnclosureCode").val(obj5);
				$("#EnclosureForm_enClosureName").val(obj1);
				$("#EnclosureForm_iycAddress").val(obj2);
				$("#EnclosureForm_cardNo").val(obj3);
				$("#EnclosureForm_cardExpDate").val(obj4);
				$("#EnclosureForm").submit();
			}
		}
			
		
				other_proof(obj1);
				
	
	}
	function popupHide()
	{		$("#errinfo").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');   // Yii Error msg is empty after close, 
			$('#edPopup').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
			$("#accSettings").hide();	 // Popup Hide
	}
	function other_proof(obj)              
			{
	
			  if(obj==1) $("#Others1").show();	
			  						
			  else
			  {
			  	 $("#Others1").hide();
				  $("#EnclosureForm_Others").val('');
				 
				 }
		
			}
function showdove(obj,obj1)
{
	
	if(obj==1)	
			$('#showdiv').show();	
	
	if(obj==2 && obj1==1){
		var msg="Are you sure you want to save this Enclosure?";
			if(!confirm(msg)){
			return false;
			}
		else {					
		url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/SaveEnclosureFormValid?&FormValid='+obj;	
		$("#test").load(url);	
		$('#showdiv').hide();
	
	
		}
	}
	}
</script>
  
		 <div class="widget"> 
		  <div id="test"></div>
         
				
                   <div id="formTitle">
                   <div class="widget-header">
                   <div class="title">
				   <h4><u><?php echo $form->labelEx($model,'enclosure',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
				   </div>	
				   </div>
				   </div>
				   
				   
				   <div class="widget-body">					
				  <table>
				  
				<?php    if(mssql_num_rows($row1)==0)  { ?>
				<tr><td colspan="1"><?php echo $form->labelEx($model,'formValid')?></td>
				<td><?php echo $form->dropDownList($model,'formValid',array(''=>'Select','1'=>'Yes','2'=>'No'),
				array('onchange'=>'showdove(this.value,1);'))?></td>
				</tr>
<?php } ?>		</table></div>	
                   <div class="widget-body">							 
                   <div id="showdiv" style="display:none;"> 
				   <div id="dt_example" class="example_alt_pagination" style="padding:10px; margin:7px;">
				   
				   <table align="right"><tr>			
				   <td><div><a href="#accSettings" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue('','','','','',1,0,'','')">Add New</a></div></td></tr></table>
				
        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">  
                        <thead><th><?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'enClosureName',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'iycAddress',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'cardNo',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'cardExpDate',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'documents',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>	
						<th><?php echo $form->labelEx($model,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                        </thead>
                        <tbody>
						<?php $i=0;?>
                          <?php while($field=mssql_fetch_array($row1))
						  { $i++; ?>
                          	
                      		<tr class="gradeX"><td><?php echo $i;?></td>
                            <td><?php if($field['Enclosure_Code']=='1'){ echo $field['Others'];}
							else{ echo $field['Enclouse']; }?></td>
							<td><?php if($field['IYC_Address'] =='Y') echo "Yes"; else  echo "No"; ?></td>
                            <td><?php echo $field['Card_No'];?></td>
							<?php if(strlen($field['Card_Exp_Date'])==10) 
							$raiseddate = date('d-m-Y',strtotime($field['Card_Exp_Date']));
							else $raiseddate="";
							?>
						
                            <td><?php echo $raiseddate;?></td>
							<td> 
							
							<?php  $filename=Yii::app()->basePath.'/docs/'.$field['Enclosure_Code'].'_'.Yii::app()->session['Header_Code'].'.jpg' ;
							
							 	$filename1=Yii::app()->basePath.'/docs/'.$field['IVC_Enclosure_Code'].'_'.Yii::app()->session['Header_Code'].'.jpg' ; 
							
							 $filename2=Yii::app()->basePath.'/docs/'.$field['Others'].'_'.Yii::app()->session['Header_Code'].'.jpg' ;
							 
						if(file_exists($filename)){	?>
							<a href="../../protected/docs/<?php echo $field['Enclosure_Code']."_".Yii::app()->session['Header_Code'];?>.jpg">
							<img src="../../protected/docs/<?php echo $field['Enclosure_Code']."_".Yii::app()->session['Header_Code'];?>.jpg?cod=<?php echo rand() ?>" class="avatar" style="width: 20px; height: 20px;" alt="Avatar">
							</a>
						<?php }  elseif(file_exists($filename1)) {?>
							<a href="../../protected/docs/<?php echo $field['IVC_Enclosure_Code']."_".Yii::app()->session['Header_Code'];?>.jpg">
							<img src="../../protected/docs/<?php echo $field['IVC_Enclosure_Code']."_".Yii::app()->session['Header_Code'];?>.jpg?cod=<?php echo rand() ?>" class="avatar" style="width: 20px; height: 20px;" alt="Avatar">
						<?php	} elseif(file_exists($filename2)) {?>
							<a href="../../protected/docs/<?php echo $field['Others']."_".Yii::app()->session['Header_Code'];?>.jpg">
							<img src="../../protected/docs/<?php echo $field['Others']."_".Yii::app()->session['Header_Code'];?>.jpg?cod=<?php echo rand() ?>" class="avatar" style="width: 20px; height: 20px;" alt="Avatar">
						<?php	} ?>

 	 <!--a href="download.php?filename=Beta/protected/docs/2_748.jpg"><img src="Beta/protected/docs/2_748.jpg" style="width:25px; height:25px; border:dashed 1px #000000; "/></a-->
                 
		  
		  </div>
			                <td style="width:20px;" class="hidden-phone">
							<a href="#accSettings" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue('<?php echo $field['Enclosure_Code'] ?>','<?php echo $field['IYC_Address'] ?>','<?php echo $field['Card_No'] ?>','<?php echo $raiseddate;?>','<?php echo $field['IVC_Enclosure_Code'] ?>',2,0,'<?php echo $field['Others'] ?>')">&#x270E</a>
							<?php if(Yii::app()->session['Freeze']=="N") { ?>
                            <a class="btn btn-success btn-small hidden-phone"  id="delbtn"   data-original-title="Delete" onclick="popupValue('<?php echo $field['Enclosure_Code'] ?>','<?php echo $field['IYC_Address'] ?>','<?php echo $field['Card_No'] ?>','<?php echo $field['Card_Exp_Date'];?>','<?php echo $field['IVC_Enclosure_Code'] ?>',3,0,'<?php echo $field['Others'] ?>')">&#x2717</a>
							<?php } ?>
                            </td>
                        </tr>
					<?php } ?>
                	</tbody>
                    </table>
		<div id="accSettings" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 	<div id="errinfo" class="error" align="center" > <?php echo $msg1; ?> </div>
		<div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide()"> x </button>
         	<h4><u><?php echo $form->labelEx($model,'enclosurePopup',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
           </div>       
        <div class="modal-body">
   		  <div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
             <?php echo $form->hiddenField($model,'ivcEnclosureCode'); ?>
			   <?php echo $form->hiddenField($model,'action'); ?>
			 <?php echo $form->labelEx($model,'enClosureName'); ?>
             </div>
			 <?php echo $form->dropDownList($model,'enClosureName',CHtml::listData($enclosure,'Enclose_Code','Enclouse'),array('prompt'=>'Select','onchange'=>'other_proof(this.value);'));
			 echo $form->error($model,'enClosureName');
			 ?>
			 
			 <div class="row-fluid">  
			 <div id='Others1' style="display: none;">                               
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'Others'); ?>
                </div>
				<div><?php echo $form->textField($model,'Others',array('onkeydown'=>"return alphaonly('EnclosureForm_Others')",
				'maxlength'=>'100')); echo $form->error($model,'Others'); ?></div>	
				</div>
		     </div>
			  
            
        </div> 
        
          <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'iycAddress'); ?>
                </div><div> 
				<?php echo $form->dropDownList($model,'iycAddress',array('Y'=>'Yes','N'=>'No'),
				array('options' => array('iycAddress'=>array('selected'=>true))));
				 echo $form->error($model,'iycAddress');?>
                 </div>
         </div>
		 
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'cardNo'); ?>
                </div><div>
				<?php echo $form->textField($model,'cardNo',array('maxlength'=>'50')); ?>  
				<?php echo $form->error($model,'cardNo'); ?>      
                 </div>
		 
		  </div>
		  
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
					<?php echo $form->labelEx($model,'cardExpDate');?>   
                </div><div>
                	
					<?php echo $form->textField($model,'cardExpDate',array('id'=>'cardExpDate','onkeydown'=>"return periodonly('cardExpDate')",'maxlength'=>'20')); ?>
					
        	    <?php echo $form->error($model,'cardExpDate'); ?>   
                 </div>
		  </div>
		  
		  <div class="row-fluid">  
		   <div style="float:left; overflow: hidden; width:200px">   
	       	<?php echo $form->labelEx($model,'documents'); ?></div><div>
			<?php echo $form->fileField($model, 'documents',array('style'=>'width:290px','onchange'=>'fileCheck(this)')); ?>(Max.size 200kb)  
			<?php echo $form->error($model,'documents'); ?>	
			<br><b><label id="lblSize" />	           
			</div></div> 
       
   
   </div>

     
       <div class="modal-footer">
  
            <?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'e&%sr','class'=>'btn btn-primary')); ?> 
             <button type="button" class="btn" aria-hidden="true" data-dismiss="modal" onclick="popupHide()"> Close </button>
        	
       </div></div></div></div>
      	</div>
        
<?php			
            if($model->enClosureName==1 ){
						 echo "<script>other_proof('".$model->enClosureName."')</script>"; 
						 } 

 if($model->action<>3 and (strlen($msg1)>3 or $model->errflag)) {
				 echo "<div id='edPopup' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
				 echo "<script>popupValue('".$model->enClosureName."','".$model->iycAddress."','".$model->cardNo."','".$model->cardExpDate."','".$model->ivcEnclosureCode."','".$model->action."',1,'".$model->Others."')</script>"; } ?>                    
 		<div class="clearfix"></div>
       
 <?php if($model->formValid>0)	
	 echo "<script>showdove('".$model->formValid."',0)</script>"; ?>
	  	<?php if(strlen($msg1)>3)	
	 echo "<script>showdove('1',0)</script>"; ?>
	
     <?php $this->endWidget(); ?>