<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'ProgramForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
<script>

	function popupValue(obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9)
	{	
		
		if(obj7!=3) // Coming Add & Edit
		{
				if(obj9!=0) {  // If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings").show();			
				$('#accSettings').removeClass('modal hide fade').addClass('modal hide fade in');
			}
		$("#ProgramForm_pgmName").val(obj1);
		$("#ProgramForm_pgmCenter").val(obj2);
		$("#ProgramForm_pgmMonth").val(obj3);
		$("#ProgramForm_pgmYear").val(obj4);
		$("#ProgramForm_pgmTeacher").val(obj5);
		$("#ProgramForm_rdsPgmCode").val(obj6);
		$("#ProgramForm_Others").val(obj8);
		$("#ProgramForm_action").val(obj7);
		
		}
		if(obj7==3) {  // Delete
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
		$("#ProgramForm_pgmName").val(obj1);
		$("#ProgramForm_pgmCenter").val(obj2);
		$("#ProgramForm_pgmMonth").val(obj3);
		$("#ProgramForm_pgmYear").val(obj4);
		$("#ProgramForm_pgmTeacher").val(obj5);
		$("#ProgramForm_rdsPgmCode").val(obj6);
		$("#ProgramForm_Others").val(obj8);
		$("#ProgramForm_action").val(obj7);
		$("#ProgramForm").submit();
			}
		}
		if(obj1=='Others')
		{
	
			other_program (obj1);
								
		}
	}
	function popupHide()
	{		$("#errinfo").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');   // Yii Error msg is empty after close, 
			$('#edPopup').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
			$("#accSettings").hide();	 // Popup Hide
	}
	
	function other_program(obj)              
			{
			if($.trim(obj)=="SAMYAMA") 
        		alert('If you attended multiple samyama , you can enter only the first one');	
			  if(obj=='Others') $("#Others").show();	
			  						
			  else {
			  	$("#Others").hide();
					$("#ProgramForm_Others").val('');
			
				
			}
		
			}
			$( document ).ready(function() {
    $("#data-table span").html('');
});
</script>
  
		
			
		<div class="widget">
                 
				  <div id="formTitle"> 
				  <div class="widget-header">
                  <div class="title" style="width: 300px">
				  <h4><u><?php echo $form->labelEx($model,'program',array('style'=>'font-weight: bold;font-size:18px;')); ?> 	 </u></h4>
				  </div>
				  </div>
				  </div>	
				  <div class="widget-body">	
				  <div id="dt_example" class="example_alt_pagination">				 
                  <table align="right"><tr>
				  <td><div><a href="#accSettings" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue('','','','','','',1,'',0)">Add New</a></div></td></tr></table>
				
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">                        
                        <thead><th><?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                            <th><?php echo $form->labelEx($model,'pgmName',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model,'pgmCenter',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model,'date',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model,'pgmTeacher',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                       </thead>
                        <tbody>
                          <?php $i=0;?>
                          <?php while($field=mssql_fetch_array($row1))
						  
						  { $i++;
						  ?>
                          	
                      		<tr class="gradeX">
							<td align="center"><?php echo $i; ?></td>
							<td><?php if($field['RDS_Pgm_Name']=='Others'){ echo $field['RDS_Pgm_Others'];}
							else{ echo $field['RDS_Pgm_Name']; }?></td>
                            <td><?php echo $field['RDS_Pgm_Center'];?></td>
							<?php $pgmmonth=date('m',strtotime($field['RDS_Pgm_Date']));?>
							<?php $pgmyear=date('Y',strtotime($field['RDS_Pgm_Date']));?>
                            <td><?php echo date('d-m-Y',strtotime($field['RDS_Pgm_Date']));?></td>
							<td><?php echo $field['RDS_Pgm_Teacher'];?></td>
                            <td style="width:20px;" class="hidden-phone">
							<a href="#accSettings" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue('<?php echo $field['RDS_Pgm_Name']; ?>','<?php echo $field['RDS_Pgm_Center'] ?>','<?php echo $pgmmonth; ?>','<?php echo $pgmyear; ?>','<?php echo $field['RDS_Pgm_Teacher'] ?>','<?php echo $field['RDS_Pgm_Code'] ?>',2,'<?php echo $field['RDS_Pgm_Others'] ?>',0)">&#x270E</a>
							<?php if(Yii::app()->session['Freeze']=="N") { ?>
                            <a class="btn btn-success btn-small hidden-phone"  id="delbtn"   data-original-title="Delete" onclick="popupValue('<?php echo $field['RDS_Pgm_Name'] ?>','<?php echo $field['RDS_Pgm_Center'] ?>','<?php echo $pgmmonth; ?>','<?php echo $pgmyear; ?>','<?php echo $field['RDS_Pgm_Teacher'];?>','<?php echo $field['RDS_Pgm_Code'] ?>',3,'<?php echo $field['RDS_Pgm_Others'] ?>',0)">&#x2717</a>
							<?php } ?>
                            </td>
                        </tr>
					<?php } ?>
                	</tbody>
                    </table>
			<div id="accSettings" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 		<div id="errinfo" class="error" align="center" > <?php echo $msg1; ?> </div>
		<div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide()"> x </button>
         	<h4><u><?php echo $form->labelEx($model,'program',array('style'=>'font-weight: bold;font-size:18px;')); ?> 	 </u></h4>
    	</div> 
   <div class="modal-body">
   		<div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
             <?php echo $form->hiddenField($model,'rdsPgmCode'); ?>
			   <?php echo $form->hiddenField($model,'action'); ?>
			 <?php echo $form->labelEx($model,'pgmName'); ?>
             </div><div>
			 <?php echo $form->dropDownList($model,'pgmName',CHtml::listData($programName,'Program_Name','Program_Name'),array('prompt'=>'Select','onchange'=>'other_program(this.value);'));

			 echo $form->error($model,'pgmName');
			 ?>		
			 
             </div>
        </div> 
		 <div class="row-fluid" id='Others' style="display: none;">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'Others'); ?>
                </div>
				<div>
				<?php echo $form->textField($model,'Others',array('maxlength'=>'50')); ?>					
				</div>	
		     </div>
       <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'pgmCenter'); ?>
                </div><div>
				<?php echo $form->textField($model,'pgmCenter',array('maxlength'=>'50')); ?>					
				<?php echo $form->error($model,'pgmCenter'); ?>      
                 </div>
	  </div>
		   		 
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'pgmMonth'); ?>
				</div><div>
	           <?php echo $form->dropDownList($model,'pgmMonth',$month,
				array('prompt'=>'Select','style'=>'width:100px'),
				array('options' => array('pgmMonth'=>array('selected'=>true)))); 
				echo $form->error($model,'pgmMonth'); ?>
				</div>
		  </div>
				
	 <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'pgmYear'); ?>
				</div><div>
	           <?php echo $form->dropDownList($model,'pgmYear',$year,
				array('prompt'=>'Select','style'=>'width:100px'),
				array('options' => array('pgmYear'=>array('selected'=>true)))); 
				echo $form->error($model,'pgmYear'); ?>
				</div>
		  </div>
		 		 
		 <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'pgmTeacher'); ?>
                </div><div>
				<?php echo $form->textField($model,'pgmTeacher',array('onkeydown'=>"return alphaonly('ProgramForm_pgmTeacher')",'maxlength'=>'50')); ?>  
				<?php echo $form->error($model,'pgmTeacher'); ?>      
                 </div>
	  </div>
   </div>
                        
    
    <div class="modal-footer">
  
            <?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'p81&6','class'=>'btn btn-primary')); ?> 
             <button type="button" class="btn" aria-hidden="true" data-dismiss="modal" onclick="popupHide()"> Close </button>
        	
   </div>
                    
</div>

	   <?php if($model->pgmName=='Others' ){
						 echo "<script>other_program('".$model->pgmName."')</script>"; 
						 } 
			
			
			 if($model->action<>3 and (strlen($msg1)>3 or $model->errflag==1)){
				 echo "<div id='edPopup' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
				 echo "<script>popupValue('".$model->pgmName."','".$model->pgmCenter."','".$model->pgmMonth."','".$model->pgmYear."','".$model->pgmTeacher."','".$model->rdsPgmCode."',".$model->action.",'".$model->Others."',1)</script>"; } ?>                      
 		<div class="clearfix">	</div>
     </div></div> 
	 </div>   
	
 <?php $this->endWidget(); ?>
