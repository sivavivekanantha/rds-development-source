<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'FamilyForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
<script>
    $( document ).ready(function() {
    $("#data-table span").html('');
    });
	function popupValue(obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9,obj10,obj11,obj12,obj13,obj14,obj15)
	{		
		if(obj14!=3) // Coming Add & Edit
		{
			if(obj15!=0) {  // If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings").show();			
				$('#accSettings').removeClass('modal hide fade').addClass('modal hide fade in');
			}
		$("#FamilyForm_memberName").val(obj1);
		$("#FamilyForm_relation").val(obj2);
		$("#FamilyForm_gender").val(obj3);
		$("#FamilyForm_famAge").val(obj4);		
		$("#FamilyForm_famOccupation").val(obj5);
		$('#FamilyForm_occupationAddress').val(unescape(obj6));
		$('#FamilyForm_famContactAddress').val(unescape(obj7));
		//$("#FamilyForm_occupationAddress").val(obj6);
		//$("#FamilyForm_famContactAddress").val(obj7);
	    if(obj8=='' || obj8==0); //obj8=102;
		$("#FamilyForm_famPhonecode").val(obj8);
		$("#FamilyForm_famContact").val(obj9);
		$("#FamilyForm_famEmailid").val(obj10);		
		$("#FamilyForm_famAssociation").val(obj11);
		$("#FamilyForm_famDepartment").val(obj12);
	
		$("#FamilyForm_familyCode").val(obj13);
		$("#FamilyForm_action").val(obj14);	
		ajaxdepartment_onload();
		}
		if(obj14==3) {  // Delete
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
		$("#FamilyForm_memberName").val(obj1);
		$("#FamilyForm_relation").val(obj2);
		$("#FamilyForm_gender").val(obj3);
		$("#FamilyForm_famAge").val(obj4);		
		$("#FamilyForm_famOccupation").val(obj5);
		$('#FamilyForm_occupationAddress').val(unescape(obj6));
		$('#FamilyForm_famContactAddress').val(unescape(obj7));
		//$("#FamilyForm_occupationAddress").val(obj6);
		//$("#FamilyForm_famContactAddress").val(obj7);
		if(obj8=='' || obj8==0);//obj8=102;
		$("#FamilyForm_famPhonecode").val(obj8);
		$("#FamilyForm_famContact").val(obj9);
		$("#FamilyForm_famEmailid").val(obj10);		
		$("#FamilyForm_famAssociation").val(obj11);
		$("#FamilyForm_famDepartment").val(obj12);
		$("#FamilyForm_familyCode").val(obj13);
		$("#FamilyForm_action").val(obj14);	
		ajaxdepartment_onload();
		$("#FamilyForm").submit();
			}
		}
		
	}

	function popupHide()
	{
		$("#errinfo").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
		$(".errorMessage").html('');   // Yii Error msg is empty after close, 
		$('#edPopup').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
		$("#accSettings").hide();	 // Popup Hide	
	}	
			
	function genderC(obj)
	{
		if(obj=="Father") 
			$('#FamilyForm_gender').val('1');
		if(obj=="Mother")
			$('#FamilyForm_gender').val('2');
	}

	function ajaxdepartment(obj,obj1)
	{
		if(obj=='' || obj=='None of the above')
		{
			$("#noneofthis").hide();
			$("#noneofthese").hide();	
			$('#FamilyForm_famDepartment').val('');
			$("#deptSpan").hide();
		}
		else
		{			
			$("#noneofthis").show();
			$("#noneofthese").show();
			$("#deptSpan").show();

			var url='<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/Ajaxdepartment?category='+obj+'&dept='+obj1;
			$("#FamilyForm_famDepartment").load(url);
		}
	}
	function ajaxdepartment_onload()
	{
		var obj = $("#FamilyForm_famAssociation").val();
		if(obj=='' || obj=='None of the above')
		{
			//$('#FamilyForm_famDepartment').hide();
			//document.getElementById("deptSpan").style.visibility='hidden';
			$("#deptSpan").hide();
		}
		else
		{
			$("#deptSpan").show();
			//$('#FamilyForm_famDepartment').show();
			//document.getElementById("deptSpan").style.visibility='visible';
		}
	}


</script>
<div class="widget">
 <div class="widget-header">
 <div class="title">
 <div><h4><u><?php echo $form->labelEx($model,'family',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4></div>
 </div></<div></div>
 <div class="widget">
 <div id="errinfo5" class="error" align="center" > <?php echo $msg2; ?> </div>
  <div>
  	
			<div>
		       <div id="dt_example" class="example_alt_pagination" >
			    <div class="title" style="margin-left: 15px;"><h4><u><?php echo $form->labelEx($model,'familyHint',array('style'=>'font-weight: bold;font-size:15px;')); ?></u></h4></div>
			    	 
                  <table align="right"><tr>
			
				  <td><div><a href="#accSettings" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue('','','','','','','','','','','','','',1,0)">Add New</a></div></td></tr></table>
				  
				 
				
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table" width="100px;">                        
                        <thead><th><?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'memberName',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'relation',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'gender',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'famAge',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'famOccupation',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'occupationAddress',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'famContactAddress',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'famContact',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'famEmailid',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'famAssociation',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'famDepartment',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                        </thead>
						
                        <tbody>
						<?php $i=0; ?>
						<?php if(mssql_num_rows($row1)>0) {
                          while($field = mssql_fetch_array($row1))
						   {$i++; ?>
                      		<tr class="gradeX">
							<td align="center"><?php echo $i; ?></td>
                            <td><?php echo $field['Member_Name'];?></td>		
							<td><?php echo $field['Fam_Relation'];?></td>	
                            <td><?php if($field['Fam_Gender'] =='1') echo "Male"; else if($field['Fam_Gender'] =='2')  echo "Female"; ?></td>							
							<td><?php echo $field['Fam_Age'];?></td>	
                            <td><?php echo $field['Fam_Occupation'];?></td>	
                            <td><?php echo $field['Fam_Occupation_Address'];?></td>								
							<td><?php echo $field['Fam_Address'];?></td>	
							
									<?php if(strlen($field['Fam_Contact']>0))  $d="-";else $d=''; ?>
																
							<td><?php echo $field['Phone_Code'].$d.$field['Fam_Contact']; ?></td>	
							
							<td><?php echo $field['Fam_Emailid'];?></td>							
							<td><?php echo $field['Fam_Association'];?></td>			
							<td><?php echo $field['Department'];?></td>	     							
                            <td style="width:20px;" class="hidden-phone">
							
							<a href="#accSettings" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue('<?php echo $field['Member_Name'] ?>','<?php echo $field['Fam_Relation'] ?>','<?php echo $field['Fam_Gender'] ?>','<?php echo $field['Fam_Age'] ?>','<?php echo $field['Fam_Occupation'] ?>','<?php echo rawurlencode($field['Fam_Occupation_Address']); ?>','<?php echo rawurlencode($field['Fam_Address']); ?>','<?php echo $field['Fam_Phonecode'] ?>','<?php echo $field['Fam_Contact'] ?>','<?php echo $field['Fam_Emailid'] ?>','<?php echo $field['Fam_Association'] ?>','<?php echo $field['Fam_Department'] ?>','<?php echo $field['Family_Code'] ?>',2,0)">&#x270E</a>
							
			
							<?php if(Yii::app()->session['Freeze']=="N") { ?>
                            <a class="btn btn-success btn-small hidden-phone"  id="delbtn"   data-original-title="Delete" onclick="popupValue('<?php echo $field['Member_Name'] ?>','<?php echo $field['Fam_Relation'] ?>','<?php echo $field['Fam_Gender'];?>','<?php echo $field['Fam_Age'];?>','<?php echo $field['Fam_Occupation'] ?>','<?php echo rawurlencode($field['Fam_Occupation_Address']);?>','<?php echo rawurlencode($field['Fam_Address']);?>','<?php echo $field['Fam_Phonecode'] ?>','<?php echo $field['Fam_Contact'] ?>','<?php echo $field['Fam_Emailid'] ?>','<?php echo $field['Fam_Association'] ?>','<?php echo $field['Fam_Department'] ?>','<?php echo $field['Family_Code'] ?>',3,0)">&#x2717</a>
							<?php } ?>
                            </td>
		
                        </tr>
														
					<?php } 
					}?>
                	</tbody>
                    </table>
					
			<div id="accSettings" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 		<div id="errinfo" class="error" align="center" > <?php echo $msg1; ?> </div>
		<div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide()"> x </button>
         	<h4><u><?php echo $form->labelEx($model,'familyDetails',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
    	</div> 
   <div class="modal-body">
   		<div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'memberName'); ?>
                </div><div>
				<?php echo $form->textField($model,'memberName',array('onkeydown'=>"return alphaonly('FamilyForm_memberName')",'maxlength'=>'50')); ?>  
				<?php echo $form->error($model,'memberName'); ?>      
                 </div>		 
		       </div>
			   
			  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'relation'); ?>
                </div><div> 
				<?php echo $form->dropDownList($model,'relation',array('Father'=>'Father','Mother'=>'Mother','Spouse'=>'Spouse','Children'=>'Children','Siblings'=>'Siblings'),array('prompt'=>'Select','onchange'=>'return genderC(this.value);'));
				 echo $form->error($model,'relation');?>
                 </div>
            </div>
			
			  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'gender'); ?>
                </div><div> 
				<?php echo $form->dropDownList($model,'gender',
				array(''=>'Select','1'=>'Male','2'=>'Female'));
				 echo $form->error($model,'gender');?>
                 </div>
         </div>
		   
			   <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'famAge'); ?>
                </div><div>
				<?php echo $form->textField($model,'famAge',array('onkeydown'=>"return numberonly('FamilyForm_famAge')",'maxlength'=>'3')); ?>  
				<?php echo $form->error($model,'famAge'); ?>      
                 </div>		 
		       </div>
			 
			
		   
		     <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'famOccupation'); ?>
                </div><div>
				<?php echo $form->textField($model,'famOccupation',array('onkeydown'=>"return alphaonly('FamilyForm_famOccupation')",'maxlength'=>'100')); ?>  
				<?php echo $form->error($model,'famOccupation'); ?>      
                 </div>		 
		       </div>	
		  
		    <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'occupationAddress'); ?>
                </div><div>
				<?php echo $form->textArea($model,'occupationAddress',array('placeholder'=>'max. Length 500 chars.','maxlength'=>'500'));?>
				<?php echo $form->error($model,'occupationAddress'); ?>      
                 </div>
		 
		    </div>
		  
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'famContactAddress'); ?>
                </div><div>
				<?php echo $form->textArea($model,'famContactAddress',array('placeholder'=>'max. Length 500 chars.','maxlength'=>'500'));?>
				<?php echo $form->error($model,'famContactAddress'); ?>      
                 </div>
		 
		  </div>
		
		  
			 <div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px">             		  
			 <?php echo $form->labelEx($model,'famPhonecode'); ?>
             </div><div>
			 <?php echo $form->dropDownList($model,'famPhonecode',CHtml::listData($phone,'Country_Code','Country_Name'),array('prompt'=>'Select'));
			 echo $form->error($model,'famPhonecode');
			 ?>			 
             </div>
			 </div>  
		  	 <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'famContact'); ?>
                </div><div>
				<?php echo $form->textField($model,'famContact',array('onkeydown'=>"return numberonly('FamilyForm_famContact')",'maxlength'=>'10')); ?>  
				<?php echo $form->error($model,'famContact'); ?>      
                 </div>
		 
		  </div>
		  	 <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'famEmailid'); ?>
                </div><div>
				<?php echo $form->textField($model,'famEmailid',array('maxlength'=>'50')); ?>  
				<?php echo $form->error($model,'famEmailid'); ?>      
                 </div>
		 
		  </div>	
		  	
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'famAssociation'); ?>
                </div><div> 
				<?php echo $form->dropDownList($model,'famAssociation',array('Resident'=>'Resident','Employee'=>'Employee','Student'=>'Student','None of the above'=>'None of the above'),
					array('prompt'=>'Select','onchange'=>'ajaxdepartment(this.value);'),
					array('options' => array('famAssociation'=>array('selected'=>true))));
			    	echo $form->error($model,'famAssociation');?>
                 </div>
         </div>	 
		 
		
		 <div id="deptSpan" class="row-fluid">  
	        	 <div style="float:left; overflow: hidden; width:200px">
					<?php echo $form->hiddenField($model,'action'); ?>	
					<?php echo $form->hiddenField($model,'familyCode'); ?>
					 <?php echo $form->labelEx($model,'famDepartment'); ?>
	             </div>
	             <div>
					 <?php echo $form->dropDownList($model,'famDepartment',
					 	CHtml::listData($department,'Department_Code','Department'),
					 	array('prompt'=>'Select'));
					 echo $form->error($model,'famDepartment');
					 ?>
	            </div>
 		</div>  
       
   </div>
                    
    
    <div class="modal-footer">
  
            <?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'f17$3','class'=>'btn btn-primary')); ?> 
             <button type="button" class="btn" aria-hidden="true" data-dismiss="modal"
			  onclick="popupHide()"> Close </button>
        	
   </div>
                    
</div>

<?php 

			if(strlen($model->memberName)>0 and $model->action<>3  and strlen($msg1)>3 or $model->errflag){		
				 echo "<div id='edPopup' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
				 echo "<script>popupValue('".$model->memberName."','".$model->relation."','".$model->gender."','".$model->famAge."','".$model->famOccupation."','".rawurlencode($model->occupationAddress)."','".rawurlencode($model->famContactAddress)."','".$model->famPhonecode."','".$model->famContact."','".$model->famEmailid."','".$model->famAssociation."','".$model->famDepartment."','".$model->familyCode."',".$model->action.",1)</script>"; } ?>
 		<div class="clearfix">	</div>
     </div>    
	 </div>
	</div></div></div></div>

 <?php $this->endWidget(); ?>
 
 <?php /////////////////////////////////////////////////////////////////////////Third Form///////////////////////////////////////////?>
 <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'FamilyTextAreaForm',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
));  ?>
<div class="widget">
<div class="row-fluid">
<table width="98%" border="0" cellpadding="1" >

<?php echo $form->hiddenField($model2,'action2'); ?>
    <tr>
    <td><?php echo $form->labelEx($model2,'responsibility',array('style'=>'margin-bottom:80px;'));?></td>
     <td><?php echo $form->textArea($model2,'responsibility',array('placeholder'=>'max. Length 500 chars.','style'=>'width: 450px; height: 103px;','maxlength'=>'500'));?></td><td align="right"><?php echo CHtml::submitButton('Save',array('id'=>'saveform1','name'=>'f&$m#','size'=>75,'class'=>'btn btn-primary')) ?></td>
   </tr>
   </table>
   <table >  <tr>
    	
    </tr></table>
 </div></div>
 <?php $this->endWidget(); ?>
 
 
 <?php /////////////////////////////second form//////////////////////////////?>
 
 <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'FamilyRelativeForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
<script>

	function popupValue1(obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9,obj10,obj11,obj12)
	{	
	
		if(obj11!=3) // Coming Add & Edit
		{
			if(obj12!=0) {  // If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings1").show();			
				$('#accSettings1').removeClass('modal hide fade').addClass('modal hide fade in');
			}
		$("#FamilyRelativeForm_memberName1").val(obj1);
		$("#FamilyRelativeForm_relation1").val(obj2);		
		$("#FamilyRelativeForm_gender1").val(obj3);	
		$('#FamilyRelativeForm_famContactAddress1').val(unescape(obj4));		
		//$("#FamilyRelativeForm_famContactAddress1").val(obj4);	
		if(obj5=='' || obj5==0);// obj5=102;	
		$("#FamilyRelativeForm_famPhonecode1").val(obj5);
		$("#FamilyRelativeForm_famContact1").val(obj6);
		$("#FamilyRelativeForm_famEmailid1").val(obj7);		
		$("#FamilyRelativeForm_famAssociation1").val(obj8);
		$("#FamilyRelativeForm_famDepartment1").val(obj9);
		$("#FamilyRelativeForm_familyCode1").val(obj10);
		$("#FamilyRelativeForm_action1").val(obj11);	
		ajaxdepartment_onload1();
		}
		if(obj11==3) { // Delete	
		
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
	    $("#FamilyRelativeForm_memberName1").val(obj1);
		$("#FamilyRelativeForm_relation1").val(obj2);		
		$("#FamilyRelativeForm_gender1").val(obj3);		
		//$("#FamilyRelativeForm_famContactAddress1").val(obj4);
		$('#FamilyRelativeForm_famContactAddress1').val(unescape(obj4));				
		$("#FamilyRelativeForm_famPhonecode1").val(obj5);
		$("#FamilyRelativeForm_famContact1").val(obj6);
		$("#FamilyRelativeForm_famEmailid1").val(obj7);		
		$("#FamilyRelativeForm_famAssociation1").val(obj8);
		$("#FamilyRelativeForm_famDepartment1").val(obj9);
		$("#FamilyRelativeForm_familyCode1").val(obj10);
		$("#FamilyRelativeForm_action1").val(obj11);
		ajaxdepartment_onload1();
		$("#FamilyRelativeForm").submit();
			}
		}
		
	}
	
	function ajaxdepartment_onload1()
	{
		var obj = $("#FamilyRelativeForm_famAssociation1").val();
		if(obj=='' || obj=='None of the above')
		{
			$("#relativeDeptSpan").hide();
		}
		else
		{
			$("#relativeDeptSpan").show();
		}
	}

						function popupHide1()
						{		$("#errinfo2").html('');   // DB msg empty for next record clicking. But   $msg1 is not empty
								$(".errorMessage").html('');   // Yii Error msg is empty after close, 
						    $('#edPopup1').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
								$("#accSettings1").hide();	 // Popup Hide	
								}
			function ajaxdepartment1(obj,obj1)
						{
						        if(obj=='None of the above')
						{
						        $("#noneofthis1").hide();
						        $("#noneofthese1").hide();	
						        $('#FamilyRelativeForm_famDepartment1').val('');
						        $("#relativeDeptSpan").hide();				
						}
						else
						{			
						         $("#noneofthis1").show();
						         $("#noneofthese1").show();
						         $("#relativeDeptSpan").show();	 
						       var url='<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/Ajaxdepartment1?category='+obj+'&dept='+obj1;
							   $("#FamilyRelativeForm_famDepartment1").load(url);
						}
						}	
</script>
<div class="widget">
   <div style="padding:10px; margin:7px;>
									<div class="widget-body">
								
										  <div id="dt_example" class="example_alt_pagination" >	
				<h4><u><?php echo $form->labelEx($model1,'familyRelative',array('style'=>'font-weight: bold;font-size:15px;')); ?></u></h4>
										  <table align="right"><tr>
									
										  <td><div><a href="#accSettings1" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue1('','','','','','','','','','',1,0)">Add New</a></div></td></tr></table>
										  
										 
										
			   <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">                        
						<thead><th><?php echo $form->labelEx($model1,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model1,'memberName1',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model1,'relation1',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model1,'gender1',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model1,'famContactAddress1',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model1,'famContact1',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model1,'famEmailid1',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model1,'famAssociation1',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model1,'famDepartment1',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>						<th><?php echo $form->labelEx($model1,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						</thead>
												<tbody><?php $i=0;?>
												  <?php while($field1=mssql_fetch_array($row2))
												  {$i++; ?>
													
													<tr class="gradeX"><td align="center"><?php echo $i; ?></td>
													<td><?php echo $field1['Member_Name'];?></td>
													<td><?php echo $field1['Fam_Relation'];?></td>										
													<td><?php if($field1['Fam_Gender'] =='1') echo "Male"; 
													else if($field1['Fam_Gender'] =='2')  echo "Female"; ?></td>
													<td><?php echo $field1['Fam_Address'];?></td>
													<td><?php echo $field1['Phone_Code']." <br> ". $field1['Fam_Contact']; ?></td>	
													<td><?php echo $field1['Fam_Emailid'];?></td>
													<td><?php echo $field1['Fam_Association'];?></td>			
													<td><?php echo $field1['Department'];?></td>	                                          					
													<td style="width:20px;" class="hidden-phone">
													
	<a href="#accSettings1" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' 
	data-original-	title='Edit' onclick="popupValue1('<?php echo $field1['Member_Name'] ?>','<?php echo $field1['Fam_Relation'] ?>','<?php echo $field1['Fam_Gender'] ?>','<?php echo rawurlencode($field1['Fam_Address']); ?>','<?php echo $field1['Fam_Phonecode'] ?>','<?php echo $field1['Fam_Contact'] ?>','<?php echo $field1['Fam_Emailid'] ?>','<?php echo $field1['Fam_Association'] ?>','<?php echo $field1['Fam_Department'] ?>','<?php echo $field1['Family_Code'] ?>',2,0)">&#x270E</a>
		<?php if(Yii::app()->session['Freeze']=="N") { ?>		
		<a class="btn btn-success btn-small hidden-phone"  id="delbtn" data-original-title="Delete" onclick="popupValue1('<?php echo $field1['Member_Name'] ?>','<?php echo $field1['Fam_Relation'] ?>','<?php echo $field1['Fam_Gender'];?>','<?php echo rawurlencode($field1['Fam_Address']);?>','<?php echo $field1['Fam_Phonecode'];?>','<?php echo $field1['Fam_Contact'];?>','<?php echo $field1['Fam_Emailid'];?>','<?php echo $field1['Fam_Association'];?>','<?php echo $field1['Fam_Department'];?>','<?php echo $field1['Family_Code'] ?>',3,0)">&#x2717</a>
		<?php } ?>
													</td>
												</tr>
											<?php } ?>
											</tbody>
											</table>
			<div id="accSettings1" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
									 <div id="errinfo2" class="error" align="center" > <?php echo $msg3; ?> </div>	
								<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide1()"> x </button>
			<h4><u><?php echo $form->labelEx($model1,'familyRelativeHint',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
						</div> 
						   <div class="modal-body">
								
								
									  <div class="row-fluid">                                 
										<div style="float:left; overflow: hidden; width:200px"> 
										<?php echo $form->labelEx($model1,'memberName1'); ?>
										</div><div>
										<?php echo $form->textField($model1,'memberName1',array('onkeydown'=>"return alphaonly('FamilyRelativeForm_memberName1')",'maxlength'=>'50')); ?>  
										<?php echo $form->error($model1,'memberName1'); ?>      
										 </div>
								 
									   </div>
										 <div class="row-fluid">                                 
										<div style="float:left; overflow: hidden; width:200px"> 
										<?php echo $form->labelEx($model1,'relation1'); ?>
										</div><div>
										<?php echo $form->textField($model1,'relation1',array('onkeydown'=>"return alphaonly('FamilyRelativeForm_relation1')",'maxlength'=>'50')); ?>  
										<?php echo $form->error($model1,'relation1'); ?>      
										 </div>
								 
									   </div>
									  <div class="row-fluid">                                 
										<div style="float:left; overflow: hidden; width:200px"> 
										<?php echo $form->labelEx($model1,'gender1'); ?>
										</div><div> 
										<?php echo $form->dropDownList($model1,'gender1',array('1'=>'Male','2'=>'Female'),
										array('prompt'=>'Select'),
										array('options' => array('gender1'=>array('selected'=>true))));
										 echo $form->error($model1,'gender1');?>
										 </div>
								 </div>
										 <div class="row-fluid">                                 
										<div style="float:left; overflow: hidden; width:200px"> 
										<?php echo $form->labelEx($model1,'famContactAddress1'); ?>
										</div><div>
										<?php echo $form->textArea($model1,'famContactAddress1',array('placeholder'=>'max. Length 500 chars.','maxlength'=>'500'));?>
										<?php echo $form->error($model1,'famContactAddress1'); ?>      
										 </div>
								 
								  </div>
								  
									 <div class="row-fluid">  
									 <div style="float:left; overflow: hidden; width:200px">             		  
									 <?php echo $form->labelEx($model1,'famPhonecode1'); ?>
									 </div><div>
									 <?php echo $form->dropDownList($model1,'famPhonecode1',CHtml::listData($phone,'Country_Code','Country_Name'),array('prompt'=>'Select'));
									 echo $form->error($model1,'famPhonecode1');
									 ?>			 
									 </div>
									 </div> 
									 <div class="row-fluid">                                 
										<div style="float:left; overflow: hidden; width:200px"> 
										<?php echo $form->labelEx($model1,'famContact1'); ?>
										</div><div>
										<?php echo $form->textField($model1,'famContact1',array('onkeydown'=>"return numberonly('FamilyRelativeForm_famContact1')",'maxlength'=>'10')); ?>  
										<?php echo $form->error($model1,'famContact1'); ?>      
										 </div>
								 
								  </div>
									   
										
										 <div class="row-fluid">                                 
										<div style="float:left; overflow: hidden; width:200px"> 
										<?php echo $form->labelEx($model1,'famEmailid1'); ?>
										</div><div>
										<?php echo $form->textField($model1,'famEmailid1',array('maxlength'=>'50')); ?>  
										<?php echo $form->error($model1,'famEmailid1'); ?>      
										 </div>
								 
									   </div>										
														   
									<div class="row-fluid">                                 
									<div style="float:left; overflow: hidden; width:200px"> 
									<?php echo $form->labelEx($model1,'famAssociation1'); ?>
									</div><div> 
									<?php echo $form->dropDownList($model1,'famAssociation1',array('Resident'=>'Resident','Employee'=>'Employee','Student'=>'Student','None of the above'=>'None of the above'),
									array('prompt'=>'Select','onchange'=>'ajaxdepartment1(this.value);'),
									array('options' => array('famAssociation1'=>array('selected'=>true))));
									echo $form->error($model1,'famAssociation1');?>
									 </div>
									</div>	 
									   
									<div id="relativeDeptSpan" class="row-fluid">  
										 <div style="float:left; overflow: hidden; width:200px"> 									
											 <?php echo $form->hiddenField($model1,'action1'); ?>	
											 <?php echo $form->hiddenField($model1,'familyCode1'); ?>			  
											 <?php echo $form->labelEx($model1,'famDepartment1'); ?>
										 </div><div>
											 <?php echo $form->dropDownList($model1,'famDepartment1',CHtml::listData($department,'Department_Code','Department'),array('prompt'=>'Select')); echo $form->error($model1,'famDepartment1'); ?>
										 </div>
									</div> 		    
										 
						   </div>
							<div class="modal-footer">  
									<?php echo CHtml::submitButton('Save',array('id'=>'saveform2','name'=>'6w$%y','class'=>'btn btn-primary')); ?> 
				<button type="button" class="btn" aria-hidden="true" data-dismiss="modal"  onclick="popupHide1()"> Close </button>        	
						   </div>                    
						</div>
	<?php if($model1->action1<>3 and (strlen($msg3)>3 or $model1->errflag)) {
		
	
		echo "<div id='edPopup1' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
echo "<script>popupValue1('".$model1->memberName1."','".$model1->relation1."','".$model1->gender1."','".rawurlencode($model1->famContactAddress1)."',
'".$model1->famPhonecode1."','".$model1->famContact1."','".$model1->famEmailid1."','".$model1->famAssociation1."','".$model1->famDepartment1."',
   '".$model1->familyCode1."','".$model1->action1."',1)</script>"; } ?>  
										  
								<div class="clearfix">	</div>
							 </div>    
							 </div>
							 </div>
							
<?php if(strlen($model->relation)>0) {?>
      <script>genderC('<?php echo $model->relation ?>')</script> 
	  <?php }?>
                     <?php $this->endWidget(); ?>
