	<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'ChangePasswordForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
    
	<script type="text/javascript">		
	function Clear(){
		$("#ChangePasswordForm_currentPassword").val('');
		$("#ChangePasswordForm_newPassword").val('');
		$("#ChangePasswordForm_confirmPassword").val('');
		
       }
	</script>
	<div style="padding:10px; margin:7px;" align="center">
	<div class="widget" style="width: 60%;" align="center"> 
    <div class="widget-header">
    <div class="title">
    <div><h4><u><?php echo $form->labelEx($model,'password',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4></div>
    </div></div>
   
	<div class="row-fluid" style="width: 90%;">	
	<div align="center"><?php echo $msg; ?></div>
	<table align="center" width="90%"  border="0" cellpadding="1">
    <tr>
	<td>
	<tr>
	<td>
	<?php echo $form->labelEx($model,'currentPassword');?> 
	</td>
	<td>	
	<div>
	<?php echo $form->passwordField($model,'currentPassword',array('style'=>'width:215px','onkeydown'=>"return alphanumeric('ChangePasswordForm_currentPassword')",'maxlength'=>'50'));
	echo $form->error ($model,'currentPassword');?>
	</div>
	</td>
	</tr>	
	
	<tr>
	<td>
	<?php echo $form->labelEx($model,'newPassword');?> 
	</td>
	<td width="150px;">
	<div>	
	<?php echo $form->passwordField($model,'newPassword',array('style'=>'width:215px','onkeydown'=>"return alphanumeric('ChangePasswordForm_newPassword')",'maxlength'=>'100'));
	echo $form->error($model,'newPassword');?>
	</div>
	</td>
	</tr>	
	<tr>
	<td>
	<?php echo $form->labelEx($model,'confirmPassword');?> 
	</td>
	<td width="150px;"><div>
	
	<?php echo $form->passwordField($model,'confirmPassword',array('style'=>'width:215px','onkeydown'=>"return emailonly('ChangePasswordForm_confirmPassword')",'maxlength'=>'50'));
	echo $form->error    ($model,'confirmPassword');?>
	</div>
	</td>
	</tr>
		
	</td>
	</tr>
	</table>	
	</div>
	<table align="right">
	<tr> 	
	<td><?php echo CHtml::submitButton('Submit',array('size'=>75,'id'=>'saveform','name'=>'c&%4P','class'=>'btn btn-primary')) ?>
	</td>
	</tr></table>	</div></div>
	
	

	<?php $this->endWidget(); ?>
