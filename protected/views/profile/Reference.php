<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'ReferenceForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
<script>
$( document ).ready(function() {
    $("#data-table span").html('');
});

	function popupValue(obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8)
	{		
		if(obj7!=3) // Coming Add & Edit
		{
			if(obj8!=0) {  // If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings").show();			
				$('#accSettings').removeClass('modal hide fade').addClass('modal hide fade in');
			}
		$("#ReferenceForm_refernceCode").val(obj1);
		$("#ReferenceForm_name").val(obj2);
		//$("#ReferenceForm_designation").val(obj3);
		$('#ReferenceForm_designation').val(unescape(obj3));
		$("#ReferenceForm_relationship").val(obj4);		
		$("#ReferenceForm_contactNo").val(obj5);
		$("#ReferenceForm_emailId").val(obj6);
		$("#ReferenceForm_action").val(obj7);
		
		}
		if(obj7==3) {  // Delete
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
		$("#ReferenceForm_refernceCode").val(obj1);
		$("#ReferenceForm_name").val(obj2);
		//$("#ReferenceForm_designation").val(obj3);
		$('#ReferenceForm_designation').val(unescape(obj3));
		$("#ReferenceForm_relationship").val(obj4);		
		$("#ReferenceForm_contactNo").val(obj5);
		$("#ReferenceForm_emailId").val(obj6);
		$("#ReferenceForm_action").val(obj7);
		$("#ReferenceForm").submit();
			}
		}
		
	}
	function popupHide()
	{		$("#errinfo").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');   // Yii Error msg is empty after close, 
			$('#edPopup').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
			$("#accSettings").hide();	 // Popup Hide	
			}	
			
	
			
</script>


<div class="widget">
<div class="widget-header">
<div class="title">
 <h4><u><?php echo $form->labelEx($model,'referenceA',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4></div>
</div>
  <div>
  			
		       <div id="dt_example" class="example_alt_pagination" >
			 <div class="title" style="margin-left: 10px;"><h4><u><?php echo $form->labelEx($model,'referenceB',array('style'=>'font-weight: bold;font-size:15px;')); ?></u></h4></div>
			    	 
                  <table align="right"><tr>
			
				  <td><div><a href="#accSettings" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue('','','','','','',1,0)">Add New</a></div></td></tr></table>
				  
				 
				
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">                        
                        <thead><th><?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'name',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'designation',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'relationship',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'contactNo',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'emailId',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						</thead>
						 <tbody>
						 <?php $i=0;?>
						<?php 
					
						  if(mssql_num_rows($row1)>0) {
                          while($field = mssql_fetch_array($row1))
						   { $i++;?>
                      		<tr class="gradeX">
							<td align="center"><?php echo $i; ?></td>
                            <td><?php echo $field['Name'];?></td>
							<td><?php echo $field['Designation_Address'];?></td>		
							<td><?php echo $field['Relationship'];?></td>		
							<td><?php echo $field['Contact_No'];?></td>				
							<td><?php echo $field['Email_Id'];?></td>	                            
                            <td style="width:20px;" class="hidden-phone">
							
							<a href="#accSettings" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue('<?php echo $field['IVC_Reference_Code'] ?>','<?php echo $field['Name'] ?>','<?php echo rawurlencode($field['Designation_Address']); ?>','<?php echo $field['Relationship'] ?>','<?php echo $field['Contact_No'] ?>','<?php echo $field['Email_Id'] ?>',2,0)">&#x270E</a>
							
			
							<?php if(Yii::app()->session['Freeze']=="N") { ?>
                            <a class="btn btn-success btn-small hidden-phone"  id="delbtn"   data-original-title="Delete" onclick="popupValue('<?php echo $field['IVC_Reference_Code'] ?>','<?php echo $field['Name'] ?>','<?php echo rawurlencode($field['Designation_Address']); ?>','<?php echo $field['Relationship'] ?>','<?php echo $field['Contact_No'] ?>','<?php echo $field['Email_Id'] ?>',3,0)">&#x2717</a>
							<?php } ?>
                            </td>
		
                        </tr>
														
					<?php } 
					}?>
                	</tbody>


                    </table>
					
			<div id="accSettings" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 		<div id="errinfo" class="error" align="center" > <?php echo $msg1; ?> </div>
		<div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide()"> x </button>
         	<h4><u><?php echo $form->labelEx($model,'referenceE',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
    	</div> 
   <div class="modal-body">
   		<div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px">
				 <?php echo $form->hiddenField($model,'action'); ?>	
                 <?php echo $form->hiddenField($model,'refernceCode'); ?>	 
				<?php echo $form->labelEx($model,'name'); ?>
                </div><div>
				<?php echo $form->textField($model,'name',array('onkeydown'=>"return alphaonly('ReferenceForm_name')",'maxlength'=>'50')); ?>  
				<?php echo $form->error($model,'name'); ?>      
                 </div>		 
		       </div>
			   
			   <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'designation'); ?>
                </div><div>
				<?php echo $form->textArea($model,'designation',array('placeholder'=>'max. Length 500 chars.','maxlength'=>'500'));?>
				<?php echo $form->error($model,'designation'); ?>      
                 </div>		 
		       </div>
			   
			   <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'relationship'); ?>
                </div><div>
				<?php echo $form->textField($model,'relationship',array('onkeydown'=>"return alphaonly('ReferenceForm_relationship')",'maxlength'=>'50')); ?>  
				<?php echo $form->error($model,'relationship'); ?>      
                 </div>		 
		       </div>
			   
			   <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'contactNo'); ?>
                </div><div>
				<?php echo $form->textField($model,'contactNo',array('onkeydown'=>"return numberonly('ReferenceForm_contactNo')",'maxlength'=>'10')); ?>  
				<?php echo $form->error($model,'contactNo'); ?>      
                 </div>		 
		       </div>
			   
			   <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'emailId'); ?>
                </div><div>
				<?php echo $form->textField($model,'emailId',array('onkeydown'=>"return emailonly('ReferenceForm_emailId')",'maxlength'=>'50')); ?>  
				<?php echo $form->error($model,'emailId'); ?>      
                 </div>		 
		       </div>
   </div>
                    
    
    <div class="modal-footer">
  
            <?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'%$r#f','class'=>'btn btn-primary')); ?> 
             <button type="button" class="btn" aria-hidden="true" data-dismiss="modal"
			  onclick="popupHide()"> Close </button>
        	
   </div>
                    
</div>

<?php 

			if($model->action<>3 and (strlen($msg1)>3 or $model->errflag)) {			
				 echo "<div id='edPopup' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
				 echo "<script>popupValue('".$model->refernceCode."','".$model->name."','".rawurlencode($model->designation)."','".$model->relationship."','".$model->contactNo."','".$model->emailId."','".$model->action."',1)</script>"; } ?>                    
 		<div class="clearfix">	</div>
     </div>    
	 </div></div>
	
 <?php $this->endWidget(); ?>
 
 <?php /////////////////////////////////////////////////////////////////////////Third Form///////////////////////////////////////////?>
 
 
 <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'ReferenceIshaForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
<script>

	function popupValue5(obj1,obj2,obj3,obj4,obj5,obj6,obj7)
	{		
	
		if(obj6!=3) // Coming Add & Edit
		{
			if(obj7!=0) {  // If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings5").show();			
				$('#accSettings5').removeClass('modal hide fade').addClass('modal hide fade in');
			}
		$("#ReferenceIshaForm_referenceIshaCode").val(obj1);
		$("#ReferenceIshaForm_rName").val(obj2);
		$("#ReferenceIshaForm_centerName").val(obj3);
		$("#ReferenceIshaForm_phoneNo").val(obj4);		
		$("#ReferenceIshaForm_rEmailId").val(obj5);
		$("#ReferenceIshaForm_action1").val(obj6);
		
		}
		if(obj6==3) {  // Delete
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
		$("#ReferenceIshaForm_referenceIshaCode").val(obj1);
		$("#ReferenceIshaForm_rName").val(obj2);
		$("#ReferenceIshaForm_centerName").val(obj3);
		$("#ReferenceIshaForm_phoneNo").val(obj4);		
		$("#ReferenceIshaForm_rEmailId").val(obj5);
		$("#ReferenceIshaForm_action1").val(obj6);
		$("#ReferenceIshaForm").submit();
			}
		}
		
	}
	function popupHide5()
	{		$("#errinfo5").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');   // Yii Error msg is empty after close, 
			$('#edPopup5').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
			$("#accSettings5").hide();	 // Popup Hide	
			}	
			$( document ).ready(function() {
    $("#data-table5 span").html('');
});
	
			
</script>
<div class="widget">
 
  
		       <div id="dt_example5" class="example_alt_pagination" >
			  <div class="title" style="margin-left: 10px;"><h4><u><?php echo $form->labelEx($model1,'referenceC',array('style'=>'font-weight: bold;font-size:15px;')); ?></u></h4></div>
			    	 
                  <table align="right"><tr>
			
				  <td><div><a href="#accSettings5" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue5('','','','','',1,0)">Add New</a></div></td></tr></table>
				  
				 
				
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table5">                        
                        <thead><th><?php echo $form->labelEx($model1,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model1,'rName',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model1,'centerName',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model1,'phoneNo',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model1,'rEmailId',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model1,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						</thead>
						
						 <tbody>
						 <?php $i=0;?>
						<?php 
					
						  if(mssql_num_rows($row2)>0) {
                          while($field5 = mssql_fetch_array($row2))
						   { $i++;?>
                      		<tr class="gradeX">
							<td align="center"><?php echo $i; ?></td>
                            <td><?php echo $field5['Name'];?></td>
							<td><?php echo $field5['Center_Name'];?></td>		
							<td><?php echo $field5['Contact_No'];?></td>		
							<td><?php echo $field5['Email_Id'];?></td>	                            
                            <td style="width:20px;" class="hidden-phone">
							
							<a href="#accSettings5" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue5('<?php echo $field5['IVC_Reference_Code'] ?>','<?php echo $field5['Name'] ?>','<?php echo $field5['Center'] ?>','<?php echo $field5['Contact_No'] ?>','<?php echo $field5['Email_Id'] ?>',2,0)">&#x270E</a>
							
			
							<?php if(Yii::app()->session['Freeze']=="N") { ?>
                            <a class="btn btn-success btn-small hidden-phone"  id="delbtn1"   data-original-title="Delete" onclick="popupValue5('<?php echo $field5['IVC_Reference_Code'] ?>','<?php echo $field5['Name'] ?>','<?php echo $field5['Center'] ?>','<?php echo $field5['Contact_No'] ?>','<?php echo $field5['Email_Id'] ?>',3,0)">&#x2717</a>
							<?php } ?>
                            </td>
		
                        </tr>
														
					<?php } 
					}?>
                	</tbody>
                    </table>
					
			<div id="accSettings5" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 		<div id="errinfo5" class="error" align="center" > <?php echo $msg2; ?> </div>
		<div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide5()"> x </button>
         <h4><u><?php echo $form->labelEx($model1,'referenceF',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
    	</div> 
   <div class="modal-body">
   		<div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px">
				 <?php echo $form->hiddenField($model1,'action1'); ?>	
                 <?php echo $form->hiddenField($model1,'referenceIshaCode'); ?>	 
				<?php echo $form->labelEx($model1,'rName'); ?>
                </div><div>
				<?php echo $form->textField($model1,'rName',array('onkeydown'=>"return alphaonly('ReferenceIshaForm_rName')",'maxlength'=>'50')); ?>  
				<?php echo $form->error($model1,'rName'); ?>      
                 </div>		 
		       </div>
			   
			   
			   
			   <div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
             <?php echo $form->labelEx($model1,'centerName'); ?>
             </div><div>
			 <?php echo $form->dropDownList($model1,'centerName',CHtml::listData($center,'Center_Code','Center_Name'),array('prompt'=>'Select'));
			 echo $form->error($model1,'centerName');
			 ?>
			 
             </div>
			 </div>  		
			   
			   <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model1,'phoneNo'); ?>
                </div><div>
				<?php echo $form->textField($model1,'phoneNo',array('onkeydown'=>"return numberonly('ReferenceIshaForm_phoneNo')",'maxlength'=>'10')); ?>  
				<?php echo $form->error($model1,'phoneNo'); ?>      
                 </div>		 
		       </div>
			   
			   <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model1,'rEmailId'); ?>
                </div><div>
				<?php echo $form->textField($model1,'rEmailId',array('maxlength'=>'50','onkeydown'=>"return emailonly('ReferenceIshaForm_rEmailId')")); ?>  
				<?php echo $form->error($model1,'rEmailId'); ?>      
                 </div>		 
		       </div>
   </div>
                    
    
    <div class="modal-footer">
  
            <?php echo CHtml::submitButton('Save',array('id'=>'saveform1','name'=>'*gf%&','class'=>'btn btn-primary')); ?> 
             <button type="button" class="btn" aria-hidden="true" data-dismiss="modal"
			  onclick="popupHide5()"> Close </button>
        	
   </div>
                    
</div>

<?php 

			if($model1->action1<>3 and (strlen($msg2)>3 or $model1->errflag)) {					
				 echo "<div id='edPopup5' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
				 echo "<script>popupValue5('".$model1->referenceIshaCode."','".$model1->rName."','".$model1->centerName."','".$model1->phoneNo."','".$model1->rEmailId."','".$model1->action1."',1)</script>"; } ?>                    
 		<div class="clearfix">	</div>
     </div>    
	 </div>
 <?php $this->endWidget(); ?>
 
 
 <?php ///////////////// ////////////second form//////////////////////////////?>
 
 <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'ReferenceResidentForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
<script>

	function popupValue1(obj1,obj2,obj3,obj4,obj5,obj6)
	{	
	
		if(obj5!=3) // Coming Add & Edit
		{
			if(obj6!=0) {  // If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings1").show();			
				$('#accSettings1').removeClass('modal hide fade').addClass('modal hide fade in');
			}
		$("#ReferenceResidentForm_referenceResidentCode").val(obj1);
		$("#ReferenceResidentForm_rRname").val(obj2);		
		$("#ReferenceResidentForm_department").val(obj3);		
		$("#ReferenceResidentForm_cNo").val(obj4);	
		$("#ReferenceResidentForm_action2").val(obj5);	
			
		}
		if(obj5==3) { // Delete	
		
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
	   $("#ReferenceResidentForm_referenceResidentCode").val(obj1);
		$("#ReferenceResidentForm_rRname").val(obj2);		
		$("#ReferenceResidentForm_department").val(obj3);		
		$("#ReferenceResidentForm_cNo").val(obj4);	
		$("#ReferenceResidentForm_action2").val(obj5);	
		$("#ReferenceResidentForm").submit();
			}
		}
	
	}
		function popupHide1()
		{		$("#errinfo2").html('');   // DB msg empty for next record clicking. But   $msg1 is not empty
		$(".errorMessage").html('');   // Yii Error msg is empty after close, 
		$('#edPopup1').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
		$("#accSettings1").hide();	 // Popup Hide	
		}
		
		
			
</script>
  <div class="widget">
  <div id="dt_example" class="example_alt_pagination" >	
					<div class="title" style="margin-left: 10px;"><h4><u><?php echo $form->labelEx($model2,'referenceD',array('style'=>'font-weight: bold;font-size:15px;')); ?></u></h4></div>
										  <table align="right"><tr>
									
										  <td><div><a href="#accSettings1" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue1('','','','',1,0)">Add New</a></div></td></tr></table>
										  
										 
										
			<table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">                        
					<thead><th><?php echo $form->labelEx($model2,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
					<th><?php echo $form->labelEx($model2,'rRname',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
					<th><?php echo $form->labelEx($model2,'department',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
					<th><?php echo $form->labelEx($model2,'cNo',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
					<th><?php echo $form->labelEx($model2,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
												
												</thead>
												
												<tbody>
						 <?php $i=0;?>
						<?php 
					
						  if(mssql_num_rows($row3)>0) {
                          while($field6 = mssql_fetch_array($row3))
						   { $i++;?>
                      		<tr class="gradeX">
							<td align="center"><?php echo $i; ?></td>
                            <td><?php echo $field6['Name'];?></td>
							<td><?php echo $field6['Department'];?></td>
							<td><?php echo $field6['Contact_No'];?></td>		
							<td style="width:20px;" class="hidden-phone">
							
							<a href="#accSettings1" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue1('<?php echo $field6['IVC_Reference_Code'] ?>','<?php echo $field6['Name'] ?>','<?php echo $field6['Department_Code'] ?>','<?php echo $field6['Contact_No'] ?>',2,0)">&#x270E</a>
							
			
							<?php if(Yii::app()->session['Freeze']=="N") { ?>
                            <a class="btn btn-success btn-small hidden-phone"  id="delbtn2"   data-original-title="Delete" onclick="popupValue1('<?php echo $field6['IVC_Reference_Code'] ?>','<?php echo $field6['Name'] ?>','<?php echo $field6['Department_Code'] ?>','<?php echo $field6['Contact_No'] ?>',3,0)">&#x2717</a>
							<?php } ?>
                            </td>
		
                        </tr>
														
					<?php } 
					}?>
                	</tbody>
												
											</table>
			<div id="accSettings1" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
									 <div id="errinfo2" class="error" align="center" > <?php echo $msg3; ?> </div>	
								<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide1()"> x </button>
						<h4><u><?php echo $form->labelEx($model2,'referenceG',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
						</div> 
						   <div class="modal-body">
								
								
									  <div class="row-fluid">                                 
										<div style="float:left; overflow: hidden; width:200px"> 
										<?php echo $form->labelEx($model2,'rRname'); ?>
										</div><div>
										<?php echo $form->textField($model2,'rRname',array('onkeydown'=>"return alphaonly('ReferenceResidentForm_rRname')",'maxlength'=>'50')); ?>  
										<?php echo $form->error($model2,'rRname'); ?>      
										 </div>
								 
									   </div>
									   
									   
									   <div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
             <?php echo $form->hiddenField($model2,'action2'); ?>	
             <?php echo $form->hiddenField($model2,'referenceResidentCode'); ?>				 
			 <?php echo $form->labelEx($model2,'department'); ?>
             </div><div>
			 <?php echo $form->dropDownList($model2,'department',CHtml::listData($department,'Department_Code','Department'),array('prompt'=>'Select'));
			 echo $form->error($model2,'department');
			 ?>
					 
             </div>
			 </div>  		
			 
			 
			  <div class="row-fluid">                                 
										<div style="float:left; overflow: hidden; width:200px"> 
										<?php echo $form->labelEx($model2,'cNo'); ?>
										</div><div>
										<?php echo $form->textField($model2,'cNo',array('onkeydown'=>"return numberonly('ReferenceResidentForm_cNo')",'maxlength'=>'10')); ?>  
										<?php echo $form->error($model2,'cNo'); ?>      
										 </div>
								 
									   </div>
			 
			 
			 			 
						   </div>
							<div class="modal-footer">  
									<?php echo CHtml::submitButton('Save',array('id'=>'saveform2','name'=>'r$e*y',
									'class'=>'btn btn-primary')); ?> 
				<button type="button" class="btn" aria-hidden="true" data-dismiss="modal"  onclick="popupHide1()"> Close </button>        	
						   </div>                    
						</div>
	<?php
	
	
	 if($model2->action2<>3 and (strlen($msg3)>3 or $model2->errflag)) {					
	
		echo "<div id='edPopup1' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
		echo "<script>popupValue1('".$model2->referenceResidentCode."','".$model2->rRname."','".$model2->department."','".$model2->cNo."',
'".$model2->action2."',1)</script>"; } ?>  
										  
				<div class="clearfix">	</div>
				</div>    
			</div>
		
<?php $this->endWidget(); ?>