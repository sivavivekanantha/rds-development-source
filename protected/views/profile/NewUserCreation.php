	<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'NewUserCreationForm',
	'enableClientValidation'=>true,
	'htmlOptions'=>array('class'=>'signin-wrapper'),
	'enableClientValidation'=>true,
	'clientOptions'=>array(
	'validateOnSubmit'=>true,
	),
)); 	?>
    
	<script type="text/javascript">		
	function Clear(){
		$("#NewUserCreationForm_fullName").val('');
		$("#NewUserCreationForm_userName").val('');
		$("#NewUserCreationForm_passWord").val('');
		$("#NewUserCreationForm_confirmPassword").val('');
		$("#NewUserCreationForm_emailId").val('');
		$("#NewUserCreationForm_department").val('');
		$("#NewUserCreationForm_referredFromDept").val('');
		$("#NewUserCreationForm_referredFromVcd").val('');
		
       }
	</script>
	
<div style="padding:10px; margin:7px; border:dashed 1px #666666;">

	<div id="formTitle"><h4><u><?php echo $form->labelEx($model,'Title',array('style'=>'font-weight: bold;font-size:20px;')); ?></u></h4></div>	
	<div class="content">
    <div class="row-fluid">	
	<div align="center"><?php echo $msg; ?></div>
	<table width="100%" border="0" cellpadding="1">
    <tr>
	<td>
	<tr>
	<td>
	<?php echo $form->labelEx($model,'fullName');?> 
	</td>
	<td width="150px;">	<div>
	<?php echo $form->textField($model,'fullName',array('style'=>'width:215px','onkeydown'=>"return alphaonly('NewUserCreationForm_fullName')",'maxlength'=>'100'));
	echo $form->error($model,'fullName');?>
	</div>
	</td>
	</tr>
	<tr>
	<td>
	<?php echo $form->labelEx($model,'userName');?> 
	</td>
	<td>	<div>
	<?php echo $form->textField($model,'userName',array('style'=>'width:215px','onkeydown'=>"return alphanumeric('NewUserCreationForm_userName')",'maxlength'=>'50'));
	echo $form->error ($model,'userName');?>
	</div>
	</td>
	</tr>	
	<tr>
	<td>
	<?php echo $form->labelEx($model,'passWord');?> 
	</td>
	<td width="150px;"><div>
	
	<?php echo $form->passwordField($model,'passWord',array('style'=>'width:215px','onkeydown'=>"return alphanumeric('NewUserCreationForm_passWord')",'maxlength'=>'50'));
	echo $form->error    ($model,'passWord');?>
	</div>
	</td>
	</tr>
	<tr>	
	<td>
	<?php echo $form->labelEx($model,'confirmPassword');?> 
	</td>
	<td><div>	
	<?php echo $form->passwordField($model,'confirmPassword',array('style'=>'width:215px','onkeydown'=>"return alphanumeric('NewUserCreationForm_confirmPassword')",'maxlength'=>'50'));
	echo $form->error ($model,'confirmPassword');?>
	</div>

	</td>
	</tr>
	<tr>
	<td>
	<?php echo $form->labelEx($model,'emailId');?> 
	</td>
	<td width="150px;"><div>
	
	<?php echo $form->textField($model,'emailId',array('style'=>'width:215px','onkeydown'=>"return emailonly('NewUserCreationForm_emailId')",'maxlength'=>'50'));
	echo $form->error($model,'emailId');?>
	</div>

	</td>
	</tr>
	<tr>
	<td>
	<?php echo $form->labelEx($model,'department');?> 
	</td>
	<td width="150px;"><div>
	
	<?php echo $form->dropDownList($model,'department',CHtml::listData($department,'Department_Code','Department'),array('prompt'=>'-- Select--')); echo $form->error($model,'department'); ?>
</div>
	</td>
	</tr>
	<tr>
	<td>
	<?php echo $form->labelEx($model,'referredFromDept');?> 
	</td>
	<td width="150px;"><div>
	
	<?php echo $form->textField($model,'referredFromDept',array('style'=>'width:215px','onkeydown'=>"return alphaonly('NewUserCreationForm_referredFromDept')",'maxlength'=>'50'));
	echo $form->error    ($model,'referredFromDept');?>
	</div>
	</td>
	</tr>
	
	<tr>
	<td>
	<?php echo $form->labelEx($model,'referredFromVcd');?> 
	</td>
	<td width="150px;"><div>
	
	<?php echo $form->textField($model,'referredFromVcd',array('style'=>'width:215px','onkeydown'=>"return alphaonly('NewUserCreationForm_referredFromVcd')",'maxlength'=>'50'));
	echo $form->error    ($model,'referredFromVcd');?>
	</div>
	</td>
	</tr>
		
	</td>
	</tr>
	</table>	
	</div>
	</div>
	<table>
	<tr> 	
	<td><?php echo CHtml::submitButton('Submit',array('size'=>75,'class'=>'btn btn-primary')) ?></td>
	<td><?php echo CHtml::button('Clear',array('size'=>75,'class'=>'btn','onClick'=>'Clear()')) ?></td>
	</tr></table></div>
	<?php $this->endWidget(); ?>
