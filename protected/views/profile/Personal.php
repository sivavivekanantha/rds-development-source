	<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'personalForm1',
	'enableClientValidation'=>true,
	'htmlOptions' => array(
   'enctype' => 'multipart/form-data',
    ),
	'clientOptions'=>array(
	'validateOnSubmit'=>true,
	),
	)); 	
	?>
	<script>
	$(document).ready(function() {
   $("#PersonalForm_photo").change(function () 
   { 
     var iSize = ($("#PersonalForm_photo")[0].files[0].size / 1024); 
     if(1025>iSize) 
     {
        iSize = (Math.round(iSize * 100) / 100)
        $("#lblSize").html("File size is:"+ iSize  + "kb"); 
     }  
	 else
	 {
	 	 iSize = (Math.round((iSize / 1024) * 100) / 100)
         $("#lblSize").html("File size is:"+ iSize + "Mb");	 	
		 alert('File size too large') ; 
		 $('#PersonalForm_photo').val('');
	 }
  }); 
});
</script>
	<script type="text/javascript">
    function fileCheck(obj) {
	
            var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
           	 if ($.inArray($(obj).val().split('.').pop().toLowerCase(), fileExtension) == -1){
			 	 alert("Only '.jpeg','.jpg', '.png', '.gif', '.bmp' formats are allowed.");
			 	$('#PersonalForm_photo').val('');
			 }
    }
</script>
	<script type="text/javascript">
		function namechange(obj) 
			{		

				if(obj==1) 	 
				{	
				 $("#PersonalForm_previousName").attr('disabled', true);
		 		 $("#gazetteChange").attr('disabled', true);
					$("#PersonalForm_refNo").attr('disabled', true);
					$("#dob2").attr('disabled', true);
					$("#PersonalForm_pageNo").attr('disabled', true);	
				 }			 
				if(obj==2)	
				{ 
				    $("#PersonalForm_previousName").attr('disabled', false);
				 	$("#gazetteChange").attr('disabled', false);			 
					
				}
		  }	
		  function other_rel(obj)              
			{
			if(obj==9999) $("#otherReligion1").show();							
			else $("#otherReligion1").hide();
			}
		  function gazette(obj)              
			//If click Acadamic infomation details  button show and hidden like our usage
			{	
			if(obj==2)	{
				 $("#PersonalForm_refNo").attr('disabled', false);
				 	$("#dob2").attr('disabled', false);
						$("#PersonalForm_pageNo").attr('disabled', false);
				//$("#PageNo").removeAttr("disabled");
			  	//$("#refNo").removeAttr("disabled");
			  	//$("#DOB2").removeAttr("disabled");	
				
			  }			  		 
			  else{
			   $("#PersonalForm_refNo").attr('disabled', true);
				$("#dob2").attr('disabled', true);
				$("#PersonalForm_pageNo").attr('disabled', true);
			     $('#PersonalForm_refNo').val('');
			     $('#dob2').val('');
				 $('#PersonalForm_pageNo').val('');
				 
			   }
			}
			
			function Recivemail2(obj)              
			{	
			if(obj==2)	{
			     $("#PersonalForm_receiveIshangaMail").attr('disabled', false);
				 }			
			  else
			  {
			  $("#PersonalForm_receiveIshangaMail").attr('disabled', true);
			    $('#PersonalForm_receiveIshangaMail').val('');
			  }
		
			}
			
			function AgeCal(){                       	//AGE CALCULATION
		var dateStr = $("#dob").val();
		if(($("#dob").val().length)!=10) 
		{
			$("#age").val('');	
			return false;
			}
		var d =dateStr.split('-');
		// now d is the array ['31','01','2000'] 
		var today=new Date();
		//var fy=today.getFullYear();
		//var today=new Date('31-'+'07-'+today.getFullYear()); 
		// The year, month, and day of the variable today
		// are 2006, 1, and 1, respectively.
		// Remember, I am a JavaScript interpreter.
		var bday=new Date(d[2],d[1],d[0]);
		// The year of bday is 2000.
		// The month of bday would be 1 (remember, to me, 1 is February)
		// except that the given date (31) is too large for February
		// so we go into March -- the 2nd day of March, to be exact.
		// To me, March is month 2.
		// So, the month and day of bday are 2 and 2, respectively. 
		var by=bday.getFullYear(); // this is 2000. 
		var bm=bday.getMonth()-1; // this is 1 (because 2-1=1)
		var bd=bday.getDate(); // this is 2
		var Age=0; var dif=bday; 
		while(dif<= today ){                              //month calculation checking
		var dif = new Date(by+Age,bm,bd);
			Age++;
		}
		/*if(bm+1<=07)
		{	
		 if(bm+1==07 && bd<31){
       	Age+=1;						}
		//document.myform.Age.value=Age;
		else {
			Age+=1;	
		 		}			
				}		
		//}
		// so you loop with the 2nd of February as the date of birth!!
		*/ 
		// what is the above line supposed to do?
		//alert('You are '+Age+' years old') 
		//if(today[1]<07){
		//Age+=+1;
		//else
		//Age;
		Age +=-2 ;

	if(Age>0) 		                          //Print  actual age here 
			$("#age").val(Age);            
		else  
		{                                 //assign age zreo
			$("#age").val(" ");	
			$("#dob").val(""); 
			$("#dob").focus(); 
			alert('DOB must be greater than a year');
			Age=0;
		} 
 			}
		
	</script>
           <div>
           <div class="row-fluid">
		   <div class="widget">
		   <div class="widget-header">
     <div class="title" style="width: 300px">		   
		   <table><tr><td> <div align"top"><h4><u><?php echo $form->labelEx($model,'header',
		   array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4></div></td></tr></table>
	 </div>
	
	  </div>
<table width="100%" border="0">
<tr><td colspan="2"><table align="center"><?php echo $msg;?></table></td></tr>
  <tr>
    <td><table width="95%" border="0" cellspacing="3" cellpadding="3">
  <tr>
   <td colspan="5">
   	<table>
		<tr>
   			<td width="24%"><?php echo $form->labelEx($model,'title');?></td>
	 		<td ><?php echo $form->dropDownList($model,'title',array(''=>'Select','1'=>'Mr.','2'=>'Mrs.','3'=>'Ms.','4'=>'Bramachari')
	,array('style'=>'width:100px;'));
								echo $form->error($model,'title');?> </td>
    		<td colspan="3">
			<div class="input-small">
				<?php echo $form->textField($model,'firstName',array('readonly'=>'readonly','style'=>'width:615px;'));?> 
				<?php echo $form->error($model,'firstName');?>  
			</div></td>
   		</tr>
  </table>
 </td>	
	<td rowspan="3" align="right">
	<?php $mode = Yii::app()->request->baseUrl."/protected/photos/".Yii::app()->session['Header_Code'].".jpg?cod=".rand();			  	  //$mode."==".$yourImageUrl = Yii::app()->assetManager->publish($mode);  				?>
     <img src="<?php echo $mode ?>" class="avatar" alt="Avatar" height="130" width="130" >
	</td>							
    </tr>
  <tr>
  <td colspan="5">
  <table width="85%">	  	
 
  <tr>
  <td width="40%" ><?php echo $form->labelEx($model,'Aliasname');?> 
</td>
    <td colspan="3"><div class="input-small">
						<?php echo $form->textField($model,'Aliasname',array('style'=>'width:545px','maxlength'=>'50','onkeydown'=>"return alphaonly('PersonalForm_Aliasname')")); 
							  echo $form->error($model,'Aliasname');?>
					</div></td>
  	
  </tr> </table></td>
    
    </tr>
  <tr> 
  <td colspan="6">  
    <table width="85%" >
		<tr>
			<td >
				<?php echo $form->labelEx($model,'changeName');?> 
			</td>
    		<td>
				<?php echo $form->dropDownList($model,'changeName',array('1'=>'No','2'=>'Yes'),array('style'=>'width:180px','id'=>'changeName','onchange'=>'return namechange(this.value);' ));
				echo $form->error($model,'changeName');?></td>
			
			<td>
				<?php echo $form->labelEx($model,'previousName');?>				
			</td>
    		<td align="right">
				<?php echo $form->textField($model,'previousName',array('style'=>'width:260px','maxlength'=>'50','onkeydown'=>"return alphaonly('PersonalForm_previousName')"));
				 echo $form->error($model,'previousName');?> 				 	
		   </td>
		</tr>
			</table></td>
    </tr>
  <tr>
    <td><?php echo $form->labelEx($model,'gazetteChange');?></td>
    <td>	<?php echo $form->dropDownList($model,'gazetteChange',array(''=>'Select','1'=>'No','2'=>'Yes'),array('style'=>'width:220px','id'=>'gazetteChange','onclick'=>'return gazette(this.value);'));
			echo $form->error($model,'gazetteChange');?> </td>
    <td colspan="2">&nbsp;</td>
	
    <td colspan="2"><table width="100%" border="0" cellpadding="1">
  <tr>
    <td><?php echo $form->labelEx($model,'refNo');?> </td>
    <td>
	
	<?php echo $form->textField($model,'refNo',array('style'=>'width:40px;','maxlength'=>'15','onkeydown'=>"return alphanumeric('PersonalForm_refNo')"
	));
							echo $form->error($model,'refNo');?></td>
    <td><?php echo $form->labelEx($model,'dob2');?> </td>
    <td><?php echo $form->textField($model,'dob2',array('id'=>'dob2','style'=>'width:80px;')); ?>
							<?php echo $form->error($model,'dob2'); ?></td>
    <td><?php echo $form->labelEx($model,'pageNo');?>  </td>
	<?php $maxmob=6; ?>
    <td><?php echo $form->textField($model,'pageNo',array('style'=>'width:25px;','onkeydown'=>"return numberonly('PersonalForm_pageNo')",
	'maxlength'=>$maxmob));
							echo $form->error($model,'pageNo');?></td>
  </tr>

</table>
 	 <div class="tab-widget">
  <ul class="signups"> 
           <li><div class="info"><h6>Hint: Please refer below Govt. emblem in Gazette</h6></div></li></ul></div></td>
    </tr>	
  <tr>
    <td>	<?php echo $form->labelEx($model,'gender');?></td>
    <td><?php echo $form->dropDownList($model,'gender',array(''=>'Select','1'=>'Male','2'=>'Female'),array('style'=>'width:220px'));
				echo $form->error($model,'gender');?> </td>
    <td colspan="2">&nbsp;</td>
    <td colspan="2"><table width="98%" border="0" cellpadding="0">
      <tr>	  
        <td width="33%">
		<div class="">		
			<?php echo $form->labelEx($model,'dob');?>
			</div>	
		</td>		
        <td>
			<div class="">			
		    <?php echo $form->textField($model,'dob',array('id'=>'dob','style'=>'width:180px',
			'onfocus="AgeCal();'));?>
			<?php echo $form->error($model,'dob');?></div> </td>
		<td>&nbsp;</td>
        <td ><?php echo $form->labelEx($model,'age');?> </td>
        <td>	<?php echo $form->textField($model,'age',array('style'=>'width:40px;','id'=>'age','onClick'=>'AgeCal();','readonly'=>'readonly'));
						echo $form->error    ($model,'age');?></td>
      </tr>
	   <script>AgeCal()</script> 
    </table></td>
    </tr>
  <tr>
    <td><?php echo $form->labelEx($model,'maritalStatus');?></td>
    <td><?php echo $form->dropDownList($model,'maritalStatus',array(''=>'Select','1'=>'Unmarried','2'=>'Married','3'=>'Widow','4'=>'Widower',
			'5'=>'Divorced','6'=>'Separated'),array('style'=>'width:220px'));
			echo $form->error($model,'maritalStatus');?></td>
    <td colspan="2">&nbsp;</td>
    <td><?php echo $form->labelEx($model,'nationality');?></td>
    <td>	<?php echo $form->dropDownList($model,'nationality',CHtml::listData($nationality,'Nationality_Code','Nationality'),array('prompt'=>'Select','style'=>'width:220px'));
			echo $form->error($model,'nationality');?> </td>
  </tr>
  <tr>
    <td>	<?php echo $form->labelEx($model,'birthCountry');?> </td>
    <td><?php echo $form->dropDownList($model,'birthCountry',CHtml::listData($birthCountry,'Country_Code','Country_Name'),array('prompt'=>'Select','style'=>'width:220px'));
			echo $form->error($model,'birthCountry');?> </td>
    <td colspan="2">&nbsp;</td>
    <td><?php echo $form->labelEx($model,'birthCity');?> </td>
    <td><?php echo $form->textField($model,'birthCity',array('onkeydown'=>"return alphaonly('PersonalForm_birthCity')"));
			echo $form->error    ($model,'birthCity');?></td>
  </tr>
  <tr>
    <td><?php echo $form->labelEx($model,'birthTown');?> </td>
    <td><?php echo $form->textField($model,'birthTown',array('onkeydown'=>"return alphaonly('PersonalForm_birthTown')"));
			echo $form->error    ($model,'birthTown');?>  </td>
    <td>&nbsp;</td>

   <td> </td>
    <td>
	<?php echo $form->labelEx($model,'religion');?></td>
				<td><?php echo $form->dropDownList($model,'religion',CHtml::listData($Religion,'Religion_id','Religion'),array('prompt'=>'Select','style'=>'width:215px','onchange'=>'other_rel(this.value);',));
			echo $form->error($model,'religion');?> 
			<div id='otherReligion1' style="display: none;">
			<div>	
				<?php
					 echo $form->textField($model,'otherReligion');
					 echo $form->error($model,'otherReligion');				
				?>
				</div>
			</div> </td>
	
    </tr>
  <tr>
    <td><?php echo $form->labelEx($model,'FTV_DOJ_Isha');?> </td>
    <td>	<?php echo $form->textField($model,'FTV_DOJ_Isha',array('id'=>'FTV_DOJ_Isha','style'=>'width:205px')); ?>
			<?php echo $form->error($model,'FTV_DOJ_Isha');?></td>
    <td>&nbsp;</td>
	 <td>&nbsp;</td>
	 <td>	
	 <?php echo $form->labelEx($model,'photo'); ?>	 	
	 </td>
	 <td>
			<?php echo $form->fileField($model,'photo',array('style'=>'width:250px','onchange'=>'fileCheck(this)')); ?>  
			<?php echo $form->error($model,'photo'); ?>
			<br><b><label id="lblSize" />
			
	</td>
	
    </tr>
</table>
</td>
  </tr>
</table>
		<table align="right">
				<tr> 	
					<td><?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'12as#','size'=>75,'class'=>'btn btn-primary')) ?></td>
				</tr>
			</table>

			
		</div>
		 </div>
	</div>
	        
	<?php if(strlen($model->changeName)==0)	$model->changeName=1;
	if(strlen($model->reciveMail)==0)	$model->reciveMail=1;
		?>
	
	<script>namechange(<?php echo $model->changeName;?>)</script>
	<script>Recivemail2(<?php echo $model->reciveMail;?>)</script>
	<script>gazette(<?php echo $model->gazetteChange;?>)</script>
	<?php if($model->religion==9999){?>
	<script>other_rel(<?php echo $model->religion;?>)</script>
	<?php } ?>
	
	
	<?php $this->endWidget(); ?>