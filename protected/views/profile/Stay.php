<?php $form=$this->beginWidget('CActiveForm', array(
	  'id'=>'StayForm',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); 
 ?>
 <script type="text/javascript">
 	function ajaxCottage(obj,obj1)
	{	
		if(obj=='') return false;
		if(obj=='' || obj1==undefined) obj1=0;
		var url='<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/AjaxFloor?cottageCode='+obj+'&Stay_Code='+obj1;
		$("#StayForm_floorName").load(url);
	}
 
 
	function stay(obj)
	{
		if(obj==3) 
			$("#Cottage_view").show();
		else 
	   		$("#Cottage_view").hide();
		if(obj==1)
			$("#Stayprovide_view").show();
		else
			$("#Stayprovide_view").hide();
		
	}
	
	function registeredmember(obj)
	{
		if(obj==2 || obj==3) 
			$("#cottageMember").show();
		else {
			$("#cottageMember").hide();
			primarymember('');
		}
	}
	
	function primarymember(obj)
	{
		if(obj!=1) 
		{
			$("#member").show();
			$("#StayForm_cottageMember").val(obj);
		} else {
			$("#StayForm_cottageMember").val(obj);
			$("#StayForm_memberName").val('');
			$("#member").hide();
		}
	}
	
 </script>
<div style="padding:10px; margin:7px;" align="center">
<div class="widget" style="width: 60%;" align="center"> 
			<div class="widget-header">
               <div class="title">
				<div id="formTitle">
          		<h4><u><?php echo $form->labelEx($model,'stay',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
	      		</div></div></div>
				<div class="widget-body">	
     <div align="center"> <?php echo $msg; ?></div></div>
	    	<tr>
  			<td>
				<table width="90%" align="center" border="0" cellpadding="1">
   					<tr>
			    		<td width="46%"><?php echo $form->labelEx($model,'stayPlace');?></td>	
			   			<td align="left">
			   				<div>
				   				<?php echo $form->dropDownList($model,'stayPlace',
						    		CHtml::listData($stay1,'Stay_Code','Stay_Area'),
						    		array('prompt'=>'Select',
						    		'onchange'=>'stay(this.value)')
						    		);  ?>
								<?php echo $form->error($model,'stayPlace'); ?>
							</div>
						</td>
				    </tr>
					</table>
	<table  width="90%" align="center" border="0" cellpadding="1" id="Cottage_view"  style="display:none;" >
		<tr>
			<td><?php echo $form->labelEx($model,'cottageName'); ?></td>
			<td><?php echo $form->dropDownList($model,'cottageName',
				CHtml::listData($cottage1,'Stay_Code','Stay_Area'),
				array('prompt'=>'Select',
				'onchange'=>'ajaxCottage(this.value)')
				);?><?php echo $form->error($model,'cottageName'); ?>
			</td>
		</tr>
		<tr>
			<td><?php echo $form->labelEx($model,'floorName'); ?></td>
    		<td><div><?php echo $form->dropDownList($model,'floorName',array(''=>'Select'));?>
				<?php echo $form->error($model,'floorName'); ?></div>
			</td>
		</tr>
		<tr>
			<td><?php echo $form->labelEx($model,'cottageNo'); ?></td>
			<td><div><?php echo $form->textField($model,'cottageNo',array('maxlength'=>'10')); ?>
				<?php echo $form->error($model,'cottageNo'); ?></div></td>
		</tr>
		<tr>
			<td><?php echo $form->labelEx($model,'occupayYear'); ?></td>
			<td width=""> <?php echo $form->dropDownList($model,'occupayMonth',$month,
				array('prompt'=>'Select','style'=>'width:100px'),
				array('options' => array('occupayMonth'=>array('selected'=>true))));  ?>
			<?php echo $form->dropDownList($model,'occupayYear',$year,
				array('prompt'=>'Select','style'=>'width:100px'),
				array('options' => array('occupayYear'=>array('selected'=>true))));  ?>
		
			</td>
		</tr>
		<tr>
			<td><?php echo $form->labelEx($model,'cottageAllot'); ?></td>
			<td><div><?php echo $form->dropDownList($model,'cottageAllot',
					array(''=>'Select',
					'1'=>'Complimentary Accommodation provided by Isha',
					'2'=>'Cottage allotted against Payment',
					'3'=>'Cottage allotted against any other property',
					'4'=>'Cottage allotted on rental basis'),
					array('onchange'=>'registeredmember(this.value)')
					); ?><?php echo $form->error($model,'cottageAllot'); ?></div>
				</td>
		</tr>
		<tr id="cottageMember"  style="display:none"><td><?php echo $form->labelEx($model,'cottageMember'); ?></td>
			<td><div><?php echo $form->dropDownList($model,'cottageMember',
					array(''=>'Select',
					'1'=>'Primary Member',
					'2'=>'Registered Member',
					'3'=>'Spouse of Primary Member'
					),
					array('onchange'=>'primarymember(this.value)')
					);  ?><?php echo $form->error($model,'cottageMember'); ?></div></td>
		</tr>
		<tr id="member" style="display: none"><td><?php echo $form->labelEx($model,'memberName'); ?></td>
			<td><div><?php echo $form->textField($model,'memberName',array('maxlength'=>'50')); ?>
			<?php echo $form->error($model,'memberName'); ?>	
			</div></td>
		</tr>
	</table>
	
			<table width="90%" align="center" border="0" cellpadding="1" id="Stayprovide_view"  style="display:none;" >
				<tr>
					<td width="46%"><?php echo $form->labelEx($model,'stayProvide',array('style'=>'width:200px;') ); ?></td>
					<td><div><?php echo $form->dropDownList($model,'stayProvide',array(''=>'Select','1'=>'Yes','2'=>'No')); ?>
					<?php echo $form->error($model,'stayProvide'); ?>			
					</div></td>
					</tr>
					<tr>
					<td><?php echo $form->labelEx($model,'stayAddress',array('style'=>'width:150px;'));?></td>
				 	<td><div><?php echo $form->textArea($model,'stayAddress');?>
					<?php echo $form->error($model,'stayAddress'); ?>		
					</div></td>
				</tr>
			</table>
  		</td>
	</tr> 
	<br>
  <table align="right">
	<tr>
    	<td><?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'0as$%','size'=>75,'class'=>'btn btn-primary')) ?></td>
    </tr>
	</table>	
</div></div>

    
	 <?php  if($model->stayPlace == 1 or $model->stayPlace == 3 )
	 echo "<script>stay('".$model->stayPlace."')</script>"; ?>
  
	<?php  if($model->cottageName > 0 )
		echo "<script>ajaxCottage('".$model->cottageName."','".$model->floorName."')</script>"; ?>
	<?php  if($model->cottageAllot > 0 )
		echo "<script>registeredmember('".$model->cottageAllot."')</script>"; 
		echo "<script>primarymember('".$model->cottageMember."')</script>"; ?>

	<?php $this->endWidget(); ?>
