	<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'PersonalContactForm',
	'enableClientValidation'=>true,
	'htmlOptions' => array(
   'enctype' => 'multipart/form-data',
    ),
	'clientOptions'=>array(
	'validateOnSubmit'=>true,
	),
	)); 	
	?>
	<script type="text/javascript">		
	function ajaxState(obj,obj1)
	{	
		if(obj=='') return false;
		var url='<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/Ajaxsate?countryCode='+obj+'&statecode='+obj1;
		$("#state").load(url);			
	}
	function ajaxState1(obj,obj1)
	{	
		if(obj=='') return false;
	
		var url='<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/Ajaxsate?countryCode='+obj+'&statecode='+obj1;
		$("#state1").load(url);			
	}
	function ajaxDistrict(obj,obj1)
	{
	if(obj=='') return false;
	if(obj==9999){
		ajaxCity(0,0,9999);
		 $('#otherState').show(); } else $('#otherState').hide();		
		$('#PersonalContactForm_oState').val('');
		var url='<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/Ajaxdistrict?stateCode='+obj+'&DistrictCode='+obj1;		
		$("#district").load(url);
	}
	
	function ajaxDistrict1(obj,obj1)
	{
		
	if(obj=='') return false;
	if(obj==9999) {
		$('#otherState1').show();
		ajaxCity1(0,0,9999);} else $('#otherState1').hide();		
		$('#PersonalContactForm_oState1').val('');
		var url='<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/Ajaxdistrict?stateCode='+obj+'&DistrictCode='+obj1;		
		$("#district1").load(url);
		
	}
	
	function ajaxCity(obj,obj1,obj2)
	{  
    	if(obj1==0) obj1 = $("#state").val();
		if(obj=='' && obj2=='') return false;	
		if(obj2==9999) $("#city").val();
		var url='<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/AjaxCity?districtCode='+obj+'&stateCode='+obj1+'&CityCode='+obj2;	
		$("#city").load(url);
			
		}
		
		function ajaxCity1(obj,obj1,obj2)
	    {  
		 
		if(obj1==0) obj1 = $("#state1").val();
		if(obj=='' && obj2=='') return false;
		if(obj2==9999) $("#city").val();	
		var url='<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/AjaxCity?districtCode='+obj+'&stateCode='+obj1+'&CityCode='+obj2;	
		$("#city1").load(url);
			
		}
		function ajaxothercity(obj)
		{	
			if(obj==99999)
			{
				 $('#otherCity').show();
			     $('#PersonalContactForm_presentoCity').val('');
			 }
			 else
			  {
			 	 $('#otherCity').hide();
			 	$('#PersonalContactForm_presentoCity').val('');
			 	
			 }
			
		}
		
		function ajaxothercity1(obj)
		{		
			if(obj==99999)
			{
				 $('#otherCity1').show();
			     $('#PersonalContactForm_permanentoCity').val('');
			 }
			 else
			  {
			 	 $('#otherCity1').hide();
			 	$('#PersonalContactForm_permanentoCity').val('');
			 	
			 }
			
		}
		
		function Clear(){
		$("#PersonalContactForm_presentAddress").val('');
		$("#PersonalContactForm_presentVillage").val('');
		$("#PersonalContactForm_presentCountry").val('');
		$("#PersonalContactForm_presentState").val('');
		$("#PersonalContactForm_presentoState").val('');
		$("#PersonalContactForm_presentDistrict").val('');
		$("#PersonalContactForm_presentCity").val('');
		$("#PersonalContactForm_presentoCity").val('');
		$("#PersonalContactForm_presentPincode").val('');
		$("#PersonalContactForm_permanentaddress").val('');
		$("#PersonalContactForm_permanentvillage").val('');
		$("#PersonalContactForm_permanentcountry").val('');
		$("#PersonalContactForm_permanentstate").val('');
		$("#PersonalContactForm_permanentoState").val('');
		$("#PersonalContactForm_permanentdistrict").val('');
		$("#PersonalContactForm_permanentcity").val('');
		$("#PersonalContactForm_permanentoCity").val('');
		$("#PersonalContactForm_permanentpincode").val('');

		
		
       }
		
		
		
		
		
	</script>
	
	<div class="widget">
	<div class="widget-header">
    <div class="title">
	<div><h4><u><?php echo $form->labelEx($model,'presentA',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4></div>
	</div></div></div>
<div align="center"> <?php echo $msg; ?></div>
    <table cellpadding="3" cellspacing="1" width="100%">
	<tr><td width="50%">
	 <div class="widget">
     	<div class="widget-header">
        	<div class="title">
            <u><?php echo $form->labelEx($model,'presentB',array('style'=>'font-weight: bold;font-size:18px;')); ?></u>
            </div>
        </div>
        <div class="widget-body"> 
		
		<table width="100%" border="0" cellpadding="1" style="padding:10px; margin:7px;">
		<tr>
			 	<?php echo $form->hiddenField($model,'addressType',array('id'=>'addressType')); ?>
	 			<?php echo $form->hiddenField($model,'aType',array('id'=>'aType')); ?>
	 		<td><?php echo $form->labelEx($model,'presentAddress');?></td>
     		<td><div>
	 			<?php echo $form->textArea($model,'presentAddress',array('placeholder'=>'max. Length 500 chars.','maxlength'=>'500'));
		 		echo $form->error($model,'presentAddress'); ?>
			</div></td>
		</tr> 
		<tr>
	<td><?php echo $form->labelEx($model,'presentVillage');?></td>
	<td>
	<div>
		<?php echo $form->textField($model,'presentVillage',array('onkeydown'=>"return alphaonly('PersonalContactForm_presentVillage')",'maxlength'=>'50'));
	 echo $form->error($model,'presentVillage');?>  
	</div></td></tr>
	<tr>
	 <td><?php echo $form->labelEx($model,'presentCountry');?></td>
     <td>
	 <div>
	 	 <?php echo $form->dropDownList($model,'presentCountry',CHtml::listData($country,'Country_Code','Country_Name')
	 ,array('prompt'=>'Select','onchange'=>'ajaxState(this.value)')); 
	  echo $form->error($model,'presentCountry'); ?>
	 </div>
	</td></tr>
	<tr>
	 <td><?php echo $form->labelEx($model,'presentState');?></td>
     <td>
	 <div><?php echo $form->dropDownList($model,'presentState',array('prompt'=>'Select'),array('id'=>'state','onchange'=>'ajaxDistrict(this.value)'));
	  echo $form->error($model,'presentState'); ?>
	 <div id='otherState' style="display: none;"><?php echo $form->textField($model,'presentoState',array('maxlength'=>'50','onkeydown'=>"return alphaonly('PersonalContactForm_presentoState')"));echo $form->error($model,'presentoState'); ?></div></div>
	 </td></tr>
	 <tr>
	<td><?php echo $form->labelEx($model,'presentDistrict');?></td>
    <td><?php echo $form->dropDownList($model,'presentDistrict',array(''=>'Select'),array('id'=>'district',
	'onchange'=>'ajaxCity(this.value,0,0)'));?></td></tr>
	<tr>
	 <td><?php echo $form->labelEx($model,'presentCity');?></td>
     <td><?php echo $form->dropDownList($model,'presentCity',array(''=>'Select'),array('id'=>'city','onchange'=>'ajaxothercity(this.value)'));
	 echo $form->error($model,'presentCity'); ?>
	 <div id='otherCity' style="display: none;"><?php echo $form->textField($model,'presentoCity',array('maxlength'=>'50','onkeydown'=>"return alphaonly('PersonalContactForm_presentoCity')"));echo $form->error($model,'presentoCity'); ?></div></td></tr>
	 <tr>
     <td><?php echo $form->labelEx($model,'presentPincode');?></td>	 
	 <td><?php echo $form->textField($model,'presentPincode',array('onkeydown'=>"return alphanumeric('PersonalContactForm_presentPincode')",'maxlength'=>'10'));?></td></tr>
		</table>
		</div>
	</div>
	</td>
	<td width="50%">
	 <div class="widget">
     	<div class="widget-header">
        	<div class="title">
            <u><?php echo $form->labelEx($model,'presentC',array('style'=>'font-weight: bold;font-size:18px;')); ?></u>
            </div>
        </div>
        <div class="widget-body"> 
		<table width="100%" border="0" cellpadding="1" style="padding:10px; margin:7px;">
		<tr>
	  <td><?php echo $form->labelEx($model,'permanentAddress');?></td>
     <td>
	 <div>
	 <?php echo $form->textArea($model,'permanentAddress',array('placeholder'=>'max. Length 500 chars.','maxlength'=>'500'));
	 echo $form->error($model,'permanentAddress'); ?>  
	 	
	 </div></td>
		</tr> <tr>
		<td><?php echo $form->labelEx($model,'permanentVillage');?></td>
	<td>	
	<div>
	<?php echo $form->textField($model,'permanentVillage',array('onkeydown'=>"return alphaonly('PersonalContactForm_permanentVillage')",'maxlength'=>'50'));
	 echo $form->error($model,'permanentVillage');?>  
	 </div>
	</td>
	</tr><tr>
	 <td><?php echo $form->labelEx($model,'permanentCountry');?></td>
     <td>
	 	<div><?php echo $form->dropDownList($model,'permanentCountry',CHtml::listData($country,'Country_Code','Country_Name')
	 ,array('prompt'=>'Select','onchange'=>'ajaxState1(this.value)')); 
	 echo $form->error($model,'permanentCountry'); ?>
	 	
	 </div></td></tr>
	 
	  <tr>
	  <td><?php echo $form->labelEx($model,'permanentState');?></td>
     <td>
	 <div><?php echo $form->dropDownList($model,'permanentState',array('prompt'=>'Select'),array('id'=>'state1','onchange'=>'ajaxDistrict1(this.value)'));
	  echo $form->error($model,'permanentState');?>
	 <div id='otherState1' style="display: none;"><?php echo $form->textField($model,'permanentoState',array('maxlength'=>'50','onkeydown'=>"return alphaonly('PersonalContactForm_permanentoState')"));echo $form->error($model,'permanentoState');?></div></div>
	 </td>
	 </tr>
	<tr><td><?php echo $form->labelEx($model,'permanentDistrict');?></td>
    <td><?php echo $form->dropDownList($model,'permanentDistrict',array(''=>'Select'),array('id'=>'district1',
	'onchange'=>'ajaxCity1(this.value,0,0)'));?></td>
	</tr>
	<tr>
	<td><?php echo $form->labelEx($model,'permanentCity');?></td>
     <td><?php echo $form->dropDownList($model,'permanentCity',array(''=>'Select'),array('id'=>'city1','onchange'=>'ajaxothercity1(this.value)'));
	 echo $form->error($model,'permanentCity'); ?>
	 <div id='otherCity1' style="display: none;"><?php echo $form->textField($model,'permanentoCity',array('maxlength'=>'50','onkeydown'=>"return alphaonly('PersonalContactForm_permanentoCity')"));echo $form->error($model,'permanentoCity');?></div></td>
	 </tr>
	 <tr><td><?php echo $form->labelEx($model,'permanentPincode');?></td>	 
	 <td><?php echo $form->textField($model,'permanentPincode',array('onkeydown'=>"return alphanumeric('PersonalContactForm_permanentPincode')",'maxlength'=>'10'));?></td>
</tr>




		</table>
		</div>
		<table align="right"><tr> 	
	<td><?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'oc&*5','size'=>75,'class'=>'btn btn-primary')) ?></td>
	</tr></table>

	</div>
</td></tr></table>

    	
	
<script>ajaxState('<?php echo $model->presentCountry ?>','<?php echo $model->presentState ?>')</script>

<script>ajaxDistrict('<?php echo $model->presentState ?>','<?php echo $model->presentDistrict ?>')</script>

<?php if(strlen($model->presentState)>0) { ?>

<script>ajaxCity('<?php echo $model->presentDistrict ?>','<?php echo $model->presentState ?>','<?php echo $model->presentCity ?>')

</script>
<?php } 
  if($model->presentCity=='99999' and $model->errflag>0)
 { ?> 
  <script>ajaxothercity('<?php echo $model->presentCity ?>','0')</script> 
<?php } ?>



<script>ajaxState1('<?php echo $model->permanentCountry ?>','<?php echo $model->permanentState ?>')</script>

<script>ajaxDistrict1('<?php echo $model->permanentState ?>','<?php echo $model->permanentDistrict ?>')</script>

<?php if(strlen($model->permanentState)>0) { ?>

<script>ajaxCity1('<?php echo $model->permanentDistrict ?>','<?php echo $model->permanentState ?>','<?php echo $model->permanentCity ?>')
</script>
<?php } 
  if($model->permanentCity=='99999' and $model->errflag>0)
 { ?> 
  <script>ajaxothercity1('<?php echo $model->permanentCity ?>','0')</script> 
<?php } ?>
	
	
	<?php $this->endWidget(); ?>