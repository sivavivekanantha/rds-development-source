<?php $form = $this->beginWidget('CActiveForm', array(
		'id'                    =>'MedicalForm',
		'enableClientValidation'=>true,
		'clientOptions'         =>array(
			'validateOnSubmit'=>true,),
	));
?>
<script>
	$( document ).ready(function()
		{
			$("#data-table span").html('');
		});

	function popupValue(obj1,obj2,obj3,obj4,obj5,obj6,obj7)
	{
		if(obj6!=3) // Coming Add & Edit
		{
			if(obj7!=0)
			{
				// If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings").show();
				$('#accSettings').removeClass('modal hide fade').addClass('modal hide fade in');
			}
			$("#MedicalForm_physicalAllignment").val(obj1);
			$("#MedicalForm_physicalFrequency").val(obj2);
			$("#MedicalForm_physicalStatus").val(obj3);
			$("#MedicalForm_physical").val(obj4);
			$("#MedicalForm_medicalPhysicalCode").val(obj5);
			$("#MedicalForm_action").val(obj6);

		}
		if(obj6==3)
		{
			// Delete
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg))
			{
				return false;
			}
			else
			{
				$("#MedicalForm_physicalAllignment").val(obj1);
				$("#MedicalForm_physicalFrequency").val(obj2);
				$("#MedicalForm_physicalStatus").val(obj3);
				$("#MedicalForm_physical").val(obj4);
				$("#MedicalForm_medicalPhysicalCode").val(obj5);
				$("#MedicalForm_action").val(obj6);
				$("#MedicalForm").submit();
			}
		}

	}
	function popupHide()
	{
		$("#errinfo").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
		$(".errorMessage").html('');   // Yii Error msg is empty after close,
		$('#edPopup').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
		$("#accSettings").hide();	 // Popup Hide
	}

	function saveBlood(obj)
	{
		var msg="Are you sure you want to save this Blood Group?";
		if(!confirm(msg))
		{
			return false;
		}
		else
		{
			url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/Blood?&blood='+obj;

			$("#test").load(url);
			//$("#MedicalForm").submit();

		}
	}

	function showdove(obj,obj1)
	{

		if(obj==1)
		$('#showdiv').show();

		if(obj==2 && obj1==1)
		{

			var msg="Are you sure you want to save Any Physical Illness?";
			if(!confirm(msg))
			{
				return false;
			}
			else
			{
				url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/SavePhysicalFormValid?&FormValid='+obj;

				$("#test").load(url);
				//$("#MedicalForm_action").val('0');
				$('#showdiv').hide();


			}
		}

	}
</script>
<div id="test" style="display: none;">
</div>


<div class="widget">

	<div class="widget-header">
		<div class="title">
			<h4>
				<u>
					<?php echo $form->labelEx($model,'Medical',array('style'=>'font-weight: bold;font-size:18px;')); ?>
				</u>
			</h4>
		</div>
	</div>
	<div class="widget-body" style="margin-left: 50px;">
		<table>
			<tr>
				<td>
					<div>
						<?php echo $form->labelEx($model,'bloodName') ?>
					</div>
				</td>
				<td>
					<div>
						<?php echo $form->dropDownList($model,'bloodName',CHtml::listData($blood,'BloodGroup_Code','BloodGroup_Name'),array('prompt'  =>'Select','onchange'=>'saveBlood(this.value)'));
						echo $form->error($model,'bloodName');	 ?>
					</div>
				</td>
			</tr>
		</table>
	</div>

	<div class="widget-body">
		<div id="errinfo3" class="error" align="center" >
			<?php echo $msg3; ?>
		</div>
	</div>
	<div style="padding:10px; margin:7px;">
		<div class="widget">
			<div class="widget-body">
				<div id="dt_example" class="example_alt_pagination" style="padding:10px; margin:7px; ">
					<div class="widget-body">
						<table style="margin-left:0px;" >

							<?php
							if(mssql_num_rows($row1) == 0)
							{
								if($model->formValid <> 2) $model->formValid = '';
								?>
								<tr>
									<td colspan="1">
										<?php echo $form->labelEx($model,'formValid')?>
									</td>
									<td>
										<?php echo $form->dropDownList($model,'formValid',array('' =>'Select','1'=>'Yes','2'=>'No'),
											array('onchange'=>'showdove(this.value,1);'))?>
									</td>
								</tr>
								<?php
							} ?>
						</table>
					</div>

					<div id="showdiv" style="display:none;">
						<table align="right" >
							<tr>

								<td>
									<div>
										<a href="#accSettings" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue('','','','','',1,0)">
											Add New
										</a>
									</div>
								</td>
							</tr>
						</table>

						<table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
							<thead>
								<th>
									<?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?>
								</th>
								<th>
									<?php echo $form->labelEx($model,'physicalAllignment',array('style'=>'font-weight: bold;font-size:13px;')); ?>
								</th>
								<th>
									<?php echo $form->labelEx($model,'physicalFrequency',array('style'=>'font-weight: bold;font-size:13px;')); ?>
								</th>
								<th>
									<?php echo $form->labelEx($model,'physicalStatus',array('style'=>'font-weight: bold;font-size:13px;')); ?>
								</th>
								<th>
									<?php echo $form->labelEx($model,'physical',array('style'=>'font-weight: bold;font-size:13px;')); ?>
								</th>
								<th>
									<?php echo $form->labelEx($model,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?>
								</th>
							</thead>
							<tbody>
								<?php $i = 0;?>
								<?php while($field = mssql_fetch_array($row1)){
									$i++;
									?>

									<tr class="gradeX">
										<td align="center">
											<?php echo $i; ?>
										</td>
										<td>
											<?php echo $field['Physical_Alignment'];?>
										</td>
										<td>
											<?php echo $field['Physical_Frequency'];?>
										</td>
										<td>
											<?php echo $field['Physical_Status'];?>
										</td>
										<td>
											<?php
											if($field['Physical'] == 'P') echo "Present";
											else
											if($field['Physical'] == 'T')
											echo "Treated";?>
										</td>
										<td style="width:20px;" class="hidden-phone">

											<a href="#accSettings" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue('<?php echo $field['Physical_Alignment'] ?>','<?php echo $field['Physical_Frequency'] ?>','<?php echo $field['Physical_Status'] ?>','<?php echo $field['Physical'];?>','<?php echo $field['Medical_Physical_Code'] ?>',2,0)">
												&#x270E
											</a>
											<?php
											if(Yii::app()->session['Freeze'] == "N")
											{
												?>
												<a class="btn btn-success btn-small hidden-phone"  id="delbtn"   data-original-title="Delete" onclick="popupValue('<?php echo $field['Physical_Alignment'] ?>','<?php echo $field['Physical_Frequency'] ?>','<?php echo $field['Physical_Status'] ?>','<?php echo $field['Physical'];?>','<?php echo $field['Medical_Physical_Code'] ?>',3,0)">
													&#x2717
												</a>
												<?php
											} ?>
										</td>
									</tr>
									<?php
								} ?>
							</tbody>
						</table>
						<div id="accSettings" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
							<div id="errinfo" class="error" align="center" >
								<?php echo $msg1; ?>
							</div>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide()">
									x
								</button>
								<h4>
									<u>
										<?php echo $form->labelEx($model,'MedicalPopup',array('style'=>'font-weight: bold;font-size:18px;')); ?>
									</u>
								</h4>
							</div>
							<div class="modal-body">
								<div class="row-fluid">
									<div style="float:left; overflow: hidden; width:200px">
										<?php echo $form->hiddenField($model,'action'); ?>
										<?php echo $form->hiddenField($model,'medicalPhysicalCode'); ?>
										<?php echo $form->labelEx($model,'physicalAllignment'); ?>
									</div>
									<div>
										<?php echo $form->textField($model,'physicalAllignment',array('maxlength'=>'250')); ?>
										<?php echo $form->error($model,'physicalAllignment'); ?>
									</div>

								</div>

								<div class="row-fluid">
									<div style="float:left; overflow: hidden; width:200px">
										<?php echo $form->labelEx($model,'physicalFrequency'); ?>
									</div>
									<div>
										<?php echo $form->textField($model,'physicalFrequency',array('maxlength'=>'250')); ?>
										<?php echo $form->error($model,'physicalFrequency'); ?>
									</div>

								</div>

								<div class="row-fluid">
									<div style="float:left; overflow: hidden; width:200px">
										<?php echo $form->labelEx($model,'physicalStatus'); ?>
									</div>
									<div>
										<?php echo $form->textField($model,'physicalStatus',array('maxlength'=>'250')); ?>
										<?php echo $form->error($model,'physicalStatus'); ?>
									</div>

								</div>

								<div class="row-fluid">
									<div style="float:left; overflow: hidden; width:200px">
										<?php echo $form->labelEx($model,'physical'); ?>
									</div>
									<div>
										<?php echo $form->dropDownList($model,'physical',array('P'=>'Present','T'=>'Treated'),
											array('prompt'=>'Select'),
											array('options' => array('physical'=>array('selected'=>true))));
										echo $form->error($model,'physical');?>
									</div>
								</div>

							</div>


							<div class="modal-footer">

								<?php echo CHtml::submitButton('Save',array('id'   =>'saveform','name' =>'hj&%4','class'=>'btn btn-primary')); ?>
								<button type="button" class="btn" aria-hidden="true" data-dismiss="modal" onclick="popupHide()">
									Close
								</button>

							</div>

						</div>
					</div>

					<?php
					if($model->action <> 3 and (strlen($msg1) > 3 or $model->errflag == 1))
					{
						echo "<div id='edPopup' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
						echo "<script>popupValue('".$model->physicalAllignment."','".$model->physicalFrequency."','".$model->physicalStatus."','".$model->physical."','".$model->medicalPhysicalCode."',".$model->action.",1)</script>";
					}
					?>
					<div class="clearfix">
					</div>
				</div>
			</div>
		</div>
		<?php
		if($model->formValid > 0)
		echo "<script>showdove('".$model->formValid."',0)</script>"; ?>
		<?php $this->endWidget(); ?>

		<?php ////////////////////////////////SECOND FORM///////////////////////////////////////////////////////////?>

		<?php $form = $this->beginWidget('CActiveForm', array(
				'id'                    =>'MedicalMentalForm',
				'enableClientValidation'=>true,
				'clientOptions'         =>array(
					'validateOnSubmit'=>true,),
			));
		?>
		<script>

			function popupValue1(obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9)
			{

				if(obj8!=3) // Coming Add & Edit
				{
					if(obj9!=0)
					{
						// If u click edit background is fade
						$("#fade_form").attr("class", "modal-backdrop fade in");
						$("#accSettings1").show();
						$('#accSettings1').removeClass('modal hide fade').addClass('modal hide fade in');
					}
					$("#MedicalMentalForm_mentalAllignment").val(obj1);
					$("#MedicalMentalForm_mentalMedication").val(obj2);
					$("#MedicalMentalForm_hospitalHistory").val(obj3);
					$("#MedicalMentalForm_mentalFrequency").val(obj4);
					$("#MedicalMentalForm_mentalStatus").val(obj5);
					$("#MedicalMentalForm_mental").val(obj6);
					$("#MedicalMentalForm_medicalMentalCode").val(obj7);
					$("#MedicalMentalForm_action1").val(obj8);

				}
				if(obj8==3)
				{
					// Delete
					var msg="Are you sure you want to delete this record?";
					if(!confirm(msg))
					{
						return false;
					}
					else
					{
						$("#MedicalMentalForm_mentalAllignment").val(obj1);
						$("#MedicalMentalForm_mentalMedication").val(obj2);
						$("#MedicalMentalForm_hospitalHistory").val(obj3);
						$("#MedicalMentalForm_mentalFrequency").val(obj4);
						$("#MedicalMentalForm_mentalStatus").val(obj5);
						$("#MedicalMentalForm_mental").val(obj6);
						$("#MedicalMentalForm_medicalMentalCode").val(obj7);
						$("#MedicalMentalForm_action1").val(obj8);
						$("#MedicalMentalForm").submit();
					}
				}

			}
			function popupHide1()
			{
				$("#errinfo1").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
				$(".errorMessage").html('');   // Yii Error msg is empty after close,
				$('#edPopup1').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
				$("#accSettings1").hide();	 // Popup Hide
			}

			function showdove1(obj,obj1)
			{

				if(obj==1)
				$('#showdiv1').show();

				if(obj==2 && obj1==1)
				{

					var msg="Are you sure you want to save  Any Mental Illness?";
					if(!confirm(msg))
					{
						return false;
					}
					else
					{
						url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/SaveMentalFormValid?&FormValid='+obj;
						$("#test1").load(url);
						$('#showdiv1').hide();


					}
				}

			}
		</script>
		<div class="widget">
			<div class="widget-body">
				<div id="test1" style="display: none;">
				</div>
				<div id="dt_example" class="example_alt_pagination" style="padding:10px; margin:7px;" >

					<div class="widget-body">
						<table style="margin-left:0px;">
							<?php
							if(mssql_num_rows($row2) == 0)
							{
								if($model1->formValid1 <> 2) $model1->formValid1 = '';?>
								<tr>
									<td colspan="1">
										<?php echo $form->labelEx($model1,'formValid1')?>
									</td>
									<td>
										<?php echo $form->dropDownList($model1,'formValid1',array('' =>'Select','1'=>'Yes','2'=>'No'),
											array('onchange'=>'showdove1(this.value,1);'))?>
									</td>
								</tr>
								<?php
							} ?>
						</table>
					</div>
					<div id="showdiv1" style="display: none;">
						<table align="right">
							<tr>

								<td>
									<div>
										<a href="#accSettings1" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue1('','','','','','','',1,0)">
											Add New
										</a>
									</div>
								</td>
							</tr>
						</table>



						<table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
							<thead>
								<th>
									<?php echo $form->labelEx($model1,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?>
								</th>
								<th>
									<?php echo $form->labelEx($model1,'mentalAllignment',array('style'=>'font-weight: bold;font-size:13px;')); ?>
								</th>
								<th>
									<?php echo $form->labelEx($model1,'mentalMedication',array('style'=>'font-weight: bold;font-size:13px;')); ?>
								</th>
								<th>
									<?php echo $form->labelEx($model1,'hospitalHistory',array('style'=>'font-weight: bold;font-size:13px;')); ?>
								</th>
								<th>
									<?php echo $form->labelEx($model1,'mentalFrequency',array('style'=>'font-weight: bold;font-size:13px;')); ?>
								</th>
								<th>
									<?php echo $form->labelEx($model1,'mentalStatus',array('style'=>'font-weight: bold;font-size:13px;')); ?>
								</th>
								<th>
									<?php echo $form->labelEx($model1,'mental',array('style'=>'font-weight: bold;font-size:13px;')); ?>
								</th>
								<th>
									<?php echo $form->labelEx($model1,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?>
								</th>
							</thead>
							<tbody>
								<?php $i = 0;?>
								<?php while($field1 = mssql_fetch_array($row2)){
									$i++;
									?>
									<tr class="gradeX">
										<td>
											<?php echo $i;?>
										</td>
										<td>
											<?php echo $field1['Mental_Alignment'];?>
										</td>
										<td>
											<?php echo $field1['Mental_Medication'];?>
										</td>
										<td>
											<?php echo $field1['Hospital_History'];?>
										</td>
										<td>
											<?php echo $field1['Mental_Frequency'];?>
										</td>
										<td>
											<?php echo $field1['Mental_Status'];?>
										</td>


										<td>
											<?php
											if($field1['Mental'] == 'P') echo "Present";
											else
											if($field1['Mental'] == 'T')
											echo "Treated";?>
										</td>
										<td style="width:20px;" class="hidden-phone">


											<a href="#accSettings1" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue1('<?php echo $field1['Mental_Alignment'] ?>','<?php echo $field1['Mental_Medication'] ?>','<?php echo $field1['Hospital_History'] ?>','<?php echo $field1['Mental_Frequency'];?>','<?php echo $field1['Mental_Status'];?>','<?php echo $field1['Mental'];?>','<?php echo $field1['Medical_Mental_Code'] ?>',2,0)">
												&#x270E
											</a>
											<?php
											if(Yii::app()->session['Freeze'] == "N")
											{
												?>
												<a class="btn btn-success btn-small hidden-phone"  id="delbtn1"   data-original-title="Delete" onclick="popupValue1('<?php echo $field1['Mental_Alignment'] ?>','<?php echo $field1['Mental_Medication'] ?>','<?php echo $field1['Hospital_History'] ?>','<?php echo $field1['Mental_Frequency'] ?>','<?php echo $field1['Mental_Status'] ?>','<?php echo $field1['Mental'];?>','<?php echo $field1['Medical_Mental_Code'] ?>',3,0)">
													&#x2717
												</a>
												<?php
											} ?>
										</td>
									</tr>
									<?php
								} ?>
							</tbody>
						</table>
						<div id="accSettings1" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
							<div id="errinfo1" class="error" align="center" >
								<?php echo $msg2; ?>
							</div>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide1()">
									x
								</button>
								<h4>
									<u>
										<?php echo $form->labelEx($model1,'Mental',array('style'=>'font-weight: bold;font-size:18px;')); ?>
									</u>
								</h4>
							</div>
							<div class="modal-body">
								<div class="row-fluid">
									<div style="float:left; overflow: hidden; width:200px">
										<?php echo $form->hiddenField($model1,'action1'); ?>
										<?php echo $form->hiddenField($model1,'medicalMentalCode'); ?>
										<?php echo $form->labelEx($model1,'mentalAllignment'); ?>
									</div>
									<div>
										<?php echo $form->textField($model1,'mentalAllignment',array('maxlength'=>'250')); ?>
										<?php echo $form->error($model1,'mentalAllignment'); ?>
									</div>

								</div>

								<div class="row-fluid">
									<div style="float:left; overflow: hidden; width:200px">
										<?php echo $form->labelEx($model1,'mentalMedication'); ?>
									</div>
									<div>
										<?php echo $form->textField($model1,'mentalMedication',array('maxlength'=>'250')); ?>
										<?php echo $form->error($model1,'mentalMedication'); ?>
									</div>

								</div>

								<div class="row-fluid">
									<div style="float:left; overflow: hidden; width:200px">
										<?php echo $form->labelEx($model1,'hospitalHistory'); ?>
									</div>
									<div>
										<?php echo $form->textField($model1,'hospitalHistory',array('maxlength'=>'250')); ?>
										<?php echo $form->error($model1,'hospitalHistory'); ?>
									</div>

								</div>

								<div class="row-fluid">
									<div style="float:left; overflow: hidden; width:200px">
										<?php echo $form->labelEx($model1,'mentalFrequency'); ?>
									</div>
									<div>
										<?php echo $form->textField($model1,'mentalFrequency',array('maxlength'=>'250')); ?>
										<?php echo $form->error($model1,'mentalFrequency'); ?>
									</div>

								</div>


								<div class="row-fluid">
									<div style="float:left; overflow: hidden; width:200px">
										<?php echo $form->labelEx($model1,'mentalStatus'); ?>
									</div>
									<div>
										<?php echo $form->textField($model1,'mentalStatus',array('maxlength'=>'250')); ?>
										<?php echo $form->error($model1,'mentalStatus'); ?>
									</div>

								</div>

								<div class="row-fluid">
									<div style="float:left; overflow: hidden; width:200px">
										<?php echo $form->labelEx($model1,'mental'); ?>
									</div>
									<div>
										<?php echo $form->dropDownList($model1,'mental',array('P'=>'Present','T'=>'Treated'),
											array('prompt'=>'Select'),
											array('options' => array('mental'=>array('selected'=>true))));
										echo $form->error($model1,'mental');?>
									</div>
								</div>


							</div>


							<div class="modal-footer">

								<?php echo CHtml::submitButton('Save',array('id'   =>'saveform1','name' =>'m#4a2','class'=>'btn btn-primary')); ?>
								<button type="button" class="btn" aria-hidden="true" data-dismiss="modal"
			  onclick="popupHide1()">
									Close
								</button>

							</div>

						</div>
					</div>

					<?php

					if($model1->action1 <> 3 and (strlen($msg2) > 3 or $model1->errflag == 1))
					{
						echo "<div id='edPopup1' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
						echo "<script>popupValue1('".$model1->mentalAllignment."','".$model1->mentalMedication."','".$model1->hospitalHistory."','".$model1->mentalFrequency."','".$model1->mentalStatus."','".$model1->mental."','".$model1->medicalMentalCode."',".$model1->action1.",1)</script>";
					} ?>
					<div class="clearfix">
					</div>
				</div>
			</div>
		</div>
		<?php
		if($model1->formValid1 > 0)
		echo "<script>showdove1('".$model1->formValid1."',0)</script>"; ?>
		<?php $this->endWidget(); ?>
		<?php /////////////////////////////////////////////////////////////////////////Third Form///////////////////////////////////////////?>
		<?php $form = $this->beginWidget('CActiveForm', array(
				'id'                    =>'MedicalTextAreaForm',
				'enableClientValidation'=>true,
				'clientOptions'         =>array(
					'validateOnSubmit'=>true,
				),
			));  ?>

		<div class="row-fluid">

			<div class="widget">
				<table width="98%" border="0" cellpadding="1" style="padding:5px; margin:7px;">
					<?php echo $form->hiddenField($model2,'action2'); ?>
					<tr>

						<td>
							<?php echo $form->labelEx($model2,'majorSurgeries',array('style'=>'margin-bottom:80px;'));?>
						</td>
						<td>
							<?php echo $form->textArea($model2,'majorSurgeries',array('placeholder'=>'max. Length 500 chars.','style'      =>'width: 450px; height: 103px;','maxlength'  =>'500'));?>
						</td>
					</tr>

					<tr>
						<td>
							<?php echo $form->labelEx($model2,'pmChallenged',array('style'=>'margin-bottom:80px;'));?>
						</td>
						<td>
							<?php echo $form->textArea($model2,'pmChallenged',array('placeholder'=>'max. Length 500 chars.','style'      =>'width: 450px; height: 103px;','maxlength'  =>'500'));?>
						</td>
					</tr>

					<tr>
						<td>
							<?php echo $form->labelEx($model2,'physicalIssue',array('style'=>'margin-bottom:80px;'));?>
						</td>
						<td>
							<?php echo $form->textArea($model2,'physicalIssue',array('placeholder'=>'max. Length 500 chars.','style'      =>'width: 450px; height: 103px;','maxlength'  =>'500'));?>
						</td>
						<td align="right">
							<?php echo CHtml::submitButton('Save',array('id'   =>'saveform2','name' =>'ml9*7','size' =>75,'class'=>'btn btn-primary')) ?>
						</td>
					</tr>


				</table>
			</div>
			<table >
				<tr>

				</tr>
			</table>
		</div>
	</div>
</div>


<?php $this->endWidget(); ?>


