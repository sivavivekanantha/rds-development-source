<?php $form=$this->beginWidget('CActiveForm',array(
						'id'=>'AcadamicForm',
						'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
					));

?>
<script>

$( document ).ready(function() {
    $("#data-table span").html('');
});
	function graduate(obj)
	{
		$('#acadamic_Others').hide();
		$("#degree_Others").hide();
		$('#acadamic_UG').hide();
		$('#acadamic_PG').hide();
		$('#sublbl').hide();			
		if(obj==1 || obj==2 || obj==7)	{
			$('#sublbl').show();			
			$('#acadamic_Others').show();
		}
		if(obj==4){
			$('#sublbl').show();			
			$('#acadamic_UG').show();
		}
		
		if(obj==5){
			$('#sublbl').show();			
			$('#acadamic_PG').show();
		}
			
	}
	function degreeOther(obj)
	{

		$("#degree_Others").hide();
		if(obj==1 || obj==2) $("#degree_Others").show();
	}
	function popupValue(obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9,obj10,obj11,obj12)
	{
		if(obj6==0) obj6=''; 
		if(obj9!=3) // Coming Add & Edit
		{ 
			graduate(obj1);
			if(obj2==1 || obj2==2) degreeOther(obj2);
			if(obj10!=0) {  // If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings").show();			
				$('#accSettings').removeClass('modal hide fade').addClass('modal hide fade in');
			}
		
		$("#AcadamicForm_category").val(obj1);
		//alert(obj1);
		$("#AcadamicForm_categoryOthers").val(obj11);
		$("#AcadamicForm_degreeOthers").val(obj12);
		if(obj1==4)
			$("#AcadamicForm_ugDegree").val(obj2);
		else if(obj1==5)
		$("#AcadamicForm_pgDegree").val(obj2);
		$("#AcadamicForm_institutionName").val(obj3);
		$("#AcadamicForm_institutionCity").val(obj4);
		$("#AcadamicForm_specialization").val(obj5);
		$("#AcadamicForm_yop").val(obj6);
		$("#AcadamicForm_grade").val(obj7);
		$("#AcadamicForm_ivcAcadamicCode").val(obj8);
		$("#AcadamicForm_action").val(obj9);
		
		}
		if(obj9==3) {  // Delete
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
				$("#AcadamicForm_action").val(obj9);
				$("#AcadamicForm_ivcAcadamicCode").val(obj8);
				$("#AcadamicForm_category").val(obj1);
				$("#AcadamicForm_institutionName").val(obj3);
				$("#AcadamicForm").submit();
			}
		}
		
	}
	function popupHide()
	{		$("#errinfo").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');   // Yii Error msg is empty after close, 
			$("#AcadamicForm_ugDegree").val('');
			$("#AcadamicForm_pgDegree").val('');
			$('#edPopup').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
			$("#accSettings").hide();	 // Popup Hide
	}
</script>
     <div>
     <div class="widget">
     <div id="formTitle">		 
	 <div class="widget-header">
     <div class="title" style="width: 300px">
	 <h4><u><?php echo $form->labelEx($model,'academic',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
	 </div>
     </div>
	 </div>
	 <div class="widget-body">	
	    <div align="left" class="tab-widget" style="padding:0px";>
         <ul class="signups"> 
           <li><div  class="info"><h6>Hint: Start from lowest qualification till your highest degree</h6></div></li></ul></div>     
</div>
	     <div class="widget-body">	
		 <div id="dt_example" class="example_alt_pagination">
	
                        <table align="right"><tr>
				  <td><div><a href="#accSettings" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue('','','','','','','','',1,0,'')">Add New</a></div></td></tr></table>
				
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">  
							<thead>
						    <th><?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model,'category',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model,'pgDegree',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model,'institutionName',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model,'institutionCity',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model,'specialization',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model,'yop',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model,'grade',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                        </thead>
                        <tbody>
						<?php $i=0;?>
                          <?php while($field=mssql_fetch_array($acadamicInfo))
						  
						  { $i++;
						  ?>
						<tr class="gradeX">
							<td align="center"><?php echo $i; ?></td>
                            <td><?php if($field['Category']==1) echo "SSLC/10th Grade"; elseif($field['Category']==2) echo "HSC/12th Grade";elseif($field['Category']==3) echo "Diploma";elseif($field['Category']==4) echo "Under Graduate";elseif($field['Category']==5) echo "Post Graduate";elseif($field['Category']==6) echo "PH.D";
	   elseif($field['Category']==7) echo "Others";	?></td>
                            <td><?php if($field['Category_Others']<>"") echo $field['Category_Others']; elseif($field['Degree_Others']<>"")  echo $field['Degree_Others']; else echo $field['Degree']; ?></td>
							
                            <td><?php echo $field['Institute_Name'];?></td>
							<td><?php echo $field['Institute_City'];?></td>
							<td><?php echo $field['Specialisation'];?></td>
							<?php if($field['YOP']==0) $field['YOP']=''; ?>
							<td><?php echo $field['YOP'];?></td>
							<td><?php echo $field['Grade'];?></td>
                            <td>
							<a href="#accSettings" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue('<?php echo $field['Category'] ?>','<?php echo $field['Degree_Name'] ?>','<?php echo $field['Institute_Name'];?>','<?php echo $field['Institute_City'];?>','<?php echo $field['Specialisation'];?>','<?php echo $field['YOP'];?>','<?php echo $field['Grade'];?>','<?php echo $field['IVC_Acadamic_Code'] ?>',2,0,'<?php echo $field['Category_Others'] ?>','<?php echo $field['Degree_Others'] ?>')">&#x270E</a>
							<?php if(Yii::app()->session['Freeze']=="N") { ?>
                            <a class="btn btn-success btn-small hidden-phone"  id="delbtn"   data-original-title="Delete" onclick="popupValue('<?php echo $field['Category'] ?>','<?php echo $field['Degree_Name'] ?>','<?php echo $field['Institute_Name'];?>','<?php echo $field['Institute_City'];?>','<?php echo $field['Specialisation'];?>','<?php echo $field['YOP'];?>','<?php echo $field['Grade'];?>','<?php echo $field['IVC_Acadamic_Code'] ?>',3,0,'<?php echo $field['Category_Others'] ?>','<?php echo $field['Degree_Others'] ?>')">&#x2717</a>
							<?php } ?>
                            </td>
                        </tr>
						<?php } ?>
						</tbody>
						</table>

		<div id="accSettings" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 		<div id="errinfo" class="error" align="center" > <?php echo $msg1; ?> </div>
			<div class="modal-header">
         		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide()"> x </button>
         		<h4><u><?php echo $form->labelEx($model,'academic',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
    		</div> 
   <div class="modal-body">
   		<div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
    	         <?php 	echo $form->hiddenField($model,'ivcAcadamicCode'); 
				 		echo $form->hiddenField($model,'action'); 
						echo $form->labelEx($model,'category'); ?>
             </div>
			 <div>
			 	<?php echo $form->dropDownList($model,'category',array(''=>'Select','1'=>'SSLC/10th Grade','2'=>'HSC/12th Grade','3'=>'Diploma','4'=>'Under graduate','5'=>'Post Graduate','6'=>'PH.D','7'=>'Others'),
				array('options' => array('category'=>array('selected'=>true)),'onChange'=>'graduate(this.value)') );
			 	echo $form->error($model,'category');			 		 ?>
			</div>
		</div> 
		<div class="row-fluid">  
        	        <div style="float:left; overflow: hidden; width:200px"> 
			 	<?php echo $form->labelEx($model,'ugDegree',array('id'=>'sublbl','style'=>'display:none;')); ?>
             </div>
                    <div>
			 		<div id='acadamic_UG' style="display:none;">
					 <?php echo $form->dropDownList($model,'ugDegree',CHtml::listData($ugDegreeLst,'Deg_No','Degree_Name'),array('prompt'=>'Select','onChange'=>'degreeOther(this.value)'));	echo $form->error($model,'ugDegree');			 		 ?>
					</div>
                    <div id='acadamic_PG' style="display:none;">
					 <?php echo $form->dropDownList($model,'pgDegree',CHtml::listData($pgDegreeLst,'Deg_No','Degree_Name'),array('prompt'=>'Select','onChange'=>'degreeOther(this.value)'));	echo $form->error($model,'pgDegree');			 		 ?>
					</div>
                    <div id='acadamic_Others'  style="display:none;">
					 <?php echo $form->textField($model,'categoryOthers' ,array('onkeydown'=>"return alphaonly('AcadamicForm_categoryOthers')",'maxlength'=>'50'));	echo $form->error($model,'categoryOthers');			 		 ?>
					</div>
			 		<div id='degree_Others'  style="display:none;">
					 <?php echo $form->textField($model,'degreeOthers',array('onkeydown'=>"return alphaonly('AcadamicForm_degreeOthers')",'maxlength'=>'50'));	echo $form->error($model,'degreeOthers');			 		 ?>
					</div>
					</div>
			 </div>
		
		<div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
			 	<?php echo $form->labelEx($model,'institutionName'); ?>
             </div><div>
			 <?php echo $form->textField($model,'institutionName',array('onkeydown'=>"return alphaonly('AcadamicForm_institutionName')",'maxlength'=>'50'));
			 echo $form->error($model,'institutionName');			 ?>
			 </div>
		</div> 
		<div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
			 <?php echo $form->labelEx($model,'institutionCity'); ?>
             </div><div>
			 <?php echo $form->textField($model,'institutionCity',array('onkeydown'=>"return alphaonly('AcadamicForm_institutionCity')",'maxlength'=>'50'));
			 echo $form->error($model,'institutionCity');			 ?>
			 </div>
		</div> 
		<div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
			 <?php echo $form->labelEx($model,'Specialization'); ?>
             </div><div>
			 <?php echo $form->textField($model,'specialization',array('onkeydown'=>"return alphaonly('AcadamicForm_specialization')",'maxlength'=>'50'));
			 echo $form->error($model,'Specialization');			 ?>
			 </div>
		</div> 
		<div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
			 <?php echo $form->labelEx($model,'Year of Passing'); ?>
             </div><div>
			 <?php echo $form->textField($model,'yop',array('onkeydown'=>"return numberonly('AcadamicForm_yop')",'maxlength'=>'4'));
			 echo $form->error($model,'Year of Passing');			 ?>
			 </div>
		</div> 
		<div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
			 <?php echo $form->labelEx($model,'grade'); ?>
             </div><div>
			 <?php echo $form->textField($model,'grade',array('onkeydown'=>"return alphanumeric('AcadamicForm_grade')",'maxlength'=>'4'));
			 echo $form->error($model,'grade');			 ?>
             </div>
        </div> 
   </div>
                        
    
    <div class="modal-footer">
          <?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'ai40*','class'=>'btn btn-primary')); ?> 
          <button type="button" class="btn" aria-hidden="true" data-dismiss="modal" onclick="popupHide()"> Close </button>
   </div>
                    
</div>
<?php if($model->category>0 and ($model->action<>3 or $model->errflag==1))
{
	 echo "<div id='edPopup' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition				
	 $degree=0;
	
	 if($model->category==4) $degree = $model->ugDegree; 
	 elseif($model->category==5) $degree = $model->pgDegree;
	 
	echo "<script>popupValue('".$model->category."','".$degree."','".$model->institutionName."','".$model->institutionCity."','".$model->specialization."','".$model->yop."','".$model->grade."','".$model->ivcAcadamicCode."','".$model->action."',1,'".$model->categoryOthers."','".$model->degreeOthers."')</script>"; 
} ?>                    
 		<div class="clearfix">	</div>
		
		
 </div>    
 </div>
 </div>
 
 
 
 
 
 
 
 
 
  <?php $this->endWidget(); ?>
