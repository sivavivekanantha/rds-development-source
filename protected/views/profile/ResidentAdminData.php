	<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ResidentAdminForm',
	'enableClientValidation'=>true,
	'htmlOptions'=>array('class'=>'signin-wrapper'),
	'enableClientValidation'=>true,
	'clientOptions'=>array(
	'validateOnSubmit'=>true,
	),
	)); 	
	?>
<script type="text/javascript">
function Amenties(obj1)              
			{	
						
			if(obj1==3 || obj1==4)	{
			     $("#amentiesCexpiry").attr('disabled', false);
				 }			
			  else
			  {
			  $("#amentiesCexpiry").attr('disabled', true);
			    $('#amentiesCexpiry').val('');
			  }
		
			}			

function Medical(obj)              
			{	
			if(obj==2)	{
			     $("#mCardExpiry").attr('disabled', false);
				 }			
			  else
			  {
			  $("#mCardExpiry").attr('disabled', true);
			    $('#mCardExpiry').val('');
			  }
		
			}			
			
			
</script>
<div class="widget">
<div class="widget-header">
 <div class="title">
  <div id="formTitle">
     <h4><u><?php echo $form->labelEx($model,'resident',array('style'=>'font-weight: bold;font-size:18px;')); ?>
    	</u></h4>
     </div></div></div>
	<div class="content">
          <div class="row-fluid">
	  
	<table width="100%" border="0" cellpadding="1" style="padding:10px; margin:7px;">
	<br>
	 <?php echo $msg; ?>
<tr>
	<td><table width="98%" border="0" cellpadding="1">
	<br>
	<tr><td colspan="3"><table><tr>
	<td>
	
	<?php echo $form->labelEx($model,'doj');?>
	</td>
	<td>       
	<?php echo $form->textField($model,'doj',array('id'=>'doj','style'=>'width:150px'));?>
    <?php echo $form->error($model,'doj');?>        
	
	</td>
	<td width="100px"></td>
	<td>
	
	<?php echo $form->labelEx($model,'fTVCdate');?>
	</td><td></td>
	<td>       
	<?php echo $form->textField($model,'fTVCdate',array('id'=>'fTVCdate','style'=>'width:150px'));?>
    <?php echo $form->error($model,'fTVCdate');?>        
	
	</td>	
	 </tr>
	<tr>
	<td>	
	<?php echo $form->labelEx($model,'uId');?>
	</td>
	<td>
	<div class="input-small">
		<?php echo $form->textField($model,'uId',array('style'=>'width:150px','onkeydown'=>"return alphanumeric('ResidentAdminForm_uId')",'maxlength'=>'7')); 
			  echo $form->error($model,'uId');?>
		
	</div>	
	</td>
	<td width="100px"></td>
	<td>
	<?php echo $form->labelEx($model,'alertFlag');?>
	</td><td></td>
	<td>
	<?php echo $form->dropDownList($model,'alertFlag',CHtml::listData($Alert_Flag,'Alert_Flag_Code','Alert_Flag'),array('prompt'=>'Select','style'=>'width:150px'));
	echo $form->error($model,'alertFlag');?> 
	</td>	
	</tr>	
	<tr>
	<td>
	<?php echo $form->labelEx($model,'activeStatus');?>
	</td>
	<td>
	<?php echo $form->dropDownList($model,'activeStatus',CHtml::listData($Status,'Active_Status_Code','Active_Status'),array('prompt'=>'Select','style'=>'width:150px'));
	echo $form->error($model,'activeStatus');?> 
	</td>	
	<td width="100px"></td>
	<td>
	<?php echo $form->labelEx($model,'alertType');?>
	</td><td></td>
	<td>
	<?php echo $form->dropDownList($model,'alertType',CHtml::listData($Alert_Type,'Security_Alert_Code','Security_Alert_Type'),array('prompt'=>'Select','style'=>'width:150px'));
	echo $form->error($model,'alertType');?> 
	</td>	
	</tr>
	<tr>
	<td>
	<?php echo $form->labelEx($model,'category');?>
	</td>
	<td>
	<?php echo $form->dropDownList($model,'category',CHtml::listData($Res_Category,'Category_Code','Category'),array('prompt'=>'Select','style'=>'width:150px'));
	echo $form->error($model,'category');?> 
	</td>
	<td width="100px"></td>
	<td>	
	<?php echo $form->labelEx($model,'honorariumAmount');?>
	</td><td></td>
	<td>
	<div class="input-small">
		<?php echo $form->textField($model,'honorariumAmount',array('style'=>'width:150px','onkeydown'=>"return numberonly('ResidentAdminForm_honorariumAmount')")); 
			  echo $form->error($model,'honorariumAmount');?>
		
	</div>	
	</td>
	</tr>
	<tr>
	<td><?php echo $form->labelEx($model,'idCard',array('style'=>'font-weight: bold; : font-weight: normal;'));?></td>
	<td></td><td></td>
	<td><?php echo $form->labelEx($model,'amentiesCard',array('style'=>'font-weight: bold; : font-weight: normal;'));?></td>	
	</tr>
	<tr>
	<td>
	<?php echo $form->labelEx($model,'cardType');?>
	</td>
	<td>
	<?php echo $form->dropDownList($model,'cardType',CHtml::listData($Res_CardType,'Category_Code','Category'),array('prompt'=>'Select','style'=>'width:150px'));
	echo $form->error($model,'cardType');?> 
	</td>
	<td></td>
	<td>
	<?php echo $form->labelEx($model,'amentiesCtype');?>
	</td><td width="100px"></td>
	<td>
	<?php echo $form->dropDownList($model,'amentiesCtype',CHtml::listData($Amenties_Type,'Amenties_Card_Code','Amenties_Card'),array('prompt'=>'Select','style'=>'width:150px','onchange'=>'return Amenties(this.value);'));
	echo $form->error($model,'amentiesCtype');?> 
	</td>	
	
	<td width="100px"></td>
	
	</tr>
	<tr>
	<td>	
	<?php echo $form->labelEx($model,'cardSerialNo');?>
	</td>
	<td>
	<div class="input-small">
		<?php echo $form->textField($model,'cardSerialNo',array('style'=>'width:150px','onkeydown'=>"return alphanumeric('ResidentAdminForm_cardSerialNo')",'maxlength'=>'7')); 
			  echo $form->error($model,'cardSerialNo');?>
		
	</div>	
	</td>
	<td width="100px"></td>
	<td>
	<?php echo $form->labelEx($model,'amentiesCexpiry');?>
	</td><td></td>
	<td>       
	<?php echo $form->textField($model,'amentiesCexpiry',array('id'=>'amentiesCexpiry','style'=>'width:150px'));?>
	<?php echo $form->error($model,'amentiesCexpiry');?>        
	
	</td>
	
	</tr>
	<tr>
	<td>
	<?php echo $form->labelEx($model,'cardIssueDate');?>
	</td>
	<td>       
	<?php echo $form->textField($model,'cardIssueDate',array('id'=>'cardIssueDate','style'=>'width:150px'));?>
    <?php echo $form->error($model,'cardIssueDate');?> 
	</td><td></td>
	<td>
	<?php echo $form->labelEx($model,'medicalCard',array('style'=>'font-weight: bold; : font-weight: normal;'));?>
	</td>
	
	</tr> 
	
	<tr>
	<td>
	<?php echo $form->labelEx($model,'cardExpiryDate');?>
	</td>
	<td>       
	<?php echo $form->textField($model,'cardExpiryDate',array('id'=>'cardExpiryDate','style'=>'width:150px'));?>
	<?php echo $form->error($model,'cardExpiryDate');?></td><td></td>
	<td>
	<?php echo $form->labelEx($model,'mCardIssued');?> 
	</td><td></td>
	<td>
	<?php echo $form->dropDownList($model,'mCardIssued',array('1'=>'No','2'=>'Yes'),array('style'=>'width:150px','id'=>'mCardIssued',
	'onchange'=>'return Medical(this.value);'));
	echo $form->error($model,'mCardIssued');?> 
	</td>
	
	</tr> 
	<tr>
	<td>
	<?php echo $form->labelEx($model,'cardStatus');?>
	</td>
	<td>
	<?php echo $form->dropDownList($model,'cardStatus',CHtml::listData($Res_Card_Status,'Card_Status_Code','Card_Status'),array('prompt'=>'Select','style'=>'width:150px'));
	echo $form->error($model,'cardStatus');?> 
	</td><td></td>
	<td>
	<?php echo $form->labelEx($model,'mCardExpiry');?>
	</td><td></td>
	<td>       
	<?php echo $form->textField($model,'mCardExpiry',array('id'=>'mCardExpiry','style'=>'width:150px'));?>
	<?php echo $form->error($model,'mCardExpiry');?> 
	</td>
	</tr>
	
	</tr></table></td></tr>	
	<tr>
		<td>
			<table align="right">
	<tr> 	
	<td  ><?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'r#*d6','size'=>75,'class'=>'btn btn-primary')) ?></td>
	
	</tr></table>
			
		</td>
		
	</tr>
	
	
	
	</table>
	
	
	</div>
	</div>
	
	
	
	
	</div>
	
	
	
 		<script>Medical(<?php echo $model->mCardIssued;?>)</script>
		
		
 		<script>Amenties(<?php echo $model->amentiesCtype;?>)</script>
		
	<?php $this->endWidget(); ?>
