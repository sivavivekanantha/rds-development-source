<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'YatraForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
<script>

$( document ).ready(function() {
    $("#data-table span").html('');
});

	function popupValue(obj1,obj2,obj3,obj4,obj5,obj6)
	{	
		
		if(obj5!=3) // Coming Add & Edit
		{
			if(obj6!=0) {  // If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings").show();			
				$('#accSettings').removeClass('modal hide fade').addClass('modal hide fade in');
			}
		$("#YatraForm_yatraName").val(obj1);
		$("#YatraForm_noOfTimesAttend").val(obj2);
		$("#YatraForm_yearLastAttend").val(obj3);
		$("#YatraForm_ivcYatraCode").val(obj4);
		$("#YatraForm_action").val(obj5);
		
		}
		if(obj5==3) {  // Delete
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
				$("#YatraForm_action").val(obj5);
				$("#YatraForm_yatraName").val(obj1);
				$("#YatraForm_noOfTimesAttend").val(obj2);
				$("#YatraForm_yearLastAttend").val(obj3);
				$("#YatraForm_ivcYatraCode").val(obj4);
				$("#YatraForm").submit();
			}
		}
		
	}
function popupHide()
	{		$("#errinfo").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');   // Yii Error msg is empty after close, 
			$('#edPopup').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
			$("#accSettings").hide();	 // Popup Hide
	}
	function showdove(obj,obj1)
{
	
	if(obj==1)	
			$('#showdiv').show();	
	
	if(obj==2 && obj1==1){
		var msg="Are you sure you want to save this Yatra?";
			if(!confirm(msg)){
			return false;
			}
		else {					
		url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/SaveYatraFormValid?&FormValid='+obj;	
		$("#test").load(url);	
		$('#showdiv').hide();
	
	
		}
	}
	
}
</script>
	 <div class="widget">
	 <div id="test"> </div>
      		
		<div id="formTitle">
		<div class="widget-header">
     	<div class="title">
		<h4><u><?php echo $form->labelEx($model,'yatra',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
		</div>
		</div>
	    </div>
		
		<div class="widget-body">					
				 <table>
				  <?php if(mssql_num_rows($row1)==0)  {
						if($model->formValid<>2) $model->formValid=''; ?>
				<tr><td><?php echo $form->labelEx($model,'formValid')?></td>
				<td><?php echo $form->dropDownList($model,'formValid',array(''=>'Select','1'=>'Yes','2'=>'No'),
				array('onchange'=>'showdove(this.value,1);'))?></td>
				</tr>
<?php } ?>		</table></div>	

                    <div class="widget-body">					
                 <div id="showdiv" style="display:none;"> 
				  <div id="dt_example" class="example_alt_pagination" style="padding:10px; margin:7px;" >
				 <table align="right"><tr>			
				  <td><div><a href="#accSettings" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue('','','','',1,0)">Add New</a></div></td></tr></table>
				
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">                        
                        <thead><th><?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                            <th><?php echo $form->labelEx($model,'yatraName',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                            <th><?php echo $form->labelEx($model,'noOfTimesAttend',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                           <th><?php echo $form->labelEx($model,'yearLastAttend',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                           <th><?php echo $form->labelEx($model,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                        </thead>
                        <tbody>
                          <?php $i=0;?>
                          <?php while($field=mssql_fetch_array($row1))
						  { $i++;
						  ?>                          	
                      		<tr class="gradeX">
							<td align="center"><?php echo $i; ?></td>
                            <td ><?php echo $field['Yatra_Name'];?></td>
                            <td ><?php echo $field['Yatra_Count'];?></td>
                            <td  class='hidden-phone'><?php echo $field['Yatra_Year'];?></td>
                            <td style="width:20px;" class="hidden-phone">
							<a href="#accSettings" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue('<?php echo $field['Yatra_Code'] ?>','<?php echo $field['Yatra_Count'] ?>','<?php echo $field['Yatra_Year'];?>','<?php echo $field['IVC_Yatra_Code'] ?>',2,0)">&#x270E</a>
							<?php if(Yii::app()->session['Freeze']=="N") { ?>
                            <a class="btn btn-success btn-small hidden-phone"  id="delbtn"   data-original-title="Delete" onclick="popupValue('<?php echo $field['Yatra_Code'] ?>','<?php echo $field['Yatra_Count'] ?>','<?php echo $field['Yatra_Year'];?>','<?php echo $field['IVC_Yatra_Code'] ?>',3,0)">&#x2717</a>
							<?php } ?>
                            </td>
                        </tr>
					<?php } ?>
                	</tbody>
                    </table>
			<div id="accSettings" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 		<div id="errinfo" align="center" > <?php echo $msg1; ?> </div>
		<div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide()"> x </button>
         	<h4><u><?php echo $form->labelEx($model,'yatraPopup',array('style'=>'font-weight: bold;font-size:18px;')); ?> 	 </u></h4>
    	</div> 
   <div class="modal-body">
   		<div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
             <?php echo $form->hiddenField($model,'ivcYatraCode'); ?>
			   <?php echo $form->hiddenField($model,'action'); ?>
			 <?php echo $form->labelEx($model,'yatraName'); ?>
             </div><div>
			 <?php echo $form->dropDownList($model,'yatraName',CHtml::listData($yatraName,'Yatra_Code','Yatra_Name'),
			 array('prompt'=>'Select'));
			 echo $form->error($model,'yatraName');
			 ?>
			 
             </div>
        </div> 
        <div class="row-fluid">  
         	<div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'noOfTimesAttend'); ?>
            </div><div> 
			<?php echo $form->dropDownList($model,'noOfTimesAttend',array(''=>'Select','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6'),
			array('options' => array('noOfTimesAttend'=>array('selected'=>true))));	
			 echo $form->error($model,'noOfTimesAttend');?>
             </div> 
			      
        </div> 
	                                 
        <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'yearLastAttend'); ?>
                </div><div> 
				<?php echo $form->dropDownList($model,'yearLastAttend',$year,
				array('prompt'=>'Select'),
				array('options' => array('yearLastAttend'=>array('selected'=>true))));
				 echo $form->error($model,'yearLastAttend');?>
                 </div>
         </div>
   </div>
                        
    
    <div class="modal-footer">
            <?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'y&@ta','class'=>'btn btn-primary')); ?> 
             <button type="button" class="btn" aria-hidden="true" data-dismiss="modal" onclick="popupHide()"> Close </button>
        	
   </div>
                    
</div>


	        <?php if($model->action<>3 and (strlen($msg1)>3 or $model->errflag==1)){
				 echo "<div id='edPopup' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
				 echo "<script>popupValue('".$model->yatraName."','".$model->noOfTimesAttend."','".$model->yearLastAttend."','".$model->ivcYatraCode."','".$model->action."',1)</script>"; } ?>                    
 		<div class="clearfix">	</div>
     </div>  </div> </div></div>
	 

	 	<?php if($model->formValid>0)	
	 echo "<script>showdove('".$model->formValid."',0)</script>"; ?>
 <?php $this->endWidget(); ?>