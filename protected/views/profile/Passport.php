	<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'passportForm',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); 
 ?>
 <script type="text/javascript">
 
function dualcitizenship1(obj)
{

if(obj==2)
	{
	$("#PassportForm_dualDetails").removeAttr("disabled"); 
	}			
	else
	{
	$("#PassportForm_dualDetails").attr("disabled","disabled");
	$('#PassportForm_dualDetails').val('');
	}
}
function visa_category1(obj)              
{	

if(obj==1)	{
	$("#PassportForm_visaType").removeAttr("disabled"); 
	$("#PassportForm_documentNumber").removeAttr("disabled"); 
	$("#visaIssueDate").removeAttr("disabled"); 
	$("#visaExpiryDate").removeAttr("disabled"); 
}		
else if(obj==0)	
{
	$("#PassportForm_visaType").attr("disabled","disabled");
	$("#PassportForm_documentNumber").attr("disabled","disabled");
	$("#visaIssueDate").attr("disabled","disabled");
	$("#visaExpiryDate").attr("disabled","disabled");
	$('#PassportForm_visaType').val('');
	$('#PassportForm_documentNumber').val('');
		$('#visaIssueDate').val('');
		$('#visaExpiryDate').val('');
	
}
else
{
	$("#PassportForm_visaType").attr("disabled","disabled");
	$("#PassportForm_documentNumber").removeAttr("disabled"); 
	$("#visaIssueDate").removeAttr("disabled"); 
	$("#visaExpiryDate").removeAttr("disabled"); 
	$('#PassportForm_visaType').val('');
}
}
function showdove(obj,obj1)
{	
	if(obj==1)	{	
			$('#showdiv').show();
			$('#saveBtn').show();	
			}
	if(obj==2 && obj1==1){
		var msg="Are you sure you want to save this Passport?";
			if(!confirm(msg)){
				$('#showdiv').hide();
				$("#PassportForm_passportNo").val('');
				$("#passportExpiryDate").val('');
				$("#PassportForm_placeOfIssue").val('');
				$("#PassportForm_nameInPassport").val('');
				$("#PassportForm_dualCitizenship").val('');
				$("#PassportForm_dualDetails").val('');
				$("#PassportForm_visaCategory").val('');
				$("#PassportForm_visaType").val('');
				$("#PassportForm_documentNumber").val('');
				$("#visaIssueDate").val('');
				$("#visaExpiryDate").val('');
				$(".error").html('');
				$('#saveBtn').hide();
					
							
			}
			else {					
				url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/SavePassportFormValid?&FormValid='+obj;	
	
				$("#test").load(url);	
				$("#PassportForm_passportNo").val('');
				$("#passportExpiryDate").val('');
				$("#PassportForm_placeOfIssue").val('');
				$("#PassportForm_nameInPassport").val('');
				$("#PassportForm_dualCitizenship").val('');
				$("#PassportForm_dualDetails").val('');
				$("#PassportForm_visaCategory").val('');
				$("#PassportForm_visaType").val('');
				$("#PassportForm_documentNumber").val('');
				$("#visaIssueDate").val('');
				$("#visaExpiryDate").val('');
				$(".error").html('');	
				$('#showdiv').hide();
				$('#saveBtn').hide();	
				
			}
		}
	
}
 </script>
 <div id="test"></div>
      


<div class="widget">
<table width="100%" >
<tr><td> <div>
<div class="widget-header">
<div class="title" style="width: 300px">
<h4><u><?php echo $form->labelEx($model,'passportDetails',array('style'=>'font-weight: bold;font-size:18px;')); ?> 	
</u></h4>
</div>
</div> 
</div></td></tr>
<tr><td colspan="2"><table align="center"><?php echo $msg;?></table></td></tr>
<table style="margin-left:0px;">
<?php  if(strlen($model->passportNo)==0 or $model->formValid<>1 ){  ?>
<tr><td colspan="1"><?php echo $form->labelEx($model,'formValid')?></td>
<td><?php echo $form->dropDownList($model,'formValid',array(''=>'Select','1'=>'Yes','2'=>'No'),
array('onchange'=>'showdove(this.value,1);'))?>
</td>
</tr>
<?php } ?>	
</table>

  <tr>
    <td colspan="2">
	<table width="100%" border="0" cellpadding="1" style="padding:10px; margin:7px; display: none;" id="showdiv">
  <tr>
  	  <td>
	  	<?php echo $form->labelEx($model,'nationality');?>      </td>	
     <td>
     <div class="">
		<?php echo $form->textField($model,'nationality' ,array('disabled'=>'disabled'));?> 
        <?php // echo $form->error($model,'nationality'); ?>     </div>  	</td>   
    <td>	
		<?php echo $form->labelEx($model,'passportNo');?>    </td>
	<td>
    	<div class="">
			<?php echo $form->textField($model,'passportNo',array('onkeydown'=>"return alphanumeric1only('PassportForm_passportNo')",'maxlength'=>'20')); ?> 
        	<?php echo $form->error($model,'passportNo'); ?>
		</div>
	</td>
  </tr>
  <tr>
    <td>
    		<?php echo $form->labelEx($model,'passportExpiryDate');?>    </td>
    <td>
    	<div class="">             
          
			<?php echo $form->textField($model,'passportExpiryDate',array('id'=>'passportExpiryDate')); ?>
               
        	<?php echo $form->error($model,'passportExpiryDate'); ?>        </div>    </td>
    <td>
    		<?php echo $form->labelEx($model,'placeOfIssue');?>    </td>
    <td>
    	<div class="">
        	<?php echo $form->textField($model,'placeOfIssue',array('onkeydown'=>"return alphaonly	('PassportForm_placeOfIssue')",'maxlength'=>'50'));
				  echo $form->error    ($model,'placeOfIssue');
			 ?>        </div>    </td>
  </tr>
  <tr>
    		<td>
    			<?php echo $form->labelEx($model,'nameInPassport');?>            </td>
    <td colspan="3">
    <div class="input-block-level">
	<?php 
	         echo $form->textField($model,'nameInPassport',array('style'=>'width:580px','onkeydown'=>"return alphaonly('PassportForm_nameInPassport')",'maxlength'=>'50'));
			 echo $form->error    ($model,'nameInPassport');
			 ?>
             </div>     </td>
    </tr>
  <tr>
   		 <td>
    	<?php echo $form->labelEx($model,'dualCitizenship');?>        </td>
    <td><div class="">
        	<?php echo $form->dropDownList($model,'dualCitizenship',array('1'=>'No','2'=>'Yes'),array('onChange'=>'dualcitizenship1(this.value);'));
				  echo $form->error    ($model,'dualCitizenship');
			 ?>
        	
        </div></td>
    <td>
    		<?php echo $form->labelEx($model,'dualDetails');?>    </td>
    <td>
    	<div class="">
        	<?php echo $form->textArea($model,'dualDetails',array('disabled'=>'disabled','maxlength'=>'500','placeholder'=>'max. Length 500 chars.'));
				  echo $form->error    ($model,'dualDetails');	 ?>        </div>    </td>
  </tr>
 

  <tr>
    <td colspan="4" class="more_text" style="text-decoration:underline; padding-left:" valign="top " ><h4><u><?php echo $form->labelEx($model,'visaDetails',array('style'=>'font-weight: bold;font-size:15px;')); ?></u></h4>
	 </td>
    </tr>
  <tr>
    <td>
		<?php echo $form->labelEx($model,'visaCategory');?> </td>
    <td>
    	<?php echo $form->dropDownList($model,'visaCategory',array('0'=>'Select','1'=>'VISA','2'=>'PIO','3'=>'OCI'),array('style'=>'width:215px','onchange'=>'visa_category1(this.value);'));
		      echo $form->error($model,'visaCategory');?> 
            </td>
    <td>
    	<?php echo $form->labelEx($model,'visaType');?> 
    </td>
    <td>
    	<?php 
			  echo $form->dropDownList($model,'visaType',array(''=>'Select','TO'=>'Tourist Visa','X'=>'X(Entry Visa)','E'=>'Employment Visa','S'=>'Student Visa','B'=>'Business Visa','C'=>'Conference Visa','J'=>'Journalist Visa','R'=>'Research  Visa','M'=>'Medical  Visa','T'=>'Transit Visa'),array('style'=>'width:215px','disabled'=>'disabled'));
		      echo $form->error($model,'visaType');?> 
    </td>
  </tr>
  <tr>
    <td><?php echo $form->labelEx($model,'documentNumber'); ?> </td>
    <td>
    	<div class="">
        	<?php echo $form->textField($model,'documentNumber',array('disabled'=>'disabled','onkeydown'=>"return alphanumeric1only('PassportForm_documentNumber')",'maxlength'=>'50'));
				  echo $form->error    ($model,'documentNumber');
			 ?>
        	
        </div>
    
    </td>
    
   		 <td>
   		 <?php echo $form->labelEx($model,'visaIssueDate');	?> 
         </td>
    	<td>
        	<div class="">
       		          
					<?php echo $form->textField($model,'visaIssueDate',array('id'=>'visaIssueDate','disabled'=>'disabled')); ?>
                	
               
        		<?php echo $form->error($model,'visaIssueDate'); ?>        
            </div> 
    	</td>
  </tr>
  <tr> 
 	 <td>
    	<?php echo $form->labelEx($model,'visaExpiryDate');?> 
    </td>
    <td>
    	<div class="">
       		        
					<?php echo $form->textField($model,'visaExpiryDate',array('id'=>'visaExpiryDate','disabled'=>'disabled')); ?>
        		<?php echo $form->error($model,'visaExpiryDate'); ?>        
            </div> 
    </td>
   <td></td>
   <td></td>
    </tr>
	<tr><td colspan="2">
	 <div class="tab-widget">
  <ul class="signups"> 
  <li><div class="info"><h6>Hint: If the VISA got extended,please provide the extended date.</h6></div></li></ul></div>
</td></tr>
   
</table>
</td>
  </tr>
 </table>
  <table align="right" id="saveBtn" style="display: none;"><tr>
    	<td><?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'op21%','size'=>75,'class'=>'btn btn-primary')) ?></td>
    </tr></table>	
</div>


    
		<?php if($model->dualCitizenship==2) {?>
 		<script>dualcitizenship1(<?php echo $model->dualCitizenship;?>)</script>
		<?php } ?>
		<?php if($model->visaCategory>0) {?>
		<script>visa_category1(<?php echo $model->visaCategory;?>)</script>
		
	<?php }  
	
	if($model->formValid>0)	
	 echo "<script>showdove('".$model->formValid."')</script>";
	 if(strlen($msg)>0)
	  echo "<script>showdove('1')</script>";
	?>
	
<?php $this->endWidget(); ?>
