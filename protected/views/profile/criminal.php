	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'CriminalForm',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); 
 ?>
<script type="text/javascript">
	function criminal1(obj)
	{

		$("#Convict").hide();
		if(obj=='Y') 
			$("#Convict").show();
		if(obj=='N') 
			$("#CriminalForm_criminalConvictedDetails").val('');	
	}
	function criminal2(obj)
	{
		$("#Proceeding").hide();
		if(obj=='Y') 
			$("#Proceeding").show();
		if(obj=='N') 	
			$("#CriminalForm_criminalProceedingDetails").val('');
	}
	function formfilled(obj)   
{	

    var st='N';
	if($("#Form_Filled").is(':checked'))
      var st='Y';	
	   		url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/SaveFormCompletion?&Form_Filled='+st;
		
	   if(st=='Y')  alert('Thanks for Form Completion.');
	    $("#show_fill").load(url);
}	
</script>
 
<div class="widget"> 
<div class="widget-header">
<div class="title">
      <div class="pull-left">
      <h4><u><?php echo $form->labelEx($model,'criminal',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
     </div></div></div>

  <div class="row-fluid">
  
	<table width="100%" border="0" cellpadding="1">
<?php echo $msg;?>
  <tr>
    <td>
	<table align="center">
	<tr>
	<td><?php echo $form->labelEx($model,'isCriminalConvicted')?></td>
	<td>
		<?php echo $form->dropDownList($model,'isCriminalConvicted',array(''=>'Select','Y'=>'Yes','N'=>'No'),array('onchange'=>'criminal1(this.value);'))?>	
	</td></tr>
	<tr>
	<td colspan="2" style="display: none;" id="Convict"><div>Please provide details <br>
		<?php echo $form->textArea($model,'criminalConvictedDetails',array('placeholder'=>'max. Length 500 chars.','style'=>'width: 780px; height: 70px;','maxlength'=>'500'));?><?php echo $form->error($model,'criminalConvictedDetails'); ?></div>		
	</td>
	
		</tr>
		<tr>
	<td><?php echo $form->labelEx($model,'isCriminalProceeding')?></td>
	<td>
		<?php echo $form->dropDownList($model,'isCriminalProceeding',array(''=>'Select','Y'=>'Yes','N'=>'No'),array('onchange'=>'criminal2(this.value);'))?>	
	</td></tr>
	<tr>
	<td colspan="2" style="display: none;" id="Proceeding"><div>Please provide details <br>
		<?php echo $form->textArea($model,'criminalProceedingDetails',array('placeholder'=>'max. Length 500 chars.','style'=>'width: 780px; height: 70px;','maxlength'=>'500'));?><?php echo $form->error($model,'criminalProceedingDetails'); ?>		
	</div></td>
	
		</tr></table>
	
</td>
  </tr>
 
</table>

		</div>
		<table align="right">  <tr>
    	<td><?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'c*%fn','size'=>75,'class'=>'btn btn-primary')) ?></td>
    </tr></table>
		</div>
		
		<div class="widget"> 
		<div>
			<table width="100%" >
			<tr><td align="center">
		    <table width="75%"  border="0" align="center" cellpadding="2" cellspacing="3" style="padding:10px; margin:5px;">
			<tr>
      			  <td class="more_text" style="text-decoration:underline;color:#993300;" align="center"> RESIDENT DATABASE SYSTEM - COMPLETION STATUS   
                  </td>
      			</tr>
			<tr> 
				<td  class="normal_text" align="center"> &nbsp;If you have completely filled all the details <br />
					 please tick here in order to process your data further.</td>
            </tr>
            <tr><td></td></tr>
              <tr><td></td></tr>
		<tr><td  align="center" class="normal_text"> <input  type="checkbox" id="Form_Filled" name="Form_Filled" style="width:20px;" value="Y" onclick="formfilled(this.value)" <?php if($fillcheck=='Y') echo "checked";?><?php if($Freeze=='Y'){?> disabled="disabled"<?php }?>  />&nbsp;&nbsp;I have filled in all the details</td>
		    	
	</tr>
	<tr><td><div id="show_fill"></div></td></tr>
	</table>
	
  </td></tr></table>
		</div></div>
		<?php 
	
		if($model->isCriminalConvicted=='Y' )	
			echo "<script>criminal1('".$model->isCriminalConvicted."')</script>";
		if($model->isCriminalProceeding=='Y' )	
			echo "<script>criminal2('".$model->isCriminalProceeding."')</script>";
		?>

<?php $this->endWidget(); ?>

