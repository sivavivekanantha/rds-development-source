<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'OfficialForm',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); 
 ?>
<style type="text/css" >
 th {
    background-color: rgb(54, 54, 54);
    color: rgb(255, 255, 255);
    font-size: 12px;
    font-weight: bold;
    height: 20px;
    padding: 2px;
    text-align: center;
}
.row1
{
background:#E4E4E4;
}
.row2
{
background:white;
}
</style>
 <script type="text/javascript">
 
	function ajaxState(obj,obj1)
	{	
	
		if(obj=='') return false;
	
		var url='<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/Ajaxsate?countryCode='+obj+'&statecode='+obj1;
		
		$("#state").load(url);		
		
	}
	function ajaxDistrict(obj,obj1)
	{
	if(obj=='') return false;
	if(obj==9999) {
		ajaxCity(0,0,obj);	
		$('#otherState').show(); 
	}	else 
	{
		ajaxCity(0,0,obj);	
		$('#otherState').hide();
	}
		$('#OfficialForm_oState').val('');
		var url='<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/Ajaxdistrict?stateCode='+obj+'&DistrictCode='+obj1;
		
		$("#district").load(url);
		
	}
	function ajaxCity(obj,obj1,obj2)
	{ 
	    if(obj1==0) obj1 = $("#state").val();
		if(obj=='' && obj2=='') return false;
		var url='<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/AjaxCity?districtCode='+obj+'&stateCode='+obj1+'&CityCode='+obj2;	
		$("#city").load(url);
		}
		function ajaxothercity(obj)
		{		
			if(obj==99999)
			{
				 $('#otherCity').show();
			     $('#OfficialForm_oCity').val('');
			 }
			 else $('#otherCity').hide();
		}
function editdata(obj,obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9,obj10,obj11,obj12,obj13,obj14)
	{
	
		$(".error").html('');   // Yii Error msg is empty after close, 
		if(obj8>0) ajaxState(obj8,obj9);
		if(obj9>0) ajaxDistrict(obj9,obj10);
		if(obj10>0) ajaxCity(obj10,obj9,obj11);
		else if(obj10=='') ajaxCity(obj10,obj9,obj11);
		    
		$('#OfficialForm_ivcOfficialInactiveCode').val(obj);		
		$('#OfficialForm_action').val(obj1);
		$('#OfficialForm_Occupationtype').val(obj2);
		$('#OfficialForm_Designation').val(obj3);
		//$('#OfficialForm_Responsibility').val(obj4);
		$('#OfficialForm_Responsibility').val(unescape(obj4));	
		$('#OfficialForm_CompanyName').val(obj5);
		//$('#OfficialForm_Address').val(obj6);
		$('#OfficialForm_Address').val(unescape(obj6));	
		$('#OfficialForm_Area').val(obj7);
		$('#OfficialForm_country').val(obj8);		
		$('#state').val(obj9);
		$('#OfficialForm_district').val(obj10);
		$('#city').val(obj11);
		
		$('#OfficialForm_pincode').val(obj12);
		$('#fromdate_start').val(obj13);	
	
		$('#todate_end').val(obj14);
		Formshow(3,0);
		
	}
	function deldata(obj,obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9,obj10,obj11,obj12,obj13,obj14)
	{
		var msg="Are you sure you want to delete this record?";
		if(!confirm(msg)){
			return false;
		}	
		else {		
		if(obj8>0) ajaxState(obj8,obj9);
		if(obj9>0) ajaxDistrict(obj9,obj10);
		if(obj10>0) ajaxCity(obj10,obj9,obj11);
		$('#OfficialForm_ivcOfficialInactiveCode').val(obj);
		
		$('#OfficialForm_action').val(obj1);
		$('#OfficialForm_Occupationtype').val(obj2);
		$('#OfficialForm_Designation').val(obj3);
		//$('#OfficialForm_Responsibility').val(obj4);
		$('#OfficialForm_Responsibility').val(unescape(obj4));	
		$('#OfficialForm_CompanyName').val(obj5);
		//$('#OfficialForm_Address').val(obj6);
		$('#OfficialForm_Address').val(unescape(obj6));	
		$('#OfficialForm_Area').val(obj7);
		$('#OfficialForm_country').val(obj8);		
		$('#state').val(obj9);
		$('#OfficialForm_district').val(obj10);
		$('#city').val(obj11);
		$('#OfficialForm_pincode').val(obj12);
		$('#fromdate_start').val(obj13);	
		$('#otherState').hide();
		 $('#otherCity').hide();
		$('#todate_end').val(obj14);
		$('#OfficialForm').submit();
		}
		
	}
	function Formshow(obj,obj1)
	{
		
		if(obj==1 || obj==3){
		$('#Formshow').show();
		$("#OfficialOtherForm_OfficialOther").prop("disabled", true);
			
		}
		if(obj==1 || obj==1){
		$('#showdiv').show();
		$('#OfficialForm_formValid').val('1');
		
		
			
		}
		if(obj==2){		
		$('#OfficialForm_action').val('');
		$('#ContactForm_Name').val('');
		$('#Formshow').hide();
		//$(".error").html(''); 
		$(".errorMessage").html('');
		}   
		// Yii Error msg is empty after close, 
		//$('#Formshow').hide();
		if(obj!=3 && obj1==0 ){
			
		$('#OfficialForm_Occupationtype').val('');
		$('#OfficialForm_Designation').val('');
		$('#OfficialForm_Responsibility').val('');
		$('#OfficialForm_CompanyName').val('');
		$('#OfficialForm_Address').val('');
		$('#OfficialForm_Area').val('');
		$('#OfficialForm_country').val('');		
		$('#state').val('');
		$('#OfficialForm_oState').val('');		
		$('#district').val('');
		$('#city').val('');		
		$('#otherState').hide();
		$('#otherCity').hide();
		$('#OfficialForm_city').val('');
		$('#OfficialForm_oCity').val('');		
		$('#OfficialForm_pincode').val('');
		$('#OfficialForm_action').val('');
		$('#OfficialForm_ivcOfficialInactiveCode').val('');
		$('#fromdate_start').val('');
	
			
		$('#todate_end').val('');
		$("#OfficialOtherForm_OfficialOther").prop("disabled", false);
		//$('#OfficialForm').submit();
		}
		
		
		
	}
	
function showdove(obj,obj1)
{
	
	if(obj==1)	
			$('#showdiv').show();	
	
	if(obj==2 && obj1==1){
			var msg="Are you sure you want to save this Details?";
			if(!confirm(msg)){
			return false;
			}
		else {	
					
		url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/SaveOfficialFormValid?&FormValid='+obj;

		$("#test").load(url);	
		$('#showdiv').hide();
		Formshow(obj,0);
	
	
		}
	}
	}		
 </script>
<div class="widget">
 <div id="test"></div>
   <div id="dt_example" class="example_alt_pagination">  
   <div class="widget-header">
  <div class="title">    
          <div id="formTitle"><h4><u><?php echo $form->labelEx($model,'official',array('style'=>'font-weight: bold;font-size:18px;')); ?> 	
 </u></h4> </div></div></div>
 
 
 
 <table style="margin-left:0px;" align="left" width="50%;">				   
				  <?php if(mssql_num_rows($row1)==0)  { 
					if($model->formValid<>2) $model->formValid='';?>
				<tr><td colspan="1"><?php echo $form->labelEx($model,'formValid')?></td>
				<td><?php echo $form->dropDownList($model,'formValid',array(''=>'Select','1'=>'Yes','2'=>'No'),
				array('onchange'=>'showdove(this.value,1);'))?></td>
				</tr>
<?php } ?>		</table>					 

           <div id="showdiv" style="display:none;">  <div class="row-fluid">
		
		  <table align="right"><tr><td><div><a href="#accSettings" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="Formshow(1,0)">Add New</a></div></td></tr>
		  </table>
</div>
  <div class="row-fluid">
    <div id="Formshow" style="display: none;">         
     <table width="100%" border="0" cellpadding="1">
	 	<?php echo $msg1;?>
	<tr>
	   <?php echo $form->hiddenField($model,'ivcOfficialInactiveCode'); ?>
		<?php echo $form->hiddenField($model,'action'); ?>
	<td><table width="100%" border="0" cellpadding="1" >
	 <tr>
	 
    <td><?php echo $form->labelEx($model,'Occupationtype');?></td>
    <td><div class=""><?php echo $form->dropDownList($model,'Occupationtype',CHtml::listData($Occupation,'Designation_Code','Designation'),array('prompt'=>'Select'));
		echo $form->error($model,'Occupationtype'); ?>
	</div></td>

    <td><?php echo $form->labelEx($model,'Designation');?></td>
    <td><div class=""><?php echo $form->textField($model,'Designation',array('onkeydown'=>"return alphaonly('OfficialForm_Designation')",'maxlength'=>'50'));
	echo $form->error($model,'Designation');
	?></div></td>
   </tr>
  <tr>
    <td><?php echo $form->labelEx($model,'Responsibility');?></td>
    <td colspan="3"><?php echo $form->textArea($model,'Responsibility',array('style'=>'width:685px;','placeholder'=>'max. Length 500 chars.','maxlength'=>'500'));?></td>
   
   </tr>
  <tr>
      <td><?php echo $form->labelEx($model,'CompanyName');?></td>
     <td ><?php echo $form->textField($model,'CompanyName',array('maxlength'=>'50'));?></td>
      <td><?php echo $form->labelEx($model,'Address');?></td>
     <td><?php echo $form->textArea($model,'Address',array('placeholder'=>'max. Length 500 chars.','maxlength'=>'500'));?></td>
   </tr>
  <tr>
   <td><?php echo $form->labelEx($model,'Area');?></td>
      <td ><?php echo $form->textField($model,'Area',array('onkeydown'=>"return alphaonly('OfficialForm_Area')",'maxlength'=>'50'));?></td>
     <td><?php echo $form->labelEx($model,'country');?></td>
    <td><?php echo $form->dropDownList($model,'country',CHtml::listData($country,'Country_Code','Country_Name'),array('prompt'=>'Select','onchange'=>'ajaxState(this.value)')); ?></td>
  </tr>
  <tr>
       <td><?php echo $form->labelEx($model,'state');?></td>
     <td><?php echo $form->dropDownList($model,'state',array(''=>'Select'),array('id'=>'state','onchange'=>'ajaxDistrict(this.value)'));?>
	 		<div id='otherState' style="display: none;">
			<div>
			<?php echo $form->textField($model,'oState',array('onkeydown'=>"return alphaonly('OfficialForm_oState')",'maxlength'=>'50'));
			echo $form->error($model,'oState');?>				
			</div>
			</div>
	 </td>
       <td><?php echo $form->labelEx($model,'district');?></td>
    <td><?php echo $form->dropDownList($model,'district',array(''=>'Select'),array('id'=>'district','onchange'=>'ajaxCity(this.value,0,0)'));?></td>
  </tr>
  <tr>
       <td><?php echo $form->labelEx($model,'city');?></td>
       <td><?php echo $form->dropDownList($model,'city',array(''=>'Select'),array('id'=>'city','onchange'=>'ajaxothercity(this.value)'));?>
	   		<div id='otherCity' style="display: none;">
			<div><?php echo $form->textField($model,'oCity',array('onkeydown'=>"return alphaonly('OfficialForm_oCity')",'maxlength'=>'50'));
				echo $form->error($model,'oCity'); ?></div></div>
	   </td>
        <td><?php echo $form->labelEx($model,'pincode');?></td>
    <td ><?php echo $form->textField($model,'pincode',array('onkeydown'=>"return alphanumberic('OfficialForm_pincode')",'maxlength'=>'10'));?></td>
  </tr>
  <tr>
       <td><?php echo $form->labelEx($model,'fromdate');?></td>
    <td>              
          <div>
			<?php echo $form->textField($model,'fromdate',array('id'=>'fromdate_start'));?>
			</div>
			<div><?php echo $form->error($model,'fromdate'); ?></div>
               </td>
       <td><?php echo $form->labelEx($model,'todate');?></td>
    <td>              
		<?php echo $form->textField($model,'todate',array('id'=>'todate_end')); ?>
        </td>
  </tr>
		</table>
	</td>
</tr>
</table>
<table align="right">  <tr>
    	<td><?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'o$3fl','size'=>75,'class'=>'btn btn-primary')) ?>	</td>
			<td><?php echo CHtml::button('Cancel',array('size'=>75,'id'=>'saveform2','class'=>'btn','onClick'=>'Formshow(2)')) ?></td>
	
    </tr></table>
		
</div>

<table width="100%" border="0" align="left"><thead>
<colgroup><col width=10% /><col width=6% /><col width=10% /><col width=15% /><col width=10% /><col width=10% /><col width=15% /></colgroup>
     <th><?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
   <th><?php echo $form->labelEx($model,'nameAndAddress',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
   <th><?php echo $form->labelEx($model,'Designation',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
   <th><?php echo $form->labelEx($model,'fromdate',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
   <th><?php echo $form->labelEx($model,'todate',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
   <th><?php echo $form->labelEx($model,'Responsibility',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
   <th><?php echo $form->labelEx($model,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th></thead>
	<?php $i=0;
	$colorflag=0;
	if(@mssql_num_rows($row1)>0) { 
		
        	while($field1=mssql_fetch_array($row1)) {
				
			$i++;
			$colorflag++;
			
			?>
      <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?>>
      
       <td align="center"><?php echo $i;?></td>
	   
	   <?php
	   	   $separator = "<br>";
		$CompanyDetails="";
		if (strlen($field1['Company_Name']) > 0 ) {
			$CompanyDetails .= $field1['Company_Name'];
		}
		if (strlen($field1['Off_Address']) > 0 ) {
			$CompanyDetails .= $separator;
			$CompanyDetails .= $field1['Off_Address'];
		}
		if (strlen($field1['City_Name']) > 0 ) {
			$CompanyDetails .= $separator;
			$CompanyDetails .= $field1['City_Name'];
		}
		if (strlen($field1['State_Name']) > 0 ) {
			$CompanyDetails .= $separator;
			$CompanyDetails .= $field1['State_Name'];
		}
		if (strlen($field1['Country_Name']) > 0 ) {
			$CompanyDetails .= $separator;
			$CompanyDetails .= $field1['Country_Name'];
		}
		$DesignationDetails="";
		if (strlen($field1['Designation']) > 0 ) {
			$DesignationDetails .= $field1['Designation'];
		}
		if (strlen($field1['Occupation']) > 0 ) {
			$DesignationDetails .= $separator;
			$DesignationDetails .= $field1['Occupation'];
		}
		
	   ?>
					
		<td align="center">
			<?php echo $CompanyDetails	?>
		</td>
		<td align="center">
			<?php echo $DesignationDetails?>
		</td>
		<td align="center" style="width:150px;" >
			<?php echo $field1['Work_From'];?>
		</td>	
		<td align="center" style="width:150px;" >
			<?php echo $field1['Work_To'];?></td>	
		<td align="center" style="width:150px;" >
			<?php echo $field1['Responsibility'];?></td>
		<td align="center"><a href="#" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' 
			onclick="editdata('<?php echo $field1['IVC_Official_inactive_Code']?>',2,'<?php echo $field1['Occupation_Type']?>','<?php echo $field1['Occupation']?>','<?php echo rawurlencode($field1['Responsibility']);?>','<?php echo $field1['Company_Name']?>','<?php echo rawurlencode($field1['Off_Address1']);?>','<?php echo $field1['Off_Area']?>','<?php echo $field1['Off_Country']?>','<?php echo $field1['Off_State']?>','<?php echo $field1['off_district']?>','<?php echo $field1['Off_City']?>','<?php echo $field1['Off_ZipCode']?>','<?php echo $field1['Work_From']?>','<?php echo $field1['Work_To'] ?>')">&#x270E</a> 
			<?php if(Yii::app()->session['Freeze']=="N") { ?>
			<a class="btn btn-success btn-small hidden-phone"  id="delbtn" data-original-title="Delete" onclick="deldata('<?php echo $field1['IVC_Official_inactive_Code']?>',3,'<?php echo $field1['Occupation_Type']?>','<?php echo $field1['Designation']?>','<?php echo rawurlencode($field1['Responsibility']);?>','<?php echo $field1['Company_Name']?>','<?php echo rawurlencode($field1['Off_Address1']);?>','<?php echo $field1['Off_Area']?>','<?php echo $field1['Off_Country']?>','<?php echo $field1['Off_State']?>','<?php echo $field1['off_district']?>','<?php echo $field1['Off_City']?>','<?php echo $field1['Off_ZipCode']?>','<?php echo $field1['Work_From']?>','<?php echo $field1['Work_To'] ?>')">&#x2717</a></td>	
       <?php 	}  }
        } 
        ?></tr>
</table>
</div>
</div></div></div>
<?php if($model->formValid>0 or (strlen($msg1)>3))	
	 echo "<script>showdove('".$model->formValid."',0)</script>"; ?>
	 
<script>ajaxState('<?php echo $model->country ?>','<?php echo $model->state ?>')</script>
<script>ajaxDistrict('<?php echo $model->state ?>','<?php echo $model->district ?>')</script>
<?php 
if(strlen($model->state)>0) { ?>
<script>ajaxCity('<?php echo $model->district ?>','<?php echo $model->state ?>','<?php echo $model->city ?>')</script>
<?php }

if($model->state=='9999' and $model->errflag>0 )
 { ?> 
 <script>ajaxDistrict('<?php echo $model->state ?>','0')</script> 
<?php }
if($model->city=='99999' and $model->errflag>0 )
 { ?>
<script>ajaxothercity('<?php echo $model->city ?>')</script> 
<?php }

if(strlen($msg1)>0  or $model->errflag==1) {?>

<script>Formshow('1','<?php echo $model->errflag ?>')</script>
<?php } 
if($model->action == 3) {?>
<script>Formshow('2','0')</script>
<?php }  $this->endWidget(); 

 $form=$this->beginWidget('CActiveForm', array(
	'id'=>'OfficialOtherForm',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
));  ?>

<div class="widget">
<div class="row-fluid">
<table align="center"><?php echo $msg2;?></table>
<table width="98%" border="0" cellpadding="1">

<?php echo $form->hiddenField($model1,'action1'); ?>

<tr>
 
      <td><?php echo $form->labelEx($model1,'OfficialOther',array('style'=>'margin-bottom:80px;'));?></td>
     <td><?php echo $form->textArea($model1,'OfficialOther',array('placeholder'=>'max. Length 500 chars.','style'=>'width: 450px; height: 103px;','maxlength'=>'500'));?></td><td align="right"><?php echo CHtml::submitButton('Save',array('class'=>'btn btn-primary','id'=>'saveform1','name'=>'o%4*7')) ?></td>
   </tr>
   
  
   </table>
   <table >  <tr>
    	
    </tr></table>
 </div>
 </div>
 
 <?php $this->endWidget(); ?>