<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'TeacherTrainingForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
<script>
$( document ).ready(function() {
    $("#data-table span").html('');
});

	function popupValue(obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8)
	{	

		if(obj6!=3) // Coming Add & Edit
		{
					
			if(obj7!=0) {  // If u click edit background is fade
		
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings").show();	
				
				$('#accSettings').removeClass('modal hide fade').addClass('modal hide fade in');
			}
		$("#TeacherTrainingForm_program").val(obj1);
		$("#TeacherTrainingForm_trainingLanguage").val(obj2);
		$("#TeacherTrainingForm_trainingCompleted").val(obj3);
	
		if($("#TeacherTrainingForm_trainingCompleted").val()=='N'){
				$("#TeacherTrainingForm_handledSession").attr("disabled","disabled");
		}
	
		$("#TeacherTrainingForm_handledSession").val(obj4);
		$("#TeacherTrainingForm_ivcTrainingCode").val(obj5);
		$("#TeacherTrainingForm_action").val(obj6);
		$("#TeacherTrainingForm_Others").val(obj8);
		
		
		}
		if(obj6==3) {  // Delete
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
				$("#TeacherTrainingForm_program").val(obj1);
				$("#TeacherTrainingForm_trainingLanguage").val(obj2);
				$("#TeacherTrainingForm_trainingCompleted").val(obj3);
				$("#TeacherTrainingForm_handledSession").val(obj4);
				$("#TeacherTrainingForm_ivcTrainingCode").val(obj5);
				$("#TeacherTrainingForm_action").val(obj6);
					$("#TeacherTrainingForm_Other").val(obj8);
				$("#TeacherTrainingForm").submit();
				
			}
		}
	
			other_teacher(obj1);
	}
	function popupHide()
	{		$("#errinfo").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');   // Yii Error msg is empty after close, 
			$('#edPopup').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
			$("#accSettings").hide();	 // Popup Hide
	}
	
	
	function other_teacher(obj)              
			{

			  if(obj==8888) $("#Others").show();	
			  						
			  else
			  {
			  	 $("#Others").hide();
				  $("#TeacherTrainingForm_Others").val('');
				 
				 }
		
			}
	
	
	function criminal1(obj)
    { 
		$("#TeacherTrainingForm_handledSession").attr("disabled","disabled");
		$('#TeacherTrainingForm_handledSession').val('');
		if(obj=='Y') 		
			$("#TeacherTrainingForm_handledSession").removeAttr("disabled");
	
    }
 function showdove(obj,obj1)
{
	if(obj==1)	
			$('#showdiv').show();	
	
	if(obj==2 && obj1==1){
			var msg="Are you sure you want to save this Details?";
			if(!confirm(msg)){
			return false;
			}
		else {	
					
		url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/SaveTeacherFormValid?&FormValid='+obj;
	
		$("#test").load(url);	
		$('#showdiv').hide();
	
	
		}
	}
	
}
</script>
   
			<div class="widget">
		    <div id="test"></div>
				   <div id="formTitle">
                   <div class="widget-header">
                   <div class="title">
				   <h4><u><?php echo $form->labelEx($model,'teacher',array('style'=>'font-weight: bold;font-size:18px;')); ?> 	 </u></h4>
				   </div>
				   </div>
				   </div>					
				
                 
				 <div class="widget-body">					
				<table>
				  <?php if(mssql_num_rows($row1)==0)  { 
						if($model->formValid<>2) $model->formValid=''; ?>
				<tr><td><?php echo $form->labelEx($model,'formValid')?></td>
				<td><?php echo $form->dropDownList($model,'formValid',array(''=>'Select','1'=>'Yes','2'=>'No'),
				array('onchange'=>'showdove(this.value,1);'))?></td>
				</tr>
<?php } ?>		</table>
</div>	
                 <div class="widget-body">					
	             <div id="showdiv" style="display: none;">                  
				 <div id="dt_example" class="example_alt_pagination" style="padding:10px; margin:7px;" >
				   <table align="right"><tr>
				  <td><div><a href="#accSettings" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue('','','','','',1,0,'')">Add New</a></div></td></tr></table>
				  
				  
				  
                       <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">  
                        <thead><th><?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'program',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'trainingLanguage',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                        <th><?php echo $form->labelEx($model,'trainingCompleted',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'handledSession',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                        <th><?php echo $form->labelEx($model,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                        </thead>
                      <tbody>
                          <?php $i=0;?>
                          <?php while($field=mssql_fetch_array($row1))
						  
						  { $i++;
						  ?>
                          	
                      		<tr class="gradeX"><td align="center"><?php echo $i; ?></td>
                            <td><?php if($field['Program_Code']<>'8888') echo $field['Program_Name'];
							else  echo $field['Others'];?></td>
                            <td><?php echo $field['Language'];?></td>
                          	<td><?php if($field['Training_Completed'] =='Y') echo "Yes"; else if($field['Training_Completed'] =='N')
							 echo "No";?></td>		
							<td><?php if($field['Handled_Session'] =='Y') echo "Yes"; else if($field['Handled_Session'] =='N')
							 echo "No";?></td>			
							<td style="width:20px;" class="hidden-phone">
							<a href="#accSettings" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue('<?php echo $field['Program_Code'] ?>','<?php echo $field['Training_Language'] ?>','<?php echo $field['Training_Completed'];?>','<?php echo $field['Handled_Session'];?>','<?php echo $field['IVC_Training_Code'] ?>',2,0,'<?php echo $field['Others'] ?>')">&#x270E</a>
							<?php if(Yii::app()->session['Freeze']=="N") { ?>
                            <a class="btn btn-success btn-small hidden-phone"  id="delbtn"   data-original-title="Delete" onclick="popupValue('<?php echo $field['Program_Code'] ?>','<?php echo $field['Training_Language'] ?>','<?php echo $field['Training_Completed'];?>','<?php echo $field['Handled_Session'];?>','<?php echo $field['IVC_Training_Code'] ?>',3,0,'<?php echo $field['Others'] ?>')">&#x2717</a>
							<?php } ?>
                            </td>
                        </tr>
					<?php } ?>
					</tbody>
                    </table>
			<div id="accSettings" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 		<div id="errinfo" class="error" align="center" > <?php echo $msg1; ?> </div>
		<div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide()"> x </button>
         	<h4><u><?php echo $form->labelEx($model,'teacherTraining',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
    	</div> 
   <div class="modal-body">
   		<div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
             <?php echo $form->hiddenField($model,'ivcTrainingCode'); ?>
			   <?php echo $form->hiddenField($model,'action'); ?>
			 <?php echo $form->labelEx($model,'program'); ?>
             </div><div>
			 <?php echo $form->dropDownList($model,'program',CHtml::listData($program,'Program_Code','Program_Name'),array('prompt'=>'Select','onchange'=>'other_teacher(this.value)'));			 
			 echo $form->error($model,'program');			
			 ?>
			 
             </div>
        </div> 
			 <div class="row-fluid" id='Others' style="display: none;">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'Others'); ?>
                </div>
				<div>
				<?php echo $form->textField($model,'Others');
				
				?>					
				</div>	
		     </div>
			 
        <div class="row-fluid">  
         	<div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'trainingLanguage'); ?>
            </div><div> 
			<?php echo $form->dropDownList($model,'trainingLanguage',CHtml::listData($language,'Language_Code','Language'),array('prompt'=>'Select'));
			 echo $form->error($model,'trainingLanguage');
			 //,array(''=>'Select','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6'),	array('options' => array('trainingLanguage'=>array('selected'=>true))));
			 ?>
			 
             </div> 
			      
        </div> 
	                                 
        <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'trainingCompleted'); ?>
                </div><div> 
				<?php echo $form->dropDownList($model,'trainingCompleted',
				array(''=>'Select','N'=>'No','Y'=>'Yes'),array('onchange'=>'return criminal1(this.value)'));
				
				 echo $form->error($model,'trainingCompleted');?>
                 </div>
         </div>
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'handledSession'); ?>
                </div><div> 
				<?php echo $form->dropDownList($model,'handledSession',
				array(''=>'Select','N'=>'No','Y'=>'Yes'),
				array('options' => array('handledSession'=>array('selected'=>true))),array('disabled'=>'disabled'));
				 echo $form->error($model,'handledSession');?>
                 </div>
         </div>
   </div>
   
                        
    
    <div class="modal-footer">
  
            <?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'t%*er','class'=>'btn btn-primary')); ?> 
             <button type="button" class="btn" aria-hidden="true" data-dismiss="modal" onclick="popupHide()"> Close </button>
        	
   </div>
                    
</div>
</div> 
	        <?php 		
			 if($model->action<>3 and (strlen($msg1)>3 or $model->errflag==1)){			
			 echo "<div id='edPopup' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
				 echo "<script>popupValue('".$model->program."','".$model->trainingLanguage."','".$model->trainingCompleted."','".$model->handledSession."','".$model->ivcTrainingCode."',".$model->action.",1,'".$model->Others."')</script>"; }  ?>                    
 		<div class="clearfix">	</div>
		</div>
                  </div>  </div>  
	        
	 	<?php if($model->formValid>0)	
	 echo "<script>showdove('".$model->formValid."',0)</script>"; ?>
	  	<?php if(strlen($msg1)>3  or $model->errflag==1)	
	 echo "<script>showdove('1',0)</script>"; ?>
	  <?php if($model->program=='8888'){
						 echo "<script>other_teacher('".$model->program."')</script>"; 
						 } ?>

 <?php $this->endWidget(); ?>
