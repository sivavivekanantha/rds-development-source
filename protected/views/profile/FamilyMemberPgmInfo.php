<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'FamilyProgramForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>

<script>
function UpdateProgramInfo(obj)
{ 
var msg="Are you sure you want to save Family Members Program Details?";
			if(!confirm(msg)){
			return false;
			}
			else {							 
 for(var i=1;i<=obj;i++){
                 var ch1 = "#famcode_"+i;
                 var ch = ($(ch1).val());				
				 var ch2 = "#ishaYoga1_"+i;
				 var st='N';
				 if($("#ishaYoga1_"+i).is(':checked'))				 
				 var st='Y';				 
				 var ch3 = "#ishaYoga2_"+i;
				 var su='N';
				 if($("#ishaYoga2_"+i).is(':checked')) 
				 var su='Y';	
				 var ch4 = "#ishaYoga3_"+i;
				 var sv='N';
				 if($("#ishaYoga3_"+i).is(':checked')) 
				 var sv='Y';	
				 var ch5 = "#ishaYoga4_"+i;
				 var sw='N';
				 if($("#ishaYoga4_"+i).is(':checked')) 
				 var sw='Y';	
				 
				 
		
				 
	url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/UpdateFamilyProgram?Family_Code='+ch+'&ishaYoga1='+st+'&ishaYoga2='+su+'&ishaYoga3='+sv+'&ishaYoga4='+sw;	
	
	
	$("#fill").load(url);
	
		}	 
	}
}

 function showdove(obj,obj1)
{
	if(obj==1)	
			$('#showdiv').show();	
	
	if(obj==2 && obj1==1){
			var msg="Are you sure you want to save this Details?";
			if(!confirm(msg)){
			return false;
			}
		else {	
					
		url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/SaveFamilyMemberInfoFormValid?&FormValid='+obj;
	
		$("#test").load(url);	
		$('#showdiv').hide();
	
	
		}
	}
	
}
</script>
<div class="widget">

<div id="test"></div><div id="fill"></div>	
<div id="dt_example" class="example_alt_pagination">
<div class="widget-header">
<div class="title">
<div><h4><u><?php echo $form->labelEx($model,'FamilyPgmA',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4></div>
</div></div>
<div class="title" style="margin-left: 10px;"><h4><u><?php echo $form->labelEx($model,'FamilyPgmB',array('style'=>'font-weight: bold;font-size:15px;')); ?></u></h4></div>	
				<!--<table style="margin-left:0px;">
				  <?php 
				    if(mssql_num_rows($row)>0)  {						
					if($model->formValid<>2) $model->formValid=''; 	?>
				<tr><td colspan="1"><?php echo $form->labelEx($model,'formValid')?></td>
				<td><?php echo $form->dropDownList($model,'formValid',array(''=>'Select','1'=>'Yes','2'=>'No'),
				array('onchange'=>'showdove(this.value,1);'))?></td>
				</tr>
               <?php } ?>		
               </table>	!-->
			   
                   <div id="showdiv" style="display:block;">
                    <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">                        
                        <thead>
						<th><?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'name',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'ishaYoga',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'bsp',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'hataYoga',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'samayama',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						               
                        </thead>
                        <tbody>
						
						 <?php $i=0;
						 $rcnt=0;
						  if(mssql_num_rows($row)>0) {
						  	$rcnt=mssql_num_rows($row);
							
                          while($field = mssql_fetch_array($row))
						   {
						   	$i++;?><tr>
	    					<input type="hidden" id="famcode_<?php echo $i; ?>" name="familyCode<?php echo $i; ?>" value=<?php echo $field['Family_Code'];?> /> 
								
		<td align="center"><?php echo $i; ?></td>
		<td align="center"><?php echo $field['Member_Name']; ?></td>
		<td><input  type="checkbox" id="ishaYoga1_<?php echo $i  ?>"  style="width:20px;" value="Y",
        <?php echo $field['Family_Code']?>)", <?php if($field['Isha_Yoga']=='Y') echo "checked";?> /></td>
		<td><input  type="checkbox" id="ishaYoga2_<?php echo $i  ?>"  style="width:20px;" value="Y",
        <?php echo $field['Family_Code']?>)", <?php if($field['BSP']=='Y') echo "checked";?> /></td>
		<td><input  type="checkbox" id="ishaYoga3_<?php echo $i  ?>"  style="width:20px;" value="Y",
        <?php echo $field['Family_Code']?>)", <?php if($field['Hata_Yoga']=='Y') echo "checked";?> /></td>
		<td><input  type="checkbox" id="ishaYoga4_<?php echo $i  ?>"  style="width:20px;" value="Y",
        <?php echo $field['Family_Code']?>)", <?php if($field['Samayama']=='Y') echo "checked";?> /></td>
		
	</tr>
	<?php  } 
	}?>
	</tbody></table></div>
	
	<div class="clearfix"></div><table align="right">
<tr><td>
<?php echo CHtml::Button('Save',array('id'=>'saveform','name'=>'f*&m#','class'=>'btn btn-primary',
  'onclick'=>'return UpdateProgramInfo('.$rcnt.');')) ?></td>
</tr></table>
			</div></div>

						
	<?php if($model->formValid>0)	
	 echo "<script>showdove('".$model->formValid."',0)</script>"; ?>
	<?php if(strlen($msg1)>3  or $model->errflag==1)	
	 echo "<script>showdove('1',0)</script>"; ?>
			
	<?php $this->endWidget(); ?>
 
 	
 
 
