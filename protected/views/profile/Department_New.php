<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'PresentDepartmentForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
				<script>	
	function popupValue1(obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8)
	{	

		if(obj6!=3) // Coming Add & Edit
		{
			if(obj7!=0) {  // If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings1").show();			
				$('#accSettings1').removeClass('modal hide fade').addClass('modal hide fade in');
			} else {
			$("#errinfo1").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');    // Yii Error msg is empty after close, 
			}
			 if(obj1>0) ajaxGroup(obj1);
		$("#PresentDepartmentForm_departmentName1").val(obj1);
		$("#PresentDepartmentForm_aOccupation1").val(obj2);
		$("#fromDate1").val(obj3);		
		$("#PresentDepartmentForm_reporting1").val(obj4);		
		$("#PresentDepartmentForm_ashramOfficialCode1").val(obj5);
		$("#PresentDepartmentForm_action1").val(obj6);	
		$("#PresentDepartmentForm_group").val(obj8);
	
		}
		if(obj6==3) { // Delete		
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
			     //if(obj1>0) ajaxGroup(obj1);
			    $("#PresentDepartmentForm_action1").val(obj6);				
				$("#PresentDepartmentForm_ashramOfficialCode1").val(obj5);
				$("#PresentDepartmentForm_departmentName1").val(obj1);
				$("#PresentDepartmentForm_aOccupation1").val(obj2);
				$("#PresentDepartmentForm_fromDate1").val(obj3);				
				$("#PresentDepartmentForm_reporting1").val(obj4);				
				$("#PresentDepartmentForm_group").val(obj8);				
				$("#PresentDepartmentForm").submit();
			}
		}
		
	}
function showdove(obj,obj1)
{
	if(obj==1 )	
			$('#showdiv').show();	
	if(obj==2 && obj1==1)
	{
			var msg="Are you sure you want to save this current Department?";
			if(!confirm(msg))
			{
			return false;
			}
			else   
			{					
			url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/SavePresentDepartmentFormValid?&FormValid='+obj;

			$("#test").load(url);	
			$('#showdiv').hide();
		
			}
		}
	}	
	function popupHide1()
	{		$("#errinfo1").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');   // Yii Error msg is empty after close, 
			$('#edPopup1').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
			$("#accSettings1").hide();	 // Popup Hide	
			}	
			
  function saveDepartment(obj)
	{
	        var msg="Are you sure you want to save Primary Department?";
			if(!confirm(msg)){
			return false;
			}
			else {					
			url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/ActiveDepartment?&department='+obj;	
			//alert(url);	
		$("#test").load(url);
		}
	}
	function ajaxGroup(obj)
	{	
		$("#star").show();
	    if(obj==73) $("#star").hide();
		if(obj=='') return false;
		var url='<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/AjaxGroup?departmentCode='+obj;
		//$("#PresentDepartmentForm_group").attr('disabled', true);
		$("#group").load(url);			
	}
	$( document ).ready(function() {
    $("#data-table span").html('');
});
</script>

<div class="widget">
                   <div id="formTitle">
                   <div class="widget-header">
                   <div class="title">
 <h4><u><?php echo $form->labelEx($model1,'department',array('style'=>'font-weight: bold;font-size:18px;')); ?>
 	</u></h4></div></div></div>
<div id="test"></div>
<div class="widget-body">	
 <div id="errinfo1" class="error" align="center"> <?php echo $msg2; ?> </div></div>
                   <div class="title" style="margin-left: 10px;"><h4><u><?php echo $form->labelEx($model1,'departmentPopup',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4></div>
					<div class="widget-body">	
				   <table>				           
				  <?php  
				    if(mssql_num_rows($row2)==0 or $model1->formValid==2 )  {
						?>
				<tr><td colspan="7"><?php echo $form->labelEx($model1,'formValid')?></td>
				<td><?php echo $form->dropDownList($model1,'formValid',array('0'=>'Select','1'=>'Yes','2'=>'No'),
				array('onchange'=>'showdove(this.value,1);'))?></td>
				</tr>
<?php } ?>		</table></div>					 
	            <div class="widget-body">	
                <div id="showdiv" style="display:none;"> 
				<div id="dt_example" class="example_alt_pagination">
				  <table align="right"><tr>
			
				  <td><div><a href="#accSettings1" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue1('','','','','',1,0,'')">Add New</a></div></td></tr></table>
				  
				 
				
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">                        
                        <thead><th><?php echo $form->labelEx($model1,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                          <th><?php echo $form->labelEx($model1,'departmentName1',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                            <th><?php echo $form->labelEx($model1,'aOccupation1',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model1,'group',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model1,'fromDate1',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                            <th><?php echo $form->labelEx($model1,'reporting1',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model1,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                        </thead>
                        <tbody>
                          <?php $i=0;?>
                          <?php while($field1=mssql_fetch_array($row2))
						  
						  { $i++;
						  ?>
                          	
                      		<tr class="gradeX"><td align="center"><?php echo $i; ?></td>
                            <td><?php echo $field1['Department'];?></td>
							<td><?php echo $field1['Occupation'];?></td>
							<td><?php echo $field1['Group_Name'];?></td>
							<?php if(strlen($field1['From_Date'])==10) 
							$fromdate1 = date('d-m-Y',strtotime($field1['From_Date']));
							else $fromdate1="";
							?>
                            <td><?php echo $fromdate1;?></td>							
							<td><?php echo $field1['Reporting'];?></td>										
                            <td style="width:20px;" class="hidden-phone">
							
							<a href="#accSettings1" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue1('<?php echo $field1['Department_Code'] ?>','<?php echo $field1['Occupation'] ?>','<?php echo $field1['From_Date'] ?>','<?php echo $field1['Reporting'] ?>','<?php echo $field1['Ashram_Official_Code'] ?>',2,0,0)">&#x270E</a>
							
							
							<?php if(Yii::app()->session['Freeze']=="N") { ?>
                            <a class="btn btn-success btn-small hidden-phone"  id="delbtn"   data-original-title="Delete" onclick="popupValue1('<?php echo $field1['Department_Code'] ?>','<?php echo $field1['Occupation'] ?>','<?php echo $field1['From_Date'] ?>','<?php echo $field1['Reporting'];?>','<?php echo $field1['Ashram_Official_Code'] ?>',3,0,0)">&#x2717</a>
							<?php } ?>
                            </td>
                        </tr>
					<?php } ?>
                	</tbody>
                    </table>
			<div id="accSettings1" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 		<div id="errinfo1" class="error" align="center" > <?php echo $msg2; ?>
			 </div>
		<div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide1()"> x </button>
         	<h4><u><?php echo $form->labelEx($model1,'department',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
    	</div> 
   <div class="modal-body">
   		<div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
             <?php echo $form->hiddenField($model1,'ashramOfficialCode1'); ?>
			   <?php echo $form->hiddenField($model1,'action1'); ?>			  
			 <?php echo $form->labelEx($model1,'departmentName1'); ?>
             </div>
			 <div>
			 <?php echo $form->dropDownList($model1,'departmentName1',CHtml::listData($department,'Department_Code','Department'),array('prompt'=>'select','onchange'=>'ajaxGroup(this.value)'));
			 echo $form->error($model1,'departmentName1'); ?>
             </div>
			 </div> 
			 
			  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<table>
				<tr>
				  <td><?php echo $form->labelEx($model1,'aOccupation1'); ?></td>
				  <td id='star' style="display: block;">*</td>
				  </tr>
			</table>
				
                </div><div>
				<?php echo $form->textField($model1,'aOccupation1',array('maxlength'=>'100','onkeydown'=>"return alphaonly('PresentDepartmentForm_aOccupation1')")); ?>  
				<?php echo $form->error($model1,'aOccupation1'); ?>      
                 </div>
		 
		  </div>
			  <div class="row-fluid">
			  <div style="float:left; overflow: hidden; width:200px">                                  
			  <?php echo $form->labelEx($model1,'group');?></div><div>
              <?php echo $form->dropDownList($model1,'group',array(''=>'Select'),
			  array('id'=>'group'));?>
			  <?php echo $form->error($model1,'group'); ?>      
			  </div></div>
		   
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
					<?php echo $form->labelEx($model1,'fromDate1');?>   
                </div><div>
					<?php echo $form->textField($model1,'fromDate1',array('id'=>'fromDate1')); ?>
					<?php echo $form->error($model1,'fromDate1'); ?>   
                 </div>
		  </div>		   
		  
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model1,'reporting1'); ?>
                </div><div>
				<?php echo $form->textField($model1,'reporting1' ,array('onkeydown'=>"return alphaonly('PresentDepartmentForm_reporting1')",'maxlength'=>'100')); ?>  
				<?php echo $form->error($model1,'reporting1'); ?>      
                 </div>
		 
		  </div> 
		  
       
   </div>
                        
    
    <div class="modal-footer">
  
            <?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'de8&1','class'=>'btn btn-primary')); ?> 
             <button type="button" class="btn" aria-hidden="true" data-dismiss="modal"
			  onclick="popupHide1()"> Close </button>
        	
   </div>
                    
</div>
</div>
</div></div>
<?php 

			if($model1->action1<>3 and (strlen($msg2)>3 or $model1->errflag==1)){
				
				 echo "<div id='edPopup1' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
				 echo "<script>popupValue1('".$model1->departmentName1."','".$model1->aOccupation1."','".$model1->fromDate1."','".$model1->reporting1."','".$model1->ashramOfficialCode1."','".$model1->action1."',1,'".$model1->group."')</script>"; } ?>                    
 		<div class="clearfix">	</div>
     </div>    
	 
	 	 <?php 
		 if($model1->formValid>0){
		
		  echo "<script>showdove('".$model1->formValid."',0)</script>";
 echo "<script>ajaxGroup('".$model1->departmentName1."')</script>";
		 } ?>
		 <div class="widget">
		 <div id="testing"></div>		 
    <div class="widget-body">
	<table align="center">
	<tr>
		<td><div><?php echo $form->labelEx($model1,'activeDepartment',array('style'=>'font-weight: bold; : font-weight: normal;'));?></div></td>
		<td>
		<table><tr><td><div>
		<?php echo $form->dropDownList($model1,'activeDepartment',CHtml::listData($primaryDepartment,'Department_Code','Department'),array('prompt'=>'Select', 'onchange'=>'saveDepartment(this.value)'));
 echo $form->error($model1,'activeDepartment');	 ?>			 
			 </div></td>
			</tr></table>
		</td>
	</tr>
</table></div></div>


 <?php $this->endWidget(); ?>

 <?php
 //PastDepartmentForm_New
 
  $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'PastDepartmentForm_New',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,),
                    )); 
                    ?>
<script>

	function popupValue(obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9)
	{	
			
	   if(obj1==73) starmark(obj1);
		if(obj8!=3) // Coming Add & Edit
		{
			if(obj9!=0) {  // If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings").show();			
				$('#accSettings').removeClass('modal hide fade').addClass('modal hide fade in');
			}
		$("#PastDepartmentForm_New_departmentName").val(obj1);
		$("#PastDepartmentForm_New_aOccupation").val(obj2);
		$("#fromdate_start").val(obj3);
		$("#todate_end").val(obj4);
		$("#PastDepartmentForm_New_wasReporting").val(obj5);
		$('#PastDepartmentForm_New_aRemarks').val(unescape(obj6));
		//$("#PastDepartmentForm_New_aRemarks").val(obj6);
		$("#PastDepartmentForm_New_ashramOfficialCode").val(obj7);
		$("#PastDepartmentForm_New_action").val(obj8);		
		}
		if(obj8==3) {  // Delete
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
			    $("#PastDepartmentForm_New_action").val(obj8);				
				$("#PastDepartmentForm_New_ashramOfficialCode").val(obj7);
				$("#PastDepartmentForm_New_departmentName").val(obj1);
				$("#PastDepartmentForm_New_aOccupation").val(obj2);
				$("#PastDepartmentForm_New_fromdate").val(obj3);
				$("#PastDepartmentForm_New_todate").val(obj4);
				$("#PastDepartmentForm_New_wasReporting").val(obj5);
				//$("#PastDepartmentForm_New_aRemarks").val(obj6);
				$('#PastDepartmentForm_New_aRemarks').val(unescape(obj6));
				$("#PastDepartmentForm_New").submit();
			}
		}
		
	}
	function popupHide()
	{		$("#errinfo").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');   // Yii Error msg is empty after close, 
			$('#edPopup').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
			$("#accSettings").hide();	 // Popup Hide	
			}
			
			
function starmark(obj)
{
	$("#paststar").show();
	    if(obj==73) $("#paststar").hide();
}
</script>
  <div class="widget">
			<div class="widget-body">
			 <div id="test"> </div>
		        
				   <div><h4><u><?php echo $form->labelEx($model,'departmentHistory',array('style'=>'font-weight: bold;font-size:18px;')); ?><?php echo $form->labelEx($model,'departmentHint',array('style'=>'font-weight: bold;font-size:15px;')); ?></u></h4></div>	
				   			 
                   <div id="showdiv1	" style="display:block;"> 
                  <table align="right"><tr>
			
				  <td><div><a href="#accSettings" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue('','','','','','','',1,0)">Add New</a></div></td></tr></table>
				  
				 
				
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">                        
                        <thead><th><?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                           <th><?php echo $form->labelEx($model,'departmentName',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                           <th><?php echo $form->labelEx($model,'aOccupation',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                           <th><?php echo $form->labelEx($model,'fromdate',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model,'todate',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model,'wasReporting',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
							<th><?php echo $form->labelEx($model,'aRemarks',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                           <th><?php echo $form->labelEx($model,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                        </thead>
                        <tbody>
                         <?php $i=0;?>
                          <?php while($field=mssql_fetch_array($row1))
						  
						  { $i++;
						  ?> 	
                      		<tr class="gradeX">
							<td align="center"><?php echo $i; ?></td>
                            <td><?php echo $field['Department'];?></td>
							<td><?php echo $field['Occupation'];?></td>
							<?php if(strlen($field['From_Date'])==10) 
							$fromdate=date('d-m-Y',strtotime($field['From_Date']));
							else $fromdate='';?>
                            <td><?php echo $fromdate;?></td>
							<?php if(strlen($field['To_Date'])==10) 
							$todate=date('d-m-Y',strtotime($field['To_Date'])); else $todate=''; ?>
                            <td><?php echo $todate;?></td>
							<td><?php echo $field['WasReporting'];?></td>
							<td><?php echo $field['Remarks'];?></td>						
                            <td style="width:20px;" class="hidden-phone">
							
							<a href="#accSettings" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue('<?php echo $field['Department_Code'] ?>','<?php echo $field['Occupation'] ?>','<?php echo $field['From_Date'] ?>','<?php echo $field['To_Date'] ?>','<?php echo $field['WasReporting'] ?>','<?php echo rawurlencode($field['Remarks']); ?>','<?php echo $field['Ashram_Official_Code'] ?>',2,0)">&#x270E</a>
							
							
							<?php if(Yii::app()->session['Freeze']=="N") { ?>
                            <a class="btn btn-success btn-small hidden-phone"  id="delbtn1"   data-original-title="Delete" onclick="popupValue('<?php echo $field['Department_Code'] ?>','<?php echo $field['Occupation'] ?>','<?php echo $field['From_Date'] ?>','<?php echo $field['To_Date'];?>','<?php echo $field['WasReporting'];?>','<?php echo rawurlencode($field['Remarks']);?>','<?php echo $field['Ashram_Official_Code'] ?>',3,0)">&#x2717</a>
							<?php } ?>
                            </td>
                        </tr>
					<?php } ?>
                	</tbody>
                    </table>
			<div id="accSettings" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 		<div id="errinfo" class="error" align="center" > <?php echo $msg1; ?>
			 </div>
		<div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide()"> x </button>
        <h4><u><?php echo $form->labelEx($model1,'department',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
    	</div> 
   <div class="modal-body">
   		<div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
             <?php echo $form->hiddenField($model,'ashramOfficialCode'); ?>
			   <?php echo $form->hiddenField($model,'action'); ?>			  
			 <?php echo $form->labelEx($model,'departmentName'); ?>
             </div><div>
			 <?php echo $form->dropDownList($model,'departmentName',CHtml::listData($department,'Department_Code','Department'),array('prompt'=>'Select','onchange'=>'starmark(this.value)'));
			 echo $form->error($model,'departmentName');
			 ?>
			 
             </div>
			 </div> 
			 
			  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<table>
				<tr>
				  <td><?php echo $form->labelEx($model,'aOccupation'); ?></td>
				  <td id='paststar' style="display: block;">*</td>
				  </tr>
			</table>
			
				
                </div><div>
				<?php echo $form->textField($model,'aOccupation',array('onkeydown'=>"return alphaonly('PastDepartmentForm_New_aOccupation')",'maxlength'=>'100')); ?>  
				<?php echo $form->error($model,'aOccupation'); ?>      
                 </div>
		 
		  </div>
		   
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
					<?php echo $form->labelEx($model,'fromdate');?>   
                </div><div>
                
					<?php echo $form->textField($model,'fromdate',array('id'=>'fromdate_start','onkeydown'=>"return periodonly('fromDate')")); ?>
				  
					<?php echo $form->error($model,'fromdate'); ?>   
                 </div>
		  </div>
		  
		   <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
					<?php echo $form->labelEx($model,'todate');?>   
                </div><div>
                	
					<?php echo $form->textField($model,'todate',array('id'=>'todate_end')); ?>
					
					<?php echo $form->error($model,'todate'); ?>   
                 </div>
		  </div>
		  
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'wasReporting'); ?>
                </div><div>
				<?php echo $form->textField($model,'wasReporting',array('onkeydown'=>"return alphaonly('PastDepartmentForm_New_wasReporting')",'maxlength'=>'100')); ?>  
				<?php echo $form->error($model,'wasReporting'); ?>      
                 </div>
		 
		  </div>
		  
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'aRemarks'); ?>
                </div><div>
				<?php echo $form->textArea($model,'aRemarks',array('placeholder'=>'max. Length 500 chars.','maxlength'=>'500'));?>
				<?php echo $form->error($model,'aRemarks'); ?>      
                 </div>
		 
		  </div>
       
   </div>
                        
    
    <div class="modal-footer">
  
            <?php echo CHtml::submitButton('Save',array('id'=>'saveform1','name'=>'d12*$','class'=>'btn btn-primary')); ?> 
             <button type="button" class="btn" aria-hidden="true" data-dismiss="modal"
			  onclick="popupHide()"> Close </button>
        	
   </div>
                    
</div>

<?php 

if($model->action<>3 and (strlen($msg1)>3 or $model->errflag==1)){
		
				 echo "<div id='edPopup' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
				 echo "<script>popupValue('".$model->departmentName."','".$model->aOccupation."','".$model->fromdate."','".$model->todate."','".$model->wasReporting."','".rawurlencode($model->aRemarks)."','".$model->ashramOfficialCode."','".$model->action."',1)</script>"; 
				 
				echo "<script>starmark('".$model->departmentName."');</script>"; 
				 } ?>                    
 		<div class="clearfix">	</div>
     </div>    
	 </div>
	 </div>
	 
 <?php $this->endWidget(); ?>