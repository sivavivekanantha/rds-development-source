	<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'PersonalEmailForm',
	'enableClientValidation'=>true,
	'htmlOptions' => array(
   'enctype' => 'multipart/form-data',
    ),
	'clientOptions'=>array(
	'validateOnSubmit'=>true,
	),
	)); 	
	?>
	<script type="text/javascript">		
			
	function Recivemail2(obj)              
			{	
			if(obj==2)	{
			     $("#PersonalEmailForm_receiveIshangaMail").attr('disabled', false);
				 }			
			  else
			  {
			  $("#PersonalEmailForm_receiveIshangaMail").attr('disabled', true);
			    $('#PersonalEmailForm_receiveIshangaMail').val('');
			  }
		
			}			
	
			
	
	</script>

<div style="padding:10px; margin:7px;" align="center">
<div class="widget" style="width: 50%;" align="center"> 
<div class="widget-header">
 <div class="title">
<div><h4><u><?php echo $form->labelEx($model,'email',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4></div>
</div></div>
	<table align="center"><tr><td><?php echo $msg; ?></td></tr></table>
    <div class="row-fluid">		
	<table width="90%" align="center" border="0" cellpadding="1">	
    <tr>
	<td>	
	<tr>
	<td>
	<?php echo $form->labelEx($model,'emailId');?> 
	</td>
	<td width="150px;">	<div class="">
	<?php echo $form->textField($model,'emailId',array('style'=>'width:215px','onkeydown'=>"return emailonly('PersonalEmailForm_emailId')",'maxlength'=>'50'));
	echo $form->error($model,'emailId');?>
	</div></td>
	</tr>
	<tr>
	<td>
	<?php echo $form->labelEx($model,'isahangaMail');?> 
	</td>
	<td>
	
	<?php echo $form->textField($model,'isahangaMail',array('style'=>'width:215px','onkeydown'=>"return emailonly('PersonalEmailForm_isahangaMail')",'maxlength'=>'50'));
	echo $form->error ($model,'isahangaMail');?>
	</td>
	</tr>	
	<tr>
	<td>
	<?php echo $form->labelEx($model,'reciveMail');?> 
	</td>
	<td>
	
	<?php echo $form->dropDownList($model,'reciveMail',array('1'=>'No','2'=>'Yes'),array('style'=>'width:230px',
	'onchange'=>'return Recivemail2(this.value);'));
	echo $form->error($model,'reciveMail');?>
	</td>
	</tr>
	<tr>	
	<td>
	<?php echo $form->labelEx($model,'receiveIshangaMail');?> 
	</td>
	<td>
	
	<?php echo $form->textField($model,'receiveIshangaMail',array('style'=>'width:215px','onkeydown'=>"return emailonly('PersonalEmailForm_receiveIshangaMail')",'maxlength'=>'50'));
	echo $form->error ($model,'receiveIshangaMail');?>
	</td>
	</tr>	
	</td>
	</tr>
	</table>
	</div>
	<table align="right">
	<tr> 	
	<td><?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'e&%ma','size'=>75,'class'=>'btn btn-primary')) ?></td>
	
	</tr></table>
	</div>
	</div>
	
	<script>Recivemail2(<?php echo $model->reciveMail;?>)</script>
	
	<?php $this->endWidget(); ?>
