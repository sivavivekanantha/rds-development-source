<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'ComputerSkillForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true, ),
                    )); 
                    ?>
<script>

	function popupValue(obj1,obj2,obj3,obj4,obj5)
	{	

		if(obj4!=3) // Coming Add & Edit
		{
		
			if(obj5!=0) {  // If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings").show();			
				$('#accSettings').removeClass('modal hide fade').addClass('modal hide fade in');
			}
		
		$("#ComputerSkillForm_computer").val(obj1);
		$("#ComputerSkillForm_details").val(obj2);
		$("#ComputerSkillForm_ivcSkillCode").val(obj3);		
		$("#ComputerSkillForm_action").val(obj4);		
		}
		if(obj4==3) {  // Delete
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
				$("#ComputerSkillForm_computer").val(obj1);	
				$("#ComputerSkillForm_details").val(obj2);
				$("#ComputerSkillForm_ivcSkillCode").val(obj3);
			    $("#ComputerSkillForm_action").val(obj4);
							
				$("#ComputerSkillForm").submit();
				
			}
		}
		
	}
	function popupHide()
	{		$("#errinfo").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');   // Yii Error msg is empty after close, 
			$("#ComputerSkillForm_details").val('');
			$("#ComputerSkillForm_computer").val('');	
			$('#edPopup').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
			$("#accSettings").hide();	 // Popup Hide	
			}
			
function showdove(obj,obj1)
{
	
	if(obj==1)	
			$('#showdiv').show();	
	
	if(obj==2 && obj1==1){

		var msg="Are you sure you want to save this Computer skills?";
			if(!confirm(msg)){
			return false;
			}
		else {					
	
			url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/SaveComputerSkillsFormValid?&FormValid='+obj;
		
		$("#test").load(url);	
		$('#showdiv').hide();
	
	
		}
	}
	
}
$( document ).ready(function() {
    $("#data-table span").html('');
});
	
</script>
 <div class="widget">
  <div id="formTitle">
  <div class="widget-header">
     <div class="title" style="width: 500px">
  <h4><u><?php echo $form->labelEx($model,'computerHead',array('style'=>'font-weight: bold;font-size:18px;')); ?> 	
 </u></h4>
 </div>
 </div>
 
 </div>
			<div class="widget-body">
			<div id="test"></div>
			<div class="widget">
		       <div id="dt_example" class="example_alt_pagination" style="padding:10px; margin:7px;">
			   <div id="formTitle1"><h4><u><?php echo $form->labelEx($model,'computerHint',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4></div>
			   <table style="margin-left:0px;">				   
				  <?php if(mssql_num_rows($row1)==0)  { 
					if($model->formValid<>2) $model->formValid='';?>
				<tr><td colspan="1"><?php echo $form->labelEx($model,'formValid')?></td>
				<td><?php echo $form->dropDownList($model,'formValid',array(''=>'Select','1'=>'Yes','2'=>'No'),
				array('onchange'=>'showdove(this.value,1);'))?></td>
				</tr>
<?php } ?>		</table>					 
                  <div id="showdiv" style="display:none;"><table align="right"><tr>
			
				  <td><div><a href="#accSettings" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue('','','',1,0)">Add New</a></div></td></tr></table>				
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">     
						<colgroup><col width="10%"/><col width="25%"/><col width="25%"/><col width="10%"/></colgroup>          
                        <thead><th><?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'computer',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'details',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>                                   <th><?php echo $form->labelEx($model,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                        </thead>
                        <tbody>
                          <?php $i=0;?>
                          <?php while($field=mssql_fetch_array($row1))
						  
						  { $i++;
						  ?>
                      		<tr class="gradeX">
							<td align="center"><?php echo $i; ?></td>
                            <td><?php echo $field['Skill_Language'];?></td>
							<td><?php echo $field['Skill_Details'];?></td>											
                            <td style="width:20px;" class="hidden-phone">
							
							<a href="#accSettings" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue('<?php echo $field['Skill_Language'] ?>','<?php echo $field['Skill_Details'] ?>','<?php echo $field['Skill_Code'] ?>',2,0)">&#x270E</a>
							
							<?php if(Yii::app()->session['Freeze']=="N") { ?>							
                            <a class="btn btn-success btn-small hidden-phone"  id="delbtn"   data-original-title="Delete" onclick="popupValue('<?php echo $field['Skill_Language'] ?>','<?php echo $field['Skill_Details'] ?>','<?php echo $field['Skill_Code'] ?>',3,0)">&#x2717</a>
							<?php } ?>
                            </td>
                        </tr>
					<?php } ?>
                	</tbody>
                    </table>
			<div id="accSettings" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 		<div id="errinfo" class="errorMessage" align="center" > <?php echo $msg1; ?>
			 </div>
		<div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide()"> x </button>
         	<h4><u><?php echo $form->labelEx($model,'computerPopup',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
    	</div> 
   <div class="modal-body">  
   
   		
			 
			  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->hiddenField($model,'ivcSkillCode'); ?>
			    <?php echo $form->hiddenField($model,'action'); ?>	
				<?php echo $form->labelEx($model,'computer'); ?>
                </div><div> 
				<?php echo $form->dropDownList($model,'computer',array('MS Office'=>'MS Office','Tally'=>'Tally','Animation'=>'Animation','Design'=>'Design','Photo/Video Editing'=>'Photo/Video Editing','Programming Languages'=>'Programming Languages','Hardware'=>'Hardware','Others'=>'Others'),
				array('prompt'=>'Select'),
				array('options' => array('computer'=>array('selected'=>true))));
				 echo $form->error($model,'computer');?>
                 </div>
         </div>		
		 
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 				
				<?php echo $form->labelEx($model,'details'); ?>
                </div><div>
				<?php echo $form->textField($model,'details',array('maxlength'=>'100')); ?>  
				<?php echo $form->error($model,'details'); ?>      
                 </div>		 
		  </div> 
		  
		
       
   </div>
                        
    
    <div class="modal-footer">
  
            <?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'c^8om','class'=>'btn btn-primary')); ?> 
             <button type="button" class="btn" aria-hidden="true" data-dismiss="modal"
			  onclick="popupHide()"> Close </button>
        	
   </div>
                    
</div>
</div>
<?php 

			if((strlen($model->computer)>0 or $model->errflag==1) and $model->action<>3){
				 echo "<div id='edPopup' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
			
				 echo "<script>popupValue('".$model->computer."','".$model->details."','".$model->ivcSkillCode."','".$model->action."',1)</script>"; } ?>                    
 		<div class="clearfix">	</div>
     </div>  
	 </div>  
	 </div>
	  <?php if($model->formValid>0)	
	 echo "<script>showdove('".$model->formValid."',0)</script>"; ?>
 <?php $this->endWidget(); ?>
 
 
 <?php /* -------------- Hobbies Form                                    -------------*/?>
 
 <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'HobbiesForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true, ),
                   	 )); 
                    ?>
<script>

	function popupValue7(obj1,obj2,obj3,obj4,obj5)
	{	
        
		if(obj4!=3) // Coming Add & Edit
		{
		
			if(obj5!=0) {  
			
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings7").show();			
				$('#accSettings7').removeClass('modal hide fade').addClass('modal hide fade in');
			}
		
		$("#HobbiesForm_category").val(obj1);
		$("#HobbiesForm_details").val(obj2);
		$("#HobbiesForm_ivcHobbiesCode").val(obj3);		
		$("#HobbiesForm_action2").val(obj4);		
		//$("#HobbiesForm_Others").val(obj6);
		}
		if(obj4==3) {  // Delete
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
				$("#HobbiesForm_category").val(obj1);	
				$("#HobbiesForm_details").val(obj2);
				$("#HobbiesForm_ivcHobbiesCode").val(obj3);
			    $("#HobbiesForm_action2").val(obj4);
				$("#HobbiesForm").submit();
				
			}
		}
			}
	function popupHide7()
	{		$("#errinfo7").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');   // Yii Error msg is empty after close, 
			$("#HobbiesForm_details").val('');
				$("#HobbiesForm_category").val('');	
			$('#edPopup7').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
			$("#accSettings7").hide();	 // Popup Hide	
			}
			
function showdove7(obj,obj1)
{
	
	if(obj==1)	
			$('#showdiv7').show();	
	
	if(obj==2 && obj1==1){

		var msg="Are you sure you want to save this Hobbies skills?";
			if(!confirm(msg)){
			return false;
			}
		else {					
	
			url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/SaveHobbiesSkillsFormValid?&formcode='+obj;
		
		$("#test7").load(url);	
		$('#showdiv7').hide();
	
	
		}
	}
	
}
	
</script>

  			<div class="widget-body">
			<div id="test7"></div>
			<div class="widget">
		       <div id="dt_example" class="example_alt_pagination" style="padding:10px; margin:7px;">
			   <div id="formTitle1"><h4><u><?php echo $form->labelEx($model3,'hobbiesHead',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4></div>
			   <table style="margin-left:0px;">
				   
				  <?php 
				    if(mssql_num_rows($row3)==0)  { 
					if($model3->formValid2<>2) $model3->formValid2='';?>
				<tr><td colspan="1"><?php echo $form->labelEx($model3,'formValid2')?></td>
				<td><?php echo $form->dropDownList($model3,'formValid2',array(''=>'Select','1'=>'Yes','2'=>'No'),
				array('onchange'=>'showdove7(this.value,1);'))?></td>
				</tr>
<?php } ?>		</table>					 
                  <div id="showdiv7" style="display:none;"><table align="right"><tr>
			
				  <td><div><a href="#accSettings7" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue7('','','',1,0)">Add New</a></div></td></tr></table>
				  
				 
				
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">                                        <colgroup><col width="10%"/><col width="25%"/><col width="25%"/><col width="10%"/></colgroup>          
                        <thead><th><?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                        <th><?php echo $form->labelEx($model,'category',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
						<th><?php echo $form->labelEx($model,'details',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>            
                        <th><?php echo $form->labelEx($model,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
                        </thead>
                        <tbody>
						<?php $i=0;?>
                          <?php while($field7=mssql_fetch_array($row3))
						  { $i++;?>
                          	
                      		<tr class="gradeX">
							<td align="center"><?php echo $i; ?></td>
							<td><?php if($field7['Skill_Language']==1) echo "Hobbies"; elseif($field7['Skill_Language']==2) echo "Extra Curricular Activities";elseif($field7['Skill_Language']==3) echo "Awards";elseif($field7['Skill_Language']==4) echo "Recognition";elseif($field7['Skill_Language']==5) echo "Other Skills";?></td>
							<td><?php echo $field7['Skill_Details'];?></td>											
                            <td style="width:20px;" class="hidden-phone">
							
							<a href="#accSettings7" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue7('<?php echo $field7['Skill_Language'] ?>','<?php echo $field7['Skill_Details'] ?>','<?php echo $field7['Skill_Code'] ?>',2,0)">&#x270E</a>
							
							<?php if(Yii::app()->session['Freeze']=="N") { ?>							
                            <a class="btn btn-success btn-small hidden-phone"  id="delbtn1"   data-original-title="Delete" onclick="popupValue7('<?php echo $field7['Skill_Language'] ?>','<?php echo $field7['Skill_Details'] ?>','<?php echo $field7['Skill_Code'] ?>',3,0)">&#x2717</a>
							<?php } ?>
                            </td>
                        </tr>
					<?php } ?>
                	</tbody>
                    </table>
			<div id="accSettings7" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 		<div id="errinfo7" class="error" align="center" > <?php echo $msg7; ?>
			 </div>
		<div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide7()"> x </button>
         	<h4><u><?php echo $form->labelEx($model3,'hobbiesPopup',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
    	</div> 
   <div class="modal-body">  
   
   
   		
			 
			  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->hiddenField($model3,'ivcHobbiesCode'); ?>
			    <?php echo $form->hiddenField($model3,'action2'); ?>	
				<?php echo $form->labelEx($model3,'category'); ?>
                </div><div> 
				<?php echo $form->dropDownList($model3,'category',array(''=>'Select','1'=>'Hobbies','2'=>'Extra Curricular Activities','3'=>'Awards','4'=>'Recognition','5'=>'Other Skills'),
				array('options' => array('category'=>array('selected'=>true))));
				 echo $form->error($model3,'category');?>
                 </div>			      
        </div>
		 
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 				
				<?php echo $form->labelEx($model3,'details'); ?>
                </div><div>
				<?php echo $form->textField($model3,'details',array('maxlength'=>'100')); ?>  
				<?php echo $form->error($model3,'details'); ?>      
                 </div>		 
		  </div> 
		  
		
       
   </div>
                        
    
    <div class="modal-footer">
  
            <?php echo CHtml::submitButton('Save',array('id'=>'saveform1','name'=>'d*us$','class'=>'btn btn-primary')); ?> 
             <button type="button" class="btn" aria-hidden="true" data-dismiss="modal"
			  onclick="popupHide7()"> Close </button>
        	
   </div>
                    
</div>
</div>
<?php 
			if(strlen(($model3->category)>0 or $model3->errflag==1) and $model3->action2<>3){
				 echo "<div id='edPopup7' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
				 echo "<script>popupValue7('".$model3->category."','".$model3->details."','".$model3->ivcHobbiesCode."','".$model3->action2."',1)</script>"; } ?>                    
 		<div class="clearfix">	</div>
     </div>  
	 </div>  
	 </div>
	 </div>
	  <?php if($model3->formValid2>0)	
	 echo "<script>showdove7('".$model3->formValid2."',0)</script>"; ?>
 <?php $this->endWidget(); ?>


 
