<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'MobileDataForm',
                    'enableClientValidation'=>true,
					'htmlOptions' => array(
   					'enctype' => 'multipart/form-data',    ),
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
					
<script>

	function popupValue(obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9,obj10)
	{	
		if(obj9!=3) // Coming Add & Edit
		{
			if(obj10!=0) {  // If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings").show();			
				$('#accSettings').removeClass('modal hide fade').addClass('modal hide fade in');
			}
			
						
		$("#MobileDataForm_IVC_MobileCode").val(obj1);	
		$("#MobileDataForm_mobileDetails").val(obj2);
		$("#MobileDataForm_phoneCode").val(obj3);
		$("#MobileDataForm_mobileNumber").val(obj4);
		$("#MobileDataForm_serviceProvider").val(obj5);
		$("#MobileDataForm_issuedByIsha").val(obj6);
		$("#MobileDataForm_paidByIsha").val(obj7);
		$("#MobileDataForm_paln").val(obj8);		
		$("#MobileDataForm_action").val(obj9);
		
		
		
		}
		if(obj9==3) {  // Delete
		
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
	    $("#MobileDataForm_IVC_MobileCode").val(obj1);	
		$("#MobileDataForm_mobileDetails").val(obj2);
		$("#MobileDataForm_phoneCode").val(obj3);
		$("#MobileDataForm_mobileNumber").val(obj4);
		$("#MobileDataForm_serviceProvider").val(obj5);
		$("#MobileDataForm_issuedByIsha").val(obj6);
		$("#MobileDataForm_paidByIsha").val(obj7);
		$("#MobileDataForm_paln").val(obj8);		
		$("#MobileDataForm_action").val(obj9);
		$("#MobileDataForm").submit();
			}
		}
			
		
				
				
	
	}
	function popupHide()
	{		$("#errinfo").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');   // Yii Error msg is empty after close, 
			$('#edPopup').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
			$("#accSettings").hide();	 // Popup Hide
	}
	$( document ).ready(function() {
    $("#data-table span").html('');
});
	
</script>
  <div class="widget"> 
                   <div id="formTitle">
                   <div class="widget-header">
                   <div class="title"><h4><u><?php echo $form->labelEx($model,'mobile',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4></div>	</div></div>
		  <div id="test"></div>
		  
		        <div class="widget-body">	
                 <div id="dt_example" class="example_alt_pagination">
				  <div><table align="right">
				
				  <tr>
			
				  <td><div><a href="#accSettings" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue('','','','','','','','',1,0)">Add New</a></div></td></tr></table>
				
        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">                        
                <thead><th><?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
				<th><?php echo $form->labelEx($model,'mobileDetails',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
				<th><?php echo $form->labelEx($model,'mobileNumber',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
				<th><?php echo $form->labelEx($model,'serviceProvider',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
				<th><?php echo $form->labelEx($model,'issuedByIsha',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
				<th><?php echo $form->labelEx($model,'paidByIsha',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
				<th><?php echo $form->labelEx($model,'paln',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
				<th><?php echo $form->labelEx($model,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
				</thead>
                       <tbody>
					  <?php $i=0;?>
                          <?php while($field=mssql_fetch_array($row1))
						  
						  { $i++;
						  ?>
                          	
                      		<tr class="gradeX">
							<td align="center"><?php echo $i; ?></td>
							<td><?php if($field['Mobile_Type']==1) echo "Mobile No.1"; elseif($field['Mobile_Type']==2) echo "Mobile No.2";elseif($field['Mobile_Type']==3) echo "Isha CUG Mobile No.1";elseif($field['Mobile_Type']==4) echo "Isha CUG Mobile No.2";elseif($field['Mobile_Type']==5) echo "Data Card";?></td>
							<td><?php echo $field['Phone_Code'].'-'.$field['Mobile_No']; ?></td>	
							<td><?php echo $field['Service_Provider'];?></td>
							<td><?php if($field['Issued_By'] =='Y') echo "Yes"; else if($field['Issued_By'] =='N')
							 echo "No";?></td>
							 <td><?php if($field['Paid_By'] =='Y') echo "Yes"; else if($field['Paid_By'] =='N')
							 echo "No";?></td>
							<td><?php echo $field['Mobile_Plan'];?></td>
							
							
							<td style="width:20px;" class="hidden-phone">
							<a href="#accSettings" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue('<?php echo $field['Ivc_Mobile_Code'] ?>','<?php echo $field['Mobile_Type'] ?>','<?php echo $field['PhoneCode'] ?>','<?php echo $field['Mobile_No'] ?>','<?php echo $field['Service_Provider'] ?>','<?php echo $field['Issued_By'] ?>','<?php echo $field['Paid_By'] ?>','<?php echo $field['Mobile_Plan'] ?>',2,0)">&#x270E</a>							
							<?php if(Yii::app()->session['Freeze']=="N") { ?>
							<a class="btn btn-success btn-small hidden-phone"  id="delbtn" data-original-title="Delete" onclick="popupValue('<?php echo $field['Ivc_Mobile_Code'] ?>','<?php echo $field['Mobile_Type'] ?>','<?php echo $field['PhoneCode'] ?>','<?php echo $field['Mobile_No'] ?>','<?php echo $field['Service_Provider'] ?>','<?php echo $field['Issued_By'] ?>','<?php echo $field['Paid_By'] ?>','<?php echo $field['Mobile_Plan'] ?>',3,0)">&#x2717</a>			
							<?php } ?>
                            </td>
                        </tr>
					<?php } ?>
                	</tbody>
                    </table>
					
							<div id="accSettings" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 	<div id="errinfo" class="error" align="center" > <?php echo $msg1; ?> </div>
		<div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide()"> x </button>
         	<h4><u><?php echo $form->labelEx($model,'mobile',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
           </div>       
        <div class="modal-body">
		
		
		
		 <div class="row-fluid">  
         	<div style="float:left; overflow: hidden; width:200px"> 
			<?php echo $form->hiddenField($model,'IVC_MobileCode'); ?>
			    <?php echo $form->hiddenField($model,'action'); ?>

				<?php echo $form->labelEx($model,'mobileDetails'); ?>
            </div><div> 
				<?php echo $form->dropDownList($model,'mobileDetails',array(''=>'Select','1'=>'Mobile No.1','2'=>'Mobile No.2','3'=>'Isha CUG Mobile No.1','4'=>'Isha CUG Mobile No.2','5'=>'Data Card'),
				array('options' => array('mobileDetails'=>array('selected'=>true))));
				 echo $form->error($model,'mobileDetails');?>
                 </div>			      
        </div>
		
   		  <div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
             <?php echo $form->labelEx($model,'mobileNumber');?>    
             </div><div>
			 <?php echo $form->dropDownList($model,'phoneCode',CHtml::listData($MPhoneCode,'Country_Code','Country_Name'),array('prompt'=>'Select','style'=>'width:150px'));
	echo $form->error($model,'phoneCode');?>  
	
	<?php echo $form->textField($model,'mobileNumber',array('style'=>'width:138px','onkeydown'=>"return numberonly('MobileDataForm_mobileNumber')",'maxlength'=>'10'));
	echo $form->error($model,'mobileNumber');?>
			       
        </div> </div>
		
		
		
		 <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'serviceProvider'); ?>
                </div><div>
				<?php echo $form->textField($model,'serviceProvider',array('maxlength'=>'50','onkeydown'=>"return alphanumeric('MobileDataForm_serviceProvider')")); ?>  
				<?php echo $form->error($model,'serviceProvider'); ?>      
                 </div>
		 
		  </div>
		  
          <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'issuedByIsha'); ?>
                </div><div> 
				<?php echo $form->dropDownList($model,'issuedByIsha',array(''=>'Select','Y'=>'Yes','N'=>'No'),
				array('options' => array('issuedByIsha'=>array('selected'=>true))));
				 echo $form->error($model,'issuedByIsha');?>
                 </div>
         </div>
		 
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'paidByIsha'); ?>
                </div><div> 
				<?php echo $form->dropDownList($model,'paidByIsha',array(''=>'Select','Y'=>'Yes','N'=>'No'),
				array('options' => array('paidByIsha'=>array('selected'=>true))));
				 echo $form->error($model,'paidByIsha');?>
                 </div>
         </div>
		 
		 <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'paln'); ?>
                </div><div>
				<?php echo $form->textField($model,'paln',array('maxlength'=>'50','onkeydown'=>"return alphanumeric('MobileDataForm_paln')")); ?>  
				<?php echo $form->error($model,'paln'); ?>      
                 </div>
		 
		  </div>
   </div>

     
       <div class="modal-footer">
  
            <?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'m9^&8','class'=>'btn btn-primary')); ?> 
             <button type="button" class="btn" aria-hidden="true" data-dismiss="modal" onclick="popupHide()"> Close </button>
        	
       </div></div></div>
      	
                  
<?php			
          
 if($model->action<>3 and (strlen($msg1)>3 or $model->errflag)) {
				 echo "<div id='edPopup' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
				 echo "<script>popupValue('".$model->IVC_MobileCode."','".$model->mobileDetails."','".$model->phoneCode."','".$model->mobileNumber."','".$model->serviceProvider."','".$model->issuedByIsha."','".$model->paidByIsha."','".$model->paln."',".$model->action.",1)</script>"; } ?>                    
 		<div class="clearfix"></div>
        </div></div></div>
 	
     <?php $this->endWidget(); ?>
     
    
