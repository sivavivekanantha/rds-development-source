
 
<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'PresentDepartmentForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
<script>

	function popupValue1(obj1,obj2,obj3,obj4,obj5,obj6,obj7)
	{	

		if(obj6!=3) // Coming Add & Edit
		{
			if(obj7!=0) {  // If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings1").show();			
				$('#accSettings1').removeClass('modal hide fade').addClass('modal hide fade in');
			}
		$("#DepartmentForm_departmentName1").val(obj1);
		$("#DepartmentForm_aOccupation1").val(obj2);
		$("#fromDate1").val(obj3);		
		$("#DepartmentForm_reporting1").val(obj4);		
		$("#DepartmentForm_ashramOfficialCode1").val(obj5);
		$("#DepartmentForm_action1").val(obj6);	
	
		}
		if(obj6==3) { // Delete		
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
			    $("#DepartmentForm_action1").val(obj6);				
				$("#DepartmentForm_ashramOfficialCode1").val(obj5);
				$("#DepartmentForm_departmentName1").val(obj1);
				$("#DepartmentForm_aOccupation1").val(obj2);
				$("#DepartmentForm_fromDate1").val(obj3);				
				$("#DepartmentForm_reporting1").val(obj4);				
				$("#DepartmentForm").submit();
			}
		}
		
	}
function showdove(obj,obj1)
{
	if(obj==1 )	
			$('#showdiv').show();	
	if(obj==2 && obj1==1)
	{
			var msg="Are you sure you want to save this current Department?";
			if(!confirm(msg))
			{
			return false;
			}
			else   
			{					
			url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/SaveDepartmentFormValid?&FormValid='+obj;
	
			$("#test").load(url);	
			$('#showdiv').hide();
		
			}
		}
	}	
	function popupHide1()
	{		$("#errinfo1").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');   // Yii Error msg is empty after close, 
			$('#edPopup1').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
			$("#accSettings1").hide();	 // Popup Hide	
			}	
			

</script>
  
			<div class="widget-body">
		
				    <div id="formTitle"> <h4><u>Current Department details:-  </u>	</h4></div>	
				   <table style="margin-left:0px;" width="100">
				  
				  <?php if(mssql_num_rows($row2)==0)  {			?>
				<tr><td><?php echo $form->labelEx($model1,'formValid')?></td>
				<td><?php echo $form->dropDownList($model1,'formValid',array(''=>'Select','1'=>'Yes','2'=>'No'),
				array('onchange'=>'showdove(this.value,1);'))?></td>
				</tr>
<?php } ?>		</table>					 
				<div id="dt_example" class="example_alt_pagination" style="padding:10px; margin:7px; border:dashed 1px #666666;">		
                <div id="showdiv" style="display:none;"> 
				  <table align="right"><tr>
			
				  <td><div><a href="#accSettings1" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue1('','','','','',1,0)">Add New</a></div></td></tr></table>
				  
				 
				
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">                         <colgroup><col width="5%"/><col width="10%"/><col width="10%"/><col width="10%"/><col width="10%"/><col width="10%"/><col width="10%"/></colgroup>
                        <thead>
                            <th>Department</th>
                            <th>Role In Department</th>
                            <th>From Date</th>							
							<th>Reporting To</th>							
                            <th style="width:9%" class="hidden-phone">Action</th>
                        </thead>
                        <tbody>
                          <?php 
						  //print_r(mssql_num_rows($row2));
						  while($field1=mssql_fetch_array($row2))
						  { ?>
                          	
                      		<tr class="gradeX">
                            <td><?php echo $field1['Department'];?></td>
							<td><?php echo $field1['Occupation'];?></td>
							<?php if(strlen($field1['From_Date'])==10) 
							$fromdate1 = date('d-m-Y',strtotime($field1['From_Date']));
							else $fromdate1="";
							?>
                            <td><?php echo $fromdate1;?></td>							
							<td><?php echo $field1['Reporting'];?></td>										
                            <td style="width:20px;" class="hidden-phone">
							
							<a href="#accSettings1" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue1('<?php echo $field1['Department_Code'] ?>','<?php echo $field1['Occupation'] ?>','<?php echo $field1['From_Date'] ?>','<?php echo $field1['Reporting'] ?>','<?php echo $field1['Ashram_Official_Code'] ?>',2,0)">&#x270E</a>
							
							
							
                            <a class="btn btn-success btn-small hidden-phone"  id="delbtn"   data-original-title="Delete" onclick="popupValue1('<?php echo $field1['Department_Code'] ?>','<?php echo $field1['Occupation'] ?>','<?php echo $field1['From_Date'] ?>','<?php echo $field1['Reporting'];?>','<?php echo $field1['Ashram_Official_Code'] ?>',3,0)">&#x2717</a>
                            </td>
                        </tr>
					<?php } ?>
                	</tbody>
                    </table>
			<div id="accSettings1" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 		<div id="errinfo1" class="error" align="center" > <?php echo $msg2; ?>
			 </div>
		<div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide1()"> x </button>
         	<h4 id="myModalLabel1">Department</h4>
    	</div> 
   <div class="modal-body">
   		<div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
             <?php echo $form->hiddenField($model1,'ashramOfficialCode1'); ?>
			   <?php echo $form->hiddenField($model1,'action1'); ?>			  
			 <?php echo $form->labelEx($model1,'departmentName1'); ?>
             </div><div>
			 <?php echo $form->dropDownList($model1,'departmentName1',CHtml::listData($department,'Department_Code','Department'),array('prompt'=>'Select'));
			 echo $form->error($model1,'departmentName1');
			 ?>
			 
             </div>
			 </div> 
			 
			  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model1,'aOccupation1'); ?>
                </div><div>
				<?php echo $form->textField($model1,'aOccupation1',array('onkeydown'=>"return alphaonly('DepartmentForm_aOccupation1')",'maxlength'=>'100')); ?>  
				<?php echo $form->error($model1,'aOccupation1'); ?>      
                 </div>
		 
		  </div>
		   
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
					<?php echo $form->labelEx($model1,'fromDate1');?>   
                </div><div>
                	<a href="javascript:NewCal('fromDate1','ddmmyyyy')">
					<?php echo $form->textField($model1,'fromDate1',array('id'=>'fromDate1')); ?>
					<img src="../../images/cal.gif" width="16" height="16" border="0" 
					alt="Pick a date"></a>  
					<?php echo $form->error($model1,'fromDate1'); ?>   
                 </div>
		  </div>		   
		  
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model1,'reporting1'); ?>
                </div><div>
				<?php echo $form->textField($model1,'reporting1' ,array('onkeydown'=>"return alphaonly('DepartmentForm_reporting1')")); ?>  
				<?php echo $form->error($model1,'reporting1'); ?>      
                 </div>
		 
		  </div> 
		  
       
   </div>
                        
    
    <div class="modal-footer">
  
            <?php echo CHtml::submitButton('Save',array('id'=>'saveform','class'=>'btn btn-primary')); ?> 
             <button type="button" class="btn" aria-hidden="true" data-dismiss="modal"
			  onclick="popupHide1()"> Close </button>
        	
   </div>
                    
</div>
</div>
<?php 

			if($model1->departmentName1>0 and $model1->action1<>3 and strlen($msg2)>3){
				
				 echo "<div id='edPopup1' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
				 echo "<script>popupValue1('".$model1->departmentName1."','".$model1->aOccupation1."','".$model1->fromDate1."','".$model1->reporting1."','".$model1->ashramOfficialCode1."','".$model1->action1."',1)</script>"; } ?>                    
 		<div class="clearfix">	</div>
     </div>    
	 </div>
	 	 <?php  print_r("hi".$model1->formValid) ;
		 if($model1->formValid>0){
		
		  echo "<script>showdove('".$model1->formValid."',0)</script>";
		 } 
 $this->endWidget(); ?>
 
<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'PastDepartmentForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
<script>

	function popupValue(obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9)
	{	
	
		if(obj8!=3) // Coming Add & Edit
		{
			if(obj9!=0) {  // If u click edit background is fade
				$("#fade_form").attr("class", "modal-backdrop fade in");
				$("#accSettings").show();			
				$('#accSettings').removeClass('modal hide fade').addClass('modal hide fade in');
			}
		$("#PastDepartmentForm_departmentName").val(obj1);
		$("#PastDepartmentForm_aOccupation").val(obj2);
		$("#fromDate").val(obj3);
		$("#toDate").val(obj4);
		$("#PastDepartmentForm_wasReporting").val(obj5);
		$("#PastDepartmentForm_aRemarks").val(obj6);
		$("#PastDepartmentForm_ashramOfficialCode").val(obj7);
		$("#PastDepartmentForm_action").val(obj8);		
		}
		if(obj8==3) {  // Delete
			var msg="Are you sure you want to delete this record?";
			if(!confirm(msg)){
			return false;
			}
			else {	
			    $("#PastDepartmentForm_action").val(obj8);				
				$("#PastDepartmentForm_ashramOfficialCode").val(obj7);
				$("#PastDepartmentForm_departmentName").val(obj1);
				$("#PastDepartmentForm_aOccupation").val(obj2);
				$("#PastDepartmentForm_fromDate").val(obj3);
				$("#PastDepartmentForm_toDate").val(obj4);
				$("#PastDepartmentForm_wasReporting").val(obj5);
				$("#PastDepartmentForm_aRemarks").val(obj6);
				$("#PastDepartmentForm").submit();
			}
		}
		
	}
	function popupHide()
	{		$("#errinfo").html('');   // DB msg empty for next record clicking. But $msg1 is not empty
			$(".errorMessage").html('');   // Yii Error msg is empty after close, 
			$('#edPopup').removeClass('modal hide fade in').addClass('modal hide fade'); // After close popup fade in remove
			$("#accSettings").hide();	 // Popup Hide	
			}
			
			

</script>
  
			<div class="widget-body">
			 <div id="test"> </div>
		        <div id="dt_example" class="example_alt_pagination" style="padding:10px; margin:7px; border:dashed 1px #666666;">			
				   <div id="formTitle8"> <h4><u>Department History:( Please fill your past department info in ISHA ) </u>	</h4></div>	
				   			 
                   <div id="showdiv1	" style="display:block;"> 
                  <table align="right"><tr>
			
				  <td><div><a href="#accSettings" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="popupValue('','','','','','','',1,0)">Add New</a></div></td></tr></table>
				  
				 
				
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">                        
                        <thead>
                            <th>Department</th>
                            <th>Role In Department</th>
                            <th>From Date</th>
							<th>To Date</th>
							<th>Was Reporting To</th>
							<th>Remarks</th>
                            <th style="width:9%" class="hidden-phone">Action</th>
                        </thead>
                        <tbody>
                          <?php while($field=mssql_fetch_array($row1))
						  { ?>
                          	
                      		<tr class="gradeX">
                            <td><?php echo $field['Department'];?></td>
							<td><?php echo $field['Occupation'];?></td>
							<?php if(strlen($field['From_Date'])==10) 
							$fromdate=date('d-m-Y',strtotime($field['From_Date']));
							else $fromdate='';?>
                            <td><?php echo $fromdate;?></td>
							<?php if(strlen($field['To_Date'])==10) 
							$todate=date('d-m-Y',strtotime($field['To_Date'])); else $todate=''; ?>
                            <td><?php echo $todate;?></td>
							<td><?php echo $field['WasReporting'];?></td>
							<td><?php echo $field['Remarks'];?></td>						
                            <td style="width:20px;" class="hidden-phone">
							
							<a href="#accSettings" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="popupValue('<?php echo $field['Department_Code'] ?>','<?php echo $field['Occupation'] ?>','<?php echo $field['From_Date'] ?>','<?php echo $field['To_Date'] ?>','<?php echo $field['WasReporting'] ?>','<?php echo $field['Remarks'] ?>','<?php echo $field['Ashram_Official_Code'] ?>',2,0)">&#x270E</a>
							
							
							
                            <a class="btn btn-success btn-small hidden-phone"  id="delbtn"   data-original-title="Delete" onclick="popupValue('<?php echo $field['Department_Code'] ?>','<?php echo $field['Occupation'] ?>','<?php echo $field['From_Date'] ?>','<?php echo $field['To_Date'];?>','<?php echo $field['WasReporting'];?>','<?php echo $field['Remarks'];?>','<?php echo $field['Ashram_Official_Code'] ?>',3,0)">&#x2717</a>
                            </td>
                        </tr>
					<?php } ?>
                	</tbody>
                    </table>
			<div id="accSettings" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	 		<div id="errinfo" class="error" align="center" > <?php echo $msg1; ?>
			 </div>
		<div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="popupHide()"> x </button>
         	<h4 id="myModalLabel1">Department</h4>
    	</div> 
   <div class="modal-body">
   		<div class="row-fluid">  
        	 <div style="float:left; overflow: hidden; width:200px"> 
             <?php echo $form->hiddenField($model,'ashramOfficialCode'); ?>
			   <?php echo $form->hiddenField($model,'action'); ?>			  
			 <?php echo $form->labelEx($model,'departmentName'); ?>
             </div><div>
			 <?php echo $form->dropDownList($model,'departmentName',CHtml::listData($department,'Department_Code','Department'),array('prompt'=>'Select'));
			 echo $form->error($model,'departmentName');
			 ?>
			 
             </div>
			 </div> 
			 
			  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'aOccupation'); ?>
                </div><div>
				<?php echo $form->textField($model,'aOccupation',array('onkeydown'=>"return alphaonly('PastDepartmentForm_aOccupation')",'maxlength'=>'100')); ?>  
				<?php echo $form->error($model,'aOccupation'); ?>      
                 </div>
		 
		  </div>
		   
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
					<?php echo $form->labelEx($model,'fromDate');?>   
                </div><div>
                	<a href="javascript:NewCal('fromDate','ddmmyyyy')">
					<?php echo $form->textField($model,'fromDate',array('id'=>'fromDate','onkeydown'=>"return periodonly('fromDate')")); ?>
					<img src="../../images/cal.gif" width="16" height="16" border="0" alt="Pick a date"></a>  
					<?php echo $form->error($model,'fromDate'); ?>   
                 </div>
		  </div>
		  
		   <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
					<?php echo $form->labelEx($model,'toDate');?>   
                </div><div>
                	<a href="javascript:NewCal('toDate','ddmmyyyy')">
					<?php echo $form->textField($model,'toDate',array('id'=>'toDate')); ?>
					<img src="../../images/cal.gif" width="16" height="16" border="0" 
					alt="Pick a date"></a>  
					<?php echo $form->error($model,'toDate'); ?>   
                 </div>
		  </div>
		  
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'wasReporting'); ?>
                </div><div>
				<?php echo $form->textField($model,'wasReporting',array('onkeydown'=>"return alphaonly('PastDepartmentForm_wasReporting')")); ?>  
				<?php echo $form->error($model,'wasReporting'); ?>      
                 </div>
		 
		  </div>
		  
		  <div class="row-fluid">                                 
                <div style="float:left; overflow: hidden; width:200px"> 
				<?php echo $form->labelEx($model,'aRemarks'); ?>
                </div><div>
				<?php echo $form->textArea($model,'aRemarks',array('placeholder'=>'max. Length 500 chars.','maxlength'=>'500'));?>
				<?php echo $form->error($model,'aRemarks'); ?>      
                 </div>
		 
		  </div>
       
   </div>
                        
    
    <div class="modal-footer">
  
            <?php echo CHtml::submitButton('Save',array('id'=>'saveform','class'=>'btn btn-primary')); ?> 
             <button type="button" class="btn" aria-hidden="true" data-dismiss="modal"
			  onclick="popupHide()"> Close </button>
        	
   </div>
                    
</div>

<?php 
			if($model->departmentName>0 and $model->action<>3 and strlen($msg1)>3){
				 echo "<div id='edPopup' class='modal-backdrop fade in'></div>"; // This for background fade effect after submition
				 echo "<script>popupValue('".$model->departmentName."','".$model->aOccupation."','".$model->fromDate."','".$model->toDate."','".$model->wasReporting."','".$model->aRemarks."','".$model->ashramOfficialCode."','".$model->action."',1)</script>"; } ?>                    
 		<div class="clearfix">	</div>
     </div>    
	 </div>
	 </div>
	 
 <?php $this->endWidget(); ?>
 
 


 
