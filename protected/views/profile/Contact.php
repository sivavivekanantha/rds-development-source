	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'ContactForm',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); 
 ?>
<style type="text/css" >
 th {
    background-color: rgb(54, 54, 54);
    color: rgb(255, 255, 255);
    font-size: 12px;
    font-weight: bold;
    height: 20px;
    padding: 2px;
    text-align: center;
}
.row1
{
background:#E4E4E4;
}
.row2
{
background:white;
}
</style>
<script type="text/javascript">
$( document ).ready(function() {
    $("#data-table span").html('');
});

	function ajaxState(obj,obj1)
	{	
		if(obj=='') return false;
	
		var url='<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/Ajaxsate?countryCode='+obj+'&statecode='+obj1;
		$("#state").load(url);			
	}
	function ajaxDistrict(obj,obj1)
	{
		
	if(obj=='') return false;
	if(obj==9999) 
	{
		ajaxCity(0,0,obj);
		$('#otherState').show();
	}
	else {
		ajaxCity(0,0,obj);
		$('#otherState').hide();
	    }
	
	
		$('#ContactForm_oState').val('');
		var url='<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/Ajaxdistrict?stateCode='+obj+'&DistrictCode='+obj1;		
		$("#district").load(url);
		
		
	}
	function ajaxCity(obj,obj1,obj2)
	{  
		 
		if(obj1==0) obj1 = $("#state").val();
		if(obj=='' && obj2=='') return false;	
		if(obj2==9999) $("#city").val();
		var url='<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/AjaxCity?districtCode='+obj+'&stateCode='+obj1+'&CityCode='+obj2;	
		$("#city").load(url);
			
		}
		function ajaxothercity(obj)
		{		
			if(obj==99999)
			{
				 $('#otherCity').show();
			     $('#ContactForm_oCity').val('');
			 }
			 else
			  {
			 	 $('#otherCity').hide();
			 	$('#ContactForm_oCity').val('');
			 	
			 }
			
		}
		
	function editdata(obj,obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9,obj10,obj11,obj12,obj13)
	{   
	    $(".error").html('');   // Yii Error msg is empty after close, 
		if(obj6>0) ajaxState(obj6,obj7);
		if(obj7>0) ajaxDistrict(obj7,obj8);
		if(obj8>0) ajaxCity(obj8,obj7,obj9);
		else if(obj8=='') ajaxCity(obj8,obj7,obj9);
		$('#ContactForm_IVCEmergencyCode').val(obj);		
		$('#ContactForm_action').val(obj1);
		$('#ContactForm_ContactType').val(obj2);
		$('#ContactForm_Name').val(obj3);
		$('#ContactForm_Relationship').val(obj4);
		//$('#ContactForm_Address').val(obj5);
		$('#ContactForm_Address').val(unescape(obj5));			
		$('#ContactForm_country').val(obj6);		
		$('#ContactForm_state').val(obj7);
		$('#ContactForm_district').val(obj8);
		$('#ContactForm_city').val(obj9);
		$('#ContactForm_pincode').val(obj10);
		$('#ContactForm_PhoneCode').val(obj11);
		$('#ContactForm_ContactNo').val(obj12);
		$('#ContactForm_EmailId').val(obj13);
	Formshow(3,0);
		
	}
	function deldata(obj,obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9,obj10,obj11,obj12,obj13)
	{
		var msg="Are you sure you want to delete this record?";
		if(!confirm(msg)){
			return false;
		}
	else 
	{
	
		if(obj6>0) ajaxState(obj6,obj7);
		if(obj7>0) ajaxDistrict(obj7,obj8);
		if(obj8>0) ajaxCity(obj8,obj6,obj9);
		$('#ContactForm_IVCEmergencyCode').val(obj);		
		$('#ContactForm_action').val(obj1);
		$('#ContactForm_ContactType').val(obj2);
		$('#ContactForm_Name').val(obj3);
		$('#ContactForm_Relationship').val(obj4);
		//$('#ContactForm_Address').val(obj5);	
		$('#ContactForm_Address').val(unescape(obj5));				
		$('#ContactForm_country').val(obj6);		
		$('#ContactForm_state').val(obj7);
		$('#ContactForm_district').val(obj8);
		$('#ContactForm_city').val(obj9);
		if(obj10=='' || obj10==0) obj10=102;
		$('#ContactForm_pincode').val(obj10);
		$('#ContactForm_ContactNo').val(obj11);
		$('#ContactForm_PhoneCode').val(obj12);
		$('#ContactForm_EmailId').val(obj13);
	
		$('#ContactForm').submit();
		
	}
	}
	function Formshow(obj,obj1)
	{
		if(obj==1 || obj==3){
			$('#Formshow').show();
		
		}
		if(obj==2){	
		 	$(".error").html('');	
			$(".errorMessage").html('');			
			 // Yii Error msg is empty after close, 
			$('#ContactForm_action').val('');
			$('#ContactForm_ContactType').val('');
			$('#ContactForm_Name').val('');
			$('#Formshow').hide();
		}
		if(obj!=3 && obj1==0 ){
			
		
			$('#ContactForm_ContactType').val('');
			$('#ContactForm_Name').val('');
			$('#ContactForm_Relationship').val('');
			$('#ContactForm_Address').val('');		
			$('#ContactForm_country').val('');		
			$('#state').val('');
			$('#ContactForm_oState').val('');		
			$('#district').val('');
			$('#city').val('');
			$('#ContactForm_oCity').val('');	
			$('#otherState').hide();
		    $('#otherCity').hide();	
			$('#ContactForm_pincode').val('');
			$('#ContactForm_ContactNo').val('');
			$('#ContactForm_PhoneCode').val('');
			$('#ContactForm_EmailId').val('');	
		}
		
	}
</script>
<div class="widget">
 <div class="widget-header">
  <div class="title">
  <div class="formTitle">
  <h4><u><?php echo $form->labelEx($model,'contact',array('style'=>'font-weight: bold;font-size:18px;')); ?></u></h4>
  </div></div></div><br><br>
	<div>
		<div align="left"><h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please provide </h6></div>
		<th><?php echo $form->labelEx($model,'EmrContact',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
		<th><?php echo $form->labelEx($model,'ContactPerson',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
		<th><?php echo $form->labelEx($model,'EmrRes',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
	</div>
	<div class="row-fluid">
		<table align="right">
			<tr>
				<td>
					<div>
						<a href="#accSettings" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="" onclick="Formshow(1,0)">Add New</a>
					</div>
				</td>
			</tr>
		</table>
	</div>
 <div class="row-fluid">
 
<div id="Formshow" style="display: none;"> 
<div class="widget"> 
     <table width="100%" border="0" cellpadding="1" style="padding:10px; margin:7px";>
	 <tr><td colspan="4" align="center"><table><?php echo $msg1;?></table></td></tr>
	   <?php echo $form->hiddenField($model,'IVCEmergencyCode'); ?>
	   <?php echo $form->hiddenField($model,'action'); ?>
	<tr>
    <td><?php echo $form->labelEx($model,'ContactType');?></td>
    <td colspan="3"><div><?php echo $form->dropDownList($model,'ContactType',array(''=>'Select','1'=>'Emergency contact address','2'=>'Emergency contact person in your family circle who is supportive of your involvement in Isha','3'=>'Residential address other than Isha where you usually visit during breaks'),array('style'=>'width:700px;'),array('options' => array('ContactType'=>array('selected'=>true)))); ?>
    <?php echo $form->error($model,'ContactType');?></div></td>
  </tr>
  
  <div class="row-fluid">
  <tr>
    <td><div><?php echo $form->labelEx($model,'Name');?></div></td>
    <td><div> <?php echo $form->textField($model,'Name',array('onkeydown'=>"return alphaonly('ContactForm_Name')",'maxlength'=>'50'));?>
				<?php echo $form->error($model,'Name');?>
	</div></td>
    <td><div><?php echo $form->labelEx($model,'Relationship');?></div></td>
    <td><div><?php echo $form->textField($model,'Relationship',array('onkeydown'=>"return alphaonly('ContactForm_Relationship')",'maxlength'=>'50'));?>
	<?php echo $form->error($model,'Relationship');?></div></td>
  </tr>
  </div>
  <tr>
     <td><?php echo $form->labelEx($model,'Address');?></td>
     <td><?php echo $form->textArea($model,'Address',array('placeholder'=>'max. Length 500 chars.','maxlength'=>'500'));?></td>
    <td><?php echo $form->labelEx($model,'country');?></td>
    <td><?php echo $form->dropDownList($model,'country',CHtml::listData($country,'Country_Code','Country_Name'),array('prompt'=>'Select','onchange'=>'ajaxState(this.value,0)')); ?></td>
  </tr>
 <tr>
       <td><?php echo $form->labelEx($model,'state');?></td>
     <td><?php echo $form->dropDownList($model,'state',array(''=>'Select'),array('id'=>'state','onchange'=>'ajaxDistrict(this.value)'));
	 ?>
	 	<div id='otherState' style="display: none;">
		<?php echo $form->textField($model,'oState',array('maxlength'=>'50','onkeydown'=>"return alphaonly('ContactForm_oState')"));
		echo $form->error($model,'oState');?></div>
	 </td>
       <td><?php echo $form->labelEx($model,'district');?></td>
    <td><?php echo $form->dropDownList($model,'district',array(''=>'Select'),array('id'=>'district','onchange'=>'ajaxCity(this.value,0,0)'));?></td>
  </tr>
  <tr>
    <td><?php echo $form->labelEx($model,'city');?></td>
       <td><?php echo $form->dropDownList($model,'city',array(''=>'Select'),array('id'=>'city','onchange'=>'ajaxothercity(this.value)'));?>
	   	<div id='otherCity' style="display: none;">
		<?php echo $form->textField($model,'oCity',array('maxlength'=>'50','onkeydown'=>"return alphaonly('ContactForm_oCity')"));
		echo $form->error($model,'oCity');
		?></div>
		
	   </td>
     <td><?php echo $form->labelEx($model,'pincode');?>
	 	
		
	 </td>
	 
	 <td><?php echo $form->textField($model,'pincode',array('onkeydown'=>"return alphanumeric('ContactForm_pincode')",'maxlength'=>'10'));?></td>
  </tr>
  <tr>
    <td><?php echo $form->labelEx($model,'ContactNo');?></td>
	<td>
	<table>
		<tr>
			<td><?php echo $form->dropDownList($model,'PhoneCode',CHtml::listData($Phonecode,'Country_Code','Country_Name'),array('prompt'=>'Select','style'=>'width:100px;'))  ?></td>
			<td><?php echo $form->textField($model,'ContactNo',array('style'=>'width:105px;','onkeydown'=>"return numberonly('ContactForm_ContactNo')",'maxlength'=>'10'));
 ?></td>
		</tr>
	</table>
	</td>
    
   <td><?php echo $form->labelEx($model,'EmailId');?></td>
    <td ><?php echo $form->textField($model,'EmailId',array('onkeydown'=>"return emailonly('ContactForm_EmailId')"));
				echo $form->error($model,'EmailId');?>
		
	</td>
  </tr><br></br>
	<table align="right">  <tr>
    	<td><?php echo CHtml::submitButton('Save',array('id'=>'saveform','name'=>'c!2@3','size'=>75,'class'=>'btn btn-primary')) ?>	</td>
			<td><?php echo CHtml::button('Cancel',array('size'=>75,'class'=>'btn','onClick'=>'Formshow(2)')) ?></td>
	
    </tr>
	</table>
</table></div>
</div>

<div class="widget">
<table width="100%" border="0" align="left" cellpadding="2" cellspacing="1" id="data-table">
<colgroup><col width=10% /><col width=6% /><col width=10% /><col width=15% /><col width=10% /><col width=10% /><col width=15% /></colgroup>
    <th><?php echo $form->labelEx($model,'sno',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
	<th><?php echo $form->labelEx($model,'Name',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
	<th><?php echo $form->labelEx($model,'Relationship',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
	<th><?php echo $form->labelEx($model,'ContactNo',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
	<th><?php echo $form->labelEx($model,'EmailId',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
	<th><?php echo $form->labelEx($model,'Address',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
	<th><?php echo $form->labelEx($model,'ContactType',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
	<th><?php echo $form->labelEx($model,'action',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
    <?php $i=0;
	$colorflag=0;
	if(@mssql_num_rows($row1)>0) { 
		
        	while($field1=mssql_fetch_array($row1)) { 
			$i++;
			$colorflag++;
			
			?>
      <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?>>
      
       <td align="center"><?php echo $i;?></td>
	   
	   
	   <td align="center"><?php echo $field1['Rel_Name'];?> </td>
	   <td align="center"><?php echo $field1['Relationship'];?></td> 	 						
	   <?php if(strlen($field1['Rel_Contact']>0))  $d="-";else $d=''; ?>
	   <td align="center" style="width:150px;" wrap><?php echo $field1['Phone_Code'].$d.$field1['Rel_Contact']; ?></td>	
	   <td align="center" style="width:150px;" wrap><?php echo $field1['Rel_Emailid'];?></td>
	   <td align="center" style="width:150px;" wrap><?php echo $field1['Rel_Address'].'<br>'.
		 $field1['City_Name'].'<br>'.$field1['State_Name'].'<br>'.$field1['Country_Name'];?></td>	
		 	 <td align="center" style="width:150px;" wrap>
			 <?php if($field1['Emergency_Add_Type']==1) echo "Emergency contact"; elseif($field1['Emergency_Add_Type']==2) echo "Emergency contact person-supportive of your involvement in isha"; elseif($field1['Emergency_Add_Type']==3) echo "Residential address other than Isha";?>
			 	
				
			 </td>
	 <td align="center"><a href="#" role="button" class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='Edit' onclick="editdata('<?php echo $field1['IVC_Emergency_Code']?>',2,'<?php echo $field1['Emergency_Add_Type']?>','<?php echo $field1['Rel_Name']?>','<?php echo $field1['Relationship']?>','<?php echo rawurlencode($field1['Rel_Address']);?>','<?php echo $field1['Rel_Country']?>','<?php echo $field1['Rel_State']?>','<?php echo $field1['Rel_District']?>','<?php echo $field1['Rel_City']?>','<?php echo $field1['Rel_Zip_Code']?>','<?php echo $field1['Rel_Phone_Code']?>','<?php echo $field1['Rel_Contact'] ?>','<?php echo $field1['Rel_Emailid'] ?>')">&#x270E</a> 
	   <?php if(Yii::app()->session['Freeze']=="N") { ?>
	   <a class="btn btn-success btn-small hidden-phone"  id="delbtn"   data-original-title="Delete" onclick="deldata('<?php echo $field1['IVC_Emergency_Code']?>',3,'<?php echo $field1['Emergency_Add_Type']?>','<?php echo $field1['Rel_Name']?>','<?php echo $field1['Relationship']?>','<?php echo rawurlencode($field1['Rel_Address']);?>','<?php echo $field1['Rel_Country']?>','<?php echo $field1['Rel_State']?>','<?php echo $field1['Rel_District']?>','<?php echo $field1['Rel_City']?>','<?php echo $field1['Rel_Zip_Code']?>','<?php echo $field1['Rel_Phone_Code']?>','<?php echo $field1['Rel_Contact'] ?>','<?php echo $field1['Rel_Emailid'] ?>')">&#x2717</a></td>	
	   
       <?php 	} }
        } 
		//else '<script>'.editdata('','','','','','','','','','','','','','').'</script>';
        ?></tr>
</table></div>
</div>
</div>
<script>ajaxState('<?php echo $model->country ?>','<?php echo $model->state ?>')</script>
<script>ajaxDistrict('<?php echo $model->state ?>','<?php echo $model->district ?>')</script>
<?php if($model->state>0) { ?>
<script>ajaxCity('<?php echo $model->district ?>','<?php echo $model->state ?>','<?php echo $model->city ?>')</script>
<?php }

if(strlen($msg1)>0 or $model->errflag>0) { ?>
	<script>Formshow('1','<?php echo $model->errflag ?>')</script>
<?php } 
if($model->action==3) { ?>
	<script>Formshow('2','0')</script>
<?php } 
if($model->city=='99999' and $model->errflag>0 )
 { ?>
<script>ajaxothercity('<?php echo $model->city ?>')</script> 
<?php }
if($model->state=='9999' and $model->errflag>0 )
 { ?>
<script>ajaxDistrict('<?php echo $model->state ?>','0')</script> 
<?php }
 $this->endWidget(); ?>