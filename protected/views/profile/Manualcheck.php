<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'ManualForm',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
	 <div id="formTitle"> <h3><u>RFID DISTRIBUTION </u>	</h3></div>				
<table width="60%" border="0" cellpadding="13" style="padding:0px;margin:50px; margin-left:200px; border:dashed 1px #666666;" align="center" >

<tr>

      <td><?php echo $form->labelEx($model,'manualcheck',array('style'=>'margin-bottom:80px;'));?></td>
     <td><div><?php echo $form->textArea($model,'manualcheck',array('style'=>'width: 450px; height: 103px;','maxlength'=>'500'));
	 echo $form->error($model,'manualcheck');
	 ?> </div>
	 <span class="help-inline "> For Ex :IYC0001,IYC0002,IYC0003 </span></td>
	
   </tr>

  <tr><td colspan="2"><table align="right"><tr><td><?php echo CHtml::submitButton('CheckIn',array('name' => 'CheckIn','class'=>'btn btn-primary')); ?></td> 
  		<td><?php echo CHtml::submitButton('Checkout',array('name' => 'Checkout','class'=>'btn btn-primary')); ?> </td></tr></table></td>
  
  </tr>
   </table>

  	<?php if(strlen($msg1)>0) {?>
					<div class="row-fluid" style="padding-left: 200px;">
            			<div class="span4">
              				<div class="widget">
                				<div class="widget-header">
                  					<div class="title">
                    					<span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span> Result
                  					</div>
                				</div>
                				<div class="widget-body">
                  					<div id="dt_example" class="example_alt_pagination">
                    					<table class="table table-condensed table-striped table-hover table-bordered" id="data-table">
                    				    <tr class="gradeA success">
										<td>
                            				<?php echo $msg1; ?> 
                          				</td></tr>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>

 <?php $this->endWidget(); ?>
