 	<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'AttendenceCount',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); 
 ?>
 <html lang="en">
 <script>
 	function dynamicreport()
	{		
		$.ajax({
	    type: 'GET',
		url: '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/ajaxpersoncountreport',
		success: function(result)
		{
			
			$('#showDynamicReport').html(result);
			
		}
	  });		
	}
	function showtotal()
	{		
		$.ajax({
	    type: 'GET',
		url: '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/ajaxtotalpersoncount',
		success: function(result)
		{
			
			$('#showtoalreport').html(result);
			
		}
	  });		
	}
	
 </script>

 <div class="row-fluid">
            <div class="span12">
              <div class="widget">
                <div class="widget-header">
				
                  <div class="title">
				  <table>
				  	<tr>
						<td><span class="fs1" aria-hidden="true" data-icon="&#xe098;"></span> Resident Movement</td>
						<td style="padding-left:800px; "><a href='javascript:void(0)' title="Refresh">
							<span class="fs1" data-icon="&#xe11c;" onclick="chardetails();" ></span></a></td>
					</tr>
				  </table>
                    
                  </div>
                </div>
				 <div id="showDynamicReport">
                <div class="widget-body" >
              	
				
			   </div>
            
                </div>
				  
				
              </div>
			    <div id="showtoalreport" align="center"></div>  
            </div>
          </div>

<head>
  
   
	
    <script type="text/javascript">
	
        function sumchart() 
		{
			
			$.ajax({
	    type: 'GET',
		url: '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/ajaxSummarychartreport',
		success: function(result)
		{
			
			
			var sampleData = result;
            // prepare jqxChart settings
            var settings = {
                title: "Resident movement",
                description: "",
                enableAnimations: false,
                showLegend: true,
                padding: { left: 5, top: 5, right: 5, bottom: 5 },
                titlePadding: { left: 90, top: 0, right: 0, bottom: 10 },
                source: result,
                xAxis:
                    {
                        dataField: 'Category',
                        showGridLines: true
                    },
                colorScheme: 'scheme01',
                seriesGroups:
                    [
                        {
                            type: 'column',
                            columnsGapPercent: 50,
                            seriesGapPercent: 0,
                            valueAxis:
                            {
                                unitInterval: 100,
                                minValue: 0,
                                maxValue: 500,
                                displayValueAxis: true,
                                description: 'Resident Movement',
                                axisSize: 'auto',
                                tickMarksColor: '#888888'
                            },
                            series: [
                                    { dataField: 'CheckIN', displayText: 'CheckIN' },
                                  
                                    { dataField: 'CheckOUT', displayText: 'CheckOUT' }
                            ]
                        }
                    ]
            };
           
            
            
            // setup the chart
            $('#jqxChart').jqxChart(settings);
			}
	  });
}
       
 function chardetails()
  {
   	dynamicreport();
	showtotal();
	sumchart();
		
   }
    </script>
	
</head>
<body class='default' onload="chardetails();">
<table align="center">
	<tr><td>
		  <div id='jqxChart' style="width: 850px; height: 500px;" align="center">
    </div>
	</td></tr>
</table>
  
    
</body>
</html>
		  <?php $this->endWidget(); ?>