	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'UserTypeForm',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	)); 
 
$row = array();
while($field=mssql_fetch_array($ulevel)){ 
	$row[] = array(
		'UserType' => $field['User_Type']
    	   
	);
}
$result = json_encode($row); ?>
		
<script type="text/javascript">
	$(document).ready(function () { 
	
			var source =
			{
				datatype: "local",
				datafields: [
					{ name: 'UserType'}
				],
				localdata: <?php echo $result; ?>,
			};
			var dataAdapter = new $.jqx.dataAdapter(source);
			// Create a jqxComboBox
			$("#jqxComboBox").jqxComboBox({ 
					selectedIndex: 0, 
					source: dataAdapter, 
					displayMember: "UserType",
					valueMember: "UserType", 
					width: 200,
					height: 25});
				
				
			/*	 $("#jqxComboBox").on('select', function (event) {
			if (event.args) {
			var item = event.args.item;
			if (item) {
			var item = $("#jqxComboBox").jqxComboBox('getSelectedItem'); 	
			$("#UserTypeForm_usertype").val(item.label);
			}
			}
			}); */
								
		});
				
		
	function getmenus()
	{
		$("#Error_Text").html('');
		$("#checkedItemsLog").html('');
		$("#checkedItemsLog1").html('');
		$("#checkedItemsLog3").html('');
		var cmbval = $("#jqxComboBox").jqxComboBox('val');
		if(cmbval==" ") 
		{	
			$("#jqxDropDownList1").jqxDropDownList('clearSelection', true);
			$("#jqxDropDownList2").jqxDropDownList('clearSelection', true);
			$("#jqxDropDownList4").jqxDropDownList('clearSelection', true);
			$("#Assign_Type_Div").hide();     	
			alert('Please choose or type user level');
			return false;
		}	
		
	
	
		$("#View_Assign_Type").show();
		$("#View_Assign_Type_Button").show();
		$("#Assign_Type_Div").show();
		$("#UserTypeForm_editmenucode").val('');
		$("#UserTypeForm_accessmenucode").val('');	
		$("#UserTypeForm_viewmenucode").val('');
	
		//var item = $("#jqxComboBox").jqxComboBox('getSelectedItem');
		var ulevel = $("#jqxComboBox").jqxComboBox('val');

		$.ajax({
				type: 'GET',
				data: {'UserType':ulevel},
				url : '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/ajaxUserMenu',
				//alert(url);
				success: function(result)
				{ 
					//console.log(result[2]);
					var source =
					{
						localdata: result[0],
						datatype: "json",
						datafields: [ 
							{ name: 'Menu_Name'},
							{ name: 'Menu_Code'}
						],
				
						async: false
				
					};
					/* View Drop down box */				
					var dataAdapter = new $.jqx.dataAdapter(source);
					$("#jqxDropDownList1").jqxDropDownList(
						{
							checkboxes: true,
							source: dataAdapter,
							width: 200,
							height: 25,
							displayMember: 'Menu_Name',
							valueMember: 'Menu_Code'
			
						});
	
	
					/* EDIT Drop down box */
					$("#jqxDropDownList2").jqxDropDownList(
						{
							checkboxes: true,
							source: dataAdapter,
							width: 200,
							height: 25,
							displayMember: 'Menu_Name',
							valueMember: 'Menu_Code'
						});
	
					//$("#jqxDropDownList2").jqxDropDownList('checkIndex', 0);
                
					$("#jqxDropDownList2").on('checkChange', function (event) {
							if (event.args) {
								var item = event.args.item;
								if (item) {	
									var items = $("#jqxDropDownList2").jqxDropDownList('getCheckedItems');
									var checkedItems = "";
									var em_val = "";
									$.each(items, function (index) {
											checkedItems += this.label + ", ";  
											em_val += this.value+ ",";                        
										});
									$("#checkedItemsLog1").text(checkedItems);
									$("#UserTypeForm_editmenucode").val(em_val);
								}
								else {
									$("#checkedItemsLog1").text('');
									$("#UserTypeForm_editmenucode").val('');
								}
							}
						});

					if(result[2].length>0) {
						for(var j=0;j<result[2][0].Edit_Menu_Code.length;j++){
							$("#jqxDropDownList2").jqxDropDownList('checkItem',result[2][0].Edit_Menu_Code[j]);
						}
					}	
				
					//view dropdowncheck box

					$("#jqxDropDownList1").on('checkChange', function (event) {
							if (event.args) {
								var item = event.args.item;
								if (item) {
									var items = $("#jqxDropDownList1").jqxDropDownList('getCheckedItems');
									var checkedItems = ""; 
									var vm_val = "";
									$.each(items, function (index) {
											checkedItems += this.label + ", ";                          
											vm_val += this.value+ ",";
										});
									$("#checkedItemsLog").text(checkedItems);
									$("#UserTypeForm_viewmenucode").val(vm_val);
								}
								else { 
									$("#checkedItemsLog").text('');
									$("#UserTypeForm_viewmenucode").val('');
								}
						
							}
						});		
					if(result[2].length>0) {
						for(var j=0;j<result[2][0].View_Menu_Code.length;j++){
							$("#jqxDropDownList1").jqxDropDownList('checkItem',result[2][0].View_Menu_Code[j]);
						}
					}													
					var source1 =
					{
						localdata: result[1],
						datatype: "json",
						datafields: [ 
							{ name: 'Acc_Menu_Name'},
							{ name: 'Acc_Menu_Code'}
						],				
						async: false
					};
				
					var dataAdapter1 = new $.jqx.dataAdapter(source1);
					$("#jqxDropDownList4").jqxDropDownList(
						{
							checkboxes: true,
							source: dataAdapter1,
							width: 200,
							height: 25,
							displayMember: 'Acc_Menu_Name',
							valueMember: 'Acc_Menu_Code'
						});
				
					//Access Menus select
					$("#jqxDropDownList4").on('checkChange', function (event) {
							if (event.args) {
								var item = event.args.item;
								if (item) {	
									var items = $("#jqxDropDownList4").jqxDropDownList('getCheckedItems');
									var checkedItems = "";
									var am_val = "";
									$.each(items, function (index) {
											checkedItems += this.label + ", ";  
											am_val += this.value+ ",";                        
										});
									$("#checkedItemsLog3").text(checkedItems);
									$("#UserTypeForm_accessmenucode").val(am_val);
								}
								else {
									$("#checkedItemsLog3").text('');
									$("#UserTypeForm_accessmenucode").val('');
								}
							}
						});
					if(result[2].length>0) {
						for(var j=0;j<result[2][0].Acess_Menu_Code.length;j++){
							$("#jqxDropDownList4").jqxDropDownList('checkItem',result[2][0].Acess_Menu_Code[j]);
						}
					}	
			
				}
		  
				//  $("#jqxDropDownList1").jqxDropDownList('clearSelection', true);
				// $("#jqxDropDownList2").jqxDropDownList('clearSelection', true);
				//  $("#jqxDropDownList4").jqxDropDownList('clearSelection', true);
			});
		$("#UserTypeForm_usertype").val(ulevel);	
	
	}	
 
	function Clear()
	{
		$("#UserTypeForm_userlevel").val('');
		$(".jqx-combobox-input jqx-widget-content jqx-rc-all").val('');
		// $("#jqxDropDownList2").val('');
		$("#Error_Text").html('');
		$("#View_Assign_Type").hide();
		$("#View_Assign_Type_Button").hide();
		$("#Assign_Type_Div").hide();
		$("#checkedItemsLog").text('');
		$("#checkedItemsLog1").text('');
		$("#checkedItemsLog3").text('');
   
  
	}
 
</script>  
			
<table cellpadding="3" cellspacing="1" width="100%">
	<tr><td >
			<div class="widget">
				<div class="widget-header">
					<div class="title" style="width: 200px">
						<span class="fs1" aria-hidden="true" data-icon="&#xe14a;"></span><u> 
							<?php echo $form->labelEx($model,'Assignusertype',array('style'=>'font-weight: bold;font-size:18px;')); ?>
						</u></div>
				</div>
				<div class="widget-body"> 		
					<table width="70%" style="padding-left: 100px">
						<tr><td id="Error_Text" align="center" ><span style="color:red"><?php echo $msg ?></span></td></tr>
					</table><br>		  
		 
					<div class="row-fluid">
						<table width="50%"><tr>
								<td><?php echo $form->labelEx($model,'userlevel'); ?></td>
								<td></td>
								<td>
									<div id='jqxComboBox'>
									</div>
									<div style="font-size: 12px; font-family: Verdana;" id="selectionlog"></div>
									<?php echo $form->hiddenField($model,'usertype'); ?>
				
								</td>
		 
								<td><?php echo CHtml::button('Go',array('onclick'=>'getmenus()', 'size'=>50,'class'=>'btn btn-primary')) ?></td> 
								<td><?php echo CHtml::button('Cancel',array('size'=>50,'class'=>'btn btn-primary','onclick'=>'Clear();')) ?></td>
		 
							</tr></table>
		  
					</div>
		  
				</div>  
		
			
			</div>
		</td>
	</tr></table>
	
<div class="widget" id="Assign_Type_Div" style="display: none" >
	<table width="70%" border="1" cellspacing="5" cellpadding="5" style="display:none; padding:10px; margin:7px;" id="View_Assign_Type">
	
		<tr ><td ><?php echo $form->labelEx($model,'viewmenus'); ?></td>
			<td ><div id='jqxDropDownList1'> 
					<div style='max-width: 300px; margin-top: 20px;' id="checkedItemsLog"></div>
				</div>
				<div style='max-width: 300px; margin-top: 20px;' id="checkedItemsLog"></div>
				<?php echo $form->hiddenField($model,'viewmenucode'); ?>
			</td>
		</tr>	  
		<tr  ><td><?php echo $form->labelEx($model,'editmenus'); ?></td>
			<td><div id='jqxDropDownList2'> 
					<div style='max-width: 300px; margin-top: 20px;' id="checkedItemsLog1"></div>
				</div>
				<div style='max-width: 300px; margin-top: 20px;' id="checkedItemsLog1"></div>
				<?php echo $form->hiddenField($model,'editmenucode'); ?>	      
			</td></tr>
		<tr ><td><?php echo $form->labelEx($model,'accessmenus'); ?></td>
			<td><div id='jqxDropDownList4'> 
					<div style='max-width: 300px; margin-top: 20px;' id="checkedItemsLog3"></div>
				</div>
				<div style='max-width: 300px; margin-top: 20px;' id="checkedItemsLog3"></div>
				<?php echo $form->hiddenField($model,'accessmenucode'); ?>	      
			</td></tr>	  	
		
	</table>
	<table  width="70%" style="display:none;padding:100px;" id="View_Assign_Type_Button" ><tr>
			<td align="right" ><?php echo CHtml::Submitbutton('Save',array('size'=>50,'class'=>'btn btn-primary','name'=>'u5t6y8')) ?></td></tr></table>
</div>	
	
	
<?php $this->endWidget(); ?>
