	<?php $form = $this->beginWidget('CActiveForm', array(
		'id'=>'UserTypeForm',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	)); ?>


<script type="text/javascript">
	function getusers()
	{
	
		$("#levelcheck").html('') ;
		$("#Error_Text").html('');
		$("#userlevel").hide();
		var utype		 =	$("#UserTypeForm_utype").val();
		var searchtext	 =	$("#UserTypeForm_searchtext").val();

		if(utype >=1 && (searchtext.length)>0 )
		{
			$("#View_main").show();

		}
		else alert("Please fill all fields ");

		var source =
		{

			datatype: "json",
			datafields: [
				{
					name: 'UName'
				},
				{
					name: 'Header_Code'
				}
			],
			url : '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/ajaxUserType?Utype='+utype+'&Searchtext='+searchtext,
			async: false
		};
		var dataAdapter = new $.jqx.dataAdapter(source);
		$("#jqxDropDownList").jqxDropDownList(
			{
				source: dataAdapter,
				width: 200,
				height: 25,

				displayMember: 'UName',
				valueMember: 'Header_Code'
			});

		$('#jqxDropDownList').on('select', function (event)
			{
				if (event.args)
				{
					var item = event.args.item;
					if (item)
					{
						$("#UserTypeForm_headercode").val(item.value);
					}

				}
			});

		$('#jqxDropDownList').on('change', function (event)
			{
				var args = event.args;
				if (args.item.value>0)
				{
					getlevel();
					$("#User_Menu").show();
					$("#levelcheck").html('');
					$("#jqxDropDownList1").jqxDropDownList('clearSelection', true);

				}
				else
				{

					$("#User_Menu").hide();

					$("#userlevel").hide();
				}
			});

	}

	function insertLevel()
	{

		var headercode = $("#jqxDropDownList").jqxDropDownList('val');
		var level = $("#jqxDropDownList1").jqxDropDownList('val');

		$.ajax(
			{
				type: 'POST',
				data:
				{
					'headercode':headercode,'Level':level
				},
				url : '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/ajaxsaveuserLevel',

				success: function(result)
				{
					checkuserlevel(headercode);
					alert("User privileges Updated");
				}
			});


	}
	function checkuserlevel(obj)
	{
		$.ajax(
			{
				type: 'POST',
				data:
				{
					'headercode':obj
				},
				url : '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/checkuserLevel',
				success: function(result)
				{
					$("#levelcheck").html(result);	
				}
			});

	}
	function getlevel()
	{

		$("#userlevel").show();
		var headercode=	$("#UserTypeForm_headercode").val();
		checkuserlevel(headercode);
		$.ajax(
			{
				type: 'POST',
				url : '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/ajaxuserLevel',
				success: function(result)
				{


					var source =
					{
						localdata: result,
						datatype: "json",
						datafields: [
							{
								name: 'LevelName'
							},
							{
								name: 'levelCode'
							}
						],

						async: false

					};
					/* View Drop down box */
					var dataAdapter = new $.jqx.dataAdapter(source);
					$("#jqxDropDownList1").jqxDropDownList(
						{

							source: dataAdapter,
							width: 200,
							height: 25,
							displayMember: 'LevelName',
							valueMember: 'levelCode'
						});
				}
			});
	}

	function Clear()
	{
		alert('hi');
		$("#UserTypeForm_utype").val('');
		$("#UserTypeForm_searchtext").val('');
		$("#Error_Text").html('');
		$("#View_main").hide();
		$("#userlevel").hide();
		$("#levelcheck").html('') ;
		$("#jqxDropDownList").jqxDropDownList('clearSelection', true);


	}
</script>

<table cellpadding="3" cellspacing="1" width="100%">
	<tr>
		<td >
			<div class="widget">
				<div class="widget-header">
					<div class="title" style="width: 200px">
						<span class="fs1" aria-hidden="true" data-icon="&#xe14a;">
						</span>
						<u>
							<?php echo $form->labelEx($model,'accesspermission',array('style'=>'font-weight: bold;font-size:18px;')); ?>
						</u>
					</div>
				</div>
				<div class="widget-body">
					<table width="100%" border="0" cellpadding="1" style="padding:10px; margin:7px;">
						<table width="70%" style="padding-left: 100px">
							<tr>
								<td id="Error_Text" align="center" >
									<span style="color:red">
										<?php echo $msg ?>
									</span>
								</td>
							</tr>
						</table><br>

						<div class="row-fluid">
							<table width="70%" border="0" cellpadding="1" style="padding:10px; margin:7px;">
								<tr>
									<td>
										<?php echo $form->labelEx($model,'utype'); ?>
									</td>
									<td>
										<div>
											<?php echo $form->dropDownList($model,'utype',array('0'=>'Select','1'=>'User name','2'=>'UID','3'=>'Full name')); ?>
											<?php echo $form->error($model,'utype'); ?>
										</div>
									</td>

									<td width="30px">
										<?php echo $form->labelEx($model,'as'); ?>
									</td>
									<td>
										<?php echo $form->textField($model,'searchtext',array('style'=>'height=30px;'));?>
									</td>
									
								<td><?php echo CHtml::button('Go',array('onclick'=>'getusers()', 'size'=>50,'class'=>'btn btn-primary')) ?></td> 

									
									<td>
										<?php echo CHtml::button('Cancel',array('size'   =>50,'class'  =>'btn btn-primary','onclick'=>'Clear();')) ?>
									</td>
								</tr>
							</table>
							<div id="View_main" style="display: none">
								<table width="70%" border="" cellspacing="5" cellpadding="5" style="padding:10px; margin:7px;" id="View_Assign_Type">
									<tr>
										<td>
											<?php echo $form->labelEx($model,'users'); ?>

										</td>


										<td>
											<div id='jqxDropDownList'>
											</div>
											<div id="levelcheck">
											</div>
											<?php echo $form->hiddenField($model,'headercode'); ?>
										</td>
									</tr>
									<tr style="display: none;" id="userlevel">
										<td>
											<?php echo $form->labelEx($model,'level'); ?>

										</td>
										<td>
											<div id='jqxDropDownList1'>
											</div>
											<?php echo $form->hiddenField($model,'ulevel'); ?>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<?php echo CHtml::button('save',array('size'   =>50,'class'  =>'btn btn-primary','onclick'=>'insertLevel();')) ?>

										</td>
									</tr>




								</table>

							</div>
						</div>
					</table>
				</div>
			</div>
		</td>
	</tr>
</table>
<?php $this->endWidget(); ?>