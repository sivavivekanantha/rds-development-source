<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'UserTypeReportForm',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
));  ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'>This demo illustrates the basic functionality of the Grid plugin. The jQWidgets Grid plugin offers rich support for interacting with data, including paging, grouping and sorting. 
    </title>
	
    
    <script type="text/javascript">
function Clear()
{
  $("#UserTypeReportForm_usertype").val('');
    $("#View_Data").hide();
 }
	
		
	function ExportUserType()
	{
		var usertype		 =	$("#UserTypeReportForm_usertype").val();
		if(usertype>0)
		{
			$("#View_Data").show();
		}		
		else alert("Please Select User Type");

$.ajax({
	    type: 'GET',
		data: {'usertype':usertype},
		url: '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/ajaxusertypereport?usertype',
		success: function(result)
		{
			//console.log(result[0]);
			//SUMMARY Grid
			var source = {localdata: result[0][1]};
			//console.log(result[0][1]);
			var addfilter = function () {
                var filtergroup = new $.jqx.filter();
                var filter_or_operator = 1;
                var filtervalue = '';
                var filtercondition = 'contains';
                var filter1 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
                filtervalue = '';
                filtercondition = 'contains';
                var filter2 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
                filtergroup.addfilter(filter_or_operator, filter1);
                filtergroup.addfilter(filter_or_operator, filter2);
                // add the filters.
                $("#jqxgrid").jqxGrid('addfilter', 'firstname', filtergroup);
                // apply the filters.
                $("#jqxgrid").jqxGrid('applyfilters');
            }
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#jqxgrid").jqxGrid(
            {
                width: '1000',
                source: dataAdapter,
                filterable: true,
                sortable: true,
                autoshowfiltericon: true,
                pageable: true, 
				pagermode: 'simple',
                autoheight: true,
				columnsresize: true,
				groupable: true,
				editable: true,
                ready: function () {
                    addfilter();
                    var localizationObject = {
                        filterstringcomparisonoperators: ['contains', 'does not contain'],
                        // filter numeric comparison operators.
                        filternumericcomparisonoperators: ['less than', 'greater than'],
                        // filter date comparison operators.
                        filterdatecomparisonoperators: ['less than', 'greater than'],
                        // filter bool comparison operators.
                        filterbooleancomparisonoperators: ['equal', 'not equal']
                    }
                    $("#jqxgrid").jqxGrid('localizestrings', localizationObject);
                },
                updatefilterconditions: function (type, defaultconditions) {
                    var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
                    var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                    var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                    var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
                    switch (type) {
                        case 'stringfilter':
                            return stringcomparisonoperators;
                        case 'numericfilter':
                            return numericcomparisonoperators;
                        case 'datefilter':
                            return datecomparisonoperators;
                        case 'booleanfilter':
                            return booleancomparisonoperators;
                    }
                },
                updatefilterpanel: function (filtertypedropdown1, filtertypedropdown2, filteroperatordropdown, filterinputfield1, filterinputfield2, filterbutton, clearbutton,
                 columnfilter, filtertype, filterconditions) {
                    var index1 = 0;
                    var index2 = 0;
                    if (columnfilter != null) {
                        var filter1 = columnfilter.getfilterat(0);
                        var filter2 = columnfilter.getfilterat(1);
                        if (filter1) {
                            index1 = filterconditions.indexOf(filter1.comparisonoperator);
                            var value1 = filter1.filtervalue;
                            filterinputfield1.val(value1);
                        }
                        if (filter2) {
                            index2 = filterconditions.indexOf(filter2.comparisonoperator);
                            var value2 = filter2.filtervalue;
                            filterinputfield2.val(value2);
                        }
                    }
                    filtertypedropdown1.jqxDropDownList({ autoDropDownHeight: true, selectedIndex: index1 });
                    filtertypedropdown2.jqxDropDownList({ autoDropDownHeight: true, selectedIndex: index2 });
                },
               columns:result[0][0],
			 
            });
			
			//$("#jqxgrid").jqxGrid('autoresizecolumns');
           // $('#events').jqxPanel({ width: 300, height: 80});
		     var width = 20 * $("#jqxgrid").width() / 100;
		     $("#jqxgrid").jqxGrid('setcolumnproperty', 'UID', 'width', width);
		     $("#jqxgrid").jqxGrid('setcolumnproperty', 'Username', 'width', width);
		     $("#jqxgrid").jqxGrid('setcolumnproperty', 'Fullname', 'width', width);
		     $("#jqxgrid").jqxGrid('setcolumnproperty', 'Category', 'width', width);
		     $("#jqxgrid").jqxGrid('setcolumnproperty', 'Department', 'width', width);
		     $("#jqxgrid").jqxGrid('setcolumnproperty', 'Groupname', 'width', width);
		   
            
			$("#jqxgrid").on("filter", function (event) {
                $("#events").jqxPanel('clearcontent');
                var filterinfo = $("#jqxgrid").jqxGrid('getfilterinformation');
                var eventData = "Triggered 'filter' event";
                for (i = 0; i < filterinfo.length; i++) {
                    var eventData = "Filter Column: " + filterinfo[i].filtercolumntext;
                    $('#events').jqxPanel('prepend', '<div style="margin-top: 5px; ">' + eventData + '</div>');
                }
            });         
		 }
	
	});
  
	$("#excelExport").click(function () {
		 var currentdate = new Date();
		 var currentdate1 = currentdate.getDate() + '-' + (currentdate.getMonth()+1) + '-' + currentdate.getFullYear();
		
		 var fname= 'UserType_Report_'+currentdate1;
    	$("#jqxgrid").jqxGrid('exportdata', 'xls', fname);           
	});	
}
</script>

</head>
<body class='default'>	
	<table cellpadding="3" cellspacing="1" width="100%">
	<tr><td width="100%">
	 <div class="widget">
     	<div class="widget-header">
	     <div class="title" style="width: 300px">
	     <span class="fs1" aria-hidden="true" data-icon="&#xe14a;"></span><?php echo $form->labelEx($model,'usertype',array('style'=>'font-weight: bold;font-size:18px;')); ?>
	      </div>
	      </div>
       
	    <div class="widget-body"> 
		<table width="70%" border="0" cellpadding="1" style="padding:10px; margin:7px;">
		  	<tr><td><?php echo $form->labelEx($model,'usertype'); ?></td>
			<td><div> <?php echo $form->dropDownList($model,'usertype',array('0'=>'Select','1'=>'Standard User','2'=>'Super User')); ?>
			<?php echo $form->error($model,'usertypeype'); ?> </div> </td>
			<td><?php echo CHtml::button('Go',array('onclick'=>'ExportUserType();', 'size'=>50,'class'=>'btn btn-primary')) ?></td>
			<td><?php echo CHtml::button('Cancel',array('size'=>50,'class'=>'btn btn-primary','onclick'=>'Clear();')) ?></td>
			</tr>			
			</table>
			</div>
	</div>
	</td>
	
	</tr>
	</table>
	<div id="View_Data" name="View_Data" style="display: none">	
	<table width="100%">
	<tr><td>
	<div class="widget">
     	<div class="widget-header">
        	<div class="title">
            	<span class="fs1" aria-hidden="true" data-icon="&#xe14a;"></span> Export User Type Report
            </div>
        </div>
        <div class="widget-body"> 
		<table width="100%"><tr><td>
			<div id='jqxWidget' style="font-size: 13px; font-family: Verdana; float: left; display: block;" >		
				<div id="jqxgrid"></div>
		 		<div style="margin-top:20px;" >
				<div  style="float: left;"><input type="button" value="Export to Excel" id='excelExport' />
        		 </div></div>
    	</div>
		</td></tr></table>
		</div>
	</div>
	</td></tr>
	</table>
	</div>
</body>
</html>
<?php $this->endWidget(); ?>