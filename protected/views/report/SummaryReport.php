<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'SummaryReportForm',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
));  ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script type="text/javascript">
	(function($) {
    $(function() {
        $('input.timepicker').timepicker({ timeFormat: 'HH:mm' });
		
    });
})(jQuery);
(function($) {
    $(function() {
        $('input.timepicker1').timepicker({ timeFormat: 'HH:mm' });
		
    });
})(jQuery);
	function cleardata()
	{
		$("#checkdate_from").val('');
		$("#checkdate_To").val('');
		$("#summaryReportForm_Location").val(3);
		$("#summaryReportForm_group").val('');
		$("#departmentlist").val('');
		$("#summaryReportForm_ReportType").val(2);
		$("#summaryReportForm_Uid").val('');
		$("#summaryReportForm_Name").val('');	
		$("#summaryReportForm_Rid").val('');
		
		$("#SummaryReportForm").submit();
	}
	
	function showdata(obj)
	{
		$("#summaryReportForm_Location").attr('disabled', false);
		$("#checkdate_from").attr('disabled', false);
		$("#checkdate_To").attr('disabled', false);
		$("#summaryReportForm_Location").attr('disabled', false);
		$("#summaryReportForm_group").attr('disabled', false);
		$("#departmentlist").attr('disabled', false);		
		$("#summaryReportForm_Uid").attr('disabled', false);
		$("#summaryReportForm_Name").attr('disabled', false);
		if(obj==1){
		$("#summaryReportForm_Location").attr('disabled', true);
		$("#checkdate_from").attr('disabled', true);
		$("#checkdate_To").attr('disabled', true);
		$("#summaryReportForm_Location").attr('disabled', true);
		$("#summaryReportForm_group").attr('disabled', true);
		$("#departmentlist").attr('disabled', true);	
		$("#summaryReportForm_Uid").attr('disabled', true);
		$("#summaryReportForm_Name").attr('disabled', true);
			
		}
		
	 	
	}
	
	function ExportData()
	{
		var fromdate 	 =	$("#checkdate_from").val();	
			if(fromdate === undefined) fromdate='';
		var frmtimepicker 	 =	$('input.timepicker').val();
		
		if(frmtimepicker === undefined) frmtimepicker='';			
		var todate		 =	$("#checkdate_To").val();
	
		if(todate === undefined) todate='';
		var toTimepicker 	 =	$('input.timepicker1').val();	
		if(toTimepicker === undefined) toTimepicker='';
		
		var location 	 =	$("#summaryReportForm_Location").val();
		
		var group		 =	$("#summaryReportForm_group").val();
		if(group === undefined)	group='';
		var department	 =	$("#departmentlist").val();		
		if(department === undefined)department='';		
		var Type		 =	$("#summaryReportForm_ReportType").val();		
		var Uid			 =	$("#summaryReportForm_Uid").val();
		if(Uid === undefined)	Uid='';
		var Name =	$("#summaryReportForm_Name").val();
		if(Name === undefined)	Name='';
		if(Name === undefined)	Name='';
		var Rid 	 =	$("#summaryReportForm_Rid").val();
		if(Rid==104 || Rid==105 || Rid==106 || Rid==107) $("#sumReport").hide();
	 else  $("#sumReport").show();
		
		$.ajax({
	    type: 'POST',
		url: '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/ajaxSummaryreport?checkdate='+decodeURIComponent(fromdate)+'&checktodate='+decodeURIComponent(todate)+'&location='+decodeURIComponent(location)+'&group='+decodeURIComponent(group)+'&department='+decodeURIComponent(department)+'&Name='+decodeURIComponent(Name)+'&Uid='+encodeURIComponent(Uid)+'&frmtime='+encodeURIComponent(frmtimepicker)+'&toTime='+encodeURIComponent(toTimepicker)+'&Rid='+encodeURIComponent(Rid),
		success: function(result)
		{
			
			//SUMMARY Grid
			var source = {localdata: result[0][1]};
			//console.log(result[0][1]);
			var addfilter = function () {
                var filtergroup = new $.jqx.filter();
                var filter_or_operator = 1;
                var filtervalue = '';
                var filtercondition = 'contains';
                var filter1 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
                filtervalue = '';
                filtercondition = 'contains';
                var filter2 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
                filtergroup.addfilter(filter_or_operator, filter1);
                filtergroup.addfilter(filter_or_operator, filter2);
                // add the filters.
                $("#jqxgrid").jqxGrid('addfilter', 'firstname', filtergroup);
                // apply the filters.
                $("#jqxgrid").jqxGrid('applyfilters');
            }
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#jqxgrid").jqxGrid(
            {
				
                width: '995',
                source: dataAdapter,
                filterable: true,
                sortable: true,
                autoshowfiltericon: true,
                pageable: true, 
				pagermode: 'simple',
                autoheight: true,
				columnsresize: true,
				groupable: true,
				editable: false,
                ready: function () {
                    addfilter();
                    var localizationObject = {
                        filterstringcomparisonoperators: ['contains', 'does not contain'],
                        // filter numeric comparison operators.
                        filternumericcomparisonoperators: ['less than', 'greater than'],
                        // filter date comparison operators.
                        filterdatecomparisonoperators: ['less than', 'greater than'],
                        // filter bool comparison operators.
                        filterbooleancomparisonoperators: ['equal', 'not equal']
                    }
                    $("#jqxgrid").jqxGrid('localizestrings', localizationObject);
                },
                updatefilterconditions: function (type, defaultconditions) {
                    var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
                    var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                    var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                    var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
                    switch (type) {
                        case 'stringfilter':
                            return stringcomparisonoperators;
                        case 'numericfilter':
                            return numericcomparisonoperators;
                        case 'datefilter':
                            return datecomparisonoperators;
                        case 'booleanfilter':
                            return booleancomparisonoperators;
                    }
                },
                updatefilterpanel: function (filtertypedropdown1, filtertypedropdown2, filteroperatordropdown, filterinputfield1, filterinputfield2, filterbutton, clearbutton,
                 columnfilter, filtertype, filterconditions) {
                    var index1 = 0;
                    var index2 = 0;
                    if (columnfilter != null) {
                        var filter1 = columnfilter.getfilterat(0);
                        var filter2 = columnfilter.getfilterat(1);
                        if (filter1) {
                            index1 = filterconditions.indexOf(filter1.comparisonoperator);
                            var value1 = filter1.filtervalue;
                            filterinputfield1.val(value1);
                        }
                        if (filter2) {
                            index2 = filterconditions.indexOf(filter2.comparisonoperator);
                            var value2 = filter2.filtervalue;
                            filterinputfield2.val(value2);
                        }
                    }
                    filtertypedropdown1.jqxDropDownList({ autoDropDownHeight: true, selectedIndex: index1 });
                    filtertypedropdown2.jqxDropDownList({ autoDropDownHeight: true, selectedIndex: index2 });
                },
               columns:result[0][0],
			 
            });
			
			var width = 30 * $("#jqxgrid").width() / 100;
   			$("#jqxgrid").jqxGrid('setcolumnproperty', 'Category', 'width', width);
            
			$("#jqxgrid").on("filter", function (event) {
                $("#events").jqxPanel('clearcontent');
                var filterinfo = $("#jqxgrid").jqxGrid('getfilterinformation');
                var eventData = "Triggered 'filter' event";
                for (i = 0; i < filterinfo.length; i++) {
                    var eventData = "Filter Column: " + filterinfo[i].filtercolumntext;
                    $('#events').jqxPanel('prepend', '<div style="margin-top: 5px; ">' + eventData + '</div>');
                }
            });
			
          
			
		//DETAIL Grid
			var source1 = {localdata: result[1][1]};
			var addfilter = function () {
                var filtergroup = new $.jqx.filter();
                var filter_or_operator = 1;
                var filtervalue = '';
                var filtercondition = 'contains';
                var filter1 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
                filtervalue = '';
                filtercondition = 'contains';
                var filter2 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
                filtergroup.addfilter(filter_or_operator, filter1);
                filtergroup.addfilter(filter_or_operator, filter2);
                // add the filters.
                $("#jqxgrid1").jqxGrid('addfilter', 'firstname', filtergroup);
                // apply the filters.
                $("#jqxgrid1").jqxGrid('applyfilters');
            }
            var dataAdapter1 = new $.jqx.dataAdapter(source1);
            $("#jqxgrid1").jqxGrid(
            {	
                width: '550',
                source: dataAdapter1,
                filterable: true,
                sortable: true,
                autoshowfiltericon: true,
                pageable: true, 
				pagermode: 'simple',
                autoheight: true,
				columnsresize: true,
				groupable: true,
				editable: false,
                ready: function () {
                    addfilter();
                    var localizationObject = {
                        filterstringcomparisonoperators: ['contains', 'does not contain'],
                        // filter numeric comparison operators.
                        filternumericcomparisonoperators: ['less than', 'greater than'],
                        // filter date comparison operators.
                        filterdatecomparisonoperators: ['less than', 'greater than'],
                        // filter bool comparison operators.
                        filterbooleancomparisonoperators: ['equal', 'not equal']
                    }
                    $("#jqxgrid1").jqxGrid('localizestrings', localizationObject);
                },
                updatefilterconditions: function (type, defaultconditions) {
                    var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
                    var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                    var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                    var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
                    switch (type) {
                        case 'stringfilter':
                            return stringcomparisonoperators;
                        case 'numericfilter':
                            return numericcomparisonoperators;
                        case 'datefilter':
                            return datecomparisonoperators;
                        case 'booleanfilter':
                            return booleancomparisonoperators;
                    }
                },
                updatefilterpanel: function (filtertypedropdown1, filtertypedropdown2, filteroperatordropdown, filterinputfield1, filterinputfield2, filterbutton, clearbutton,
                 columnfilter, filtertype, filterconditions) {
                    var index1 = 0;
                    var index2 = 0;
                    if (columnfilter != null) {
                        var filter1 = columnfilter.getfilterat(0);
                        var filter2 = columnfilter.getfilterat(1);
                        if (filter1) {
                            index1 = filterconditions.indexOf(filter1.comparisonoperator);
                            var value1 = filter1.filtervalue;
                            filterinputfield1.val(value1);
                        }
                        if (filter2) {
                            index2 = filterconditions.indexOf(filter2.comparisonoperator);
                            var value2 = filter2.filtervalue;
                            filterinputfield2.val(value2);
                        }
                    }
                    filtertypedropdown1.jqxDropDownList({ autoDropDownHeight: true, selectedIndex: index1 });
                    filtertypedropdown2.jqxDropDownList({ autoDropDownHeight: true, selectedIndex: index2 });
                },
               columns:result[1][0],
			 
            });
			var width = 14 * $("#jqxgrid").width() / 100;
   			$("#jqxgrid").jqxGrid('setcolumnproperty', 'UID', 'width', width/2);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'Name', 'width', width);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'Category', 'width', width);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'Department', 'width', width);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'GroupName', 'width', width);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'Gender', 'width', width/2);

			$("#jqxgrid").jqxGrid('setcolumnproperty', 'SwipeIN', 'width', width);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'TimeIN', 'width', width/2);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'SwipingPointIN', 'width', width);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'SwipeOUT', 'width', width);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'TimeOUT', 'width', width/2);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'SwipingPointOUT', 'width', width);
			$("#jqxgrid").on("filter", function (event) {
                $("#events").jqxPanel('clearcontent');
                var filterinfo = $("#jqxgrid").jqxGrid('getfilterinformation');
                var eventData = "Triggered 'filter' event";
                for (i = 0; i < filterinfo.length; i++) {
                    var eventData = "Filter Column: " + filterinfo[i].filtercolumntext;
                    $('#events').jqxPanel('prepend', '<div style="margin-top: 5px; ">' + eventData + '</div>');
                }
            });
			
			
	    }
	
		});
  
	$("#excelExport").click(function () {
		 var currentdate = new Date();
		 var currentdate1 = currentdate.getDate() + '-' + (currentdate.getMonth()+1) + '-' + currentdate.getFullYear();
		
		 var fname= 'Residentmovement_sum_report'+currentdate1;
    	$("#jqxgrid1").jqxGrid('exportdata', 'xls', fname);           
	});	

	//summa 
	$("#excelExport1").click(function () {
		 var currentdate = new Date();
		 var currentdate1 = currentdate.getDate() + '-' + (currentdate.getMonth()+1) + '-' + currentdate.getFullYear();
		
		 var fname= 'Residentmovement_detail_report'+currentdate1;
    	$("#jqxgrid").jqxGrid('exportdata', 'xls', fname);           
	});	
}
	
	
function fetchDepartment(obj)
{
	
	$.ajax({
	    type: 'GET',
		url: '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/ajaxshowDepartment?group='+obj,
		success: function(result)
		{
			
				$('#departmentlist').html(result);
		}		
		});
}

    </script>

</head>
<body class='default'>	
	<table cellpadding="3" cellspacing="1" width="100%">
	<tr><td width="50%">
	 <div class="widget">
     	<div class="widget-header">
        	<div class="title"  >
            	<span class="fs1" aria-hidden="true" data-icon="&#xe14a;"></span><?php echo $menu;?>
            </div>
        </div>
        <div class="widget-body"> 
		<?php echo $form->hiddenField($model, 'Rid'); ?>
		<table width="100%" border="0" cellpadding="1" style="padding:10px; margin:7px;"> 
			
			<tr>
			<td>
    				<?php echo $form->labelEx($model,'FromDate');?></td>
				<td>
				<div class="">
					<?php echo $form->textField($model,'fromdate',array('id'=>'checkdate_from')); ?>               
        			<?php echo $form->error($model,'fromdate'); ?>
				 </div></td>
				 <td>
				<input type="text"  class="timepicker" name="time" style="width: 75px;" placeholder="Time" value="00:00"/>
				</td>
				 </tr>
				 
				 <?php 
				 	if($Rcode==2) 
			{ ?>
				 <tr>
				  <td>
    				<?php echo $form->labelEx($model,'Todate');?></td>
				<td>
				<div class="">
					<?php echo $form->textField($model,'todate',array('id'=>'checkdate_To')); ?>
        			<?php echo $form->error($model,'todate'); ?>
				 </div></td>
				 <td>
				<input type="text" class="timepicker1" name="time" style="width: 75px;" placeholder="Time" value="00:00"/>
				</td>
				 </tr>
				
				 <?php } ?>
				
				<?php if($Bcode==2 or $Bcode==3 or $Bcode==4 ) { ?>
			<tr>
			<td><?php echo $form->labelEx($model,'Group');?></td>
			<td><?php echo $form->dropDownList($model,'group', CHtml::listData($Grouplist,'Group_Code','Group_Name'),
				array('prompt'=>'Select','style'=>'width:215px','onchange'=>'fetchDepartment(this.value);'));
			echo $form->error($model,'group'); ?></td>
			</tr>
			<?php }
			if($Bcode==3 or $Bcode==4){ ?>
			<tr>
			<td >
				<?php echo $form->labelEx($model,'Department');?></td>
		     <td>
			 <div>
	 			<?php echo $form->dropDownList($model,'department',array('prompt'=>'Select'),array('id'=>'departmentlist'));
	  			echo $form->error($model,'department'); ?>
	 			</div>
	 		</td>
			</tr>
			<?php } ?>
			
			
			<?php if($Bcode==4) { ?>
			<tr>
			  <td ><?php echo $form->labelEx($model,'Name');?></td>
			 <td>
				<div class="">
					<?php echo $form->textField($model,'Name',array('onkeydown'=>"return alphaonly('summaryReportForm_Name')",'maxlength'=>'100')); ?>               
        			<?php echo $form->error($model,'Name'); ?>
				 </div></td>
				 </tr>
				 <tr>
			<td >
    				<?php echo $form->labelEx($model,'Uid');?></td>
				<td>
				<div class="">
					<?php echo $form->textField($model,'Uid',array('maxlength'=>'10')); ?>               
        			<?php echo $form->error($model,'Uid'); ?>
				 </div></td>
			
			 
			</tr>
			<?php } ?>
			<tr>
				<td >
					<?php echo $form->labelEx($model,'Movement Type');?>
				</td>		
			    <td>
			  		<?php echo $form->dropDownList($model,'Location',array('1'=>'InsideAshram','2'=>'OutsideAshram','3'=>'Both'),array('style'=>'width:215px'));
		      		echo $form->error($model,'Location');?> 
			    </td>
		   </tr> 
			<tr>
				<td colspan="4"> 
			<div>
			<?php 
				echo CHtml::button('Go',array('class'=>'btn btn-primary',
				'onclick'=>'ExportData();')); ?> 
				<?php echo CHtml::button('Cancel',array('class'=>'btn btn-primary'
			,'onclick'=>'cleardata();')); 
			?>
			     </div></td>
			</tr>
			
			
		</table>
			</div>
	</div>
	</td>
	<td width="50%" valign="top">
		 <div class="widget" id="sumReport" style="display: none;" >
     	<div class="widget-header">
        	<div class="title">
            	<span class="fs1" aria-hidden="true" data-icon="&#xe14a;"></span> Export Summary Report
            </div>
        </div>
        <div class="widget-body"> 
		<table><tr><td>
			<div id='jqxWidget1' style="font-size: 13px; font-family: Verdana; float: left; display: block;" >		
				<div id="jqxgrid1"></div>
		 		<div style="margin-top:20px;" >
				<div  style="float: left;"><input type="button" value="Export to Excel" id='excelExport' />
        		 </div></div>
     			</div>
			</td></tr></table>
			</div>
		</div>
	</td>
	</tr>
	<tr><td colspan="2">
	<div class="widget">
     	<div class="widget-header">
        	<div class="title">
            	<span class="fs1" aria-hidden="true" data-icon="&#xe14a;"></span> Export Detail Report
            </div>
        </div>
        <div class="widget-body"> 
		<table><tr><td>
			<div id='jqxWidget' style="font-size: 13px; font-family: Verdana; float: left; display: block;" >		
				<div id="jqxgrid"></div>
		 		<div style="margin-top:20px;" >
				<div  style="float: left;"><input type="button" value="Export to Excel" id='excelExport1' />
        		 </div></div>
    	</div>
		</td></tr></table>
		</div>
	</div>
	</td></tr>
	</table>
</body>
</html>
<?php $this->endWidget(); ?>