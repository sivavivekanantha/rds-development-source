<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'ReportForm',
					'enableClientValidation'=>true,
					'clientOptions'=>array(
                    'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
					
	<script>
		function show_mysession(obj)
		{
			if(obj==0) $("#mysession").show();
			else $("#mysession").hide();
		}
      function formfilled(obj,obj1,obj3)   
       {	
	   	 var st='N';
	    	if ($("#Freeze"+obj3).is(':checked')) 
	    	var st='Y';
    	  	url = '<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/UpdateFreeze?Header_Code='+obj1+'&Freeze='+st;
		    $("#test_fill").load(url);
       }
	</script>
	<div id="test_fill"></div>
  	<div id="dt_example" class="example_alt_pagination">
<div id="formTitle"><h4><u><?php echo $form->labelEx($model,'Login as Another User',array('style'=>'font-weight: bold;font-size:20px;')); ?></u></h4></div>
<div id="mysession" style="display: none;" align="center"><a href="<?php echo Yii::app()->request->baseUrl?>/index.php/profile/ChangeSession/987987980" ><button type="button" class="btn btn-primary" aria-hidden="true" data-dismiss="modal" onclick="show_mysession(1)"> Return to My Login</button></a></div>
	<div id="showdiv">
    <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table" align="center">                        
	<colgroup><col width="5%"><col width="15%"><col width="10%"><col width="12%"><col width="8%"><col width="5%"><col width="8%"><col width="10%"></colgroup>
	    <thead>
		<th><?php echo $form->labelEx($model,'S.No',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
		<th><?php echo $form->labelEx($model,'Name',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
		<th><?php echo $form->labelEx($model,'User Name',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
		<th><?php echo $form->labelEx($model,'Login as Another User',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
		<th><?php echo $form->labelEx($model,'Photo',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
		<th><?php echo $form->labelEx($model,'Freeze',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
		<th><?php echo $form->labelEx($model,'Profile Pending',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
		<th><?php echo $form->labelEx($model,'Profile Completed',array('style'=>'font-weight: bold;font-size:13px;')); ?></th>
		</thead>
 		<tbody>
		<?php $i=0;
		if(mssql_num_rows($dataReader)>0) {
        while($field = mssql_fetch_array($dataReader))
		{ $i++;?>
	<tr>
		<td align="center"><?php echo $i; ?></td>
		<td align="center" ><?php echo $field['First_Name'];?></td>
		<td align="center" ><?php echo $field['User_Name'];?></td>
		<td align="center"><a href="<?php echo Yii::app()->request->baseUrl?>/index.php/profile/ChangeSession/<?php echo $field['Header_Code'];?> " target='_blank'><button type="button" class="btn btn-primary" aria-hidden="true" data-dismiss="modal" onclick="show_mysession(0)">Login as Another User</button></a></td>
		<td align="center" title="Click here to Download">
			<?php $filename = Yii::app()->request->baseUrl."/protected/photos/".$field['Header_Code'].".jpg?cod=".rand();?>
        	<img src="<?php echo $filename; ?>"   width="30" height="30" style="height: 30px; border: dashed 1px #000000;" alt="img"/>
        </td>
		<td align="center"><input  type="checkbox" id="Freeze<?php echo $i?>" name="Freeze<?php echo $i?>" style="width:20px;" value="Y" 
        onclick="formfilled(this.value,'<?php echo $field['Header_Code']?>','<?php echo $i?>')" 
		<?php if( $field['Freeze']=='Y') echo "checked";?> 
         <?php if($field['Completion']<>'Y' ){?> disabled="disabled"<?php }?>/></td> 
		
		<?php $complete=$pending=$complete_item=$pending_item='';
		
			if($field['Personal']>0){ $complete++; $complete_item="Personal"; } else { $pending++; $pending_item="Personal"; }

			if($field['Personal_Contact']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Personal Contact"; else $complete_item="Personal Contact"; } else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Personal Contact"; else $pending_item="Personal Contact"; }

			if($field['Mobile_Details']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Mobile Details";
			else $complete_item="Mobile Details"; } else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Mobile Details"; else $pending_item="Mobile Details"; }

			if($field['Personal_Mail']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Personal Mail"; else $complete_item="Personal Mail"; } else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Personal Mail"; else $pending_item="Personal Mail"; }

			if($field['Family']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Family"; else $complete_item="Family"; } else 
			{ $pending++; if(strlen($pending_item)>0) $pending_item.=",Family"; else $pending_item="Family"; }

			if($field['Contact']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Contact"; else $complete_item="Contact"; } else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Contact"; else $pending_item="Contact"; }

			if($field['Acadamic']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Acadamic"; else $complete_item="Acadamic"; } else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Acadamic"; else $pending_item="Acadamic"; }

			if($field['Passport']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Passport"; else $complete_item="Passport"; }
			else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Passport"; else $pending_item="Passport"; }

			if($field['Official']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Official"; else $complete_item="Official"; }
			else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Official"; else $pending_item="Official"; }  

			if($field['Language']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Language"; else $complete_item="Language"; }
			else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Language"; else $pending_item="Language"; }  


			if($field['Computer_Skills']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Computer Skills"; else $complete_item="Computer Skills"; }
			else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Computer Skills"; else $pending_item="Computer Skills"; }

			if($field['Medical_Mental']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Medical Mental"; else $complete_item="Medical Mental"; }
			else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Medical Mental"; else $pending_item="Medical Mental"; }

			if($field['Medical_Physical']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Medical Physical"; else $complete_item="Medical Physical"; }
			else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Medical Physical"; else $pending_item="Medical Physical"; }

			if($field['Program']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Program"; else $complete_item="Program"; }
			else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Program"; else $pending_item="Program"; }  

			if($field['Teacher']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Teacher"; else $complete_item="Teacher"; }
			else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Teacher"; else $pending_item="Teacher"; }  

			if($field['Proof']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Proof"; else $complete_item="Proof"; }
			else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Proof"; else $pending_item="Proof"; }  

			if($field['Yatra']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Yatra"; else $complete_item="Yatra"; }
			else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Yatra"; else $pending_item="Yatra"; }  

			if($field['Cottage']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Cottage"; else $complete_item="Cottage"; }
			else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Cottage"; else $pending_item="Cottage"; }  

			if($field['Department']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Department"; else $complete_item="Department"; }
			else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Department"; else $pending_item="Department"; }  	  

			if($field['VolunteeringIsha']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Volunteering Isha"; else $complete_item="Volunteering Isha"; }
			else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Volunteering Isha"; else $pending_item="Volunteering Isha"; }  

			if($field['VolunteeringLocalCenter']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Volunteering LocalCenter"; else $complete_item="Volunteering LocalCenter"; }
			else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Volunteering LocalCenter"; else $pending_item="Volunteering LocalCenter"; }  	 
			if($field['Family_Program']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Family Program"; else $complete_item="Family Program"; }
			else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Family Program"; else $pending_item="Family Program"; }  

			if($field['Reference']>0){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Reference"; else $complete_item="Reference"; }
			else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Reference"; else $pending_item="Reference"; }  

			if($field['Completion']=='Y'){ $complete++; if(strlen($complete_item)>0) $complete_item.=",Completion"; else $complete_item="Completion"; }
			
			else { $pending++; if(strlen($pending_item)>0) $pending_item.=",Completion"; else $pending_item="Completion"; }  	 ?>		
			
			<td title='<?php echo $pending_item; ?>'><span class="value text-error"><b><?php echo round(($pending/24)*100)."%"; ?></b></span></td>
			<td title='<?php echo $complete_item; ?>'><span class="value text-success"><b><?php echo round(($complete/24)*100)."%"; ?></b></span></td>
			
			
	</tr>
	<?php  } 
	}  	?>
	
	
	</tbody>
	 </table>
	</div>
	<div class="clearfix">	</div>
	</div>
			 
 <?php $this->endWidget(); ?>
 
 
