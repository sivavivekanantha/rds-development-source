<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'UploadForm',
                    'enableClientValidation'=>true,
					'htmlOptions' => array('enctype' => 'multipart/form-data'),
                    'clientOptions'=>array(
                    	'validateOnSubmit'=>true,     ),
                    )); 
                    ?>
    <?php
    Yii::app()->clientScript->registerScript('search', "
        $('.search-button').click(function(){
                $('.doc-form').toggle();
                return false;
        });
        $('.doc-form form').submit(function(){
                $.fn.yiiGridView.update('document-grid', {
                        data: $(this).serialize()
                });
                return false;
        });
        ");
    ?>
	
		
	<script type="text/javascript">
    function fileCheck(obj) {
	
            var fileExtension = ['xlsx', 'xls'];
           	 if ($.inArray($(obj).val().split('.').pop().toLowerCase(), fileExtension) == -1){
			 	 alert("Only '.xls','.xlsx'  formats are allowed.");
			 	$('#UploadForm_doc_file').val('');
			 }
    }
	</script>

   <div class="widget-body">
		  <div id="test"> </div>
                  <div id="dt_example" class="example_alt_pagination" style="padding:10px; margin:7px; border:dashed 1px #666666;">
				  <div id="formTitle"> <h3><u>Upload Files </u>	</h3></div>
					  <div class="row-fluid">                                 
			                <div style="float:left; overflow: hidden; width:200px"> 
							<?php echo $form->labelEx($model,'doc_file'); ?>
			                </div><div>
							<?php echo $form->fileField($model,'doc_file',array('style'=>'width:200px','onchange'=>'fileCheck(this)')); ?>  
							<?php echo $form->error($model,'doc_file'); ?>      
							<?php echo CHtml::submitButton('Upload',array('class'=>'btn btn-primary')); ?> 
			                 </div>
					</div>
					<div style="height: 50px;"></div>
					<div id="formTitle"> <h5><u>Excel Template for</u></h5></div>	
						<div class="row-fluid"><a href='<?php echo Yii::app()->baseUrl."/protected/img/RFID_Insert 2 Device.xlsx";?>' target="helperFrame"> RFID Insert to Device </a><br/><a href='<?php echo Yii::app()->baseUrl."/protected/img/RFID_Delete From Device.xlsx";?>' target="helperFrame">RFID Remove From Device </a></div>
					
					<?php if(strlen($msg1)>0) { ?>
					<div class="row-fluid" style="padding-left: 200px;">
            			<div class="span4">
              				<div class="widget">
                				<div class="widget-header">
                  					<div class="title">
                    					<span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span> Result
                  					</div>
                				</div>
                				<div class="widget-body">
                  					<div id="dt_example" class="example_alt_pagination">
                    					<table class="table table-condensed table-striped table-hover table-bordered" id="data-table">
                    				    <tr class="gradeA success">
										<td>
                            				<?php echo $msg1; ?> 
                          				</td></tr>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
    <?php $this->endWidget(); ?>
 </div>
 </div>

