<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/lightbox.css" />
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/lightbox.js"></script>
	
	<!-- blueprint CSS framework -->
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/icomoon/style.css" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<?php 	if(isset($_SESSION)) {
			$session=new CHttpSession;
			$session->open();	
		}

		if(Yii::app()->session['Header_Code']=='')
		{
			print_r('Session Expired'); exit();
		}
		$baseUrl = Yii::app()->baseUrl;      
	   	Yii::app()->clientScript->registerCoreScript('jquery'); 
		$cs = Yii::app()->getClientScript();	 ?>

    <link type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/nvd-charts.css" rel="stylesheet">

    <!-- Bootstrap css -->
    <link type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" rel="stylesheet">
	<link type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.timepicker.min.css" rel="stylesheet">
<link type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.feedBackBox.css" rel="stylesheet">
	<link type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/<?php echo Yii::app()->session['Theme'] ?>.css" rel="stylesheet">
    <!-- fullcalendar css -->
    <link type="text/css" href='<?php echo Yii::app()->request->baseUrl; ?>/css/fullcalendar/fullcalendar.css' rel='stylesheet' />
    <link type="text/css" href='<?php echo Yii::app()->request->baseUrl; ?>/css/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
			<link rel="stylesheet" href="../../../js/jqwidgets/styles/jqx.base.css" type="text/css" />

    <script type="text/javascript" src="../../../js/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="../../../js/jqwidgets/jqxdata.js"></script> 
    <script type="text/javascript" src="../../../js/jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="../../../js/jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="../../../js/jqwidgets/jqxmenu.js"></script>
    <script type="text/javascript" src="../../../js/jqwidgets/jqxcheckbox.js"></script>
    <script type="text/javascript" src="../../../js/jqwidgets/jqxlistbox.js"></script>
    <script type="text/javascript" src="../../../js/jqwidgets/jqxdropdownlist.js"></script>
    <script type="text/javascript" src="../../../js/jqwidgets/jqxgrid.js"></script>
    <script type="text/javascript" src="../../../js/jqwidgets/jqxgrid.sort.js"></script> 
    <script type="text/javascript" src="../../../js/jqwidgets/jqxgrid.pager.js"></script> 
    <script type="text/javascript" src="../../../js/jqwidgets/jqxgrid.selection.js"></script> 
    <script type="text/javascript" src="../../../js/jqwidgets/jqxgrid.edit.js"></script> 
 <script type="text/javascript" src="../../../js/jqwidgets/jqxgrid.columnsresize.js"></script> 
 <script type="text/javascript" src="../../../js/jqwidgets/jqxdata.export.js"></script> 
    <script type="text/javascript" src="../../../js/jqwidgets/jqxgrid.export.js"></script> 
    <script type="text/javascript" src="../../../js/jqwidgets/jqxmenu.js"></script>
    <script type="text/javascript" src="../../../js/jqwidgets/jqxgrid.filter.js"></script>
    <script type="text/javascript" src="../../../js/jqwidgets/jqxgrid.selection.js"></script> 
    <script type="text/javascript" src="../../../js/jqwidgets/jqxpanel.js"></script>
    <script type="text/javascript" src="../../../js/jqwidgets/jqxcheckbox.js"></script>
	<script type="text/javascript" src="../../../js/jqwidgets/jqxgrid.grouping.js"></script>
	<script>
			$(function () {
				$(document).bind("contextmenu",function(e){
					//Laax
					return;
					e.preventDefault();
					alert("Right Click is not allowed");
				});
			});

	

// Initialize the form
function init() {

  // Hide the form initially.
  // Make submitForm() the form's submit handler.
  // Position the form so it sits in the centre of the browser window.
  $('#contactForm').hide().submit( submitForm ).addClass( 'positioned' );

  // When the "Send us an email" link is clicked:
  // 1. Fade the content out
  // 2. Display the form
  // 3. Move focus to the first field
  // 4. Prevent the link being followed

  $('a[href="#contactForm"]').click( function() {
    $('#content').fadeTo( 'slow', .2 );
    $('#contactForm').fadeIn( 'slow', function() {
      $('#senderName').focus();
    } )

    return false;
  } );
  
  // When the "Cancel" button is clicked, close the form
  $('#cancel').click( function() { 
    $('#contactForm').fadeOut();
    $('#content').fadeTo( 'slow', 1 );
  } );  

  // When the "Escape" key is pressed, close the form
  $('#contactForm').keydown( function( event ) {
  	
    if ( event.which == 27 ) {
      $('#contactForm').fadeOut();
      $('#content').fadeTo( 'slow', 1 );
	 
    }
  } );

}


// Submit the form via Ajax

function submitForm() { 

  var contactForm = $(this);
var message = $('#message').val()

  // Are all the fields filled in?

  if (!$('#message').val()) {

    // No; display a warning message and return to the form
    $('#incompleteMessage').fadeIn().delay(messageDelay).fadeOut();
    contactForm.fadeOut().delay(messageDelay).fadeIn();

  } else {
 
    // Yes; submit the form to the PHP script via Ajax

    $('#sendingMessage').fadeIn();
    contactForm.fadeOut();
    $.ajax( {
     url:"<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/Feedback?msg="+encodeURIComponent(message)+"&ajax=true",
      type: contactForm.attr( 'method' ),
      data: contactForm.serialize(),
      success: submitFinished
    } );
  }

  // Prevent the default form submission occurring
  return false;
}


// Handle the Ajax response

function submitFinished() {
  $('#sendingMessage').fadeOut();

    $('#successMessage').fadeIn().delay(messageDelay+500).fadeOut('slow',0);
  
    $('#message').val( "" );

    $('#content').delay(messageDelay+1000).fadeTo( 'slow', 1 );

}
	</script>
</head>
<body>
  <header>
      <a href="#" class="logo"><h4>Resident Database System<span style="font-size: 11px;">  (Beta Version)</span></h4></a>
      <div id="mini-nav">
        <ul class="hidden-phone">
		<?php if(Yii::app()->session['Header_Code']==809 or Yii::app()->session['Header_Code']==740 or Yii::app()->session['Header_Code']==764 or Yii::app()->session['Header_Code']==806  or Yii::app()->session['Header_Code']==815 or Yii::app()->session['Header_Code']==1032 ) { ?>
          <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/search/Personview" data-toggle="modal" data-original-title="">
           Availability in Ashram
            </a>
          </li> 
		  
		  <?php } 
		  if(Yii::app()->session['Header_Code']==2) { ?>
		  <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Dashboard/summarychat" data-toggle="modal" data-original-title="">
          Dashboard
            </a></li>
			<?php } ?>
		  
		<!--  <li>
            <a href="#contactForm" data-toggle="modal" data-original-title="">
              Feedback
            </a></li>-->
          <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
              Theming
              <span class="caret icon-white"></span>
            </a>
            <ul class="dropdown-menu pull-right">
                            <li>
                <a href="#" id="default" onclick="themeroll('main')">Default</a>
              </li>
              <li>
                <a href="#" id="facebook" onclick="themeroll('facebook')">Facebook</a>
              </li>
              <li>
                <a href="#" id="foursquare" onclick="themeroll('foursquare')">Foursquare</a>
              </li>
              <li>
                <a href="#" id="google-plus" onclick="themeroll('google-plus')">Google+</a>
              </li>
              <li>
                <a href="#" id="instagram" onclick="themeroll('instagram')">Instagram</a>
              </li>
           
              
            </ul>
          </li>
          <li>
		  <?php if(Yii::app()->session['oHeader_Code']<1) {?>
	            <a href="<?php echo Yii::app()->request->baseUrl?>/index.php/site/Logout"><span class="fs1" aria-hidden="true" data-icon="&#xe0b1;" style="margin-top:10px;"></span></a>
		<?php } ?>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
    </header>

<div class="container-fluid">
	<div class="left-sidebar hidden-tablet hidden-phone">
         <div class="user-details">
              <div class="user-img">
                <img src="../../../protected/photos/<?php echo Yii::app()->session['Header_Code']; ?>.jpg?cod=<?php echo rand() ?>" class="avatar" alt="Avatar">
              </div>
              <div class="welcome-text"><span>Namaskaram</span><p class="name"><?php echo Yii::app()->session['Name']; ?></p>
              </div>
            </div>
         <div class="content">
             <div id="accordion1" class="accordion no-margin">
                <?php $this->widget('GroupWidget'); ?>
            </div></div>
        
	</div>
	 
</div>

	</div>
	 
	 <!--<div >

         <form id="contactForm" action="" method="post">
<br/>
  <h4 align="center">Share Your Experience</h4>

  <ul>
		<br/><br/>
    <!--<li>
      <label for="senderName">Your Name</label>
      <input type="text" name="senderName" id="senderName" placeholder="Please type your name" required="required" maxlength="40" />
    </li>

    <li>
      <textarea name="message" id="message" placeholder="Share your Experience about Beta version" required="required" cols="80" rows="6" maxlength="10000"></textarea>
    </li>

  </ul>

  <div id="">
    <input class="btn btn-primary" type="submit" id="sendMessage" name="sendMessage" value="Share"  />
    <input class="btn" type="button" id="cancel" name="cancel" value="Cancel" />
  </div>

</form>
        </div>-->
		<div id="sendingMessage" class="statusMessage"><p>Sending your message. Please wait...</p></div>
<div id="successMessage" class="statusMessage"><p>Thanks for Sharing your Experience!.</p></div>
	 
	</div>

<div class="dashboard-wrapper">
		<div id="main-nav" class="hidden-phone hidden-tablet">
        	<?php $this->widget('ModuleWidget') ?>
			<div class="clearfix"></div>
        </div>
        <div class="main-container">
    	   	 <?php echo $content; ?>
	    </div>
   </div>
   <!-- Footer-->
  
<div id="footer_wapper">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr><td>
	<div id="footer">
        	<table width="100%" border="0" cellspacing="3" cellpadding="0">
				
                <tr><td align="center" class="month" style="color: white;">Isha Foundation - A Non-profit Organization - Copyright 1997 - <?php echo  date('Y'); ?>. Powered by Isha IT .</td></tr>
                
              </table>
     </div>
</td></tr>
</table>
</div>
   
   
    <!-- Zebra datapicker -->
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/js/public/css/default_zebra.css" type="text/css">
	<link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/js/libraries/syntaxhighlighter/public/css/shCoreDefault.css">
	 <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/styles/jqx.base.css" type="text/css" />

	 <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/libraries/syntaxhighlighter/public/javascript/XRegExp.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/libraries/syntaxhighlighter/public/javascript/shCore.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/libraries/syntaxhighlighter/public/javascript/shLegacy.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/libraries/syntaxhighlighter/public/javascript/shBrushJScript.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/libraries/syntaxhighlighter/public/javascript/shBrushXml.js"></script>

        <script type="text/javascript">
            SyntaxHighlighter.defaults['toolbar'] = false;
            SyntaxHighlighter.all();
        </script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/public/javascript/zebra_datepicker.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/public/javascript/core.js"></script>
    <!-- Easy Pie Chart JS -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/pie-charts/jquery.easy-pie-chart.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/moment.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/datetimepickerrds.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.filter_input.js"></script>

    <!-- Flot charts -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/flot/jquery.flot.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/flot/jquery.flot.selection.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/flot/jquery.flot.tooltip.js"></script>
	

    <!-- Google Visualization JS -->

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxdraw.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxchart.core.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxdata.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxlistbox.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxdropdownlist.js"></script>
    	
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxdropdownlist.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxgrid.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxgrid.sort.js"></script> 
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxgrid.pager.js"></script> 
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxgrid.selection.js"></script> 
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxgrid.edit.js"></script> 
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxgrid.columnsresize.js"></script> 
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxdata.export.js"></script> 
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxgrid.export.js"></script> 
   	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxmenu.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxgrid.filter.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxgrid.selection.js"></script> 
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxpanel.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxcheckbox.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxgrid.grouping.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqwidgets/jqxcombobox.js"></script>


    <!-- Tiny scrollbar js -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/tiny-scrollbar.js"></script>
    
    <!-- Sparkline charts -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/sparkline.js"></script>

    <!-- Datatables JS -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.dataTables.js"></script>

    <!-- Calendar Js -->
    <script src='<?php echo Yii::app()->request->baseUrl; ?>/js/fullcalendar/jquery-ui-1.10.2.custom.min.js'></script>
    <script src='<?php echo Yii::app()->request->baseUrl; ?>/js/fullcalendar/fullcalendar.min.js'></script>

    <!-- Custom Js -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/custom-index.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/custom-calendar.js"></script>

    
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/custom.js"></script>
	
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/html5-trunk.js"></script>
	 <!-- Js Function -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/js_function.js"></script>
	 <!-- Timepicker -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.timepicker.min.js"></script>
	

    <script>
      
	   function themeroll(obj)
	  {
	  	var obj1='<?php echo Yii::app()->baseUrl;?>/css/'+obj+'.css';
	  	var url='<?php echo Yii::app()->baseUrl;?>/index.php/Ajax/Theme?thm='+obj;
		$("#tres").load(url);
		  var cssLink = $("<link>");
		  $("head").append(cssLink); //IE hack: append before setting href
		  cssLink.attr({
		    rel:  "stylesheet",
		    type: "text/css",
		    href: obj1
		  });
	}

    </script>
    
<?php 

if((Yii::app()->session['Freeze']=="Y" and Yii::app()->session['oHeader_Code']<1) or 
  (!stristr(Yii::app()->session['ViewCode'],','.Yii::app()->session['SubMcode'].',') === FALSE and stristr(Yii::app()->session['EditCode'],','.Yii::app()->session['SubMcode'].',') === FALSE))
		{ // profile is freeze, then hide all save buttons.  
	?>
		<script>
			$("#saveform").remove();
			$("#saveform1").remove();
			$("#saveform2").remove();
		</script>
<?php } ?>

</div>
<div id='tres'></div>
</body>
</html>