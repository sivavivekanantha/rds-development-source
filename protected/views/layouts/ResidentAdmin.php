<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/icomoon/style.css" />
   	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/icomoon/main.css" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<?php $baseUrl = Yii::app()->baseUrl;      
    Yii::app()->clientScript->registerCoreScript('jquery'); 
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl.'/js/html5-trunk.js'); 
	$cs->registerScriptFile($baseUrl.'/js/jquery.min.js'); 
	$cs->registerScriptFile($baseUrl.'/js/bootstrap.js'); 
	$cs->registerScriptFile($baseUrl.'/js/datetimepickerrds.js'); 
	$cs->registerScriptFile($baseUrl.'/themes/theme1.js'); 	  
	?>
</head>
<!-- style="background-image:url(images/bg.jpg);background-repeat:no-repeat;background-position:top" -->
<body>

<header>
     
      <div id="mini-nav">
        <ul class="hidden-phone">
		  <li>
            <a href="<?php echo Yii::app()->request->baseUrl?>/index.php/profile/ResidentAdminData"><span class="fs1" aria-hidden="true" style="margin-top:10px;"></span>
              ResidentAdmin Data
            </a></li>
           
            </ul>
        <div class="clearfix"></div>
      </div>
</header>
	<div class="container-fluid">
    <div class="row-fluid">
    <div class="signin" style="margin-top: -100px; width: 500;">
   
		<?php echo $content; ?>
	
    </div></div></div>
<div class="clear"></div>
</div><!-- page -->

</body>
</html>
