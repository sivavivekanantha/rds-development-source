<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	
	<!-- blueprint CSS framework -->
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/icomoon/style.css" />
	 <link type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/css.css" rel="stylesheet">
	<!-- <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/pusher.js"></script> -->
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
		<?php $baseUrl = Yii::app()->baseUrl;      
		   Yii::app()->clientScript->registerCoreScript('jquery'); 
			//$cs = Yii::app()->getClientScript();				
			?>
</head>
<body>
<div class="dashboard-wrapper" style="margin: 0px;">
	<div class="main-container">
    	<?php echo $content; ?>
	</div>
   </div>
</body>
</html>