<?php 
Yii::import('ext.MYPDF.*');

//Yii::import('array('application.vendors.yiiexcel.YiiExcel','ext.YiiReport.*',)');
class ReportController extends CommonController
{

	
	public function Createpdf($data,$head,$title){

        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', FALSE);
        spl_autoload_register(array('YiiBase','autoload'));

        // set document information
        $pdf->SetCreator(PDF_CREATOR);  
 
        $pdf->SetTitle($title);                
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Selling Report -2013", "selling report for Jun- 2013");
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('helvetica','', 8);
        $pdf->SetTextColor(80,80,80);
        $pdf->AddPage();
 
        //Write the html
        //$html = "<div style='margin-bottom:15px;'>This is testing HTML.</div>";
        //Convert the Html to a pdf document
       // $pdf->writeHTML($html, true, false, true, false, '');
 
        $header =$head; //TODO:you can change this Header information according to your need.Also create a Dynamic Header.
 
        // data loading
      //$data = $pdf->LoadData($row); //This is the example to load a data from text file. You can change here code to generate a Data Set from your model active Records. Any how we need a Data set Array here.
        // print colored table

      $pdf->ColoredTable($header,$data);
        // reset pointer to the last page
        $pdf->lastPage();
 
        //Close and output PDF document
        $pdf->Output('filename.pdf', 'I');
        Yii::app()->end();
 
    }

public function  actionCheckInOutDetails()
 {
	$menu = '';
 		if($_GET['id']==100) 			{ $Rcode = 1;	 $Bcode = 1; $menu = 'Resident  Availablity -Based on Date '; }
		elseif($_GET['id']==101)  		{ $Rcode = 1;	 $Bcode = 2; $menu = 'Resident  Availablity -Based on Group '; }
		elseif($_GET['id']==102) 		{ $Rcode = 1;	 $Bcode = 3; $menu = 'Resident  Availablity -Based on Department '; }
		elseif($_GET['id']==103) 		{ $Rcode = 1;	 $Bcode = 4; $menu = 'Resident  Availablity -Based on Person '; }		
		elseif($_GET['id']==104) 		{ $Rcode = 2;	 $Bcode = 1; $menu = 'Resident  Movement -Based on Date Period ';}
		elseif($_GET['id']==105) 		{ $Rcode = 2;	 $Bcode = 2; $menu = 'Resident  Movement -Based on Group ';}	
		elseif($_GET['id']==106) 		{ $Rcode = 2;	 $Bcode = 3; $menu = 'Resident  Movement -Based on Department ';}
		elseif($_GET['id']==107) 		{ $Rcode = 2;	 $Bcode = 4; $menu = 'Resident  Movement -Based on Person ';} 
 		
		
	
  		$model = new summaryReportForm();
		$Grouplist  = $this->DepartmentgroupDropDownList();				
		$model->fromdate=date('d-m-Y');
		$model->todate=date('d-m-Y');
		
		$model->ReportType=2;
		$model->Location=3;
		$model->Rid=$_GET['id'];
		
  	$this->render('SummaryReport',array('model'=>$model,'Grouplist'=>$Grouplist,'Rcode'=>$Rcode,'Bcode'=>$Bcode,'menu'=>$menu));
 }



	public function actionStayReportPdf()
	{

		$tObj = new ProfileModel;
		$row =$this->stayDropDownList_pdf();
		$head=array('Sno','Stayname','YESNO','Active','Sno','Stayname','YESNO','Active');
		$title='Stay form report';
		$this->Createpdf($row,$head,$title);
	}

	public function actionStayReportPdf1()
	{
		$tObj = new ProfileModel;
		$row =$this->stayDropDownList_pdf();
		
		$r = new YiiReport(array('template'=> someTemplate.xls));
		$r->load($row);
		echo $r->render(format, name);
	}
	

 
 
public function actionCreateExcel(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  
      $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                             ->setLastModifiedBy("Maarten Balliauw")
                             ->setTitle("Office 2007 XLSX Test Document")
                             ->setSubject("Office 2007 XLSX Test Document")
                             ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Test result file");
							
							 
							$EObj = new ReportModel;
		                    $row1 = $EObj->getConsolidateReport();

							for ($i = 0; $i <mssql_num_fields($row1)-1; ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(65).chr(65+$m)."1";									
								}
								else
									$eval = chr(65+$i)."1"; 						                           

	                        	$objPHPExcel->setActiveSheetIndex(0)
                            	->setCellValue($eval,  str_replace("_"," ",$fld->name));	
							
					 
					 			
							}	
							$i=1;
							while($fld1=mssql_fetch_array($row1))
							
							{
								
								$i++;
								$HeaderCode=$fld1['Header_Code'];	
								$profilephoto ='../photos/'.$HeaderCode.'.jpg';
								
								 if((file_exists($profilephoto)))
									$photo = "Yes";
						        else 
						   	    	$photo = "No";
	                        	$objPHPExcel->setActiveSheetIndex(0)
                            	->setCellValue("A".$i, $fld1['User_Name'])
								->setCellValue("B".$i, $fld1['Name'])
								->setCellValue("C".$i, $fld1['Gender'])
								->setCellValue("D".$i, $fld1['FTV_Date_of_Joining_In_Isha'])
								->setCellValue("E".$i, $fld1['Category'])
								->setCellValue("F".$i, $fld1['Date_of_Birth'])
								->setCellValue("G".$i, $fld1['Nationality'])
								->setCellValue("H".$i, $fld1['Marital_Status'])
								->setCellValue("I".$i, $fld1['Isha_CUG_MobileNo1'])
								->setCellValue("J".$i, $fld1['Other_MobileNo1'])
								->setCellValue("K".$i, $fld1['Isha_CUG_MobileNo2'])
								->setCellValue("L".$i, $fld1['Other_MobileNo2'])
								->setCellValue("M".$i, $fld1['Emailid'])
								->setCellValue("N".$i, $fld1['Foundation_Mail_Id'])
								->setCellValue("O".$i, $fld1['Country_Name'])
								->setCellValue("P".$i, $fld1['Birth_City'])
								->setCellValue("Q".$i, $fld1['Birth_Town'])
								->setCellValue("R".$i, $fld1['Religion'])								
								->setCellValue("S".$i, $fld1['Caste'])
								->setCellValue("T".$i, $fld1['Recivemail'])
								->setCellValue("U".$i, $fld1['Receiving_Ishanga_Mail'])
								->setCellValue("V".$i, str_replace("&amp","and",$fld1['Passport_Details']))
								->setCellValue("W".$i, str_replace("&amp","and",$fld1['Acadamic_Info']))
								->setCellValue("X".$i, str_replace("&amp","and",$fld1['Work_Experience_Details']))
								->setCellValue("Y".$i, str_replace("&amp","and",$fld1['Yatra_Details']))
								->setCellValue("Z".$i, $fld1['Enclosure_Details'])
								->setCellValue("AA".$i, $fld1['Teacher_Training_Details'])
								->setCellValue("AB".$i, $fld1['Mother_Tongue'])
								->setCellValue("AC".$i, $fld1['Language_Details'])
								->setCellValue("AD".$i, str_replace("&amp","and",$fld1['Current_Department_Details']))
								->setCellValue("AE".$i, str_replace("&amp","and",$fld1['Past_Department_Details']))
								->setCellValue("AF".$i, str_replace("&amp","and",$fld1['Contact_Info']))
								->setCellValue("AG".$i, str_replace("&amp","and",$fld1['Family_Members']))
								->setCellValue("AH".$i, str_replace("&amp","and",$fld1['Family_Responsibilities']))
								->setCellValue("AI".$i, str_replace("&amp","and",$fld1['Computer_Skills_Details']))
								->setCellValue("AJ".$i, str_replace("&amp","and",$fld1['Typing_Skills_Details']))
								->setCellValue("AK".$i, str_replace("&amp","and",$fld1['Program_Info']))
								->setCellValue("AL".$i, str_replace("&amp","and",$fld1['Stay_Info']))
								->setCellValue("AM".$i, str_replace("&amp","and",$fld1['Medical_Physical_Details']))
								->setCellValue("AN".$i, str_replace("&amp","and",$fld1['Medical_Mental_Details']))
								->setCellValue("AO".$i, $fld1['Blood_Group'])
								->setCellValue("AP".$i, str_replace("&amp","and",$fld1['Major_Surgeries_Details']))
								->setCellValue("AQ".$i, str_replace("&amp","and",$fld1['Physically_Challenged']))
								->setCellValue("AR".$i, str_replace("&amp","and",$fld1['Pschycological_Issue_Details']))
								->setCellValue("AS".$i, str_replace("&amp","and",$fld1['Criminal_Law_Breaking_Details']))
								->setCellValue("AT".$i, $photo)
								->setCellValue("AU".$i, $fld1['Header_Code']);
							}
	   		
		//$this->render('CreateExcel',array('row'=>$row));
	
			
		
       
 
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Consolidate Excel Report');
 
 
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
 
$fname = "RDS_Consolidate_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
 
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}

public function actionResidentReport(){
	
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  
      $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                             ->setLastModifiedBy("Maarten Balliauw")
                             ->setTitle("Office 2007 XLSX Test Document")
                             ->setSubject("Office 2007 XLSX Test Document")
                             ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Test result file");
							
							 
							$EObj = new ReportModel;
		                    $row1 = $EObj->getResidenceReport();

							for ($i = 0; $i <mssql_num_fields($row1)-1; ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(65).chr(65+$m)."1";									
								}
								else
									$eval = chr(65+$i)."1"; 						                           

	                        	$objPHPExcel->setActiveSheetIndex(0)
                            	->setCellValue($eval,($fld->name));	
							
					 
					 			
							}	
							$i=1;
							while($fld1=mssql_fetch_array($row1))
							
							{
								
								$i++;
								 $objPHPExcel->setActiveSheetIndex(0)
                            	->setCellValue("A".$i, $fld1['Status'])
								->setCellValue("B".$i, $fld1['Existing_Card'])
								->setCellValue("C".$i, $fld1['Gender'])
								->setCellValue("D".$i, $fld1['MaxSwipeTime'])
								->setCellValue("E".$i, $fld1['Total_Bramacharies'])
								->setCellValue("F".$i, $fld1['Total_Residence'])
								->setCellValue("G".$i, $fld1['Total_Gents'])
								->setCellValue("H".$i, $fld1['Total_Ladies'])
								->setCellValue("H".$i, $fld1['Total_Ladies']);
														
							}
	   		
	
			
		
       
 
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Resident Excel Report');
 
 
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
 
$fname = "RDS_Residence_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
 
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function setProperties($objPHPExcel) 
{
$objPHPExcel->getProperties()->setCreator("Isha IT")
                             ->setLastModifiedBy("Isha IT")
                             ->setTitle("RDS Report");
}
public function actionPersonalInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
	  
       $objPHPExcel->getProperties()->setSubject("Personal Info")
                             ->setDescription("About Personal Details.")
                             ->setKeywords("Personal")
                             ->setCategory("Reports");
								$styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								$sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);								
							$report = $_GET['per'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
							$objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:U3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');						
							$objPHPExcel->setActiveSheetIndex(0)->getCell('K1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('K1')->applyFromArray($styleA);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('K1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('K2')->setValue('Personal Details Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('K2')->applyFromArray($sty);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('K2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";								
								}
								else
									$eval = chr(66+$i)."3"; 
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
								      $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('K2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
                            	->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Title'])
								->setCellValue("F".$i, $fld1['Pet_Name'])
								->setCellValue("G".$i, $fld1['Name_Changed'])
								->setCellValue("H".$i, $fld1['Old_Name'])
								->setCellValue("I".$i, $fld1['Gazette_Change'])
								->setCellValue("J".$i, $fld1['RefNo'])
								->setCellValue("K".$i, $fld1['PublishedYear'])
								->setCellValue("L".$i, $fld1['PageNo'])
								->setCellValue("M".$i, $fld1['Gender'])
								->setCellValue("N".$i, $fld1['DOB'])
								->setCellValue("O".$i, $fld1['MaterialStatus'])
								->setCellValue("P".$i, $fld1['Nationality'])
								->setCellValue("Q".$i, $fld1['Country_Name'])
								->setCellValue("R".$i, $fld1['Birth_City'])
								->setCellValue("S".$i, $fld1['Birth_Town'])
								->setCellValue("T".$i, $fld1['Religion'])
								->setCellValue("U".$i, $fld1['FTV_DOJ_Isha']);
								    $objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Personal Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_Personal_Report_".date('d-m-Y Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionLanguageInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
	  
       $objPHPExcel->getProperties()->setSubject("Language Info")
                             ->setDescription("About Language Details.")
                             ->setKeywords("Language")
                             ->setCategory("Reports");
							 $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
                               $sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['lan'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
							$objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:I3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');							
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->applyFromArray($styleA);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E2')->setValue('Language Details Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->applyFromArray($sty);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";								
								}
								else
									$eval = chr(66+$i)."3"; 
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
								      $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('E2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{	
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)
                            	->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
								->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Language'])
								->setCellValue("F".$i, $fld1['Reading'])
								->setCellValue("G".$i, $fld1['Writing'])
								->setCellValue("H".$i, $fld1['Speaking'])
								->setCellValue("I".$i, $fld1['Typing']);
								$objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Language Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_Language_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}


public function actionAcadamicInfo()
{
			Yii::import('ext.phpexcel12.XPHPExcel');    
			$objPHPExcel= XPHPExcel::createPHPExcel();	
			$this->setProperties($objPHPExcel);
					$objPHPExcel->getProperties() ->setSubject("Academic Info")
					                               ->setDescription("About Academic Details.")
												   ->setKeywords("Academic")
												   ->setCategory("Reports");
							  $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								 $sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['acd'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
							$objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:M3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('G1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('G1')->applyFromArray($styleA);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('G1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('G2')->setValue('Academic Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('G2')->applyFromArray($sty);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('G2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
                            $objPHPExcel->setActiveSheetIndex(0)->getStyle('G2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 	
	                        	$objPHPExcel->setActiveSheetIndex(0)
                            	->setCellValue($eval,  str_replace("_"," ",$fld->name));
								$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
								$objPHPExcel->setActiveSheetIndex(0)
							->getStyle('G2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Category'])
								->setCellValue("F".$i, $fld1['Category_Others'])
								->setCellValue("G".$i, $fld1['Degree_Name'])
								->setCellValue("H".$i, $fld1['Institute_Name'])
								->setCellValue("I".$i, $fld1['Institute_City'])
								->setCellValue("J".$i, $fld1['Specialisation'])
								->setCellValue("K".$i, $fld1['YOP'])
								->setCellValue("L".$i, $fld1['Grade'])
								->setCellValue("M".$i, $fld1['Degree_Others']);
								$objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Academic Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_Academic_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionComputerSkillInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
	  
       $objPHPExcel->getProperties()->setSubject("Computer Skills Info")
                             ->setDescription("About Computer Skills Details.")
                             ->setKeywords("Computer")
                             ->setCategory("Reports");
							  $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								$sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['com'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
							$objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:F3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('D1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D1')->applyFromArray($styleA);
							$objPHPExcel->setActiveSheetIndex(0)->getCell('D2')->setValue('Computer Skills Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D2')->applyFromArray($sty);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
                            	->setCellValue($eval,  str_replace("_"," ",$fld->name));
								$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
								$objPHPExcel->setActiveSheetIndex(0)
							->getStyle('D2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Skill_Language'])
								->setCellValue("F".$i, $fld1['Skill_Details']);
								$objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Computer Skills Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_ComputerSkills_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionHobbiesSkillsInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
	  
      $objPHPExcel->getProperties()->setSubject("Hobbies Skills Info")
                             ->setDescription("About Hobbies Skills Details.")
                             ->setKeywords("Hobbies")
                             ->setCategory("Reports");
							 	 $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['hob'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:F3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('D1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D1')->applyFromArray($styleA);
							$objPHPExcel->setActiveSheetIndex(0)->getCell('D2')->setValue('Hobbies Skills Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D2')->applyFromArray($sty);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
                            	->setCellValue($eval,  str_replace("_"," ",$fld->name));
								$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
								$objPHPExcel->setActiveSheetIndex(0)
							->getStyle('D2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Hobbies_Name'])
								->setCellValue("F".$i, $fld1['Skill_Details']);
								    $objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Hobbies Skills Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_HobbiesSkill_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionWorkExpInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
	  
       $objPHPExcel->getProperties()->setSubject("Work Experience Info")
                             ->setDescription("About Work Experience Details.")
                             ->setKeywords("Work Experience")
                             ->setCategory("Reports");
						 $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								$sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['wei'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:S3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('J1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('J1')->applyFromArray($styleA);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('J1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
                           $objPHPExcel->setActiveSheetIndex(0)->getCell('J2')->setValue('Work Experience Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('J2')->applyFromArray($sty);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('J2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
                            	->setCellValue($eval,  str_replace("_"," ",$fld->name));
								$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
								$objPHPExcel->setActiveSheetIndex(0)
							->getStyle('J2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Designation'])
								->setCellValue("F".$i, $fld1['Company_Name'])
								->setCellValue("G".$i, $fld1['Responsibility'])
								->setCellValue("H".$i, $fld1['Occupation'])
								->setCellValue("I".$i, $fld1['Off_Address1'])
								->setCellValue("J".$i, $fld1['Off_Address2'])
								->setCellValue("K".$i, $fld1['Off_Area'])
								->setCellValue("L".$i, $fld1['City_Name'])
								->setCellValue("M".$i, $fld1['Off_Contact'])
								->setCellValue("N".$i, $fld1['Country_Name'])
								->setCellValue("O".$i, $fld1['District_Name'])
								->setCellValue("P".$i, $fld1['State_Name'])
								->setCellValue("Q".$i, $fld1['Off_ZipCode'])
								->setCellValue("R".$i, $fld1['Work_From'])
								->setCellValue("S".$i, $fld1['Work_To']);
								    $objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Work Experience Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_WorkExperience_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionProgramInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
	  
       $objPHPExcel->getProperties()->setSubject("Program Info")
                             ->setDescription("About Program Details.")
                             ->setKeywords("Program")
                             ->setCategory("Reports");
							 $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								 $sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['pgm'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:I3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->applyFromArray($styleA);			
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E2')->setValue('Program Details Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->applyFromArray($sty);			
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
						$objPHPExcel->setActiveSheetIndex(0)
							->getStyle('E2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['RDS_Pgm_Name'])
								->setCellValue("F".$i, $fld1['RDS_Pgm_Center'])
								->setCellValue("G".$i, $fld1['RDS_Pgm_Date'])
								->setCellValue("H".$i, $fld1['RDS_Pgm_Teacher'])
								->setCellValue("I".$i, $fld1['RDS_Pgm_Others']);
								    $objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Program Info Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_Program_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionTeacherInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
	  
       $objPHPExcel->getProperties()->setSubject("Teacher Training Info")
                             ->setDescription("About Teacher Training Details.")
                             ->setKeywords("Teacher Training")
                             ->setCategory("Reports");
							 $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								$sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['tea'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:I3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->applyFromArray($styleA);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E2')->setValue('Teachers Training Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->applyFromArray($sty);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
						$objPHPExcel->setActiveSheetIndex(0)
							->getStyle('E2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Program_Name'])
								->setCellValue("F".$i, $fld1['Language'])
								->setCellValue("G".$i, $fld1['Training_Completed'])
								->setCellValue("H".$i, $fld1['Handled_Session'])
								->setCellValue("I".$i, $fld1['Others']);
								    $objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Teacher Training Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_Teacher_Training_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionYatraInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
	  
      $objPHPExcel->getProperties()->setSubject("Yatra Info")
                             ->setDescription("About Yatra Details.")
                             ->setKeywords("Yatra")
                             ->setCategory("Reports");
							 $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								$sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['yat'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:G3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');							
							$objPHPExcel->setActiveSheetIndex(0)->getCell('D1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D1')->applyFromArray($styleA);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('D2')->setValue('Yatra Reports');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D2')->applyFromArray($styleA);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
						$objPHPExcel->setActiveSheetIndex(0)
							->getStyle('D2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
									$i++;								
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Yatra_Name'])
								->setCellValue("F".$i, $fld1['Yatra_Count'])
								->setCellValue("G".$i, $fld1['Yatra_Year']);
								    $objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Yatra Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_Yatra_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionCurrentDept(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
	  
      $objPHPExcel->getProperties()->setSubject("Current Department Info")
                             ->setDescription("About Current Department Details.")
                             ->setKeywords("Current Department")
                             ->setCategory("Reports");
							 $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								$sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['cdp'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:G3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('D1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D1')->applyFromArray($styleA);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('D2')->setValue('Current Department Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D2')->applyFromArray($sty);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
                            $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('D2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Department'])
								->setCellValue("F".$i, $fld1['Occupation'])
								->setCellValue("G".$i, $fld1['From_Date']);
								    $objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Current Department Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_Current_Department_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionDeptInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
	  
       $objPHPExcel->getProperties()->setSubject("Department Info")
                             ->setDescription("About Department Details.")
                             ->setKeywords("Department")
                             ->setCategory("Reports");
							 $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								 $sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['dep'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:I3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');							
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->applyFromArray($styleA);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E2')->setValue('Past Department Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->applyFromArray($sty);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
                            $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('E2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Department'])
								->setCellValue("F".$i, $fld1['Occupation'])
								->setCellValue("G".$i, $fld1['From_Date'])
								->setCellValue("H".$i, $fld1['To_Date'])
								->setCellValue("I".$i, $fld1['Remarks']);
								    $objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Department Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_Department_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionStayInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
	  
      $objPHPExcel->getProperties()->setSubject("Stay Info")
                             ->setDescription("About Stay Place Details.")
                             ->setKeywords("Stay")
                             ->setCategory("Reports");
							 $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								 $sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['sta'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:M3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('G1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('G1')->applyFromArray($styleA);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('G1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('G2')->setValue('Stay Place Reports');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('G2')->applyFromArray($sty);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('G2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
                            $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('G2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['CurrentStay'])
								->setCellValue("F".$i, $fld1['Provide_Isha'])
								->setCellValue("G".$i, $fld1['Outside_Stay_address'])
								->setCellValue("H".$i, $fld1['CottageName'])
								->setCellValue("I".$i, $fld1['BlockFloorName'])
								->setCellValue("J".$i, $fld1['Occupancy_Date'])
								->setCellValue("K".$i, $fld1['Primary_Applicant'])
								->setCellValue("L".$i, $fld1['Room_No'])
								->setCellValue("M".$i, $fld1['Stay_Type']);
								    $objPHPExcel->setActiveSheetIndex()->getStyle(
								'A5:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Stay Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
 
$fname = "RDS_Stay_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
 
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}

public function actionPhysicalInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
      $objPHPExcel->getProperties()->setSubject("Medical Physical Info")
                             ->setDescription("About Medical Physical Info Details.")
                             ->setKeywords("Physical")
							 ->setCategory("Reports");
							  $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,'align'  => 'Center',
								'name'  => 'Verdana'								
								));
								 $sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,'align'  => 'Center',
								'name'  => 'Verdana'								
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['phy'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:H3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->applyFromArray($styleA);
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E2')->setValue('Physical Illnes Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->applyFromArray($sty);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
                            $objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							$objPHPExcel->setActiveSheetIndex(0)
							->getStyle('E2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							/*$objPHPExcel->setActiveSheetIndex(0)
							->getStyle('A3:H3')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);*/
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Physical'])
								->setCellValue("F".$i, $fld1['Physical_Alignment'])
								->setCellValue("G".$i, $fld1['Physical_Frequency'])
								->setCellValue("H".$i, $fld1['Physical_Status']);
								    $objPHPExcel->setActiveSheetIndex(0)->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex(0)->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex(0)->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Physical Illness Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_Physical_Illness_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionMentalInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
     $objPHPExcel->getProperties()->setSubject("Medical Mental Info")
                             ->setDescription("About Medical Mental Info Details.")
                             ->setKeywords("Mental")
							 ->setCategory("Reports");
							  $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								 $sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['men'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:J3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('F1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('F1')->applyFromArray($styleA);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('F1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('F2')->setValue('Mental Illness Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('F2')->applyFromArray($sty);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('F2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
                            $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('F2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Mental'])
								->setCellValue("F".$i, $fld1['Mental_Medication'])
								->setCellValue("G".$i, $fld1['Hospital_History'])
								->setCellValue("H".$i, $fld1['Mental_Frequency'])
								->setCellValue("I".$i, $fld1['Mental_Status'])
								->setCellValue("J".$i, $fld1['Mental_Alignment']);
								    $objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Mental Illness Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_Mental_Illness_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionContactInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
      $objPHPExcel->getProperties()->setSubject("Contact Info")
                             ->setDescription("About Contact Info Details.")
                             ->setKeywords("Contact")
							 ->setCategory("Reports");
							  $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								$sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['con'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:P3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('H1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('H1')->applyFromArray($styleA);						
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('H1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('H2')->setValue('Contact Details Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('H2')->applyFromArray($sty);						
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('H2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
						      $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('H2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Emergency_Add_Type'])
								->setCellValue("F".$i, $fld1['Rel_Name'])
								->setCellValue("G".$i, $fld1['Relationship'])
								->setCellValue("H".$i, $fld1['Rel_Address'])
								->setCellValue("I".$i, $fld1['Country_Name'])
								->setCellValue("J".$i, $fld1['State_Name'])
								->setCellValue("K".$i, $fld1['District_Name'])
								->setCellValue("L".$i, $fld1['City_Name'])
								->setCellValue("M".$i, $fld1['Rel_Zip_Code'])
								->setCellValue("N".$i, $fld1['Rel_Phone_Code'])
								->setCellValue("O".$i, $fld1['Rel_Contact'])
								->setCellValue("P".$i, $fld1['Rel_Emailid']);
								    $objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Contact Details Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_Contact_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionIdentityInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
       $objPHPExcel->getProperties()->setSubject("Identity Proof Info")
                             ->setDescription("About Identity Proof Details.")
                             ->setKeywords("Identity Proof")
							 ->setCategory("Reports");
							  $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								$sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['ipi'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:I3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');							
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->applyFromArray($styleA);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E2')->setValue('Identity Proof Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->applyFromArray($sty);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
						      $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('E2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Enclouser_Name'])
								->setCellValue("F".$i, $fld1['IYC_Address'])
								->setCellValue("G".$i, $fld1['Card_No'])
								->setCellValue("H".$i, $fld1['Others'])
								->setCellValue("I".$i, $fld1['Card_Exp_Date']);
								 $objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Identity Proof Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_IdentityProof_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionImFamilyInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
	  
      $objPHPExcel->getProperties()->setSubject("Immediate Family Info")
                             ->setDescription("About Immediate Family Members Details.")
                             ->setKeywords("Immediate Family Members")
                             ->setCategory("Reports");
							 $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
                                 $sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['imf'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:P3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('H1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('H1')->applyFromArray($styleA);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('H1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('H2')->setValue('Immediate Family Members Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('H2')->applyFromArray($sty);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('H2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
						      $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('H2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Member_Name'])
								->setCellValue("F".$i, $fld1['Fam_Relation'])
								->setCellValue("G".$i, $fld1['Fam_Gender'])
								->setCellValue("H".$i, $fld1['Fam_Age'])
								->setCellValue("I".$i, $fld1['Fam_Occupation'])
								->setCellValue("J".$i, $fld1['Fam_Occupation_Address'])
								->setCellValue("K".$i, $fld1['Fam_Address'])
								->setCellValue("L".$i, $fld1['Fam_Phonecode'])
								->setCellValue("M".$i, $fld1['Fam_Contact'])
								->setCellValue("N".$i, $fld1['Fam_Emailid'])
								->setCellValue("O".$i, $fld1['Fam_Association'])
								->setCellValue("P".$i, $fld1['Department']);
								    $objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Immediate Family Members Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_ImmediateFamilyMembers_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionOtherFamilyInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
	  
       $objPHPExcel->getProperties()->setSubject("Other Family Info")
                             ->setDescription("About Other Family Members Details.")
                             ->setKeywords("Other Family Members")
                             ->setCategory("Reports");
							 $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								$sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['ofi'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:M3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('G1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('G1')->applyFromArray($styleA);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('G1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('G2')->setValue('Other Family Mebers Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('G2')->applyFromArray($sty);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('G2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
                        $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('G2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Member_Name'])
								->setCellValue("F".$i, $fld1['Fam_Relatives'])
								->setCellValue("G".$i, $fld1['Fam_Gender'])
								->setCellValue("H".$i, $fld1['Fam_Address'])
								->setCellValue("I".$i, $fld1['Fam_Phonecode'])
								->setCellValue("J".$i, $fld1['Fam_Contact'])
								->setCellValue("K".$i, $fld1['Fam_Emailid'])
								->setCellValue("L".$i, $fld1['Fam_Association'])
								->setCellValue("M".$i, $fld1['Department']);
								    $objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Other Family Members Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
 
$fname = "RDS_OtherFamilyMembers_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
 
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}

public function actionPersonalMail(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
	  
       $objPHPExcel->getProperties()->setSubject("Personal Mail Info")
                             ->setDescription("About Personal Mail Details.")
                             ->setKeywords("Personal Mail")
                             ->setCategory("Reports");
							 $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								$sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['pml'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:H3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->applyFromArray($styleA);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E2')->setValue('Personal Mail Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->applyFromArray($sty);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
						      $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('E2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Isahanga_Mail'])
								->setCellValue("F".$i, $fld1['Receive_Ishanga_Mail'])
								->setCellValue("G".$i, $fld1['Recivemail'])
								->setCellValue("H".$i, $fld1['Emailid']);
								$objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Personal Mail Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_PersonalMail_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}

public function actionPresentAdd(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
       $objPHPExcel->getProperties()->setSubject("Present Address Info")
                             ->setDescription("About Present Address Details.")
                             ->setKeywords("Present Address")
							 ->setCategory("Reports");
							  $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								$sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['pad'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:K3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');						
							$objPHPExcel->setActiveSheetIndex(0)->getCell('F1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('F1')->applyFromArray($styleA);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('F1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('F2')->setValue('Present Address Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('F2')->applyFromArray($sty);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('F2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
						      $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('F2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Present_Address'])
								->setCellValue("F".$i, $fld1['Pre_Village'])
								->setCellValue("G".$i, $fld1['Country_Name'])
								->setCellValue("H".$i, $fld1['State_Name'])
								->setCellValue("I".$i, $fld1['City_Name'])
								->setCellValue("J".$i, $fld1['District_Name'])
								->setCellValue("K".$i, $fld1['Pre_Pincode']);
								$objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Present Address Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_PresentAddress_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionPermanentAdd(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
       $objPHPExcel->getProperties()->setSubject("Permanent Address Info")
                             ->setDescription("About Permanent Address Details.")
                             ->setKeywords("Permanent Address")
							 ->setCategory("Reports");
							  $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								 $sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['pea'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:K3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');						
							$objPHPExcel->setActiveSheetIndex(0)->getCell('F1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('F1')->applyFromArray($styleA);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('F1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('F2')->setValue('Permanent Address Details');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('F2')->applyFromArray($sty);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('F2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
						      $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('F2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Per_Address'])
								->setCellValue("F".$i, $fld1['Per_Village'])
								->setCellValue("G".$i, $fld1['Country_Name'])
								->setCellValue("H".$i, $fld1['State_Name'])
								->setCellValue("I".$i, $fld1['City_Name'])
								->setCellValue("J".$i, $fld1['District_Name'])
								->setCellValue("K".$i, $fld1['Per_Pincode']);
								$objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Permanent Address Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_PermanentAddress_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionMobileInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
       $objPHPExcel->getProperties()->setSubject("Mobile Data Card Info")
                             ->setDescription("About Mobile Data Card Details.")
                             ->setKeywords("Mobile")
							 ->setCategory("Reports");
							  $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								$sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['mob'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:K3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');						
							$objPHPExcel->setActiveSheetIndex(0)->getCell('F1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('F1')->applyFromArray($styleA);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('F1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('F2')->setValue('Mobile Card Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('F2')->applyFromArray($sty);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('F2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
						      $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('F2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Phone_Code'])
								->setCellValue("F".$i, $fld1['Mobile_No'])
								->setCellValue("G".$i, $fld1['Service_Provider'])
								->setCellValue("H".$i, $fld1['Issued_By'])
								->setCellValue("I".$i, $fld1['Paid_By'])
								->setCellValue("J".$i, $fld1['Mobile_Plan'])
								->setCellValue("K".$i, $fld1['Mobile_Type']);
								$objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Mobile Data Card Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_MobileDataCard_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionVolunteeringAtIsha(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
       $objPHPExcel->getProperties()->setSubject("Volunteering atIsha Info")
                             ->setDescription("AboutVolunteeringatIsha Details.")
                             ->setKeywords("Volunteering")
							 ->setCategory("Reports");
							  $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								$sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['vai'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:F3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');						
							$objPHPExcel->setActiveSheetIndex(0)->getCell('D1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D1')->applyFromArray($styleA);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('D2')->setValue('Volunteering atIsha Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D2')->applyFromArray($sty);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
                        $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('D2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Program_Name'])
								->setCellValue("F".$i, $fld1['Type_Of_Volunteering']);
								$objPHPExcel->setActiveSheetIndex()->getStyle(
								'A5:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('VolunteeringatIsha Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_VolunteeringatIsha_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionVolunteeringAtLocal(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
       $objPHPExcel->getProperties()->setSubject("Volunteering Local Center Info")
                             ->setDescription("VolunteeringLocalCenter Details.")
                             ->setKeywords("Volunteering")
							 ->setCategory("Reports");
							  $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								 $sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['val'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:G3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('D1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D1')->applyFromArray($styleA);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('D2')->setValue('Volunteering LocalCenter Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D2')->applyFromArray($sty);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('D2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
						      $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('D2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Program_Name'])
								->setCellValue("F".$i, $fld1['Center_Name'])
								->setCellValue("G".$i, $fld1['Type_Of_Volunteering']);
								$objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Volunteering LocalCenter Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_VolunteeringLocal_Center_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionReferenceIsha(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
       $objPHPExcel->getProperties()->setSubject("Reference Info")
                             ->setDescription("About Reference Details.")
                             ->setKeywords("Reference")
							 ->setCategory("Reports");
							  $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								 $sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['rfi'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:I3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');							
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->applyFromArray($styleA);						
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E2')->setValue('Reference From Isha Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->applyFromArray($sty);						
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
						      $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('E2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Name'])
								->setCellValue("F".$i, $fld1['Designation_Address'])
								->setCellValue("G".$i, $fld1['Relationship'])
								->setCellValue("H".$i, $fld1['Contact_No'])
								->setCellValue("I".$i, $fld1['Email_Id']);
								$objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Reference Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_Reference_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionReferencePerson(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
       $objPHPExcel->getProperties()->setSubject("Reference CoreVolunteers Info")
                             ->setDescription("About Reference CoreVolunteers Details.")
                             ->setKeywords("CoreVolunteers")
							 ->setCategory("Reports");
							  $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								$sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['rfp'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:H3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');						
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->applyFromArray($styleA);						
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E2')->setValue('Reference CoreVolunteers Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->applyFromArray($sty);						
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
                        $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('E2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Name'])
								->setCellValue("F".$i, $fld1['Center_Name'])
								->setCellValue("G".$i, $fld1['Contact_No'])
								->setCellValue("H".$i, $fld1['Email_Id']);
								$objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Reference CoreVolunteers Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_Reference_CoreVolunteers_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionReferenceResident(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
       $objPHPExcel->getProperties()->setSubject("Reference Resident Info")
                             ->setDescription("About Reference Details.")
                             ->setKeywords("Resident")
							 ->setCategory("Reports");
							  $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								 $sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['rfr'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
                            $objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:G3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->applyFromArray($styleA);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E2')->setValue('Reference Resident Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->applyFromArray($sty);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
						      $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('E2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Name'])
								->setCellValue("F".$i, $fld1['Department'])
								->setCellValue("G".$i, $fld1['Contact_No']);
								$objPHPExcel->setActiveSheetIndex()->getStyle(
								'A5:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Reference Resident Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_ReferenceResident_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionPassportInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
       $objPHPExcel->getProperties()->setSubject("Passport Info")
                             ->setDescription("About Passport Details.")
                             ->setKeywords("Passport")
							 ->setCategory("Reports");
							  $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								 $sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['pas'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
							$objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:P3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');							
							$objPHPExcel->setActiveSheetIndex(0)->getCell('H1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('H1')->applyFromArray($styleA);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('H1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('H2')->setValue('Passport Info Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('H2')->applyFromArray($sty);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('H2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
						$objPHPExcel->setActiveSheetIndex(0)
							->getStyle('H1:H2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Nationality'])
								->setCellValue("F".$i, $fld1['Passport_No'])
								->setCellValue("G".$i, $fld1['Exp_Date'])
								->setCellValue("H".$i, $fld1['Passport_Issue'])
								->setCellValue("I".$i, $fld1['Nameinpassport'])
								->setCellValue("J".$i, $fld1['Dual_Citizen'])
								->setCellValue("K".$i, $fld1['Dual_Citizen_Info'])
								->setCellValue("L".$i, $fld1['Visa_Category'])
								->setCellValue("M".$i, $fld1['Visa_Type'])
								->setCellValue("N".$i, $fld1['Visa_Doc_No'])
								->setCellValue("O".$i, $fld1['Visa_issue_Date'])
								->setCellValue("P".$i, $fld1['Visa_Expiry_Date']);
								$objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Passport Info Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_PassportInfo_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionFamilyPgmInfo(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
       $objPHPExcel->getProperties()->setSubject("Family Members Program Info")
                             ->setDescription("About Family Members Program Details.")
                             ->setKeywords("MembersProgram")
							 ->setCategory("Reports");
							   $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								$sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['fpi'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
							$objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:H3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->applyFromArray($styleA);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('E2')->setValue('Family Member Program Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->applyFromArray($sty);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('E2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
						      $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('E2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['BSP'])
								->setCellValue("F".$i, $fld1['Hata_Yoga'])
								->setCellValue("G".$i, $fld1['Isha_Yoga'])
								->setCellValue("H".$i, $fld1['Samayama']);
								$objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Family Members Program Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_FamilyMembersProgram_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}
public function actionResidentAdmin(){
  	Yii::import('ext.phpexcel12.XPHPExcel');    
      $objPHPExcel= XPHPExcel::createPHPExcel();
	  $this->setProperties($objPHPExcel);
       $objPHPExcel->getProperties()->setSubject("Resident Admin Data Info")
                             ->setDescription("Resident Admin Data Details.")
                             ->setKeywords("ResidentAdminData")
							 ->setCategory("Reports");
							 $styleArray = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'D2691E'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								 $styleA = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
								));
								$sty = array(
								'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
								));
								$stylwe = array(
								'borders' => array(
								'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '000000'),
								),
								),
								);
							$report = $_GET['rad'];
							$EObj = new ReportModel;
		                    $row1 = $EObj->getIndividualExcelReport($report);
							$objPHPExcel->setActiveSheetIndex(0)->getCell('A3')->setValue('S.No');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3')->applyFromArray($styleArray);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:U3')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('K1')->setValue('Resident Database System');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('K1')->applyFromArray($styleA);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('K1')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('FFE8E5E5');
							$objPHPExcel->setActiveSheetIndex(0)->getCell('K2')->setValue('Resident Admin Data Report');
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('K2')->applyFromArray($sty);							
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('K2')->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setARGB('C0C0C0');
							for ($i = 0; $i <mssql_num_fields($row1); ++$i) 
							{														
								$fld=mssql_fetch_field($row1,$i);
								if($i>25){
									$m=$i-26;
									$eval = chr(66).chr(66+$m)."3";									
								}
								else
									$eval = chr(66+$i)."3"; 						                           
	                        	$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue($eval,  str_replace("_"," ",$fld->name));
						$objPHPExcel->setActiveSheetIndex(0)->getStyle($eval)->applyFromArray($styleArray);
						      $objPHPExcel->setActiveSheetIndex(0)
							->getStyle('K2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							}	
							$i=3;
							while($fld1=mssql_fetch_array($row1))
							{
								$i++;
	                        	$objPHPExcel->setActiveSheetIndex(0)																
								->setCellValue("A".$i, $i-3)
								->setCellValue("B".$i, $fld1['IVC_Code'])
                            	->setCellValue("C".$i, $fld1['User_Name'])
								->setCellValue("D".$i, $fld1['First_Name'])
								->setCellValue("E".$i, $fld1['Doj'])
								->setCellValue("F".$i, $fld1['UID'])
								->setCellValue("G".$i, $fld1['Active_Status'])
								->setCellValue("H".$i, $fld1['Category_Name'])
								->setCellValue("I".$i, $fld1['Card_Type_Name'])
								->setCellValue("J".$i, $fld1['Card_Serial_No'])
								->setCellValue("K".$i, $fld1['Card_IssueDate'])
								->setCellValue("L".$i, $fld1['Card_Expiry_Date'])
								->setCellValue("M".$i, $fld1['Card_Status'])
								->setCellValue("N".$i, $fld1['FTVC_InterviewDate'])
								->setCellValue("O".$i, $fld1['Alert_Flag'])
								->setCellValue("P".$i, $fld1['Security_Alert_Type'])
								->setCellValue("Q".$i, $fld1['Honorarium_Amount'])
								->setCellValue("R".$i, $fld1['Amenties_Card'])
								->setCellValue("S".$i, $fld1['Acard_Expiry_Date'])
								->setCellValue("T".$i, $fld1['Mcard_Expiry_Date'])
								->setCellValue("U".$i, $fld1['Card_Issued']);
								$objPHPExcel->setActiveSheetIndex()->getStyle(
								'A3:' . 
								$objPHPExcel->setActiveSheetIndex()->getHighestColumn() . 
								$objPHPExcel->setActiveSheetIndex()->getHighestRow()
								)->applyFromArray($stylwe);
							}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Resident Admin Data Report');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$fname = "RDS_ResidentAdminData_Report_".date('d-m-Y_Hi').".xls";
// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$fname);
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
//$objPHPExcel=setCellValue('A1')->applyFromArray($styleArray);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
      Yii::app()->end();
}



public function actionReport()
	{
		$model = new ReportForm();		
		$Aobj = new ReportModel();
		$msg ='';
		$dataReader = $Aobj->getReportDetails();
		$this->render('Report',array('dataReader'=>$dataReader,'model'=>$model,'Aobj'=>$Aobj));
	}




public function actionCreate() 
	{
			
		$model = new UploadForm();
		$EObj = new ReportModel();
			$uid ='';
			$rfid = '';
			$status = '';
			$fileName = '';
			$msg1 = '';
			$msg = '';

		if(isset($_POST['UploadForm']))
        {
			require_once(Yii::app()->basepath."/extensions/PHPExcel.php");
			
            $model->attributes=$_POST['UploadForm'];
			
            $model->doc_file=CUploadedFile::getInstance($model,'doc_file');

            $fileName = $model->doc_file->getTempName();
		
		
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		
		$objReader->setReadDataOnly(true);

		$objPHPExcel = $objReader->load($fileName);
		
		$objWorksheet = $objPHPExcel->getActiveSheet();

		$highestRow = $objWorksheet->getHighestRow();
		
		$highestColumn = $objWorksheet->getHighestColumn();

		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); 
		
			
		for ($row = 2; $row <= $highestRow; ++$row) 
		{	
	  		for ($col = 0; $col <= $highestColumnIndex; ++$col) 
			{

				if($col==0) $uid = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
				
				if($col==1) $rfid = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
				
				if($col==2) $status = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
				
				
			 }
			if(strlen($uid)>0 and strlen($rfid)>0 and strlen($status)>0){
				$EObj->uploadRfidToDb($uid,$rfid,substr($status,0,1));
				if(substr($EObj->msg,0,16)!="Changed database")
					$msg1.= "Row ".$row." - ".$this->dispInfo($EObj->msg,99)."<br>";	
			} else 	$msg1.= "Row ".$row." - Invalid Entry<br>";	
			
	    }
			$msg='Uploaded Successfully. After 15 Mins. action reflecting in Device.<br><br>';
	}	
			$this->render('Create',array('model'=>$model,'msg1'=>$msg.$msg1));		
	}

public function actionUserTypeReport()
 {
  
    $model = new UserTypeReportForm();
    
    $this->render('UserTypeReport',array('model'=>$model)); 
  
 }	
} 
?>
