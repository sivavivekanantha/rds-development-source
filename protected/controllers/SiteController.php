<?php
class SiteController extends Controller
{
   
    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
	 
public function retArray($recordSet)
	{

		$lstArray=array();
		
		while($field=mssql_fetch_object($recordSet))
		{	
			$lstArray[] = $field;
		
		} 
		
	return $lstArray;
		
	}
  public function dispInfo($ex,$action)
 {
	if(substr($ex,0,16)=="Changed database") {
		if($action<>3)
			return "<tr><td align='center'><div class='errorMessage'>Account has been created successfully. <br> Waiting for Admin Team Approval</div>";
	}
	elseif($action==9)
		return "<tr><td align='center'><div class='errorMessage'>Incomplete / Invalid entries for <br>".$ex."</div></td></tr>";
	elseif($action==99)
		return $ex;
	else
		return "<tr><td align='center'><div class='errorMessage'>".$ex."</div></td></tr>";
	}
	
	public function departmentDropDownList()
	{
		$objDepartment = new ProfileModel();
	    $department = $objDepartment->getDepartment();			
		return $this->retArray($department);

	}	 
	 
    public function actionIndex()
    {
		
		$model= new LoginForm; 
		$recSet = new LoginModel;
		$errMsg='';
		$this->pageTitle = "ISHA - Resident Database System";
		$this->layout='//layouts/column1';
		
		if(isset($_POST['l#g*v'])) 		 			// check form posting
	 	{
		
			$model->attributes=$_POST['LoginForm']; 
			$data = $recSet->LoginAuthendication($model);
			
			if($data!=1)	{
				$objUserdetails = mssql_fetch_array($data);
				Yii::app()->session['Header_Code'] = $objUserdetails['Header_Code'];
				Yii::app()->session['Name'] = $objUserdetails['Name'];	
				Yii::app()->session['Theme'] = $objUserdetails['Theme'];
				Yii::app()->session['UID'] = $objUserdetails['IVC_Code'];
				Yii::app()->session['Freeze'] = $objUserdetails['Freeze'];
				
				$this->redirect(array('Profile/personal'));  // redirect  page here
			}
			else
				$errMsg="Invalid Login";
			}		
        $this->render('index',array('model'=>$model,'message'=>$errMsg));
    }
	
	
    public function actionForgetPassword()
	{	
	
		$msg='';
		$model = new ForgetPasswordForm;								
		$pObj = new ProfileModel;
		$this->layout='//layouts/column1';
		 if(isset($_POST['f%3*w'])) 	
		{
			
		    $model->attributes=$_POST['ForgetPasswordForm'];
			$model->NewUserValidate($model);
			if($model->errflag==0)
			{			
			    $row1 = $pObj->checkForgotPassword($model);
				$msg = $this->dispInfo($pObj->msg,0);
				if(@mssql_num_rows($row1)>0)
				{  
				 $field=mssql_fetch_array($row1);
				 $UserName=$field['USER_NAME'];
				 $Password=$field['IVC_Password']; 
				 				 
				 $Credential="<table>";
	$Credential.="<tr><td>Namaskaram,</td></tr><br><br>
	<tr><td>You are recently requesting your credential info.</td></tr><br>
    <tr><td>Here are the info of your account:</td></tr><br>
	<tr><td>User Name : ".$UserName."</td></tr><br> 
	<tr><td>Password &nbsp;&nbsp;: ".$Password."</td></tr><br><br><br>
	<tr><td>Pranam.</td></tr></table>";	
	
	                 $msg = '<span style="color:red"> Password is sent to your registered Email ID</span>';	
					 $this->sendmail($model->emailId,'notification+kjdmh1j_3dui@gmail.com','ISHA RDS Account Recovery',$Credential);
					
			        }
			    } 
			}	
		$this->render('ForgetPassword',array('model'=>$model,'msg'=>$msg));
	  }
	  
	  public function actionNewUserCreation()
	{	
        $msg='';
		$model = new NewUserCreationForm;								
		$pObj = new ProfileModel;	
		$department=$this->departmentDropDownList();	
		$this->layout='//layouts/column1';
	  if(isset($_POST['n%7*#'])) 	
		{
			$action=1;
		    $model->attributes=$_POST['NewUserCreationForm'];
			$model->NewUserValidate($model);
			if($model->errflag==0)
			{							   
				$pObj->saveNewUserCreation($model,Yii::app()->session['Header_Code']);						
				$msg = $this->dispInfo($pObj->msg,$action);	
				
			}
			
		} 
		$this->render('NewUserCreation',array('model'=>$model,'msg'=>$msg,'department'=>$department));
	}
	
	 public function actionHelpdesk()
	{	
	
		$msg='';
		$model = new HelpDeskForm;								
		$pObj = new ProfileModel;
		$this->layout='//layouts/column1';
		 if(isset($_POST['h%#ld'])) 	
		{	
		    $model->attributes=$_POST['HelpDeskForm'];
			$model->HelpdeskValidate($model);
			if($model->errflag==0)
			{							   
			$Credential= 'Namaskaram,'.'<br><br><br>'.			
			'Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:    '.$model->name.'<br><br>'.'Mobile No&nbsp;&nbsp;&nbsp;:    '.$model->contactNo.'<br><br>'.'Email Id&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:    '.$model->emailId.'<br><br>'.'Description &nbsp;:  ' .$model->description.'<br><br><br><br>'.'Pranam.';
			         $msg = '<span style="color:red">Your query has been sent Successfully.</span>';	
					 $this->sendmail('prabhu.p@ishafoundation.org','notification+kjdmh1j_3dui@gmail.com','ISHA RDS - Help Me',$Credential);
			   }	
			}
		$this->render('Helpdesk',array('model'=>$model,'msg'=>$msg));
	  }

	
	
	
	

   public function actionLogout()
    {
		 Yii::app()->session->destroy();
		 Yii::app()->request->cookies->clear();
		 $this->redirect(array('../'));
		
	}
 public function sendmail($sendto,$sendfrom,$subject,$message)
 	{      
		$mail=Yii::app()->Smtpmail;
        $mail->SetFrom($sendfrom, 'Resident Data Base System');
        $mail->Subject = $subject;
        $mail->MsgHTML($message);
        $mail->AddAddress($sendto, "");
        if(!$mail->Send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        }else {
            //echo "Message sent!";
        }
 	}

public function actionError()
{
    if($error=Yii::app()->errorHandler->error)
        $this->render('error', $error);
}

}
?>
