<?php 

class SearchController  extends CommonController
{

	
	public function actionSearchData()
	{
		
		$model = new SearchForm();
		$sObj = new searchModel();
		$program = $this->programDropDownList();
		$center = $this->centerDropDownList();			
		$msg = "";
		$row = "";

		if(isset($_POST['SearchForm']))	
		{		
			$model->attributes = $_POST['SearchForm'];
			//print_r($model);
			$row = $sObj->getResidentDetails($model);
		}
		
		$this->render('Search',array('msg'=>$msg,'model'=>$model,
		'program'=>$program,'center'=>$center,'row'=>$row));
	}
	
	
	public function actionResidentSearch()
	{
		
		$model = new ResidenceForm();
		$sObj = new searchModel();
		$msg = "";
		$row = "";

		if(isset($_POST['ResidenceForm']))	
		{		
			$model->attributes = $_POST['ResidenceForm'];
			
			$row = $sObj->getResidentDetails($model);
		}
		
		$this->render('ResidentSearch',array('msg'=>$msg,'model'=>$model,
		'row'=>$row));
	}
	
	
	public function actionAjaxDate1()
	{
		
	    $sObj = new searchModel();
		$programDate = $sObj->dateDropDownList($_GET['programCode'],$_GET['centerCode']);
		
		$data=CHtml::listData($programDate,'Event_Id','Pgm_Date');
		echo '<option value="">Select</option>';
    	foreach($data as $Event_Id=>$Pgm_Date)
    	{
			if($Event_Id == $_GET['eventId'])
				echo CHtml::tag('option',
                   array('value'=>$Event_Id,'selected' => 'selected'),CHtml::encode($Pgm_Date),true);
			else
        		echo CHtml::tag('option',
                   array('value'=>$Event_Id),CHtml::encode($Pgm_Date),true);
    	} 
	}
	
		public function actionPersonView()
	{     	    
	    $PersonObj = new searchModel();	
		$row=$PersonObj->GetPersonViewDetails(Yii::app()->session['Header_Code']);		
		$row1=$PersonObj->GetResidentAttDetails();		
		$row2=$PersonObj->GetPersonCountDetails();		
		$tet ='';
		//$row4 = $PersonObj->GetPersonViewDetails(Yii::app()->session['Header_Code']);
		//$row3 = $PersonObj->GetResidentAttDetails();	
		$this->render('PersonView',array('row'=>$row,'row1'=>$row1,'tet'=>$tet,'row2'=>$row2));
	}
	public function actionAjaxPersonView()
	{    
    
	    $PersonObj = new searchModel();	
		$row=$PersonObj->saveResidentAttendance($_GET['cgrp'],$_GET['ugrp'],$_GET['dcnt']);			

	}
	public function actionRFshow()
	{
		$RFObj = new searchModel();
		$model = new RfSearchForm();
		$monitor = $_GET['monitor'];

		$this->layout='//layouts/column3';
		$row = $RFObj->GetRFSearchDetails($monitor);
		$this->render('RFSearch',array('model'=>$model,'row'=>$row,'monitor'=>$monitor));
	}
	
	
	
	public function actionRFshowauth()
	{
		$RFObj = new searchModel();
		$model = new RfSearchForm();
		$this->layout='//layouts/column3';
		$monitor =  $_GET['monitor'];
		$row = $RFObj->GetRFSearchDetails($monitor);
		if(mssql_num_rows($row)>0)
		{ 
			$rVal = array();
			while($field=mssql_fetch_array($row))
			{
				unset($rVal['Status']); unset($rVal['msg']);								
				if(strlen($field['DOB'])==10) $Dob = date('d-m-Y',strtotime($field['DOB'])); else $Dob='';  
				$rVal['Status'] = $field['Status'];
				
				if($field['Header_Code'] ==  NULL and $field['Disconnect']==0)
				{	
					if($field['Status']==1) $tfld="<div id='checkin_box'>";
					elseif($field['Status']==2) $tfld="<div id='checkout_box'>";
				
				$tfld.="<table width='100%' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td width='62%' valign='top'><table width='100%' border='0' cellspacing='5' cellpadding='0'>
      <tr>
        <td class='name' align='left'>INVALID SWIPING</td>
      </tr>
	</table></td>
  </tr>
  <tr>
    <td >&nbsp;</td>
  </tr>
  <tr>
    <td class='time' align='left'>Swiping Time : ".date('d M Y h:i a',strtotime($field['Swipe_Time']))."</td>
  </tr> </table>";
				} elseif($field['Header_Code'] ==  NULL and $field['Disconnect']==1){	
						if($field['Status']==1) $tfld="<div id='checkin_box'>";
					elseif($field['Status']==2) $tfld="<div id='checkout_box'>";
				
				$tfld.="<table width='100%' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td width='62%' valign='top'><table width='100%' border='0' cellspacing='5' cellpadding='0'>
      <tr>
        <td class='name'>DEVICE NOT CONNECTED</td>
      </tr>
	</table></td>
  </tr>
  <tr>
    <td >&nbsp;</td>
  </tr>
  <tr>
    <td class='time'>Time : ".date('d M Y h:i a',strtotime($field['Swipe_Time']))."</td>
  </tr> </table>";
				
				} elseif($field['Header_Code'] >0 and $field['Status']==1){	
					$tfld="<div id='checkin_box'>
       			<table width='100%' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td width='62%' valign='top'><table width='100%' border='0' cellspacing='5' cellpadding='0'>
      <tr>
        <td class='name'>".$field['First_Name']."</td>
      </tr>
      <tr>
        <td class='normal_text'>".$field['Department']."</td>
      </tr>
     
      <tr>
        <td class='normal_text'>Stay:".$field['Stay_Area']."</td>
      </tr>
      <tr>
        <td class='normal_text'>Gate: ".$field['Gate_Name']."</td>
      </tr>
    </table></td>
    <td width='38%' align='center'><img src='../../protected/photos/".$field['Header_Code'].".jpg?cod=944663287 ' width='193' height='187' class='img' /></td>
  </tr>
</table></td>
  </tr>
  <tr>
    <td >&nbsp;</td>
  </tr>
  <tr>
    <td class='time'>Check In Time : ".date('d M Y h:i a',strtotime($field['Swipe_Time']))."</td>
  </tr> </table></div>";
}
		elseif($field['Header_Code'] >0 and $field['Status']==2) {	
				 $tfld="<div id='checkout_box'>
       			<table width='100%' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td width='62%' valign='top'><table width='100%' border='0' cellspacing='5' cellpadding='0'>
      <tr>
        <td align='left' class='name'>".$field['First_Name']."</td>
      </tr>
      <tr>
        <td align='left' class='normal_text'>".$field['Department']."</td>
      </tr>
   
      <tr>
        <td align='left' class='normal_text'>Stay:".$field['Stay_Area']."</td>
      </tr>
      <tr>
        <td align='left' class='normal_text'>Gate:".$field['Gate_Name']."</td>
      </tr>
    </table></td>
    <td width='38%' align='center'><img src='../../protected/photos/".$field['Header_Code'].".jpg' width='193' height='187' class='img' /></td>
  </tr>
</table></td>
  </tr>
  <tr>
    <td >&nbsp;</td>
  </tr>
  <tr>
    <td align='left' class='time'>Check Out Time :".date('d M Y h:i a',strtotime($field['Swipe_Time']))."</td>
  </tr>
</table>
       </div>";
				
		}
				
				$rVal['msg'] = $tfld;
			}
			echo json_encode($rVal);
		} 
	} 
}