<?php 
class ProfileController extends CommonController
{	
	//public $frm;		
	// Start----------Passport Page ------------------
	public function actionPassport()
	{	
		$msg='';
		$model = new PassportForm;
		$tObj = new ProfileModel;	
		
		$result = $tObj->formvalidcheck(yii::app()->session['Header_Code']);				
		 $pcheck = $result['Passport'];
		if($pcheck>0) $model->formValid = $pcheck ; else $model->formValid = '';
				
			if(isset($_POST['op21%'])) 	
			{	
				$model->attributes=$_POST['PassportForm'];
			
				if($model->formValid==1)
					$model->passportValidate($model);
					
				if($model->errflag==0)
				{		
					$tObj->savePassport($model,Yii::app()->session['Header_Code']);
					$msg = $this->dispInfo($tObj->msg,2);				 						
				}
			
			}
				if($model->errflag==0)
				{
					$data = $tObj->getPassportDetails(Yii::app()->session['Header_Code']);
					if(mssql_num_rows($data)>0)
					{
						$field = mssql_fetch_array($data);
						$model->nationality   =     $field['Nationality'];
						$model->passportNo   =     $field['Passport_No'];
				if(strlen($field['Exp_Date'])==10)
				 $passportExpiryDate=date('d-m-Y',strtotime($field['Exp_Date']));
				  else $passportExpiryDate='';
				        $model->passportExpiryDate   = $passportExpiryDate;						
						$model->placeOfIssue   	 = $field['Passport_Issue'];
						$model->nameInPassport   = $field['Nameinpassport'];
						$model->dualCitizenship  = $field['Dual_Citizen'];
						$model->dualDetails  	 = $field['Dual_Citizen_Info'];
						$model->visaType  		 = $field['Visa_Type'];
						$model->documentNumber   = $field['Visa_Doc_No'];
			if(strlen($field['Visa_Issue_Date'])==10) $visaIssueDate=date('d-m-Y',strtotime($field['Visa_Issue_Date'])); else $visaIssueDate='';
				        $model->visaIssueDate   = $visaIssueDate;		
		if(strlen($field['Visa_Expiry_date'])==10) $visaExpiryDate=date('d-m-Y',strtotime($field['Visa_Expiry_date'])); else $visaExpiryDate='';
				        $model->visaExpiryDate   = $visaExpiryDate;								
						$model->visaCategory   	 = $field['Visa_Category'];
					}
				
				}
		$model->nationality = $tObj->getPassportNationalityDetails(Yii::app()->session['Header_Code']);		
		
		$this->render('Passport',array('model'=>$model,'msg'=>$msg,'pcheck'=>$pcheck));
	}
	//End-------------Passport Page ------------------
	
	// Start----------Personal Page ------------------
	public function actionPersonal()
	{	
		$msg='';
		$model = new PersonalForm;								
		$pObj = new ProfileModel;					
				
		$country   		 = $this->countryDropDownList();					
		$birthCountry    = $this->birthCountryDropDownList();
					
		$Phonecode  	 = $this->PhonecodeDropDownList();
		$nationality  	 = $this->nationalityDropDownList();	
				
		if(isset($_POST['12as#'])) 	
		{
		    $action=1;
			$model->attributes=$_POST['PersonalForm'];
			
			$model->personalValidate($model);
			if($model->errflag==0)
			{							   
				$pObj->savePersonal($model,Yii::app()->session['Header_Code']);						
				$msg = $this->dispInfo($pObj->msg,$action);	
			}
			$uploadedFile = CUploadedFile::getInstance($model,'photo');
			if ((is_object($uploadedFile) and	 get_class($uploadedFile)==='CUploadedFile'))
			{
				$fileName = Yii::app()->session['Header_Code'].'.jpg';  // random number + file name
				$model->photo =Yii::app()->session['Header_Code'];		
				$uploadedFile->saveAs(Yii::app()->basePath.'/photos/'.$fileName); 
			}
				
		 } 
		 if($model->errflag<>1){
		 	$data = $pObj->getPersonalDetails(Yii::app()->session['Header_Code']);
			if(mssql_num_rows($data)>0)
			{
				$field = mssql_fetch_array($data);
				$model->title 		= $field['Title'];
				$model->firstName 	= $field['First_Name'];
				$model->Aliasname 	= $field['Pet_Name'];
				$model->previousName= $field['Old_Name'];
				$model->changeName 	= $field['Name_Changed'];
				$model->gender	    = $field['Gender'];
				if(strlen($field['DOB'])==10) $DOB=date('d-m-Y',strtotime($field['DOB'])); else $DOB='';
				$model->dob   = $DOB;
				$model->nationality   	= $field['Nationality'];			
				$model->maritalStatus  	= $field['MaterialStatus'];
				$model->gazetteChange   = $field['Gazette_Change'];
				$model->refNo        	= $field['RefNo'];
				if($field['PageNo']==0) 
				   $model->pageNo='';
				else 
				  $model->pageNo= $field['PageNo'];
				if(strlen($field['PublishedYear'])==10) 
				$PublishedYear=date('d-m-Y',strtotime($field['PublishedYear'])); else $PublishedYear='';
				$model->dob2   		 	= $PublishedYear;
				$model->religion   		= $field['Religion_Code'];
				$model->birthTown   	= $field['Birth_Town'];
				$model->birthCountry  	= $field['Birth_Country'];
				$model->birthCity   	= $field['Birth_City'];
				
				if(strlen($field['FTV_DOJ_Isha'])==10) $FTV=date('d-m-Y',strtotime($field['FTV_DOJ_Isha']));
				 else $FTV='';
				$model->FTV_DOJ_Isha   	= $FTV;
				
				$model->otherReligion   = '';
				
		}
		 }
			
		$Religion  = $this->ReligionDropDownList();	

		$this->render('Personal',array('Religion'=>$Religion,'model'=>$model,'msg'=>$msg,'Phonecode'=>$Phonecode,
		'nationality'=>$nationality,'birthCountry'=>$birthCountry));
	}
	//End----------Personal Page ------------------
	
	
	
		// Start----------PersonalContact Page ------------------
	public function actionPersonalContact()
	{	

		$msg='';
		$model = new PersonalContactForm;								
		$pObj = new ProfileModel;			
		$pObj1 = new ProfileModel;			
					
		$country   	 = $this->countryDropDownList();					
	
				
		 if(isset($_POST['oc&*5'])) 	
		 {
		    $action=1;
			$model->attributes=$_POST['PersonalContactForm'];
			
			$model->PersonalContactValidate($model);
			if($model->errflag==0)
			{							   
				$pObj->savePresentAddress($model,Yii::app()->session['Header_Code']);						
				$msg = $this->dispInfo($pObj->msg,$action);	
			}
			
		 }  
		 if($model->errflag==0)
			{							   
			$data = $pObj->getPresentAddressDetails(Yii::app()->session['Header_Code']);
			if(mssql_num_rows($data)>0)
			{
				$field = mssql_fetch_array($data);
		
				
				$model->presentAddress   	    = $field['Res_Address1'];
				$model->presentVillage   		= $field['Res_Area'];
				$model->presentCountry   	    = $field['Res_Country'];
				$model->presentState  	 	    = $field['Res_State'];
				$model->presentDistrict  	 	= $field['Res_District'];
				$model->presentCity   	        = $field['Res_City'];
				$model->presentPincode   	 	= $field['Res_Zipcode'];
				 $model->addressType   	 		=  $field['Address_Type'];
				
		}
		
		$data = $pObj1->getPermanentAddressDetails(Yii::app()->session['Header_Code']);
			if(mssql_num_rows($data)>0)
			{
				$field = mssql_fetch_array($data);
				
				
				$model->permanentAddress   	    = $field['Res_Address1'];
				$model->permanentVillage   		= $field['Res_Area'];
				$model->permanentCountry   	    = $field['Res_Country'];
				$model->permanentState  	 	= $field['Res_State'];
				$model->permanentDistrict  	 	= $field['Res_District'];
				$model->permanentCity   	    = $field['Res_City'];
				$model->permanentPincode   	 	= $field['Res_Zipcode'];
				 $model->aType   	 			= $field['Address_Type'];
		}
		}
		$this->render('PersonalContact',array('model'=>$model,'msg'=>$msg,'country'=>$country));
	}
	//End----------PersonalContact Page ------------------
	
	//Start----------PersonalEmail Page ------------------
	public function actionPersonalEmail()
	{	

		$msg='';
		$model = new PersonalEmailForm;								
		$pObj = new ProfileModel;		
				
	  if(isset($_POST['e&%ma'])) 	
		{
			 $action=1;
		    $model->attributes=$_POST['PersonalEmailForm'];
			
			$model->personalEmailValidate($model);
			if($model->errflag==0)
			{							   
				$pObj->savePersonalEmail($model,Yii::app()->session['Header_Code']);						
				$msg = $this->dispInfo($pObj->msg,$action);	
			}
			
		}   if($model->errflag==0)
		   {
			$data = $pObj->getPersonalEmailDetails(Yii::app()->session['Header_Code']);
			if(mssql_num_rows($data)>0)
			{
				$field = mssql_fetch_array($data);
				$model->emailId   	        = $field['Emailid'];
				$model->isahangaMail   		= $field['Isahanga_Mail'];
				$model->reciveMail   	    = $field['Recivemail'];
				$model->receiveIshangaMail 	= $field['Receive_Ishanga_Mail'];
				
		    }
		}
		$this->render('PersonalEmail',array('model'=>$model,'msg'=>$msg));
	}
	
	
	
	
		public function actionmobileDataCard()
	{
		
		$i=0;
		$model = new MobileDataForm;			
		$tObj = new ProfileModel;
		$MPhoneCode  	 = $this->PhonecodeDropDownList();	
		$msg1 ='';	
				
			if(isset($_POST['m9^&8'])) 		// check form postingc && add & Edit	 
			{
				$model->attributes=$_POST['MobileDataForm'];
				if($model->action<>3) $model->mobileDataValidate($model);	
					if($model->errflag==0)
					{
						$tObj->savePersonalMobile($model,Yii::app()->session['Header_Code']);
						$msg1 = $this->dispInfo($tObj->msg,$model->action);				 						
					}
			}
		$row1 = $tObj->getPersonalMobileDetails(Yii::app()->session['Header_Code']);
		$this->render('mobileDataCard',array('model'=>$model,'row1'=>$row1,'MPhoneCode'=>$MPhoneCode,'msg1'=>$msg1));
	}
	
	
	
	
	public function actionVolunteeringHistory()
	{	

		$model = new VolunteeringIshaForm;	
		$model1 = new VolunteeringLocalForm;	
		$model2 = new VolunteeringTextAreaForm;			
		$tObj = new ProfileModel;	
		$msg1 ='';
		$msg2 ='';	
		$msg3 ='';		
		
		$center=$this->centerDropDownList();	
		$program=$this->programDropDownList();	
			
			if(isset($_POST['i%$v#'])) 		// check form postingc && add & Edit
			{
				$model->attributes=$_POST['VolunteeringIshaForm'];
				if($model->action<>3) $model->volunteeringIshaValidate($model);	
				if($model->errflag==0)
				{
					$tObj->saveVolunteeringAtIsha($model,Yii::app()->session['Header_Code']);
					$msg1 = $this->dispInfo($tObj->msg,$model->action);				 							}
			} 
			if(isset($_POST['&j*#h'])) 		// check form postingc && add & Edit
			{
				$model1->attributes=$_POST['VolunteeringLocalForm'];
				if($model1->action1<>3) $model1->volunteeringLocalValidate($model1);	
					if($model1->errflag==0)
					{
						$tObj->saveVolunteeringAtLocal($model1,Yii::app()->session['Header_Code']);
						$msg2 = $this->dispInfo($tObj->msg,$model1->action1);				 						
					}
					
			}
				
			if(isset($_POST['9hj%@']))
			{	
				 $model2->attributes = $_POST['VolunteeringTextAreaForm'];
				 $tObj->saveVolunteeringTextAreaDetails($model2,Yii::app()->session['Header_Code']);				 $msg3 = $this->dispInfo($tObj->msg,$model2->action2);
			}
			
			$result = $tObj->formvalidcheck(yii::app()->session['Header_Code']);				
			$Ccheck = $result['Volunteering_Isha_Center'];
			if($Ccheck>0) $model->formValid = $Ccheck; else $model->formValid = '';
			
			$result = $tObj->formvalidcheck(yii::app()->session['Header_Code']);				
			$TYcheck = $result['Volunteering_Local_Center'];
			if($TYcheck>0) $model1->formValid1 = $TYcheck; else $model1->formValid1 = '';
			
			$row1 = $tObj->getIshaCenterDetails(Yii::app()->session['Header_Code']);
			$row2 = $tObj->getLocalCenterDetails(Yii::app()->session['Header_Code']);						$data = $tObj->getVolunteeringTextAreaDetails(Yii::app()->session['Header_Code']);
			if(mssql_num_rows($data)>0)
			{					
				$tArea=mssql_fetch_array($data);				
				$model2->stayedIsha = $tArea['Volunteering_Stay'];
			}
			
			$this->render('VolunteeringHistory',array('model'=>$model,'model1'=>$model1,'model2'=>$model2,'msg1'=>$msg1,'msg2'=>$msg2,'msg3'=>$msg3,'program'=>$program,'center'=>$center,'row1'=>$row1,'row2'=>$row2));
		}
	
	//End----------PersonalEmail Page ------------------
	
	
	
	public function actionReference()
	{	

		$model = new ReferenceForm;	
		$model1 = new ReferenceIshaForm;	
		$model2 = new ReferenceResidentForm;			
		$tObj = new ProfileModel;			
		$msg1 ='';
		$msg2 ='';	
		$msg3 ='';		
		
		$department=$this->departmentDropDownList();	
		$center=$this->centerDropDownList();	
		
				
				if(isset($_POST['%$r#f'])) 		// check form postingc && add & Edit	 
			{	
				$model->attributes = $_POST['ReferenceForm'];	
				if($model->action<>3) $model->referenceEmailValidate($model);
				if($model->errflag==0)
				{					
			    	 $tObj->saveProfessionalReference($model,Yii::app()->session['Header_Code']);	
					 $msg1 = $this->dispInfo($tObj->msg,$model->action);			 						
				} 
													
			}	
			
			if(isset($_POST['*gf%&'])) 		// check form postingc && add & Edit	 
			{	
				$model1->attributes = $_POST['ReferenceIshaForm'];	
				if($model1->action<>3) $model1->referenceIshaEmailValidate($model1);
				if($model1->errflag==0)
				{					
			    	 $tObj->saveVolunteerReference($model1,Yii::app()->session['Header_Code']);	
					 $msg2 = $this->dispInfo($tObj->msg,$model1->action);			 						
				} 
				
			}	
				if(isset($_POST['r$e*y'])) 		// check form postingc && add & Edit	 
			{	
				$model2->attributes = $_POST['ReferenceResidentForm'];	
				if($model2->action2<>3) $model2->referenceResidentEmailValidate($model2);
				if($model2->errflag==0)
				{					
			    	 $tObj->saveResidentReference($model2,Yii::app()->session['Header_Code']);	
					 $msg3 = $this->dispInfo($tObj->msg,$model2->action2);			 						
				} 
			}
		
				
			
			$row1 = $tObj->getProfessionalRefDetails(Yii::app()->session['Header_Code']);
			$row2 = $tObj->getVolunteerRefDetails(Yii::app()->session['Header_Code']);
			$row3 = $tObj->getResidentRefDetails(Yii::app()->session['Header_Code']);					
				
			
			$this->render('Reference',array('model'=>$model,'model1'=>$model1,'model2'=>$model2,'msg1'=>$msg1,
			'msg2'=>$msg2,'msg3'=>$msg3,'department'=>$department,'center'=>$center,'row1'=>$row1,'row2'=>$row2,'row3'=>$row3));
		}
	
	
	
	
	
	
	//Start--------Stay Page ------------------

	public function actionstay()
	{
			
		$model=new StayForm;
		$tObj = new ProfileModel;		
	
		
			$row='';
			$msg='';
			$stay1 = $this->stayDropDownList();		
                        $cottage1=$this->cottageDropDownList();	
			$year =$this->showYear();	
			$month =$this->showMonth();
			//$floor1=$this->floorDropDownList();	
			
		if(isset($_POST['0as$%'])) 	
		{
			$model->attributes=$_POST['StayForm'];		
			$model->stayValidate($model);
			if($model->errflag==0)
			{
			
				$tObj->saveStay($model,Yii::app()->session['Header_Code']);
				$msg = $this->dispInfo($tObj->msg,2);				 						
			} 
			
		
		}
		
		if($model->errflag==0){
			$row = $tObj->getStayDetails(Yii::app()->session['Header_Code']);
			if(mssql_num_rows($row)>0)
			{
				$field = mssql_fetch_array($row);		
				$model->stayPlace	= $field['Stay_Code'];			
				$model->cottageName	= $field['Cottage_Name'];
				$model->cottageNo	= $field['Room_No'];
				$model->floorName   = $field['Sub_Stay_Code'];
				$model->cottageMember   = $field['Applicant_Type'];
				if(strlen($field['Occupancy_Date'])==10) 
				{
					$model->occupayMonth	= date('m',strtotime($field['Occupancy_Date']));					
					$model->occupayYear		= date('Y',strtotime($field['Occupancy_Date']));
				}			
				$model->cottageAllot	= $field['Stay_Type'];
				$model->memberName		= $field['Primary_Applicant'];
				$model->stayProvide	= $field['Provide_Isha'];
				$model->stayAddress	= $field['Outside_Stay_address'];
					
			}
		}

		$this->render('Stay',array('model'=>$model,'msg'=>$msg,'row'=>$row,'stay1'=>$stay1,'cottage1'=>$cottage1,'year'=>$year,'month'=>$month));
	
	}
	//End--------Stay Page ------------------
	//Start--------Yatra Page ------------------
	
	public function actionYatra()
	{
		$model = new YatraForm;			
		$tObj = new ProfileModel;
		$msg1 ='';

		$result = $tObj->formvalidcheck(yii::app()->session['Header_Code']);				
		$Ycheck = $result['Yatra'];
		if($Ycheck>0) $model->formValid = $Ycheck ; else $model->formValid = '';
		
		$yatraName = $this->yatraDropDownList();
		$year =$this->showYear();	
				
			if(isset($_POST['y&@ta'])) 		// check form postingc && add & Edit	 
			{
				$model->attributes=$_POST['YatraForm'];
				if($model->action<>3) $model->yatraValidate($model);	
					if($model->errflag==0)
					{
						$tObj->saveYatra($model,Yii::app()->session['Header_Code']);
						$msg1 = $this->dispInfo($tObj->msg,$model->action);				 						
					} 	
			}
		$row1 = $tObj->getYatraDetails(Yii::app()->session['Header_Code']);
		$this->render('Yatra',array('model'=>$model,'row1'=>$row1,'yatraName'=>$yatraName,'msg1'=>$msg1,
		'year'=>$year));
	}

	public function showYear()
	{
		$listYear=array();
		for($year=date('Y');$year>1984;$year--)
		{	
			$listYear[$year]=$year;
		}	
		return $listYear;

	}
	public function showMonth()
	{
		$listMonth=array();
		for($month=1;$month<=12;$month++)		
		{	
			if($month<10) $month='0'.$month;
			$listMonth[$month]=date("M",strtotime("01-".$month."-2013"));
		}
		return $listMonth;
	}
//End--------Yatra Page ------------------
//Start--------TeacherTraining Page ------------------

	public function actionTeacherTraining()
	{
		$model = new TeacherTrainingForm;			
		$tObj = new ProfileModel;
					
		$msg1 ='';			
		$others='';				
		$program=$this->programDropDownList();
		$language=$this->languageDropDownList();
		
		
			if(isset($_POST['t%*er'])) 		// check form postingc && add & Edit	 
			{
				$model->attributes = $_POST['TeacherTrainingForm'];
				if($model->action<>3) $model->teacherTrainingValidate($model);	
				if($model->errflag==0)
				{				
					$tObj->saveTeacherTraining($model,Yii::app()->session['Header_Code'],$others);		
					$msg1 = $this->dispInfo($tObj->msg,$model->action);
				
				}
				
			}
			$result = $tObj->formvalidcheck(yii::app()->session['Header_Code']);				
			$Tcheck = $result['Teacher_Training'];
			
			if($Tcheck>0) $model->formValid = $Tcheck ; else $model->formValid = '';	
			$row1=$tObj->getTeacherTrainingDetails(Yii::app()->session['Header_Code']);
		$this->render('TeacherTraining',array('model'=>$model,'msg1'=>$msg1,'program'=>$program,
		'language'=>$language,'row1'=>$row1));
		
	}

//End--------TeacherTraining Page ------------------
//Start--------Official Page ------------------
	public function actionOfficial()
	{	
	
		$model 	= new OfficialForm();
		$model1 = new OfficialOtherForm();
		
		$Occupation = $this->OccupationDropDownList();
		$country    = $this->countryDropDownList();
		$Oobj = new ProfileModel();
		$msg1 = '';
		$msg2 = '';			
		$form_valid =1;
			$result = $Oobj->formvalidcheck(yii::app()->session['Header_Code']);	
			$Ocheck = $result['Official'];
			if($Ocheck>0) $model1->formValid1 = $Ocheck ; else $model1->formValid1 = '';

			if(isset($_POST['o$3fl']))
			{
				 $model->attributes = $_POST['OfficialForm'];
				 if($model->action<>3) $model->officeValidate($model);	
			
					if($model->errflag==0)
					{
					
		 				$Oobj->saveOfficialDetails($model,Yii::app()->session['Header_Code']);		
		 				$msg1 = $this->dispInfo($Oobj->msg,$model->action);
						$model->unsetAttributes(array('Occupationtype','Designation','Responsibility','CompanyName','Address','Area','country','state','district','city','pincode','fromdate','todate','oState','oCity'));
						
	 				}
			}
			if(isset($_POST['o%4*7']))
			{
			 	$model1->attributes = $_POST['OfficialOtherForm'];
		 		$Oobj->saveOfficialOtherDetails($model1,Yii::app()->session['Header_Code']);		 
		 		$msg2 = $this->dispInfo($Oobj->msg,$model1->action1);
			} 
			
			$result = $Oobj->formvalidcheck(yii::app()->session['Header_Code']);	
			$Ocheck1 = $result['Official'];
			if($Ocheck1>0) $model->formValid = $Ocheck1 ; else $model->formValid = '';	
			
		    $row1=$Oobj->getOfficialDetails(Yii::app()->session['Header_Code']);
			$model1->OfficialOther = $Oobj->getOfficialOtherDetails(Yii::app()->session['Header_Code']);	
			$this->render('Official',array('model'=>$model,'model1'=>$model1,
			'Occupation'=>$Occupation,'country'=>$country,'msg1'=>$msg1,'row1'=>$row1,'msg2'=>$msg2));
	}

//End--------Official Page ------------------

//start--------Contact page ------------------
	public function actionContact()
	{
	
		$model = new ContactForm();
		$Cobj = new ProfileModel();
		$country    = $this->countryDropDownList();
		$Phonecode  = $this->PhonecodeDropDownList();
		$row1=0;	
		$msg1 ='';	
			if(isset($_POST['c!2@3']))
			{
				 $model->attributes = $_POST['ContactForm'];
				 if($model->action<>3) $model->contactValidate($model);
			
				if($model->errflag==0)
				{
					 $Cobj ->saveContactDetails($model,Yii::app()->session['Header_Code']);	 
					 $msg1 = $this->dispInfo($Cobj->msg,$model->action);
					 $model->unsetAttributes(array('ContactType','Name','Relationship','Address','country','state',
					 'district','city','pincode','ContactNo','PhoneCode','EmailId'));
		 		}
		 		
			}
	 	$row1=$Cobj->getcontactDetails(Yii::app()->session['Header_Code']);
		$this->render('Contact',array('row1'=>$row1,'model'=>$model,'country'=>$country,'Phonecode'=>$Phonecode,
		'msg1'=>$msg1));
	}
//End--------Contact  ------------------
//Start--------Criminal  ------------------
	public function actionCriminal()
	{
	
		$model = new CriminalForm();		
		$Cobj = new ProfileModel();
		$msg ='';
		$Ecode=0;	
		
		$result = $Cobj->formfillstatus(yii::app()->session['Header_Code']);				
		$fillcheck = $result['Form_Filled'];
		$Freeze = $result['Freeze'];
		
			if(isset($_POST['c*%fn']))
			{
			 $model->attributes = $_POST['CriminalForm'];
			$model->criminalValidate($model);	
				if($model->errflag==0)
				{
				  $model->attributes = $_POST['CriminalForm'];
				  $Cobj ->saveCriminalDetails($model,Yii::app()->session['Header_Code']);	
				  $msg = $this->dispInfo($Cobj->msg,1); 
			 	}
			
				
			}if($model->errflag==0){	
			
			$data = $Cobj->getCriminalDetails(Yii::app()->session['Header_Code']);
			if(mssql_num_rows($data)>0)
			{
				$field = mssql_fetch_array($data);		
				$model->isCriminalConvicted   = $field['Is_Criminal_Convicted'];			
				$model->criminalConvictedDetails   = $field['Criminal_Convicted'];
				$model->isCriminalProceeding   	 = $field['Is_Criminal_Proceeding'];
				$model->criminalProceedingDetails   = $field['Criminal_Proceeding'];
			}
		}
		$this->render('criminal',array('msg'=>$msg,'model'=>$model,'Ecode'=>$Ecode,'fillcheck'=>$fillcheck,'Freeze'=>$Freeze));
	}
//End--------Criminal  ------------------
//start--------Program page ------------------

	public function actionProgram()
	{	
		$model = new ProgramForm;	
		$tObj = new ProfileModel;
		$msg1 ='';			
		
		$programName=$this->programDropDownList();	
		$year =$this->showYear();	
		$month =$this->showMonth();
		
			if(isset($_POST['p81&6'])) 		// check form postingc && add & Edit	 
		{
			
				$model->attributes = $_POST['ProgramForm'];	
				if($model->action<>3) $model->programValidate($model);	
				if($model->errflag==0)
				{					
				$tObj->saveProgram($model,Yii::app()->session['Header_Code']);
				$msg1 = $this->dispInfo($tObj->msg,$model->action);
				}
					
		}
			
			$row1=$tObj->getProgramDetails(Yii::app()->session['Header_Code']);
			$this->render('Program',array('model'=>$model,'msg1'=>$msg1,'programName'=>$programName,'month'=>$month,'year'=>$year,'row1'=>$row1));
		
}

	//Start--------Enclosure Page ------------------
	public function actionEnclosure()
	{
		$model = new EnclosureForm;
		$tObj = new ProfileModel;						
		$msg1 ='';						
		$enclosure=$this->enClosureDropDownList();	
		$Ecode ='';
			if(isset($_POST['e&%sr'])) 		// check form postingc && add & Edit	 
			{
				$model->attributes = $_POST['EnclosureForm'];		
				if($model->action<>3) $model->enclosureValidate($model);	
					if($model->errflag==0)
					{				
						$Ecode = $tObj->saveEnclosure($model,Yii::app()->session['Header_Code']);
						
						$msg1 = $this->dispInfo($tObj->msg,$model->action);
					}
				 	
					
					$uploadedFile = CUploadedFile::getInstance($model,'documents');
					 if ( (is_object($uploadedFile) and	 get_class($uploadedFile)==='CUploadedFile'))
					 {
					 	
							 	$fileName=$model->enClosureName."_".Yii::app()->session['Header_Code'].'.jpg';
								
							$model->documents =Yii::app()->session['Header_Code'];	
			    			if(strlen($model->Others)>0)
							{
								$fileName1=$Ecode."_".Yii::app()->session['Header_Code'].'.jpg';	
								$uploadedFile->saveAs(Yii::app()->basePath.'/docs/'.$fileName1);
							}
						
							else					
								$uploadedFile->saveAs(Yii::app()->basePath.'/docs/'.$fileName);
											
							
							 ?>
						
						
						<?php 
						//$image = Yii::app()->image->load(Yii::app()->basePath.'/encdoc/'.$fileName);
    					//$image->resize(50, 50);
   						//$image->save();
						 
					}
			}
			$result = $tObj->formvalidcheck(yii::app()->session['Header_Code']);	
			$Echeck = $result['Enclosure'];
			if($Echeck>0) $model->formValid = $Echeck ; else $model->formValid = '';	
		
		    $row1=$tObj->getEnclosureDetails(Yii::app()->session['Header_Code']);
			$this->render('Enclosure',array('model'=>$model,'msg1'=>$msg1,'enclosure'=>$enclosure,
			'row1'=>$row1,'Ecode'=>$Ecode));
	}

//End--------Enclosure Page ------------------

//Start--------Language Page ------------------

public function actionLanguage()
{	


			$model = new LanguageForm;			
			$tObj = new ProfileModel;			
			$msg1 ='';				
			$Omother ='';
			
				
			if(isset($_POST['l65!e'])) 		// check form postingc && add & Edit	 
		{
			
				$model->attributes = $_POST['LanguageForm'];	
				if($model->action<>3) $model->languageValidate($model);	
				if($model->errflag==0)
				{					
				$tObj->saveLanguage($model,Yii::app()->session['Header_Code'],$Omother);
				$msg1 = $this->dispInfo($tObj->msg,$model->action);
				}
				
		}
			$lan_Language=$this->lan_LanguageDropDownList();
			$language=$this->typingLanguageDropdown(yii::app()->session['Header_Code']);
		
			$row1=$tObj->getLanguageDetails(Yii::app()->session['Header_Code']);
			
			$model->motherTongue=$tObj->getMotherTongueDetails(Yii::app()->session['Header_Code']);
			$this->render('Language',array('model'=>$model,'msg1'=>$msg1,'lan_Language'=>$lan_Language,
			'row1'=>$row1,'language'=>$language));
		
}

//End----------Language Page ------------------

//Start--------Department Page ----------------reporting

	public function actionDepartment()
	{
		
	
			$model1 = new PresentDepartmentForm;	
			
			$model = new PastDepartmentForm_New;		
			$tObj = new ProfileModel;				
			$msg1 ='';		
			$msg2 ='';		
			$department=$this->departmentDropDownList();
			
			$result = $tObj->formvalidcheck(yii::app()->session['Header_Code']);					
			 $Dcheck = $result['Department'];			
			
			if($Dcheck>0) 
			{			
				$model->formValid = $Dcheck ; 
		 		$model1->formValid = $Dcheck ; 
			}
				
			if(isset($_POST['de8&1'])) 		// check form postingc && add & Edit	 
			{			
				$model1->attributes = $_POST['PresentDepartmentForm'];	
				if($model1->action1<>3) $model1->presentdepartmentValidate($model1);	
				if($model1->errflag==0)
				{		
								
				$tObj->savepresentDepartment($model1,Yii::app()->session['Header_Code']);
						
				$msg2 = $this->dispInfo($tObj->msg,$model1->action1);	
				}
						
			}
	
			if(isset($_POST['d12*$'])) 		// check form postingc && add & Edit	 
			{	
				
		
				$model->attributes = $_POST['PastDepartmentForm_New'];
					
				if($model->action<>3) $model->pastdepartmentValidate($model);
		
				if($model->errflag==0)
				{	
				$tObj->savePastDepartment($model,Yii::app()->session['Header_Code']);	
				$msg1 = $this->dispInfo($tObj->msg,$model->action);
				}		
					 		
			} 
			$primaryDepartment=$this->activeDepartmentDropdown(yii::app()->session['Header_Code']);	
			 $result = $tObj->formvalidcheck(yii::app()->session['Header_Code']);					
			 $Dcheck1 = $result['Department'];			
				
				$model->formValid = $Dcheck1; 
				$model1->formValid = $Dcheck1; 
			
			
		$row1=$tObj->getpastDepartmentDetails(Yii::app()->session['Header_Code']);
		$row2=$tObj->getpresentDepartmentDetails(Yii::app()->session['Header_Code']);
		$model1->activeDepartment=$tObj->getActiveDepartmentDetails(Yii::app()->session['Header_Code']);
				
		$this->render('Department_New',array('model'=>$model,'model1'=>$model1,'msg1'=>$msg1,'msg2'=>$msg2,
		'department'=>$department,'row1'=>$row1,'row2'=>$row2,'primaryDepartment'=>$primaryDepartment));	
	}

//End--------Department Page ---------------
//Start--------FAMILY Page ------------------

	public function actionFamily()
	{         
     	$model = new FamilyForm;
		$model1 = new FamilyRelativeForm;	
		$model2 = new FamilyTextAreaForm;			
		$tObj = new ProfileModel;						
		$msg1 ='';	
		$msg2 ='';	
		$msg3 ='';					
		$phone  = $this->PhonecodeDropDownList();
		$department=$this->departmentDropDownList();		
			if(isset($_POST['f17$3'])) 		// check form postingc && add & Edit	 
			{	
				$model->attributes = $_POST['FamilyForm'];	
				if($model->action<>3) $model->familyValidate($model);
				if($model->errflag==0)
				{					
			    	 $tObj->saveFamily($model,Yii::app()->session['Header_Code']);	
					 $msg1 = $this->dispInfo($tObj->msg,$model->action);
					
				} 
				
			}
			if(isset($_POST['6w$%y'])) 		// check form postingc && add & Edit	 
			{	
				$model1->attributes = $_POST['FamilyRelativeForm'];	
				if($model1->action1<>3) $model1->familyRelativeValidate($model1);
				if($model1->errflag==0)
				{													
				
				$tObj->saveFamilyRelative($model1,Yii::app()->session['Header_Code']);			
				$msg3 = $this->dispInfo($tObj->msg,$model1->action1);
			    }
				
						
			}
			if(isset($_POST['f&$m#']))
		    {	
				 $model2->attributes = $_POST['FamilyTextAreaForm'];		 
				 $tObj->saveFamilyTextAreaDetails($model2,Yii::app()->session['Header_Code']);		 
				 $msg2 = $this->dispInfo($tObj->msg,$model2->action2);
		    }
			
		$row1=$tObj->getFamilyDetails(Yii::app()->session['Header_Code']);			
		$row2=$tObj->getFamilyRelativesDetails(Yii::app()->session['Header_Code']);	
		$data = $tObj->getFamilyTextAreaDetails(Yii::app()->session['Header_Code']);
			if(mssql_num_rows($data)>0)
			{
			    $tArea=mssql_fetch_array($data);
				$model2->responsibility = $tArea['Family_Responsibility'];
			}
			
		$this->render('Family',array('model'=>$model,'model1'=>$model1,'model2'=>$model2,'msg1'=>$msg1,'msg2'=>$msg2,'msg3'=>$msg3,'phone'=>$phone,'department'=>$department,'row1'=>$row1,'row2'=>$row2));			
	}
//END--------FAMIL Page ------------------
//Start--------Language Page ------------------

	public function actionMedical()
	{	

		$model = new MedicalForm;	
		$model1 = new MedicalMentalForm;	
		$model2 = new MedicalTextAreaForm;			
		$tObj = new ProfileModel;			
		$msg1 ='';
		$msg2 ='';	
		$msg3 ='';				
		$blood=$this->bloodDropDownList();
		$result = $tObj->formvalidcheck(yii::app()->session['Header_Code']);				
		$Pcheck = $result['Physical'];
		if($Pcheck>0) $model->formValid = $Pcheck ; else $model->formValid = '';
			$result = $tObj->formvalidcheck(yii::app()->session['Header_Code']);				
			$Mcheck = $result['Mental'];
			if($Mcheck>0) $model1->formValid1 = $Mcheck ; else $model1->formValid1 = '';
			
				if(isset($_POST['hj&%4'])) 		// check form postingc && add & Edit	 
			{	
				$model->attributes = $_POST['MedicalForm'];	
				if($model->action<>3) $model->medicalValidate($model);
				if($model->errflag==0)
				{					
			    	 $tObj->saveMedical($model,Yii::app()->session['Header_Code']);	
					 $msg1 = $this->dispInfo($tObj->msg,$model->action);			 						
				} 
				
			}	
				
		
		
		if(isset($_POST['m#4a2'])) 	
		{
			$model1->attributes=$_POST['MedicalMentalForm'];
			if($model1->action1<>3) $model1->medicalMendalValidate($model1);
			if($model1->errflag==0)
			{	
				$tObj->saveMedicalMental($model1,Yii::app()->session['Header_Code']);	
				$msg2 = $this->dispInfo($tObj->msg,$model1->action1);			
			}
			
		}
		
				if(isset($_POST['ml9*7']))
				{	
			
					 $model2->attributes = $_POST['MedicalTextAreaForm'];
				 
					 $tObj->saveMedicalTextAreaDetails($model2,Yii::app()->session['Header_Code']);	 
					 $msg3 = $this->dispInfo($tObj->msg,$model2->action2);
				}
				
			$row1 = $tObj->getPhysicalDetails(Yii::app()->session['Header_Code']);
			$row2 = $tObj->getMedicalMentalDetails(Yii::app()->session['Header_Code']);					
			$data = $tObj->getMedicalTextAreaDetails(Yii::app()->session['Header_Code']);
				
				if(mssql_num_rows($data)>0)
				{					
				    $tArea=mssql_fetch_array($data);				
					$model2->majorSurgeries = $tArea['Major_Surgeries'];
					$model2->pmChallenged = $tArea['PM_Challenged'];
					$model2->physicalIssue = $tArea['Pschycological_Issue'];
				
				
				}
			$model->bloodName=$tObj->getBloodDetails(Yii::app()->session['Header_Code']);
			$this->render('Medical',array('model'=>$model,'model1'=>$model1,'model2'=>$model2,'msg1'=>$msg1,
			'msg2'=>$msg2,'msg3'=>$msg3,'blood'=>$blood,'row1'=>$row1,'row2'=>$row2));
		}

//End----------Language Page ------------------
//START----------Skills Page ------------------
		public function actionSkills()
		{
			$model = new ComputerSkillForm;	
			$model3 = new HobbiesForm;								
			$tObj = new ProfileModel;									
			$msg1 ='';	
			$msg2 ='';				
			$msg7 ='';				
			$language=$this->typingLanguageDropdown(yii::app()->session['Header_Code']);
					
			$result = $tObj->formvalidcheck(yii::app()->session['Header_Code']);				
			$Ccheck = $result['Computer_Skills'];
			if($Ccheck>0) $model->formValid = $Ccheck ; else $model->formValid = '';
			
			$result = $tObj->formvalidcheck(yii::app()->session['Header_Code']);				
			$Hcheck = $result['Hobbies'];
			if($Hcheck>0) $model3->formValid2 = $Hcheck ; else $model3->formValid2 = '';
		
			if(isset($_POST['c^8om'])) 		// check form postingc && add & Edit	 
			{
				$model->attributes = $_POST['ComputerSkillForm'];
				if($model->action<>3) $model->ComplterOtherValidate($model);	
					if($model->errflag==0)
					{										
					$tObj->saveComputerSkill($model,Yii::app()->session['Header_Code']);					
					$msg1 = $this->dispInfo($tObj->msg,$model->action);
			
				}
			
				
			}
		   	
			if(isset($_POST['d*us$'])) 		// check form postingc && add & Edit	 
			{
				$model3->attributes = $_POST['HobbiesForm'];
				if($model3->action2<>3) $model3->HobbiesOtherValidate($model3);	
					if($model3->errflag==0)
					{										
				$tObj->saveHobbiesSkill($model3,Yii::app()->session['Header_Code']);					
				$msg7 = $this->dispInfo($tObj->msg,$model3->action2);
				}
			}
		$row1 = $tObj->getComputerSkillDetails(Yii::app()->session['Header_Code']);
		$row3 = $tObj->getHobbiesViewDetails(Yii::app()->session['Header_Code']);	
	
		$this->render('Skills',array('model'=>$model,'msg1'=>$msg1,'msg2'=>$msg2,'language'=>$language,
		'row1'=>$row1,'row3'=>$row3,'model3'=>$model3,'msg7'=>$msg7));
		
	}

//End----------Skills Page ------------------	
//Start--------Acadamic--------------
public function actionAcadamic()
{
	$model = new AcadamicForm();
	$tObj = new ProfileModel();
	$msg1 ='';
	
	
	if(isset($_POST['ai40*'])) 		// check form postingc && add & Edit	 
	{
		$model->attributes = $_POST['AcadamicForm'];
		if($model->action<>3) $model->academicValidate($model);	
		if($model->errflag==0)
		{										
			$tObj->saveAcadamicInfo($model,Yii::app()->session['Header_Code']);								$msg1 = $this->dispInfo($tObj->msg,$model->action);
		}
		}
	
	$acadamicInfo= $tObj->getAcadamicinfo(Yii::app()->session['Header_Code']);
	$ugDegreeLst = $this->DegreeDropDownList(1);
	$pgDegreeLst = $this->DegreeDropDownList(2);
	$this->render('Acadamic',array('model'=>$model,'msg1'=>$msg1,'ugDegreeLst'=>$ugDegreeLst,'pgDegreeLst'=>$pgDegreeLst,'acadamicInfo'=>$acadamicInfo));
}

//End--------Acadamic----------------
	//Manual check in check out
	public function actionManualcheckin()
	{
		$msg1 = '';
		$msg2 = '';
		$msg ='';
		$chkstatus = 0;
		$model = new ManualForm();
		$tObj = new ProfileModel();
		if(isset($_POST['ManualForm']))
		{
			$model->attributes = $_POST['ManualForm'];
			if(isset($_POST['CheckIn']))  $chkstatus=1;
			if(isset($_POST['Checkout'])) $chkstatus=2;
			{	
				if(strlen($model->manualcheck)>0){
					$chkinId = split(",", $model->manualcheck);
					if(sizeof($chkinId)>0)
					{
						$msg.='Record has been saved.';
						for($i=0;$i<sizeof($chkinId);$i++)
						{			
							$checkin= $tObj->savemanualcheckin($chkinId[$i],$chkstatus	);
							if(substr($tObj->msg,0,16)!="Changed database")	
							$msg1.= $chkinId[$i]." - ".$this->dispInfo($tObj->msg,99)."<br>";
							if(strlen($msg1)>0) $msg2="Below UID are invalid.<br><br>";
						}
					}
			}	}
		}
			
		$this->render('Manualcheck',array('model'=>$model,'msg1'=>$msg.$msg2.$msg1));
		
	}
 	
	public function actionFamilyMemberPgmInfo()
	{
		$model = new FamilyProgramForm;			
		$tObj = new ProfileModel;
		$msg1 ='';	
			if(isset($_POST['f*&m#'])) 		// check form postingc && add & Edit	 
			{
				$model->attributes = $_POST['FamilyProgramForm'];								
				$tObj->saveFamilyFormValid($model,Yii::app()->session['Header_Code']);		
				$msg1 = $this->dispInfo($tObj->msg,2);				 						
				
			}
			$result = $tObj->formvalidcheck(yii::app()->session['Header_Code']);				
			$Tcheck = $result['Family_MemberPg'];			
			if($Tcheck>0) $model->formValid = $Tcheck ; else $model->formValid = '';	
			$row=$tObj->getFamilyProgramDetails(Yii::app()->session['Header_Code']);
		    $this->render('FamilyMemberPgmInfo',array('model'=>$model,'msg1'=>$msg1,'row'=>$row));
		
	}
	
	public function actionResidentAdminData()
	{	

		$msg='';
		$model = new ResidentAdminForm;								
		$pObj = new ProfileModel;					
		//$this->layout='//layouts/column4';		
		
		$Status  	 = $this->activeStatusDropDownList();	
		$Res_Category  	 = $this->residentCategoryDropDownList();
		$Res_CardType  	 = $this->residentCardTypeDropDownList();
		$Res_Card_Status  	 = $this->cardStatusDropDownList();	
		$Alert_Flag  	 = $this->alertFlagDropDownList();	
		$Alert_Type  	 = $this->alertTypeDropDownList();
		$Amenties_Type  	 = $this->amentiesTypeDropDownList();	
				
		if(isset($_POST['r#*d6'])) 	
		{
		    $action=1;
			$model->attributes=$_POST['ResidentAdminForm'];
			
			$model->residentAdminValidate($model);
			if($model->errflag==0)
			{							   
				$pObj->saveResidentAdminData($model,Yii::app()->session['Header_Code']);						
				$msg = $this->dispInfo($pObj->msg,$action);	
			}
			else {
				 $msg = $this->dispInfo($model->errmsg,9);
			}		
					
			
				
		} 
			$data = $pObj->getResidentAdminDataDetails(Yii::app()->session['Header_Code']);
			if(mssql_num_rows($data)>0)
			{
				$field = mssql_fetch_array($data);
				
				if(strlen($field['Approved_DOJ'])==10) 
				$doj=date('d-m-Y',strtotime($field['Approved_DOJ'])); else $doj='';
				$model->doj   		 	= $doj;					
				$model->uId 	= $field['IVC_Code'];
				$model->activeStatus 	= $field['Active'];
				$model->category= $field['Category_Code'];
				$model->cardType 	= $field['Card_Type'];
				$model->cardSerialNo	    = $field['Card_Serial_No'];				
				if(strlen($field['Card_IssueDate'])==10) $cardIssueDate=date('d-m-Y',strtotime($field['Card_IssueDate']));
				else $cardIssueDate='';
				$model->cardIssueDate   	= $cardIssueDate;						
				if(strlen($field['Card_Expiry_Date'])==10) $cardExpiryDate=date('d-m-Y',strtotime($field['Card_Expiry_Date']));
				else $cardExpiryDate='';
				$model->cardExpiryDate  	= $cardExpiryDate;				
				$model->cardStatus   	= $field['Card_Status'];				
				if(strlen($field['FTVC_InterviewDate'])==10) $fTVCdate=date('d-m-Y',strtotime($field['FTVC_InterviewDate']));
				else $fTVCdate='';
				$model->fTVCdate  	= $fTVCdate;
				$model->alertFlag   	= $field['Alert_Flag'];
				$model->alertType  	 	= $field['Security_Alert_Type'];
				$model->honorariumAmount   	= $field['Honorarium_Amount'];
				$model->amentiesCtype  	 	= $field['Acard_Type'];				
				if(strlen($field['Acard_Expiry_Date'])==10) $amentiesCexpiry=date('d-m-Y',strtotime($field['Acard_Expiry_Date']));
				else $amentiesCexpiry='';
				$model->amentiesCexpiry  	= $amentiesCexpiry;
				$model->mCardIssued   	 	= $field['Card_Issued'];
				if(strlen($field['Mcard_Expiry_Date'])==10) 
				$mCardExpiry=date('d-m-Y',strtotime($field['Mcard_Expiry_Date'])); else $mCardExpiry='';
				$model->mCardExpiry   		 	= $mCardExpiry;
				
		}
	
		$this->render('ResidentAdminData',array('model'=>$model,'msg'=>$msg,'Status'=>$Status,'Res_Category'=>$Res_Category,
		'Res_CardType'=>$Res_CardType,'Res_Card_Status'=>$Res_Card_Status,'Alert_Flag'=>$Alert_Flag,'Alert_Type'=>$Alert_Type,'Amenties_Type'=>$Amenties_Type));
	}
   
  public function actionNewUsersShow()
	{
		$model = new NewUsersShowForm();		
		$Aobj = new ProfileModel();
		$dataReader = $Aobj->showNewUsersDetails();
		$this->render('NewUsersShow',array('dataReader'=>$dataReader,'model'=>$model,'Aobj'=>$Aobj));
	}
	
	
	public function actionChangePassword()
	{	
		$msg='';
		$model = new ChangePasswordForm;								
		$pObj = new ProfileModel;	
		 if(isset($_POST['c&%4P'])) 	
		{
			 $action=1;
		     $model->attributes=$_POST['ChangePasswordForm'];
			$model->NewUserValidate($model);
			if($model->errflag==0)
			{	
				$pObj->saveForgotPassword($model,Yii::app()->session['Header_Code']);	
				$msg = $this->dispInfo($pObj->msg,$action);	
			}
			
		}
		$this->render('ChangePassword',array('model'=>$model,'msg'=>$msg));
	}
	
	
//---SESSION CHANGE FOR Login as Another Profile---

public function actionChangeSession()
	{  	
		if($_GET['id']==987987980){
			Yii::app()->session['Header_Code'] = Yii::app()->session['oHeader_Code'];
			Yii::app()->session['Name'] = Yii::app()->session['oName'];
			Yii::app()->session['Theme'] = Yii::app()->session['oTheme'];
			Yii::app()->session['UID'] = Yii::app()->session['oUID'];
			
			unset(Yii::app()->session['oHeader_Code']);
			unset(Yii::app()->session['oName']);
			unset(Yii::app()->session['oTheme']);
			unset(Yii::app()->session['oUID']);
			unset(Yii::app()->session['EditCode']);
			unset(Yii::app()->session['ViewCode']);
			unset(Yii::app()->session['SubMcode']);
			$this->redirect(array('report/Report'));  // redirect  page here			
		} else {
			if(Yii::app()->session['oHeader_Code'] < 1 ){
				Yii::app()->session['oHeader_Code'] = Yii::app()->session['Header_Code'];
				Yii::app()->session['oName'] = Yii::app()->session['Name'];
				Yii::app()->session['oTheme'] = Yii::app()->session['Theme'];
				Yii::app()->session['oUID'] = Yii::app()->session['UID'];
			}
				
			$recSet = new ProfileModel;
			$data = $recSet->getChangeSessionDetails($_GET['id']);
			$objUserdetails = mssql_fetch_array($data);		

			Yii::app()->session['Header_Code'] = $objUserdetails['Header_Code'];
			Yii::app()->session['Name'] = $objUserdetails['Name'];	
			Yii::app()->session['Theme'] = $objUserdetails['Theme'];				
			Yii::app()->session['UID'] = $objUserdetails['IVC_Code'];
			Yii::app()->session['EditCode'] = $objUserdetails['Edit_Menu_Code'];
			Yii::app()->session['ViewCode'] = $objUserdetails['View_Menu_Code'];
			if(Yii::app()->session['oHeader_Code']>0)
				$this->redirect(array('Profile/empty'));  // redirect  page here			
			else
				$this->redirect(array('Profile/personal'));  // redirect  page here			
		}
			
	}
	
	public function actionempty()
	{
		
		$this->render('empty');	
	}
	
} 
?>