<?php 
class AjaxController  extends CommonController
{

	//-----------Ajax Area------------------


public function actionAjaxsate()
	{
		
		$ajaxstate = $this->stateDropDownList($_GET['countryCode']);
		
		$data=CHtml::listData($ajaxstate,'State_Code','State_Name');
		echo '<option value="">Select</option>';
    	foreach($data as $State_Code=>$State_Name)
    	{
			if($State_Code == $_GET['statecode'])
				echo CHtml::tag('option',
                   array('value'=>$State_Code,'selected' => 'selected'),CHtml::encode($State_Name),true);
			else
        		echo CHtml::tag('option',
                   array('value'=>$State_Code),CHtml::encode($State_Name),true);
    	} 
	}
	public function actionAjaxdistrict()
	{
		
		$ajaxdistrict = $this->districtDropDownList($_GET['stateCode']);
		
		$data=CHtml::listData($ajaxdistrict,'District_Code','District_Name');
		echo '<option value="">Select</option>';
    	foreach($data as $District_Code=>$District_Name)
    	{
			if($District_Code == $_GET['DistrictCode'])
				echo CHtml::tag('option',
                   array('value'=>$District_Code,'selected' => 'selected'),CHtml::encode($District_Name),true);
			else
        		echo CHtml::tag('option',
                   array('value'=>$District_Code),CHtml::encode($District_Name),true);
    	} 
	}
	
	public function actionAjaxCity()
	{
		
		$ajaxcity = $this->cityDropDownList($_GET['districtCode'],$_GET['stateCode']);
	
		$data=CHtml::listData($ajaxcity,'City_Code','City_Name');
		echo '<option value="">Select</option>';
    	foreach($data as $City_Code=>$City_Name)
    	{
			if($City_Code == $_GET['CityCode'])
				echo CHtml::tag('option',
                   array('value'=>$City_Code,'selected' => 'selected'),CHtml::encode($City_Name),true);
			else
        		echo CHtml::tag('option',
                   array('value'=>$City_Code),CHtml::encode($City_Name),true);
    	} 
	}
	
	public function actionAjaxdepartment1()
	{
	
		//$ajaxdept = $this->ajaxdepartmentDropDownList($_GET['category']);
		$ajaxdeptObj1 = new AjaxModel();
		$ajaxdept1 = $ajaxdeptObj1->ajaxdepartmentDropDownList1();
		$ajaxdept1 = $this->retArray($ajaxdept1	);
	
		
		$data=CHtml::listData($ajaxdept1,'Department_Code','Department');
		
		echo '<option value="">Select</option>';
    	foreach($data as $Department_Code=>$Department)
    	{
			if(( $_GET['category']=='Student' and ($Department_Code==13 or $Department_Code==12 or $Department_Code==5 )) or (( $_GET['category']=='Employee' or $_GET['category']=='Resident') and ($Department_Code<>13 or $Department_Code<>12 or $Department_Code<>5 ))) {				
			if($Department_Code == $_GET['dept'])	
				echo CHtml::tag('option',array('value'=>$Department_Code,'selected' => 'selected'),CHtml::encode($Department),true);		else
				echo CHtml::tag('option',array('value'=>$Department_Code),CHtml::encode($Department),true);
							
			}			
    	} 
	}
	public function actionAjaxdepartment()
	{
	
		//$ajaxdept = $this->ajaxdepartmentDropDownList($_GET['category']);
		$ajaxdeptObj = new AjaxModel();
		$ajaxdept = $ajaxdeptObj->ajaxdepartmentDropDownList();
		$ajaxdept = $this->retArray($ajaxdept);
	
		
		$data=CHtml::listData($ajaxdept,'Department_Code','Department');
		
		echo '<option value="">Select</option>';
    	foreach($data as $Department_Code=>$Department)
    	{
			if(( $_GET['category']=='Student' and ($Department_Code==13 or $Department_Code==12 or $Department_Code==5 )) or (( $_GET['category']=='Employee' or $_GET['category']=='Resident') and ($Department_Code<>13 or $Department_Code<>12 or $Department_Code<>5 ))) {				
			if($Department_Code == $_GET['dept'])	
				echo CHtml::tag('option',array('value'=>$Department_Code,'selected' => 'selected'),CHtml::encode($Department),true);		else
				echo CHtml::tag('option',array('value'=>$Department_Code),CHtml::encode($Department),true);
							
			}			
    	} 
	}
	
	
	public function actionTheme()
	{
		$tObj = new AjaxModel();		
		$themeCng = $tObj->saveTheme($_GET['thm'],Yii::app()->session['Header_Code']);
		if($themeCng==1) $themeCng='';
		Yii::app()->session['Theme'] = $_GET['thm'];
		echo $themeCng;
	}
	
	public function actionAjaxFloor()
	{
		
		$ajaxfloor = $this->floorDropDownList($_GET['cottageCode']);
		//print_r($ajaxfloor);
		$data=CHtml::listData($ajaxfloor,'Stay_Code','Stay_Area');
		//print_r($data);
		echo '<option value="">Select</option>'; 
    	foreach($data as $Stay_Code=>$Stay_Area)
    	{
			if($Stay_Code == $_GET['Stay_Code'])
				echo CHtml::tag('option',
                   array('value'=>$Stay_Code,'selected' => 'selected'),CHtml::encode($Stay_Area),true);
			else
        		echo CHtml::tag('option',
                   array('value'=>$Stay_Code),CHtml::encode($Stay_Area),true);
    	} 
	}
	
   /*public function actionSaveMother()
	{
		$mobj = new AjaxModel();
		$row = $mobj->Savemother(Yii::app()->session['Header_Code'],$_GET['mother'],$_GET['mlang']);
		//echo $row;
	}*/
	
	public function actionBlood()
	{
	
		$bloodObj = new AjaxModel();
	
		/*if(isset($_POST['MedicalForm']))
			{
				 $model->attributes = $_POST['MedicalForm'];
				 $model->contactValidate($model);
			
				if($model->errflag>0)
				 $msg1 = $this->dispInfo($model->errmsg,9);
			}*/
		$row = $bloodObj->SaveBlood(Yii::app()->session['Header_Code'],$_GET['blood']);
	}
	
	public function actionSaveYatraFormValid()
	{
		
		$FormValidObj = new AjaxModel();
		$row = $FormValidObj->saveYatraFormValid(Yii::app()->session['Header_Code'],$_GET['FormValid']);	
		
		}
		public function actionSaveTeacherFormValid()
	{
		
		$FormValidObj = new AjaxModel();
		
		$row = $FormValidObj->saveTeacherFormValid(Yii::app()->session['Header_Code'],$_GET['FormValid']);	
			
		}
	public function actionSaveTypingSkillsFormValid()
	{
		
		$FormValidObj = new AjaxModel();
		$row = $FormValidObj->saveTypingSkillsFormValid(Yii::app()->session['Header_Code'],$_GET['FormValid']);	
			
		}	
	public function actionSaveComputerSkillsFormValid()
	{
		
		$FormValidObj = new AjaxModel();
		$row = $FormValidObj->saveComputerSkillsFormValid(Yii::app()->session['Header_Code'],$_GET['FormValid']);	
			
		}	
	
	public function actionSaveEnclosureFormValid()
	{
		
		$FormValidObj = new AjaxModel();
		$row = $FormValidObj->saveEnclosureFormValid(Yii::app()->session['Header_Code'],$_GET['FormValid']);	
			
		}	
	public function actionSavePresentDepartmentFormValid()
	{
	
		$FormValidObj = new AjaxModel();
		
		$row = $FormValidObj->saveDepartmentFormValid(Yii::app()->session['Header_Code'],$_GET['FormValid']);	
			
		}
	public function actionSavePhysicalFormValid()
	{

		$FormValidObj = new AjaxModel();
		$row = $FormValidObj->savePhysicalFormValid(Yii::app()->session['Header_Code'],$_GET['FormValid']);	
			
		}
	public function actionSaveMentalFormValid()
	{
	
		$FormValidObj = new AjaxModel();
		$row = $FormValidObj->savementalFormValid(Yii::app()->session['Header_Code'],$_GET['FormValid']);	
			
		}
		
		
	public function actionSaveVolunteerIshaFormValid()
	{

		$FormValidObj = new AjaxModel();
		$row = $FormValidObj->saveVolunteerIshaFormValid(Yii::app()->session['Header_Code'],$_GET['FormValid']);	
			
		}
	public function actionSaveVolunteerLocalFormValid()
	{
	
		$FormValidObj = new AjaxModel();
		$row = $FormValidObj->saveVolunteerLocalFormValid(Yii::app()->session['Header_Code'],$_GET['FormValid']);	
			
		}	
		
		
		
	public function actionSavePassportFormValid()
	{
	
		$FormValidObj = new AjaxModel();
		$row = $FormValidObj->savePassportFormValid(Yii::app()->session['Header_Code'],$_GET['FormValid']);	
			
	}	
		public function actionSaveOfficialFormValid()
	{
	
		$FormValidObj = new AjaxModel();
		$row = $FormValidObj->saveOfficialFormValid(Yii::app()->session['Header_Code'],$_GET['FormValid']);	
			
	}	
	
	
		
		
// Form completion
		
		
	public function actionSaveFormCompletion()
	{
	
		$FormFillObj = new AjaxModel();
		$row = $FormFillObj->saveFormCompleteStatus(Yii::app()->session['Header_Code'],$_GET['Form_Filled']);	
			
	}	
		public function actionFeedback()
	{
	
		$FormFillObj = new AjaxModel();
		$row = $FormFillObj->saveFeedback(Yii::app()->session['Header_Code'],$_GET['msg']);	
			
	}
	
	
	public function actionUpdateFamilyProgram()
	{
		$FormFillObj = new AjaxModel();
		
		$row = $FormFillObj->UpdateFamilyProgramInfo($_GET['Family_Code'],$_GET['ishaYoga1'],$_GET['ishaYoga2'],$_GET['ishaYoga3'],
		$_GET['ishaYoga4']);
		
		return $row;
			
	}
	
		public function actionsaveFamilyMemberInfoFormValid()
	{
		
		$FormValidObj = new AjaxModel();
		
		$row = $FormValidObj->saveFamilyMemberInfoFormValid(Yii::app()->session['Header_Code'],$_GET['FormValid']);	
		
			
		}
		
		public function actionSaveHobbiesSkillsFormValid()
	   {
		
		$FormValidObj = new AjaxModel();
		
		$row = $FormValidObj->SaveHobbiesFormValid(Yii::app()->session['Header_Code'],$_GET['formcode']);	
			
		}
		
	public function actionMother()
	{
	
	 $motherObj = new AjaxModel();
	
	 $row = $motherObj->SaveMother(Yii::app()->session['Header_Code'],$_GET['mother']);
	}
	
	public function actionActiveDepartment()
	{
	
	 $departmentObj = new AjaxModel();
	
	 $row = $departmentObj->SaveActiveDepartment(Yii::app()->session['Header_Code'],$_GET['department']);
	 
	 
	
	}
	
	public function actionAjaxGroup()
	{
		$ajaxgroup = $this->groupDropDownList($_GET['departmentCode']);
		$data=CHtml::listData($ajaxgroup,'Group_Code','Group_Name');
		//echo '<option value="">Select</option>';
    	foreach($data as $Group_Code=>$Group_Name)
    	{
			//if($Group_Code == $_GET['groupcode'])
				echo CHtml::tag('option',
                   array('value'=>$Group_Code,'selected' => 'selected'),CHtml::encode($Group_Name),true);
			//else
        	//	echo CHtml::tag('option',
              //     array('value'=>$Group_Code),CHtml::encode($Group_Name),true);
    	} 
	}

public function actionajaxtotalpersoncount()
	{
	    $PersonObj = new dashboardModel();				
		$row2=$PersonObj->GetPersonCountchartDetails();
		$ul=$li=$Incount=$Outcount='';
		
	
		while($filed=mssql_fetch_array($row2)) 
		{
			$Outcount=$Outcount+$filed['CheckOut'];
			$Incount=$Incount+$filed['CheckIn'];
			
			
				
		}
	
			echo ' <span style=color:black>Total IN</span> <span style=color:#4A9BF9>-'.$Incount.'</span>&nbsp;<span style=color:black> OUT -</span> <span style=color:red>'.$Outcount;
	}
	public function actionajaxpersoncountreport()
	{
	    $PersonObj = new dashboardModel();				
		$row2=$PersonObj->GetPersonCountchartDetails();
		$ul=$li=$Incount=$Outcount='';
		$ul ='<ul class="stats-overview">';
	
		while($filed=mssql_fetch_array($row2)) 
		{
			$Outcount=$Outcount+$filed['CheckOut'];
			$Incount=$Incount+$filed['CheckIn'];
			
			$li.='<li class="hidden-phone">
                      <span class="value">'.$filed['Category'].'</span>
                      <span class="name text-error">
					  <span style=color:black>IN-</span>
					  <span style=color:#4A9BF9>'.$filed['CheckIn'].'</span> 
					  <span style=color:black>OUT-<span style=color:red>'.$filed['CheckOut'].'</span>								
                      </span>
                    </li>'; 
				
		}
	
			echo $ul.$li.'</ul>';
	}
	
	public function actionajaxshowDepartment()
	{
	
	 $ajaxdepartment = $this->showDepartmentlist($_GET['group']);
	 $data=CHtml::listData($ajaxdepartment,'Department_Code','Department');
	 echo '<option value="">select</option>';
    	foreach($data as $Department_Code=>$Department)
    	{
			//if($Group_Code == $_GET['groupcode'])
			//	echo CHtml::tag('option',
              //     array('value'=>$Department_Code,'selected' => 'selected'),CHtml::encode($Department),true);
			//else
        	echo CHtml::tag('option',
             array('value'=>$Department_Code),CHtml::encode($Department),true);
    	} 	
		
	}
	public function actionajaxSummarychartreport()
	{
		  $PersonObj = new dashboardModel();				
		  $row2=$PersonObj->GetPersonCountchartDetails();
		  $retjson=array();
	       $retjsoncol=array();
		   $j=0;
	
	
	while($field=mssql_fetch_array($row2))
	{
		$row = array();
		$row['Category'] = $field['Category'];
		$row['CheckIN'] = $field['CheckIn'];
		$row['CheckOUT'] = $field['CheckOut'];
		
		$retjson[] = $row;
	}

	header("Content-type: application/json"); 

	echo json_encode($retjson);
	}

public function actionajaxSummaryreport()
{
	if(strlen($_GET['checktodate'])==0) $todate='';
	else $todate = date('m/d/Y',strtotime($_GET['checktodate'])) ;
	
	$data = new ReportModel;

	$result = $data->Summaryreport(date('m/d/Y',strtotime($_GET['checkdate'])),$todate,$_GET['location'],$_GET['group'],$_GET['department'],$_GET['Name'],$_GET['Uid'],$_GET['frmtime'],$_GET['toTime'],$_GET['Rid']);

	$retjson   = array();
	$retjsoncol= array();
	$lcnt=1;
 do 
 	{
	$i=0; $j=0; $headcount =0; $retjsoncol=$retjson='';
 	$headcount = mssql_num_fields($result)-1; 

 	while($i < mssql_num_fields($result)) 
    { 	
		$row = array();
		$meta = mssql_fetch_field($result, $i); 
		$row['text']=$meta->name;
		$row['datafield']=$meta->name;
		$retjsoncol[$i] = $row;
		$i++;
	} 
	$i=0;
	if(mssql_num_rows($result)>0)
	{	
		while($field=mssql_fetch_array($result))
	{
		$row = array();
		for($j=0;$j<=$headcount;$j++)
		{
			$row[$retjsoncol[$j]['text']] = $field[$retjsoncol[$j]['datafield']]; 
		}
		$retjson[$i] = $row;
	  	$i++;			
	}		
	}
	if($lcnt==1) $retarray1 = array($retjsoncol,$retjson);
	elseif($lcnt==2) { 
		$retarray2 = array($retjsoncol,$retjson); 
		break;
	}
	
	$lcnt++;
 }  while(mssql_next_result($result));
    mssql_free_result($result);

	$retarray = array($retarray1,$retarray2);
	header("Content-type: application/json"); 
 	echo json_encode($retarray);
}

public function actionUpdateAdminPermission()
	{
	
		$FormFillObj = new AjaxModel();
		$row = $FormFillObj->UpdateAdminPermission($_GET['Header_Code']);
		
		if(@mssql_num_rows($row)>0)
				{  
				 $field=mssql_fetch_array($row);
				 $UserName=$field['User_Name'];
				 $Password=$field['IVC_Password']; 
				 $Email=$field['Emailid'];
				 				 
				 $Credential="<table>";
	$Credential.="<tr><td>Namaskaram,</td></tr><br><br>
	<tr><td>Your account has been approved by admin.</td></tr><br>
    <tr><td>Here are the details of your account:</td></tr><br>
	<tr><td>User Name : ".$UserName."</td></tr><br> 
	<tr><td>Password  &nbsp;&nbsp;: ".$Password."</td></tr><br><br><br>
	<tr><td>Pranam.</td></tr></table>";	
	
	                 
					 $this->sendmail($Email,'notification+kjdmh1j_3dui@gmail.com','ISHA RDS-Account Activation',$Credential);
					
			    }
	}
	
	
	public function actionajaxUserMenu()
	{
		
				 
		$UserType=$_GET['UserType'];
		$userObj = new AjaxModel();
		$result = $userObj->getUserMenu();
		$row = array();
 		while($field=mssql_fetch_array($result))
 		{ 		 
			$row[] = array(
	        'Menu_Code' => $field['Menu_Code'],
    	    'Menu_Name' => $field['Menu_Name']
	      );
		}	
			
		$result = $userObj->getAccessMenu();
		//$result = $userObj->getUserAcessMenu();
		$row4 = array();
 		while($field=mssql_fetch_array($result))
 		{ 		 
				  
			$row4[] = array(
	        'Acc_Menu_Code' => $field['Acc_Menu_Code'],
    	    'Acc_Menu_Name' => $field['Acc_Menu_Name']
	      );
		}		
		
		$row1 = array();
		$result1 = $userObj->getUserMenu_db($UserType);
		while($field1=mssql_fetch_array($result1))
 		{ 				 
			 $row1[] = array(
	        	'View_Menu_Code' => explode(",",substr($field1['View_Menu_Code'],1)),
    	    	'Edit_Menu_Code' => explode(",",substr($field1['Edit_Menu_Code'],1)),
				'Acess_Menu_Code' => explode(",",substr($field1['Acess_Menu_Code'],1))
	      		);	
		} 		
		$retarray=array($row,$row4,$row1); 
		header("Content-type: application/json"); 	
		echo json_encode($retarray); 
	
}
		public function actionajaxuserLevel()
	{
		$header_code=$_GET['headercode'];
		
						
		$userObj = new AjaxModel();
		$result = $userObj->showuserlevel();
		//print_r($result);
		$row = array();
 		while($field=mssql_fetch_array($result))
 		{ 
			 $row[] = array(
	        'levelCode' => $field['levelCode'],
    	    'LevelName' => $field['LevelName']
	      );
 		}
 	//$retarray=array($retjsoncol,$retjson);
 	header("Content-type: application/json"); 
	echo json_encode($row);
	}
	public function actionajaxsaveuserLevel()
	{
		$FormFillObj = new AjaxModel();
  		$row = $FormFillObj->UpdateUserLevel($_GET['headercode'],$_GET['Level']);		
  		return $row;
	}
	public function actioncheckuserLevel()
	{
		$obj = new AjaxModel();
  		$row = $obj->checkUserLevel($_GET['headercode']);
		
  		echo $row;
	}
	
	public function actionajaxUserMenu()
	{
		$header_code=$_GET['headercode'];
		$userObj = new AjaxModel();
		$result = $userObj->getUserMenu();
		$row = array();
 		while($field=mssql_fetch_array($result))
 		{ 		 
			$row[] = array(
	        'Menu_Code' => $field['Menu_Code'],
    	    'Menu_Name' => $field['Menu_Name']
	      );
		}		
		$result = $userObj->getAccessMenu();
		$row4 = array();
 		while($field=mssql_fetch_array($result))
 		{ 		 
			 $fld='';
			  if($field['Parent_MenuCode']>0)
			      $fld=$field['Acc_Menu_Code'].",".$field['Parent_MenuCode']; 
			  else
			   $fld=$field['Acc_Menu_Code'];
			   $row4[] = array(
			         'Acc_Menu_Code' => $fld,
    	    'Acc_Menu_Name' => $field['Acc_Menu_Name']
	      );
		}		
		mssql_free_result($result);
		$row1 = array();
		$row2 = array();
		$row3 = array();
		$row5 = array();
		$result1 = $userObj->getUserMenu_db($header_code);
		$cnt=1;
		do
		{
		if($cnt==1)
		{		
		while($field1=mssql_fetch_array($result1))
 		{ 				 
			if($field1['Menu_Code']>0 and $field1['vMenu_Code']==0 or ($field1['Menu_Code']==0 and $field1['vMenu_Code']==0 and $field1['User_Type']=="0")) {
				$row1[] = array(
	        	'Menu_Code' => $field1['Menu_Code'],
    	    	'Menu_Name' => $field1['Menu_Name']
	      		);	
			} elseif($field1['Menu_Code']>0 and $field1['vMenu_Code']==1 or ($field1['Menu_Code']==0 and $field1['vMenu_Code']==0 and $field1['User_Type']=="0")) {
				$row2[] = array(
	        	'Menu_Code' => $field1['Menu_Code'],
    	    	'Menu_Name' => $field1['Menu_Name']
	      		);
			}
			 elseif(strlen($field1['User_Type'])>3)
			 {
			 	$row3[] = array(
	        		'User_Type' => $field1['User_Type']
	      		);
			 }
		}
		}		
		 	if($cnt==2)
			{ 
				while($field1=mssql_fetch_array($result1))
		 		{ 	
					$fld='';
				  if($field1['Parent_MenuCode']>0)
				      $fld=$field1['Acc_Menu_Code'].",".$field1['Parent_MenuCode']; 
				  else
				   $fld=$field1['Acc_Menu_Code'];
				   $row5[] = array(
				        'Acc_Menu_Code' => $fld,
		    	    	'Acc_Menu_Name' => $field1['Acc_Menu_Name']
			     	);
				} 
			}
			if($cnt==2) break;
		$cnt++;
		} 
		while(mssql_next_result($result1));	
		mssql_free_result($result1);
		$retarray=array($row,$row1,$row2,$row3,$row4,$row5); 
		header("Content-type: application/json"); 	
		echo json_encode($retarray); 
	
}
public function actionajaxusertypereport()
{
		$usertype=$_GET['usertype'];
		$reportObj = new AjaxModel();
		$result = $reportObj->getUserTypeReport($usertype);
		$retjson   = array();
 		$retjsoncol= array();
		$lcnt=1; 
 		$i=0; $j=0; $headcount =0; $retjsoncol=$retjson='';
 		$headcount = mssql_num_fields($result); 

		 while($i < mssql_num_fields($result)) 
		 {  
		  $row = array();
		  $meta = mssql_fetch_field($result, $i); 
		  $row['text']=$meta->name;
		  $row['datafield']=$meta->name;
		  $retjsoncol[$i] = $row;
		  $i++;
		 } 
		 $i=0;
		 if(mssql_num_rows($result)>0)
		 { 
		  while($field=mssql_fetch_array($result))
		 {
		  $row = array();
		  for($j=0;$j<$headcount;$j++)
		  {
		   $row[$retjsoncol[$j]['text']] = $field[$retjsoncol[$j]['datafield']]; 
		  }
		  $retjson[$i] = $row;
		    $i++;   
		 }  
		 }
		 $retarray2 = array($retjsoncol,$retjson); 
		 $retarray = array($retarray2);
		 header("Content-type: application/json"); 
		 echo json_encode($retarray);

}
	public function actionUpdateFreeze()
	{
		$FormFillObj = new AjaxModel();
  		$row = $FormFillObj->UpdateFreeze($_GET['Header_Code'],$_GET['Freeze']);
  		return $row;
	}
	
	public function actionAjaxsubmenucode()
	{
		unset(Yii::app()->session['SubMcode']);
		Yii::app()->session['SubMcode']= $_POST['Sub_Group_Code'];
	}
	
		
	


}?>>