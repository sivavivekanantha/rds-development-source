<?php
return array (
  'template' => 'default',
  'tablePrefix' => 'IVC',
  'modelPath' => 'application.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
);
