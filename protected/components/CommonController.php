<?php 
class CommonController extends Controller
{
	// -----This function Display save message or error message from db
	
 public function dispInfo($ex,$action){
	if(substr($ex,0,16)=="Changed database") {
		if($action<>3)
			return "<tr><td align='center'><div class='errorMessage'>Record has been saved</div>";
		
	}
	elseif($action==9)
		return "<tr><td align='center'><div class='errorMessage'>Incomplete / Invalid entries for <br>".$ex."</div></td></tr>";
	elseif($action==99)
		return $ex;
	else
		return "<tr><td align='center'><div class='errorMessage'>".$ex."</div></td></tr>";
	}

	// -----This function convert recordset to array for dropdown Purpose .. Common for all Dropdown list in this project
	public function retArray($recordSet)
	{

		$lstArray=array();
		
		while($field=mssql_fetch_object($recordSet))
		{	
			$lstArray[] = $field;
		
		} 
		
	return $lstArray;
		
	}
	// -----This function convert recordset to array for PDF Purpose .. Common for all PDF Report in this project
	public function retPdfArray($recordSet)
	{
		$lstArray=array();
		while($row = mssql_fetch_row($recordSet))
		{
			$lstArray[]=$row;
		}
		return $lstArray;

	}
		
	//----------------------- Stay Dropdownlist ------------------------
	public function stayDropDownList()
	{
		$objStay = new ProfileModel();
	    $stay = $objStay->getStay();
	    
		return $this->retArray($stay);
	}
	//----------------------- cottage Dropdownlist ------------------------
	public function cottageDropDownList()
	{
		$objCottage = new ProfileModel();
	    $cottage = $objCottage->getCottage();

		return $this->retArray($cottage);
	}
	
	public function floorDropDownList($obj)
	{
		//print_r($obj);
		$objFloor = new AjaxModel();
	    $floor = $objFloor->getFloor($obj);

		return $this->retArray($floor);
	}
	
	//----------------------- stayDropDownList_pdf ------------------------
	public function stayDropDownList_pdf()
	{
		$objStay = new ReportModel();
	    $stay = $objStay->getStay();
		//return $stay;
	return $this->retPdfArray($stay);

	}
	
	//----------------------- Yatra Dropdownlist ------------------------
	public function yatraDropDownList()
	{
		$objYatra = new ProfileModel();
	    $yatraName = $objYatra->getYatra();			
		return $this->retArray($yatraName);

	}
	//----------------------- Yatra Dropdownlist ------------------------
	public function programDropDownList()
	{
		$objTeacher = new ProfileModel();
	    $programName = $objTeacher->getProgram();			
		return $this->retArray($programName);

	}
	//----------------------- Language Dropdownlist ------------------------
	public function languageDropDownList()
	{
		$objTeacher = new ProfileModel();
	    $language = $objTeacher->getLanguage();
			
		return $this->retArray($language);
		
	}
		//----------------------- Language Dropdownlist ------------------------
	public function typingLanguageDropdown($headerCode)
	{
		$objTyping = new ProfileModel();
	    $language = $objTyping->getTypingLanguage($headerCode);
			
		return $this->retArray($language);
		
	}
	
	//----------------------- Center Dropdownlist ------------------------
	public function centerDropDownList()
	{
		$objCenter = new searchModel();
	    $center = $objCenter->getcenter();
			
		return $this->retArray($center);
		
	}
	
	//----------------------- Occupation  Dropdownlist ------------------------
	
	public function OccupationDropDownList()
	{
		$objOccupation = new ProfileModel();
	    $Occupation = $objOccupation->getOccupation();
			
		return $this->retArray($Occupation);
		
	}
	//----------------------- country DropDownList ------------------------
	
	public function countryDropDownList()
	{
		$objcountry = new ProfileModel();
	    $country = $objcountry->getcountry();
			
		return $this->retArray($country);
		
			}
			
	public function birthCountryDropDownList()
	{
		
		$objbirthCountry = new ProfileModel();
	
	    $birthCountry = $objbirthCountry->getbirthCountry();
			
			
		return $this->retArray($birthCountry);
	
		
			}
	public function ReligionDropDownList()
	{
		
		$objReligion = new ProfileModel();
	
	    $Religion = $objReligion->getReligion();
			
			
		return $this->retArray($Religion);
	
		
			}
			
	//----------------------- Phonecode DropDownList ------------------------
	public function PhonecodeDropDownList()
	{
	  	$objPhoneCode = new ProfileModel();
	    $PhoneCode = $objPhoneCode->getPhonecode();
			
		return $this->retArray($PhoneCode);

	}
		//----------------------- Nationality DropDownList ------------------------
	public function nationalityDropDownList()
	{
	
	  	$objnationality = new ProfileModel();
		
	    $nationality = $objnationality->getNationality();
			
		return $this->retArray($nationality);

	}
	
	
			
	
	//----------------------- Ajax state DropDownList ------------------------
	public function stateDropDownList($countrycode)
	{
	
		$objstate = new AjaxModel();
	
	    $state = $objstate->getState($countrycode);	
		
		return $this->retArray($state);
	
	}
	//----------------------- Ajax district DropDownList ------------------------
	public function districtDropDownList($statecode)
	{
	
		$objdistrict = new AjaxModel();
	
	    $district = $objdistrict->getdistrict($statecode);	
		
		return $this->retArray($district);
	
	}
	
	//----------------------- Ajax city DropDownList ------------------------
	public function cityDropDownList($districtCode,$statecode)
	{
	
		$objcity = new AjaxModel();
	
	    $city = $objcity->getCity($districtCode,$statecode);	
	
		return $this->retArray($city);
	
	}
		
	
 	//----------------------- Degree Dropdownlist ------------------------
	
	public function DegreeDropDownList($obj)
	{
		$objDegree = new ProfileModel();
	    $Degree = $objDegree->getDegree($obj);
			
		return $this->retArray($Degree);
		
	}
	
		//----------------------- Enclosure Dropdownlist ------------------------
	public function enClosureDropDownList()
	{
		$objEnclosure = new ProfileModel();
	    $enclosure = $objEnclosure->getEnclosure();			
		return $this->retArray($enclosure);

	}
	//----------------------- Department Dropdownlist ------------------------
	public function departmentDropDownList()
	{
		$objDepartment = new ProfileModel();
	    $department = $objDepartment->getDepartment();			
		return $this->retArray($department);

	}
	//----------------------- BLOOD GROUP Dropdownlist ------------------------
	
		public function bloodDropDownList()
	{
		$objBlood = new ProfileModel();
	    $blood = $objBlood->getBlood();			
		return $this->retArray($blood);

	}	
	
	//----------------Lan_Language Dropdownlist ------------------
	
	public function lan_LanguageDropDownList()
	{
		$objLan = new ProfileModel();
	    $lan_Language = $objLan->getLan_Language();
			
		return $this->retArray($lan_Language);
		
	}
		//---------------Mother_Language Dropdownlist ------------------
		public function mother_LanguageDropDownList()
	{
		$objLan = new ProfileModel();
	    $mother_Language = $objLan->getmother_Language();
			
		return $this->retArray($mother_Language);
		
	}
	
	public function activeStatusDropDownList()
	{
		$objStatus = new ProfileModel();
	    $activeStatus = $objStatus->getActiveStatus();			
		return $this->retArray($activeStatus);

	}

   public function residentCategoryDropDownList()
	{
		$objResCategory = new ProfileModel();
	    $resCategory = $objResCategory->getResCategory();			
		return $this->retArray($resCategory);

	}
	 public function residentCardTypeDropDownList()
	{
		$objResCardType = new ProfileModel();
	    $resCardType = $objResCardType->getResCardType();			
		return $this->retArray($resCardType);

	}
	public function cardStatusDropDownList()
	{
		$objCardStatus = new ProfileModel();
	    $cardStatus = $objCardStatus->getCardStatus();			
		return $this->retArray($cardStatus);

	}
    public function alertFlagDropDownList()
	{
		$objalertFlag = new ProfileModel();
	    $alertFlag = $objalertFlag->getAlertFlag();			
		return $this->retArray($alertFlag);

	}
	public function alertTypeDropDownList()
	{
		$objalertType = new ProfileModel();
	    $alertType = $objalertType->getAlertType();			
		return $this->retArray($alertType);

	}
	public function amentiesTypeDropDownList()
	{
		$objamentiesType = new ProfileModel();
	    $amentiesType = $objamentiesType->getAmentiesType();			
		return $this->retArray($amentiesType);

	}
  			
	public function activeDepartmentDropdown($headerCode)
	{
		$objActiveDept = new ProfileModel();
	    $ActiveDepartment = $objActiveDept->getActiveDepartment($headerCode);
			
		return $this->retArray($ActiveDepartment);
		
	}

	public function groupDropDownList($departmentCode)
	{

		$objgroup = new AjaxModel();
	
	    $group = $objgroup->getGroup($departmentCode);	
		
		return $this->retArray($group);

	}
	
	public function actionajaxshowDepartment()
	{
	
	 $departmentObj = new AjaxModel();
	 
	 $ajaxdepartment = $this->showDepartmentlist($_GET['group']);
		$data=CHtml::listData($ajaxdepartment,'Department_Code','Department');
	
		echo '<option value="">Select</option>';
    	foreach($data as $Department_Code=>$Department)
    	{
			//if($Group_Code == $_GET['groupcode'])
			//	echo CHtml::tag('option',
              //     array('value'=>$Department_Code,'selected' => 'selected'),CHtml::encode($Department),true);
			//else
        	echo CHtml::tag('option',
             array('value'=>$Department_Code),CHtml::encode($Department),true);
    	} 
	}
	 public function showDepartmentlist($group)
	{
	 	$objajaxdep = new AjaxModel();
	    $department = $objajaxdep->getajaxdepartment($group);
		
		return $this->retArray($department);
	}
	public function DepartmentgroupDropDownList()
	{
	
	    $ReportModel = new ReportModel();
		
	    $DGrouplist = $ReportModel->DepartmentGrouplist();
			
		return $this->retArray($DGrouplist);
	
	}


	
	
}?>