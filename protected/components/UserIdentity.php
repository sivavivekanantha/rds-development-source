<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $_id;
    private $user;

    public $ip;
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
        $model = User::model(); //->findByAttributes(array('UserName'=>$this->username));
        //echo "$this->username - $this->password - $this->ip<br>";
        $userInfo = $model->getUserInfo($this->username, $this->password, $this->ip);

        if(empty($userInfo)) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        else {
            $this->errorCode = self::ERROR_NONE;
            $this->_id = $userInfo->User_Code;

            $this->setUserState($userInfo);
        }
        return !$this->errorCode;
    }

    private function setUserState($userInfo)
    {
        $userInfo = (object)$userInfo;
        $user = Yii::app()->user;

        foreach(get_object_vars($userInfo) as $property=>$value) {
            $user->setState($property, $value);
            //echo "$property - $value<br>";
        }
    }

    public function getUserInfo()
    {
        return $this->user;
    }
        
    public function getId()
    {
        return $this->_id;
    }
}