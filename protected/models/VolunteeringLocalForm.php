<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class VolunteeringLocalForm extends CFormModel
{
			public $volunteeringCode;           
			public $programName;                
			public $typeOfVolunteering;  
			public $action;
			
			public $volunteeringCode1;         
			public $programName1;               
			public $centerName1;         
			public $typeOfVolunteering1; 
			public $action1;
			
			public  $formValid;
	                public  $formValid1;	
			
			public  $Others;	   
			public  $Others1;	   
			
			public  $errflag;
	                public  $errmsg;
			
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// Deptname, occupation, date are required
			
			array('volunteeringCode','safe'),		
			array('programName','safe'),
			array('typeOfVolunteering','safe'),		
			array('action','safe'),
			
			array('volunteeringCode1','safe'),		
			array('programName1','required'),
			array('centerName1','safe'),		
			array('typeOfVolunteering1','safe'),
			array('action1','safe'),
			
			array('formValid','safe'),
			array('formValid1','safe'),
			
			array('Others','safe'),
			array('Others1','safe'),

			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'programName1'=>Yii::t('en','Volunteering_label1'),
			'centerName1'=>Yii::t('en','Volunteering_label4'),
			'typeOfVolunteering1'=>Yii::t('en','Volunteering_label2'),
			'Others1'=>Yii::t('en','Volunteering_label3'),
			'formValid1'=>Yii::t('en','Volunteering_label8'),
			'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'VolunteeringC'=>Yii::t('en','Header_label42'),
			'VolunteeringLPopup'=>Yii::t('en','Header_label59'),
			
		);
	}
	
	public function volunteeringLocalValidate($model1)
	{
			$cVal = new CommonValidator();
			
			$dummy = $cVal->Strcheck($model1->programName1,&$this->errflag);
			if($dummy==1) $this->addError('programName1',Yii::t('en','Volunteering_label1').Yii::t('en','err_label1'));
			
			if($model1->programName1=='8888')
				{
				$dummy = $cVal->Strcheck($model1->Others1,$this->errflag);
				if($dummy==1) $this->addError('Others1',Yii::t('en','Volunteering_label3').Yii::t('en','err_label1')); 
				}
						
	}
	
	
}



?>