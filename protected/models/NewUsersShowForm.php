<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class NewUsersShowForm extends CFormModel
{
	
	public	$fullName;
	public	$userName;
	public	$emailId;
	public	$department;
	public	$departmentCo;
	public	$active;
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
			
			//array('ivcCode','safe'),	
			array('fullName','required'),	
			
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'fullName'=>Yii::t('en','NewUserCreation_label1'),
			'userName'=>Yii::t('en','NewUserCreation_label2'),
			'emailId'=>Yii::t('en','NewUserCreation_label5'),
			'department'=>Yii::t('en','NewUserCreation_label6'),
			'departmentCo'=>Yii::t('en','NewUserShow_label2'),
			'vcdMembers'=>Yii::t('en','NewUserShow_label3'),
			'select'=>Yii::t('en','NewUserShow_label1'),
			
			'sno'=>Yii::t('en','Common_label1'),
			'newUserShow'=>Yii::t('en','Header_label53'),
			
		);
	}
	
	

	

}
?>