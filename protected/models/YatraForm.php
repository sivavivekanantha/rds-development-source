<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class YatraForm extends CFormModel
{
	public	$ivcYatraCode;
	public	$yatraCode;	
	public	$yatraName;
	public	$noOfTimesAttend;
	public	$yearLastAttend;
	public  $action;
	public  $formValid;
	public  $errflag;
	public  $errmsg;
	public  $sno;
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
			
			array('ivcYatraCode','safe'),		
			array('yatraCode','safe'),
			array('yatraName','required'),		
			array('noOfTimesAttend','required'),
			array('yearLastAttend','required'),
			array('action','safe'),
			array('formValid','safe'),
			array('sno','safe'),
			array('errflag','safe'),
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'yatraName'=>Yii::t('en','Yatra_label1'),
			'noOfTimesAttend'=>Yii::t('en','Yatra_label2'),
			'yearLastAttend'=>Yii::t('en','Yatra_label3'),
			'formValid'=>Yii::t('en','Yatra_label4'),
			'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'yatra'=>Yii::t('en','Header_label14'),
			'yatraPopup'=>Yii::t('en','Header_label30'),
			
		
		);
	}
	
	public function yatraValidate($model)
	{
		
		$cVal = new CommonValidator();

		$dummy = $cVal->Strcheck($model->yatraName,$this->errflag);
		if($dummy==1) $this->addError('yatraName',Yii::t('en','Yatra_label1').Yii::t('en','err_label1'));
		
		$dummy = $cVal->Strcheck($model->noOfTimesAttend,$this->errflag);
		if($dummy==1) $this->addError('noOfTimesAttend',Yii::t('en','Yatra_label2').Yii::t('en','err_label1'));
		
		$dummy = $cVal->Strcheck($model->yearLastAttend,$this->errflag);
		if($dummy==1) $this->addError('yearLastAttend',Yii::t('en','Yatra_label3').Yii::t('en','err_label1'));
		//print_r($this->errmsg);
		
	}

	

}
?>