<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ProgramForm extends CFormModel
{
	public	$rdsPgmCode;
	public	$pgmName;	
	public	$pgmCenter;
	public	$pgmMonth;
	public	$pgmYear;
	public	$pgmTeacher;
	public  $Others;
	public  $date;
	public  $action;
	public  $errflag;
	public  $errmsg;
	public  $sno;

	
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// required
			
			array('rdsPgmCode','safe'),		
			array('pgmName','required'),
			array('pgmCenter','required'),		
			array('pgmMonth','required'),
			array('pgmYear','required'),
		    array('pgmTeacher','safe'),
			array('Others','safe'),
			array('date','safe'),
			array('action','safe'),
			array('sno','safe'),
			array('errflag','safe'),
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'pgmName'=>Yii::t('en','Program_label1'),
			'pgmCenter'=>Yii::t('en','Program_label2'),
			'pgmMonth'=>Yii::t('en','Program_label3'),
			'pgmYear'=>Yii::t('en','Program_label4'),
			'pgmTeacher'=>Yii::t('en','Program_label5'),
			'Others'=>Yii::t('en','Program_label6'),
			'date'=>Yii::t('en','Program_label7'),
			'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'program'=>Yii::t('en','Header_label12'),
		
		);
	}
	
	public function programValidate($model)
	{		
		$cVal = new CommonValidator();
	
			$dummy = $cVal->Strcheck($model->pgmName,$this->errflag);
		if($dummy==1) $this->addError('pgmName',Yii::t('en','Program_label1').Yii::t('en','err_label1'));
		
	
			$dummy = $cVal->Strcheck($model->pgmCenter,$this->errflag);
		if($dummy==1) $this->addError('pgmCenter',Yii::t('en','Program_label2').Yii::t('en','err_label1'));
		
				$dummy = $cVal->Strcheck($model->pgmMonth,$this->errflag);
		if($dummy==1) $this->addError('pgmMonth',Yii::t('en','Program_label3').Yii::t('en','err_label1'));
		
				$dummy = $cVal->Strcheck($model->pgmYear,$this->errflag);
		if($dummy==1) $this->addError('pgmYear',Yii::t('en','Program_label4').Yii::t('en','err_label1'));

				
	}
	

}
?>