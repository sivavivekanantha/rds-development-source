<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class HobbiesForm extends CFormModel
{
	public	$ivcHobbiesCode;
	public	$category;	
	public	$details;		
	public  $action2;
	public  $formValid2;
	public  $formValid;
	public  $formValid1;
	public  $errflag;
	public  $errmsg;
	
	
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// Computer are required
			
			array('ivcHobbiesCode','safe'),		
			array('category','required'),
			array('details','safe'),		
			array('action2','safe'),	
			array('formValid2','safe'),	
			array('errflag','safe'),	
				
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'category'=>Yii::t('en','Skill_label3'),
			'details'=>Yii::t('en','Skill_label2'),
			'formValid2'=>Yii::t('en','Skill_label5'),
			'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'hobbiesHead'=>Yii::t('en','Header_label28'),
			'hobbiesHint'=>Yii::t('en','Header_label10'),
			
			'hobbiesPopup'=>Yii::t('en','Header_label57'),
		);
	}
	
public function HobbiesOtherValidate($model3)
	{
		
		$cVal = new CommonValidator();
		$dummy = $cVal->Strcheck($model3->category,$this->errflag);
		if($dummy==1) $this->addError('category',Yii::t('en','Skill_label3').Yii::t('en','err_label1'));	
		
		if($model3->category=='5')
		{
	    $dummy = $cVal->Strcheck($model3->details,$this->errflag);
		if($dummy==1) $this->addError('details',Yii::t('en','Skill_label2').Yii::t('en','err_label1'));	
		}
		
		
		
	
		
	}
}


?>