<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class UserTypeReportForm extends CFormModel
{
	
	public  $usertype;
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array(
					
			
			array('usertype','safe'),
			
				  );
			
	}
	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
				
			'usertype'=>Yii::t('en','Usertype_Report_label1'),
			
		
		);
	}
	
	
	
	

}
?>