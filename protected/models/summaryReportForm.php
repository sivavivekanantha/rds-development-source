<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class summaryReportForm extends CFormModel
{

	public	$fromdate;
	public	$todate;
	public  $Location;
	public  $ReportType;
	public  $group;
	public  $department;
	public $Name;
	public $Uid;
	public $Rid;

		
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
				array('fromdate','safe'),
				array('todate','safe'),
				array('Location','safe'),
				array('ReportType','safe'),		
				array('group','safe'),
				array('department','safe'),		
					array('Name','safe'),	
						array('Uid','safe'),
						array('Rid','safe'),		
						
			);
			
	}
	public function attributeLabels()
	{
		return array('Uid'=>'UID',
		);
	}
	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
}
?>