<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel
{
		public  $IVCEmergencyCode;
		public  $action;
		public	$ContactType;
		public	$Name;
		public	$Relationship;
		public	$Address;
		public	$country;
		public	$state;
		public  $oState;
		public	$district;
		public	$city;
		public  $oCity;
		public	$pincode;
		public	$ContactNo;
		public	$PhoneCode;	
		public	$EmailId;
		public $errmsg;
		public $errflag;

		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		// name, age, city and mobileno are required
	return array( 		
		array('IVCEmergencyCode','safe'),
		array('action', 'safe'),
		array('ContactType','required'),
		array('Name','required'),
		array('Relationship','required'),
		array('Address', 'safe'),
		array('country', 'safe'),
		array('state', 'safe'),
		array('oState','safe'),
		array('district', 'safe'),
		array('city', 'safe'),
		array('oCity','safe'),
		array('pincode', 'safe'),
		array('ContactNo', 'safe'),
		array('PhoneCode', 'safe'), 
		array('EmailId', 'safe'), 
		array('errflag','safe'),
		);
		
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'ContactType'=>Yii::t('en','Contact_label1'),
			'Name'=>Yii::t('en','Contact_label2'),
			'Relationship'=>Yii::t('en','Contact_label3'),
			'Address'=>Yii::t('en','Contact_label4'),
			'country'=>Yii::t('en','Contact_label5'),
			'state'=>Yii::t('en','Contact_label6'),
			'district'=>Yii::t('en','Contact_label7'),
			'city'=>Yii::t('en','Contact_label8'),
			'pincode'=>Yii::t('en','Contact_label9'),
			'ContactNo'=>Yii::t('en','Contact_label10'),
			'EmailId'=>Yii::t('en','Contact_label11'),
			'EmrContact'=>Yii::t('en','Contact_label12'),
			'ContactPerson'=>Yii::t('en','Contact_label13'),
			'EmrRes'=>Yii::t('en','Contact_label14'),
			//'provide'=>Yii::t('en','Contact_label15'),
			'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'contact'=>Yii::t('en','Header_label23'),
			
					
		);
	}
	public function contactValidate($model)
	{
		$cVal = new CommonValidator();
		$dummy = $cVal->Strcheck($model->ContactType,$this->errflag);
		if($dummy==1) $this->addError('ContactType',Yii::t('en','Contact_label1').Yii::t('en','err_label1'));
		$dummy = $cVal->Strcheck($model->Name,$this->errflag);
		if($dummy==1) $this->addError('Name',Yii::t('en','Contact_label2').Yii::t('en','err_label1'));
		
		$dummy = $cVal->Strcheck($model->Relationship,$this->errflag);
		if($dummy==1) $this->addError('Relationship',Yii::t('en','Contact_label3').Yii::t('en','err_label1'));
		
		if($model->city==99999)
		{
			$dummy = $cVal->Strcheck($model->oCity,$this->errflag);
		if($dummy==1) $this->addError('oCity',Yii::t('en','Contact_label8').Yii::t('en','err_label1'));	
		}
	
		if($model->state==9999)
		{
				$dummy = $cVal->Strcheck($model->oState,$this->errflag);
		if($dummy==1) $this->addError('oState',ii::t('en','Contact_label6').Yii::t('en','err_label1'));	
		}
		
		
		if(strlen($model->EmailId)>0)
		{
		$dummy = $cVal->Emailcheck($model->EmailId,$this->errflag);
		if($dummy==1) $this->addError('EmailId','Invalid Email ID Format');
		}
		
		
	}

}
?>