<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class PersonalContactForm extends CFormModel
{
	
	    public	$addressType;
	    public	$presentAddress;
		public	$presentVillage;
		public	$presentCountry;
		public	$presentState;
		public  $presentoState;
		public	$presentDistrict;
		public	$presentCity;
		public  $presentoCity;
		public	$presentPincode;
		public  $errflag;
	    public  $errmsg;
		public	$aType;
		public	$permanentAddress;
		public	$permanentVillage;
		public	$permanentCountry;
		public	$permanentState;
		public  $permanentoState;
		public	$permanentDistrict;
		public	$permanentCity;
		public  $permanentoCity;
		public	$permanentPincode;
		
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
			
			    array('addressType', 'safe'),
				array('aType', 'safe'),
			    array('presentAddress', 'required'),
				array('presentVillage', 'required'),
				array('presentCountry', 'required'),
				array('presentState', 'safe'),
				array('presentoState','safe'),
				array('presentDistrict', 'safe'),
				array('presentCity', 'safe'),
				array('presentoCity','safe'),
				array('presentPincode', 'safe'),
				
				
				array('permanentAddress','required'),
				array('permanentVillage', 'required'),
				array('permanentCountry', 'required'),
				array('permanentState', 'safe'),
				array('permanentoState','safe'),
				array('permanentDistrict', 'safe'),
				array('permanentCity', 'safe'),
				array('permanentoCity','safe'),
				array('permanentPincode', 'safe'),
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
	
		
			'presentAddress'=>Yii::t('en','PersonalContact_label1'),
			'presentVillage'=>Yii::t('en','PersonalContact_label2'),
			'presentCountry'=>Yii::t('en','PersonalContact_label3'),
			'presentState'	=>Yii::t('en','PersonalContact_label4'),
			'presentDistrict'=>Yii::t('en','PersonalContact_label5'),
			'presentCity'	=>Yii::t('en','PersonalContact_label6'),
			'presentPincode'=>Yii::t('en','PersonalContact_label7'),
						
			'permanentAddress'=>Yii::t('en','PersonalContact_label1'),
			'permanentVillage'=>Yii::t('en','PersonalContact_label2'),
			'permanentCountry'=>Yii::t('en','PersonalContact_label3'),
			'permanentState'=>Yii::t('en','PersonalContact_label4'),
			'permanentDistrict'=>Yii::t('en','PersonalContact_label5'),
			'permanentCity'=>Yii::t('en','PersonalContact_label6'),
			'permanentPincode'=>Yii::t('en','PersonalContact_label7'),
			
			'presentA'=>Yii::t('en','Header_label35'),
			'presentB'=>Yii::t('en','Header_label36'),
			'presentC'=>Yii::t('en','Header_label37'),


			
		
		);
	}
	
	public function PersonalContactValidate($model)
	{
		
		$cVal = new CommonValidator();
		
		if($model->presentCity==99999)
		$dummy = $cVal->Strcheck($model->presentoCity,&$this->errmsg,&$this->errflag,"Other City");
		if($model->presentState==9999)
		$dummy = $cVal->Strcheck($model->presentoState,&$this->errmsg,&$this->errflag,"Other State");
		
		    $dummy = $cVal->Strcheck($model->presentAddress,&$this->errflag);
			if($dummy==1) $this->addError('presentAddress',Yii::t('en','PersonalContact_label1').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model->presentVillage,&$this->errflag);
			if($dummy==1) $this->addError('presentVillage',Yii::t('en','PersonalContact_label2').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model->presentCountry,&$this->errflag);
			if($dummy==1) $this->addError('presentCountry',Yii::t('en','PersonalContact_label3').Yii::t('en','err_label1'));
			
			$dummy = $cVal->Strcheck($model->permanentAddress,&$this->errflag);
			if($dummy==1) $this->addError('permanentAddress',Yii::t('en','PersonalContact_label1').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model->permanentVillage,&$this->errflag);
			if($dummy==1) $this->addError('permanentVillage',Yii::t('en','PersonalContact_label2').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model->permanentCountry,&$this->errflag);
			if($dummy==1) $this->addError('permanentCountry',Yii::t('en','PersonalContact_label3').Yii::t('en','err_label1'));
					 
					  
					  if($model->presentState==9999)
					  {
					  $dummy = $cVal->Strcheck($model->presentoState,$this->errflag);
					  if($dummy==1) $this->addError('presentoState',Yii::t('en','PersonalContact_label4').Yii::t('en','err_label1'));
					  }
					  if($model->permanentState==9999)
					  {
					  $dummy = $cVal->Strcheck($model->permanentoState,$this->errflag);
					  if($dummy==1) $this->addError('permanentoState',Yii::t('en','PersonalContact_label4').Yii::t('en','err_label1'));
					  }
					  
					  
					  if($model->presentCity==99999)
					  {
					  $dummy = $cVal->Strcheck($model->presentoCity,$this->errflag);
					  if($dummy==1) $this->addError('presentoCity',Yii::t('en','PersonalContact_label6').Yii::t('en','err_label1'));
					  }
					  if($model->permanentCity==99999)
					  {
					  $dummy = $cVal->Strcheck($model->permanentoCity,$this->errflag);
					  if($dummy==1) $this->addError('permanentoCity',Yii::t('en','PersonalContact_label6').Yii::t('en','err_label1'));
					  }
					  
  
           }

	

}
?>