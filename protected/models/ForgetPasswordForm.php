<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ForgetPasswordForm extends CFormModel
{
	
	public	$fullName;
	public	$emailId;
		
	public  $errflag;
	public  $errmsg;
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
			
			
			array('fullName','required'),	
			array('emailId','required'),	
		
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			
			'fullName'=>Yii::t('en','Forgot_label5'),
			'emailId'=>Yii::t('en','Forgot_label6'),			
			'Header'=>Yii::t('en','Header_label63'),
			
		);
	}
	
	
	
	public function NewUserValidate($model)
	{
			$cVal = new CommonValidator();
			
			
			$dummy = $cVal->Strcheck($model->fullName,&$this->errflag);
			if($dummy==1) $this->addError('fullName',Yii::t('en','Forgot_label5').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model->emailId,&$this->errflag);
			if($dummy==1) $this->addError('emailId',Yii::t('en','Forgot_label6').Yii::t('en','err_label1'));
						
		    if(strlen($model->emailId)>0)$dummy = $cVal->Emailcheck($model->emailId,$this->errflag);
			if($dummy==1) $this->addError('emailId','Invalid Email ID Format');
			
			
			
   }
   

	

}
?>