<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ResidentAdminForm extends CFormModel
{
	
	public	$ivcResidentCode;
	public	$doj;
	public	$uId;
	public	$activeStatus;
	public	$category;
	public	$idCard;
	public	$cardType;
	public	$cardSerialNo;
	public	$cardIssueDate;
	public	$cardExpiryDate;
	public	$cardStatus;
	public	$fTVCdate;

	public	$alertFlag;
	public	$alertType;
	public	$honorariumAmount;
	public	$amentiesCard;
	public	$amentiesCtype;
	public	$amentiesCexpiry;
	public	$medicalCard;
	public	$mCardIssued;	
	public	$mCardExpiry;
	
	public  $errflag;
	public  $errmsg;
	
	
	
		/*Declares the validation rules.*/
	public function rules()
	{
	 return array( 		// fullname, existingcardtype are required

	        array('ivcResidentCode','safe'),
			array('uId','safe'),			
	        array('doj','safe'),		
			array('activeStatus','safe'),	
			array('category','safe'),	
			array('idCard','safe'),				
			array('cardType','safe'),	
			array('cardSerialNo','safe'),
			array('cardIssueDate','safe'),		
			array('cardExpiryDate','safe'),	
			array('cardStatus','safe'),	
			array('fTVCdate','safe'),				
			array('alertFlag','safe'),	
			
			array('alertType','safe'),	
			array('honorariumAmount','safe'),	
			array('amentiesCard','safe'),	
			array('amentiesCtype','safe'),	
			array('amentiesCexpiry','safe'),	
			array('medicalCard','safe'),		
			array('mCardIssued','safe'),	
			array('mCardExpiry','safe'),			
			
						
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(				
			'doj'=>Yii::t('en','Resident_label1'),
			'uId'=>Yii::t('en','Resident_label2'),
			'activeStatus'=>Yii::t('en','Resident_label3'),
			'category'=>Yii::t('en','Resident_label4'),
			'idCard'=>Yii::t('en','Resident_label5'),
			'cardType'=>Yii::t('en','Resident_label6'),
			'cardSerialNo'=>Yii::t('en','Resident_label7'),
			'cardIssueDate'=>Yii::t('en','Resident_label8'),
			'cardExpiryDate'=>Yii::t('en','Resident_label9'),
			'cardStatus'=>Yii::t('en','Resident_label10'),
			'fTVCdate'=>Yii::t('en','Resident_label11'),
			'alertFlag'=>Yii::t('en','Resident_label12'),
			'alertType'=>Yii::t('en','Resident_label13'),
						
			'honorariumAmount'=>Yii::t('en','Resident_label14'),
			'amentiesCard'=>Yii::t('en','Resident_label15'),
			'amentiesCtype'=>Yii::t('en','Resident_label16'),
			'amentiesCexpiry'=>Yii::t('en','Resident_label17'),
			'medicalCard'=>Yii::t('en','Resident_label18'),
			'mCardIssued'=>Yii::t('en','Resident_label19'),
			'mCardExpiry'=>Yii::t('en','Resident_label20'),
			
			'resident'=>Yii::t('en','Header_label61'),
			
		
		);
	}
	public function residentAdminValidate($model)
	{
		$cVal = new CommonValidator();
		$dummy = $cVal->StrCheck($model->doj,&$this->errmsg,&$this->errflag,"Date of Joining");
				$dummy = $cVal->Strcheck($model->cardIssueDate,&$this->errmsg,&$this->errflag,"Card Issue Date");
				$dummy = $cVal->StrCheck($model->cardExpiryDate,&$this->errmsg,&$this->errflag,"Card Expiry Date");
				if(strlen($model->cardIssueDate)>0)
		        $dummy = $cVal->dateformatchk($model->cardIssueDate,&$this->errmsg,&$this->errflag,"Card Issue Date");
				
				
				
				if( strlen($model->cardIssueDate)==10 and strlen($model->cardExpiryDate)==10 )
				$dummy = $cVal->DateCheck($model->cardIssueDate,$model->cardExpiryDate,&$this->errmsg,&$this->errflag,"Card Issue Date must be less than Card Expiry Date");
				
				$dummy = $cVal->DOBCheck($model->cardIssueDate,&$this->errmsg,&$this->errflag,"Card Issue Date must be less than current date");
				//if($model->amentiesCtype =3 or $model->amentiesCtype =4){			
				//$dummy = $cVal->Strcheck($model->amentiesCexpiry,&$this->errmsg,&$this->errflag,"Amenties Card Expiry Date");
				//}
				if($model->mCardIssued <>1){			
				$dummy = $cVal->Strcheck($model->mCardExpiry,&$this->errmsg,&$this->errflag,"Medical Card Expiry Date");
				}
				$dummy = $cVal->Strcheck($model->fTVCdate,&$this->errmsg,&$this->errflag,"FTVC Interview Date");
				
				
		
	 } 
				 
				 
	

}
?>