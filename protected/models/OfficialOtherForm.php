<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class OfficialOtherForm extends CFormModel
{
	public  $OfficialOther;
		public  $action1;
		public  $formValid1;
	
		public $errmsg;

			

		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
		
			
				array('OfficialOther', 'safe'),
				array('formValid1', 'safe'),
			
			
			
			//array('passportExpiryDate', 'required'),
			
			//array('nameInPassport', 'safe'),
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'OfficialOther'=>'Are you a member of any professional body/organization?
<br>If yes, please provide details',
			
		
		);
	}
	

}
?>