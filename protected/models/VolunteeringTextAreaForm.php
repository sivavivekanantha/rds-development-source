<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class VolunteeringTextAreaForm extends CFormModel
{
	
		public $stayedIsha;
		public $action2;    

			

		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
		
			
				array('stayedIsha', 'safe'),
				array('action2','safe'),
			
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'stayedIsha'=>Yii::t('en','Volunteering_label6'),
		
		);
	}
	

}
?>