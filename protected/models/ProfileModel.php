<?php 
class ProfileModel extends BaseModel
{

	public function formvalidcheck($headerCode)
	{
		$query = $this->createCommand('sp_FetchValidFormData');
		mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);
	
		return mssql_fetch_array($result);
	}


//Start-----Passport----------------
	public function savePassport($model,$headerCode)
	{
	
		$query = $this->createCommand('sp_passportdetails');
		
		mssql_bind($query, '@Header_Code', $headerCode , SQLINT4 ,    	 false, 	false, 5);	
		mssql_bind($query, '@Nationality', $model->nationality ,	SQLINT4	, false,false, 5);
		mssql_bind($query, '@Passport_No', $model->passportNo ,SQLVARCHAR,false,false, strlen($model->passportNo)); 	
		if(strlen($model->passportExpiryDate)==10) 
		$passportExpiryDate= date("Y-m-d",strtotime($model->passportExpiryDate)); 	
		
		mssql_bind($query, '@Exp_Date  '        , $passportExpiryDate,	SQLVARCHAR, false, false, 
		strlen($model->passportExpiryDate));
	    mssql_bind($query, '@Passport_Issue  '  , $model->placeOfIssue,SQLVARCHAR, false, false, 
		strlen($model->placeOfIssue));
		mssql_bind($query, '@dual_citizenship'	, $model->dualCitizenship, 		SQLVARCHAR,		 false, false, 
		strlen($model->dualCitizenship));
		mssql_bind($query, '@dual_details  '		, $model->dualDetails, 			SQLVARCHAR,		 false, false, 
		strlen($model->dualDetails));
		mssql_bind($query, '@visa_category'			, $model->visaCategory, 		SQLINT4,	 false, false, 5);
		mssql_bind($query, '@Visa_Type'				, $model->visaType, 			SQLVARCHAR,		 false, false, 
		strlen($model->visaType));
		mssql_bind($query, '@VisaDocument_Number'	, $model->documentNumber,		SQLVARCHAR,		 false, false, 
		strlen($model->documentNumber));
		if(strlen($model->visaIssueDate)==10) $visaIssueDate= date("Y-m-d",strtotime($model->visaIssueDate));	
		mssql_bind($query, '@Visa_Issue_Date  ' 	, $visaIssueDate, 		SQLVARCHAR,		 false, false, 
		strlen($visaIssueDate));
		if(strlen($model->visaExpiryDate)==10) 
		$visaExpiryDate= date("Y-m-d",strtotime($model->visaExpiryDate));		
		
		mssql_bind($query, '@Visa_Exp_Date'			, $visaExpiryDate,	 	SQLVARCHAR,		 false, false, 
		strlen($model->visaIssueDate));
		mssql_bind($query, '@Nameinpassport'		, $model->nameInPassport,SQLVARCHAR, false, false, 
		strlen($model->nameInPassport));
		mssql_bind($query, '@Form_Valid', $model->formValid,	SQLVARCHAR,		 false, false,
		strlen($model->formValid));

		$result = @mssql_execute($query);		
		$this->msg = mssql_get_last_message();		
	}
	
	public function getPassportDetails($headerCode)
     {   
		$query =$this->createCommand('sp_getpassportdetails');	
		
		mssql_bind($query, '@Header_Code', 		$headerCode, 			SQLINT4, 	false, 	false, 	5);
				
    	$result = @mssql_execute($query); 		      
 		return $result;
     }
	 	public function getPassportNationalityDetails($headerCode)
     {   
		$query =$this->createCommand('sp_passportNationality');	
		
		mssql_bind($query, '@Header_Code', 		$headerCode, 			SQLINT4, 	false, 	false, 	5);
				
    	$result = @mssql_execute($query); 
		if($result)	{
			$field = mssql_fetch_array($result);
		$nationality = $field['Nationality'];  
		}
		
 		return $nationality;
     }
	 
	 
	 
	
	 
//End-----Passport----------------	
 public function getPermanentAddressDetails($headerCode)
     {   
		$query =$this->createCommand('SP_GetPermanentAddress');	
		mssql_bind($query, '@Header_Code', 		$headerCode, 			SQLINT4, 	false, 	false, 	5);		
    	$result = @mssql_execute($query); 		      
 
 		return $result;
		
     }
	 
	 public function getPresentAddressDetails($headerCode)
     {   
		$query =$this->createCommand('SP_GetPresentAddress');	
		mssql_bind($query, '@Header_Code', 		$headerCode, 			SQLINT4, 	false, 	false, 	5);		
    	$result = @mssql_execute($query);
 		return $result;
		
     }
	 
	 
	 
	  public function savePresentAddress($model,$headerCode)
	    {
		$query = $this->createCommand('sp_IVC_PersonalContact');	
	
		mssql_bind($query, '@Header_Code'   	,$headerCode,  		SQLINT4 ,  false, 	false, 5);
		mssql_bind($query, '@Pre_Address'		,$model->presentAddress, 	SQLVARCHAR,		 false,     false, 
		strlen($model->presentAddress));
		mssql_bind($query, '@Pre_Area'			,$model->presentVillage, 	SQLVARCHAR, 	     false,     false, 
		strlen($model->presentVillage));
		mssql_bind($query, '@Pre_Country'		,$model->presentCountry, 	SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Pre_State'	,		$model->presentState, 		SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Pre_District'		,$model->presentDistrict, 	SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Pre_City'	,		$model->presentCity, 		SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Pre_Zip_Code'		,$model->presentPincode, 	SQLVARCHAR,   false, 	false, 50);
		mssql_bind($query, '@Pre_Ostate_Name'	,$model->presentoState, 	SQLVARCHAR,   false, 	false, 50);
		mssql_bind($query, '@Pre_Ocity_Name'	,$model->presentoCity, 		SQLVARCHAR,   false, 	false, 50);
		mssql_bind($query, '@Address_Type'		,$model->addressType, 			SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Per_Address'		,$model->permanentAddress,  SQLVARCHAR,		 false,     false, 
		strlen($model->permanentAddress));
		mssql_bind($query, '@Per_Area'			,$model->permanentVillage,  SQLVARCHAR, 	     false,     false, 
		strlen($model->permanentVillage));
		mssql_bind($query, '@Per_Country'		,$model->permanentCountry, 	SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Per_State'			,$model->permanentState, 	SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Per_District'		,$model->permanentDistrict, SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Per_City'			,$model->permanentCity, 	SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Per_Zip_Code'		,$model->permanentPincode, 	SQLVARCHAR,   false, 	false, 50);
		mssql_bind($query, '@Per_Ostate_Name'	,$model->permanentoState, 	SQLVARCHAR,   false, 	false, 50);
		mssql_bind($query, '@Per_Ocity_Name'	,$model->permanentoCity, 	SQLVARCHAR,   false, 	false, 50);	
		mssql_bind($query, '@Address_Type1'		,$model->aType, 			SQLINT4,   false, 	false, 5);	
	
		$result = @mssql_execute($query);				
		$this->msg = mssql_get_last_message(); 
		
		}

 public function savePermanentAddress($model,$headerCode)
	    {
		$query =$this->createCommand('sp_IVC_PermanentContact_Info');	
		
		mssql_bind($query, '@Header_Code'   ,$headerCode,  SQLINT4 ,  false, 	false, 5);
		mssql_bind($query, '@Per_Address'	,$model->address1, 	SQLVARCHAR,		 false,     false, 
		strlen($model->address1));
		mssql_bind($query, '@Per_Area'		,$model->village1, 	   SQLVARCHAR, 	     false,     false, 
		strlen($model->village1));
		mssql_bind($query, '@Per_Country'	,$model->country1, 		SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Per_State'	,$model->state1, 		SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Per_District'	,$model->district1, 		SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Per_City'	,$model->city1, 		SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Per_Zip_Code'	,$model->pincode1, 		SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Per_Ostate_Name'	,$model->oState1, 		SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Per_Ocity_Name'	,$model->oCity1, 		SQLINT4,   false, 	false, 5);
		$result = @mssql_execute($query);				
		$this->msg = mssql_get_last_message(); 
		
		}




        public function savePersonalEmail($model,$headerCode)
	    {
		$query =$this->createCommand('Sp_IvcPersonalEmail_yii');	
		
		mssql_bind($query, '@Header_Code'   ,$headerCode,  SQLINT4 ,  false, 	false, 5);
		mssql_bind($query, '@Isahanga_Mail'	,$model->isahangaMail, 	SQLVARCHAR,		 false,     false, 
		strlen($model->isahangaMail));
		mssql_bind($query, '@Emailid'		,$model->emailId, 	   SQLVARCHAR, 	     false,     false, 
		strlen($model->emailId));
		mssql_bind($query, '@Receive_Ishanga_Mail',$model->receiveIshangaMail, 	   SQLVARCHAR,false,     false, 
		strlen($model->receiveIshangaMail));
		mssql_bind($query, '@Recivemail'	,$model->reciveMail, 		SQLINT4,   false, 	false, 5);
		$result = @mssql_execute($query);				
		$this->msg = mssql_get_last_message(); 
		
		}
		
		public function getPersonalEmailDetails($headerCode)
     {   
		$query =$this->createCommand('sp_EmailView');	
		mssql_bind($query, '@Header_Code', 		$headerCode, 			SQLINT4, 	false, 	false, 	5);		
    	$result = @mssql_execute($query); 		      
 
 		return $result;
		
     }
	


public function savePersonalMobile($model,$headerCode)
	    {
			
		$query =$this->createCommand('IVC_Mobile_Info1');	
		
		mssql_bind($query, '@Header_Code'   ,$headerCode,  SQLINT4 ,  false, 	false, 5);	
		mssql_bind($query, '@IVC_Mobile_Code'   ,$model->IVC_MobileCode,  SQLINT4 ,  false, 	false, 5);	
				
		mssql_bind($query, '@Phone_Code' ,$model->phoneCode,    			SQLINT4,      false, 	false, 5);	
		mssql_bind($query, '@Mobile_No'	,$model->mobileNumber, 	   			SQLVARCHAR, 	     false,     false, 
		strlen($model->mobileNumber));			
		mssql_bind($query, '@Service_Provider',$model->serviceProvider, 	   SQLVARCHAR,false,     false, 
		strlen($model->serviceProvider));		
		mssql_bind($query, '@Issued_By'	,$model->issuedByIsha, 		SQLVARCHAR,   false, 	false, 1);
		mssql_bind($query, '@Paid_By'	,$model->paidByIsha, 		SQLVARCHAR,   false, 	false, 1);
		mssql_bind($query, '@Mobile_Plan',$model->paln, 	   SQLVARCHAR,false,     false, 
		strlen($model->paln));		
		mssql_bind($query, '@Mobile_Details',$model->mobileDetails, 	   SQLVARCHAR,false,     false, 
		strlen($model->mobileDetails));
		
		mssql_bind($query, '@Action', 			$model->action, 		SQLINT4, 	false,  false, 	5);		

		
		$result = @mssql_execute($query);				
		$this->msg = mssql_get_last_message(); 
		
		}
		
		public function getPersonalMobileDetails($headerCode)
     {   
		$query =$this->createCommand('sp_MobileView');	
		mssql_bind($query, '@Header_Code', 		$headerCode, 			SQLINT4, 	false, 	false, 	5);		
    	$result = @mssql_execute($query); 		      
 
 		return $result;
		
     }
	 
	 public function getMobileType()
     {
	    $query = $this->createCommand('SP_IVC_View_MobileType');      
    	$result = @mssql_execute($query);		     
        return $result;	
     }
	

//Start-----Personal----------------
	public function savePersonal($model,$headerCode)
	{
		
		$middleName ='';
		$lastName ='';
		$vip ='';
		$dnd ='';
		$role ='';
		$subCategoryCode='';
		$zoneCode=0;
		$centerCode=0;
		$subCenter=0;
		$bloodGroup=0;
		$reciveMail=0;
		$retVal1=0;
		$retIvc='';
		$Category_Code=0;
	
		
		$query = $this->createCommand('sp_IVCpersonalinformation_yii');
		mssql_bind($query, '@Header_Code'   ,$headerCode,  SQLINT4 ,  false, 	false, 5);
		mssql_bind($query, '@IVC_Code',$model->ivcCode,  SQLVARCHAR,   false, 	false, strlen($model->ivcCode));
		mssql_bind($query, '@Title'	 ,$model->title,  SQLINT4,   false, 	false, strlen($model->title));
		
		mssql_bind($query, '@First_Name',$model->firstName, SQLVARCHAR, false, false, strlen($model->firstName));
		mssql_bind($query, '@Middle_Name'    ,$middleName,   SQLVARCHAR,false, 	false, strlen($middleName));
		mssql_bind($query, '@Last_Name' ,$lastName, SQLVARCHAR, 	     false, 	false, strlen($lastName));	
		mssql_bind($query, '@Gender',$model->gender, SQLVARCHAR, false,     false, strlen($model->gender));
			
		if(strlen($model->dob) == 10) 
		$dob = date("Y-m-d",strtotime($model->dob));
		
		mssql_bind($query, '@DOB' ,$dob, SQLVARCHAR,false, false, strlen($model->dob));
		Mssql_bind($query, '@Nationality'    ,$model->nationality,   	SQLINT4,      false, 	false, 5);
		
		mssql_bind($query, '@MaterialStatus',$model->maritalStatus,  	SQLINT4,      false, 	false, 5);
		mssql_bind($query, '@Phonecode' ,$model->phoneCode,    			SQLINT4,      false, 	false, 5);	
		mssql_bind($query, '@Contact1'	,$model->contact1, 	   			SQLVARCHAR, 	     false,     false, 
		strlen($model->contact1));
		mssql_bind($query, '@Phonecode2'  ,$model->phoneCode2,    		SQLINT4,      false, 	false, 5);		
		mssql_bind($query, '@Contact2'  ,$model->contact2, 	   			SQLVARCHAR, 	     false,     false, 
		strlen($model->contact2));
		Mssql_bind($query, '@Phonecode3'    ,$model->phoneCode3,     SQLINT4,      false, 	false, 5);	
		mssql_bind($query, '@Contact3'	 	,$model->contact3, 	   SQLVARCHAR, 	     false,     false, 
		strlen($model->contact3));
		mssql_bind($query, '@Phonecode4'     ,$model->phoneCode4,  		SQLINT4,      false, 	false, 5);		
		mssql_bind($query, '@Contact4'  ,$model->contact4, 	   SQLVARCHAR, 	     false,     false, 
		strlen($model->contact4));
		mssql_bind($query, '@VIP' ,$vip, 		   SQLVARCHAR, 	     false,     false, strlen($vip));
		mssql_bind($query, '@DND' ,$dnd, 		   SQLVARCHAR, 	     false,     false, strlen($dnd));		
		mssql_bind($query, '@Emailid'		,$model->emailId, 	   SQLVARCHAR, 	     false,     false, 
		strlen($model->emailId));
		mssql_bind($query, '@Language_Code'	,$model->languageCode, 	SQLINT4,      false, 	false, 5);		
		mssql_bind($query, '@Role'		    ,$role,SQLVARCHAR,		 false,     false, strlen($role));
		mssql_bind($query, '@Category_Code'	,$Category_Code, 		   SQLINT4,    false, 	false, 5);		
		mssql_bind($query, '@Sub_Category_Code',$subCategoryCode, SQLVARCHAR,		 false,     false, 
		strlen($subCategoryCode));
		mssql_bind($query, '@Zone_Code' ,$zoneCode,    SQLINT4,   false, 	false, 5);		
		mssql_bind($query, '@Center_Code'	,$centerCode,   SQLINT4,   false, 	false, 5);		
		mssql_bind($query, '@Subcenter'     ,$subCenter,    SQLINT4,   false, 	false, 5);	
		mssql_bind($query, '@Blood_Group'     ,$bloodGroup,    SQLINT4,   false, 	false, 5);		
		mssql_bind($query, '@Gazette_Change'		    ,$model->gazetteChange,  SQLINT4, false, 	false, 5);	
		mssql_bind($query, '@RefNo'	 ,$model->refNo, SQLVARCHAR,	 false,  false, strlen($model->refNo));
		mssql_bind($query, '@PageNo'		,$model->pageNo, SQLINT4,   false, 	false, 5);	
		
	   	if(strlen($model->dob2) == 10) 
		$dob2 = date("Y-m-d",strtotime($model->dob2)); 
			
		mssql_bind($query, '@DOB2' ,$dob2, 		   SQLVARCHAR,		 false,     false, strlen($model->dob2));	
		mssql_bind($query, '@Isahanga_Mail'	,$model->isahangaMail, 	SQLVARCHAR,		 false,     false, 
		strlen($model->isahangaMail));
		mssql_bind($query, '@Aliasname'		,$model->Aliasname,    SQLVARCHAR,		 false,     false, 
		strlen($model->aliasName));
		mssql_bind($query, '@ChangeName'	,$model->changeName, 		SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@PreviousName'	,$model->previousName,    SQLVARCHAR,		 false,     false, 
		strlen($model->previousName));
		mssql_bind($query, '@Religion'		,$model->religion, SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@caste'		    ,$model->caste, 	   SQLVARCHAR,		 false,     false, 
		strlen($model->caste));
		mssql_bind($query, '@Birth_Town'	,$model->birthTown, SQLVARCHAR,		 false,     false, 
		strlen($model->birthTown));
		mssql_bind($query, '@Birth_Country'	,$model->birthCountry, SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Birth_City'	,$model->birthCity,    SQLVARCHAR,		 false,     false, 
		strlen($model->birthCity));
		mssql_bind($query, '@Receive_Ishanga_Mail',$model->receiveIshangaMail, 	   SQLVARCHAR,false,     false, 
		strlen($model->receiveIshangaMail));
		
		if(strlen($model->FTV_DOJ_Isha) == 10) 
		$FTV_DOJ_Isha = date("Y-m-d",strtotime($model->FTV_DOJ_Isha));
		 	
		mssql_bind($query, '@FTV_DOJ_Isha',$FTV_DOJ_Isha,  SQLVARCHAR, false, false, strlen($model->FTV_DOJ_Isha));
		mssql_bind($query, '@Existing_Card'	,$model->existingCard,    SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@other_religion' ,$model->otherReligion, 	SQLVARCHAR,		 false,     false, 
		strlen($model->otherReligion));		
		mssql_bind($query, '@Recivemail'	,$model->reciveMail, 		SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Modify_By'		,$headerCode, SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@sno'		    ,$model->sno,SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Old_PID'		,$model->oldPID, SQLVARCHAR, false,  false, strlen($model->oldPID));
		mssql_bind($query, '@retval1'		,$retVal1, SQLINT4,   true, 5);
		mssql_bind($query, '@retivc'		,$retIvc, 	   SQLVARCHAR,		 true, false, strlen($retIvc));

			
		$result = @mssql_execute($query);				
		$this->msg = mssql_get_last_message(); 

	}
	
	public function getNationality()
    {   
		$query = $this->createCommand('sp_FectchNationality');      
    	$result = @mssql_execute($query); 		      
		
 		return $result;
     }
	 
 	public function getbirthCountry()
	{
	
  		$query = $this->createCommand('sp_FetchCountry');      
    	$result = @mssql_execute($query);	
		    
 	return $result;	
	

	}
 	public function getReligion()
	{
		$query = $this->createCommand('sp_selectreligon_yii');      
    	$result = @mssql_execute($query);				    
 		
		return $result;	
	
	}

	//$row1 = $tObj->getYatraDetails(Yii::app()->session['Header_Code']);
	public function getPersonalDetails($headerCode)
     {   
		$query =$this->createCommand('sp_IvcView');	
		mssql_bind($query, '@Header_Code', 		$headerCode, 			SQLINT4, 	false, 	false, 	5);		
    	$result = @mssql_execute($query); 		      
 
 		return $result;
		
     }
	
	
//End-----Personal----------------


//Start-----Stay----------------
public function saveStay($model,$headerCode)
{
		
	
	if($model->stayPlace<>1 and $model->stayPlace<>3)
		{
			//$model->stayProvide = "";
			 $model->stayAddress = $model->cottageName=$model->cottageNo = $model->occupayMonth = $model->occupayYear = $model->cottageAllot = $model->memberName=$model->floorName=$model->stayProvide = $model->stayAddress ="";	
		}
	if($model->stayPlace==1 )
	$model->cottageName=$model->cottageNo = $model->occupayMonth = $model->occupayYear = $model->cottageAllot = "";
	
		if($model->stayPlace==3)
			$model->stayProvide = $model->stayAddress ="";

	
		$query=$this->createCommand('sp_ivc_stay');
		
		mssql_bind($query, '@Header_Code', $headerCode, SQLINT4, false, false, 5);
		mssql_bind($query, '@Stay_Code', $model->stayPlace, SQLINT4, false, false, 5);
		mssql_bind($query, '@Provode_Isha', $model->stayProvide, SQLINT4, false, false, 5);
		mssql_bind($query, '@Out_Stay_Address',$model->stayAddress,SQLVARCHAR,false,false, 
		strlen($model->stayAddress));
		mssql_bind($query, '@Cottage_Name', $model->cottageName, SQLINT4, false, false, 5);
		mssql_bind($query, '@Room_No',$model->cottageNo,SQLVARCHAR,false,false, strlen($model->cottageNo));
		mssql_bind($query, '@Cottage_Floor', $model->floorName, SQLINT4, false, false, 5);
		
	if($model->occupayMonth>0 and $model->occupayYear>0) $occupy_date = "01-".$model->occupayMonth."-".$model->occupayYear; else $occupy_date="";
		mssql_bind($query, '@Cottage_Occupancy',$occupy_date,SQLVARCHAR,false,false, strlen($occupy_date));
		mssql_bind($query, '@Stay_Type', $model->cottageAllot, SQLINT4, false, false, 5);
		mssql_bind($query, '@Member_Name',$model->memberName,SQLVARCHAR,false,false, strlen($model->memberName));
		mssql_bind($query, '@Applicant_Type', $model->cottageMember, SQLINT4, false, false, 5);
		$result = @mssql_execute($query);
		
		$this->msg = mssql_get_last_message(); 

	}
	 public function getStay()
     {   
		$query = $this->createCommand('sp_Fetchstay');      
    	$result = @mssql_execute($query); 
		      
        return $result;
     }
	 
	 
	 public function getCottage()
     {   
		$query = $this->createCommand('sp_Fetchcottage');      
    	$result = @mssql_execute($query); 
		      
        return $result;
     }
	 
	 
	public function getStayDetails($head)
	{
		
		$query = $this->createCommand('sp_Fetch_Stay_info');
		mssql_bind($query, '@Header_Code',$head,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);	
	
		return $result;	
	}
	 
	 
	 
	 
//End-----Stay----------------

//Start-----Yatra----------------
	public function saveYatra($model,$headerCode)
	{

		$query =$this->createCommand('IVC_yatra_Info');	
		mssql_bind($query, '@Header_Code', 		$headerCode, 			SQLINT4, 	false, 	false, 	5);			
		mssql_bind($query, '@IVC_Yatra_Code', 	$model->ivcYatraCode, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Yatra_Code', 		$model->yatraName, 		SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@YOL', 				$model->yearLastAttend, SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Att_Time', 		$model->noOfTimesAttend,SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Form_Valid',		$model->formValid,		SQLVARCHAR,	false,	false,	1);
		mssql_bind($query, '@Action', 			$model->action, 		SQLINT4, 	false,  false, 	5);		
		
		$result = @mssql_execute($query);
		
		$this->msg = mssql_get_last_message();	  
	}

	public function getYatraDetails($headerCode)
	{

	$query =$this->createCommand('sp_FetchYatra_info');
	mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
	$result = @mssql_execute($query);	
	
	return $result;
	
	}

	 public function getYatra()
     {   
		$query = $this->createCommand('sp_FetchYatra');      
    	$result = @mssql_execute($query);
 		
		return $result;
     }

//End-----Yatra----------------
//Start-----TeacherTraining----------------
	
	public function getProgram()
	{
  		$query = $this->createCommand('sp_FetchProgram_yii');      
    	$result = @mssql_execute($query);		     
 
 		return $result;	
	
	}
	public function getLanguage()
	{	
	    $query = $this->createCommand('sp_GetLanguage');      
    	$result = @mssql_execute($query);		     
 
 		return $result;	
	}
		public function getTypingLanguage($headerCode)
	{	
	    $query = $this->createCommand('sp_Fetch_Language_info'); 
		mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);     
    	$result = @mssql_execute($query);		     
 
 		return $result;	
	}
	
	
	public function getTeacherTrainingDetails($headerCode)
	{
	$query = $this->createCommand('sp_viewteachertraining');
	mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
	$result = @mssql_execute($query);	
	
	return $result;
		
	}
	
	public function saveTeacherTraining($model,$headerCode,$Others)
	{
		$query =$this->createCommand('SP_IVC_Teachertraining');	
		mssql_bind($query, '@Header_Code', 			$headerCode, 				SQLINT4, 	false, 	false, 	5);		
		mssql_bind($query, '@IVC_Training_Code', 	$model->ivcTrainingCode,	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Program_Code', 		$model->program, 			SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Language', 			$model->trainingLanguage, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Training_Completed', 	$model->trainingCompleted,	SQLVARCHAR, false, 	false, 	
		strlen($model->trainingCompleted));		
		if($model->handledSession<>"") $hSession = $model->handledSession; else $hSession=""	;
		mssql_bind($query, '@Handled_Session',$hSession,SQLVARCHAR, false,	false,	10);
		mssql_bind($query, '@Form_Valid',$model->formValid,SQLVARCHAR,	false,	false,	strlen($model->formValid));
		mssql_bind($query,	'@Others',	$model->Others,SQLVARCHAR,	false,	false,	strlen($model->Others));
	
		mssql_bind($query, '@Action',	$model->action, 	SQLINT4, 	false,  false, 	5);		
		$result = @mssql_execute($query);
		
		$this->msg = mssql_get_last_message();	  
	}
//End-----TeacherTraining----------------
//Start-----Official----------------
	public function getOccupation()
	{
  		$query = $this->createCommand('sp_FetchDesignation');      
    	$result = @mssql_execute($query);		     
 		
		return $result;	
	}
	public function getcountry()
	{
  		$query = $this->createCommand('sp_FetchCountry');      
    	$result = @mssql_execute($query);		     
 
 		return $result;	
	}

	public function saveOfficialDetails($model,$headerCode)
	{

	$query =$this->createCommand('IVC_Official_Info_New');
	
		mssql_bind($query, '@Header_Code', $headerCode, SQLINT4, false, false, 5);
		mssql_bind($query, '@IVC_Official_inactive_Code', $model->ivcOfficialInactiveCode,SQLINT4, false, false,5);
		mssql_bind($query, '@Occupation_Type', $model->Occupationtype, SQLINT4, false, false, 5);
		mssql_bind($query, '@Occupation', $model->Designation, SQLVARCHAR, false, false, 
		strlen($model->Designation));
		mssql_bind($query, '@Responsibility', $model->Responsibility, SQLVARCHAR, false, false, 
		strlen($model->Responsibility));
		mssql_bind($query, '@Company_Name', $model->CompanyName, SQLVARCHAR, false, false, 
		strlen($model->CompanyName));
		mssql_bind($query, '@Off_Address1', $model->Address, SQLVARCHAR, false, false,strlen($model->Address));
		mssql_bind($query, '@Off_Area', $model->Area, SQLVARCHAR, false, false,strlen($model->Area));
		mssql_bind($query, '@Off_City', $model->city, SQLINT4, false, false, 5);
		mssql_bind($query, '@Ocity_Name', $model->oCity, SQLVARCHAR, false, false,strlen($model->oCity));		
		mssql_bind($query, '@Off_State', $model->state, SQLINT4, false, false, 5);
		mssql_bind($query, '@Ostate_Name', $model->oState, SQLVARCHAR, false, false,strlen($model->oState));
		mssql_bind($query, '@Off_district', $model->district, SQLINT4, false, false, 5);
		mssql_bind($query, '@Off_Country', $model->country, SQLINT4, false, false, 5);
		mssql_bind($query, '@Off_ZipCode', $model->pincode, SQLVARCHAR, false, false, 
		strlen($model->pincode));
		
		if(strlen($model->fromdate)==10) 
		$wfromdate=date('d-m-Y',strtotime($model->fromdate));
		mssql_bind($query, '@WFrom_Date  ',$wfromdate, SQLVARCHAR, false, false, strlen($wfromdate));
		
		if(strlen($model->todate)==10) $wtodate=date('d-m-Y',strtotime($model->todate));
		
		mssql_bind($query, '@WTo_Date  ',$wtodate, SQLVARCHAR, false, false, strlen($wtodate));
		mssql_bind($query,'@Form_Valid',$model->formValid,SQLVARCHAR,false,false,strlen($model->formValid	));
		
		if($model->action==0)$model->action =1;
		
		mssql_bind($query, '@Action ',$model->action  , SQLINT4, false, false, 5);
		$result = @mssql_execute($query);
		
		$this->msg = mssql_get_last_message();	
	}
	public function saveOfficialOtherDetails($model1,$headerCode)
	{
		$query =$this->createCommand('IVC_OfficialOther_Info');
		mssql_bind($query, '@Header_Code', $headerCode, SQLINT4, false, false, 5);
		mssql_bind($query, '@Other_Organization', $model1->OfficialOther, SQLVARCHAR, false, false,
		strlen($model1->OfficialOther));
		mssql_bind($query,'@Form_Valid',$model1->formValid1,SQLVARCHAR,false,false,strlen($model1->formValid1));
		if($model1->action1==0)$model1->action1 = 1;
		mssql_bind($query, '@Action ',$model1->action1, SQLINT4, false, false, 5);
		
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();	
	}
	public function getOfficialDetails($head)
	{
		$query = $this->createCommand('sp_FetchOfficial_info');
		mssql_bind($query, '@Header_Code',$head,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);	
		
		return $result;	
	}

	public function getOfficialOtherDetails($head)
	{
		$query = $this->createCommand('sp_FetchOfficialOthers');
		mssql_bind($query, '@Header_Code',$head,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);

		if(mssql_num_rows($result)>0)
		{
			$fld=mssql_fetch_array($result);
			return $fld['Member_Others'];
		}
	}
//End-----Official----------------
//statr----Contact----------------

	public function getPhonecode()
	{
		$query = $this->createCommand('sp_selectphonecode_yii');		
		$result = @mssql_execute($query);	
		
		return $result;	
	}
	public function saveContactDetails($model,$headerCode)
 	{
 
		$query = $this->createCommand('sp_IVC_Emergency_Info_yii');
		
		mssql_bind($query, '@Header_Code', $headerCode , SQLINT4, false, false, 5);
		mssql_bind($query, '@IVC_Emergency_Code', $model->IVCEmergencyCode , SQLINT4,false,false, 5);
		mssql_bind($query, '@Rel_Name', $model->Name, SQLVARCHAR, false, false, strlen($model->Name));
		mssql_bind($query, '@Relationship',$model->Relationship, SQLVARCHAR, false, false,
		strlen($model->Relationship));
		mssql_bind($query, '@Rel_Address',$model->Address, SQLVARCHAR, false, false, 
		strlen($model->Address));
		mssql_bind($query, '@Rel_Country', $model->country, SQLINT4, false, false, 5);
		mssql_bind($query, '@Rel_State',$model->state, SQLINT4, false, false, 5);
		mssql_bind($query, '@Rel_District', $model->district, SQLINT4, false, false, 5);
		mssql_bind($query, '@Rel_City',$model->city, SQLINT4, false, false, 5);
		mssql_bind($query, '@Rel_Zip_Code', $model->pincode, SQLVARCHAR, false, false, 	strlen($model->pincode));
		mssql_bind($query, '@Rel_Phone_Code', $model->PhoneCode, SQLINT4, false, false, 5);
		mssql_bind($query, '@Rel_Contact',$model->ContactNo, SQLVARCHAR, false, false,strlen($model->ContactNo));
		mssql_bind($query, '@Rel_Emailid',$model->EmailId , SQLVARCHAR, false, false,strlen($model->EmailId));
		mssql_bind($query, '@Add_Type',$model->ContactType , SQLINT4, false, false, 5);
		mssql_bind($query, '@Ostate_Name', $model->oState, SQLVARCHAR, false, false,strlen($model->oState));
		mssql_bind($query, '@Ocity_Name', $model->oCity, SQLVARCHAR, false, false,strlen($model->oCity));
		
		if($model->action==0)$model->action =1;
		
		mssql_bind($query, '@Action ',$model->action  , SQLINT4, false, false, 5);
		
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();	
	}

	public function getcontactDetails($head)
	{
		$query = $this->createCommand('sp_Emergencyview');
		mssql_bind($query, '@Header_Code',$head,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);	
	
		return $result;	
	}
//End-----Contact----------------
//Start------Acadamic--------------------

	public function saveAcadamicInfo($model,$headerCode)
	{ 
		if($model->category==1 or $model->category==2 or $model->category==7) $categoryOthers = $model->categoryOthers;
		else $categoryOthers="";
		if($model->category==4) $degree = $model->ugDegree;
		elseif($model->category==5) $degree = $model->pgDegree;
	
		$query=$this->createCommand('ivc_academic_info');
		mssql_bind($query, '@Header_Code', $headerCode, SQLINT4, false, false, 5);
		mssql_bind($query, '@Academic_Code', $model->ivcAcadamicCode , SQLINT4, false, false, 5);	
		mssql_bind($query, '@Category', $model->category , SQLINT4, false, false, 5);	
		mssql_bind($query, '@Category_Others', $categoryOthers, SQLVARCHAR, false, false,
		strlen($model->categoryOthers));
		mssql_bind($query, '@Degree_Name', $degree, SQLINT4, false, false,5);
		mssql_bind($query, '@Degree_Others', $model->degreeOthers, SQLVARCHAR, false, false,
		strlen($model->degreeOthers));
		mssql_bind($query, '@university_name', $model->institutionName, SQLVARCHAR, false, false,
		strlen($model->institutionName));
		mssql_bind($query, '@university_city', $model->institutionCity, SQLVARCHAR, false, false,
		strlen($model->institutionCity));
		mssql_bind($query, '@specialization', $model->specialization, SQLVARCHAR, false, false,
		strlen($model->specialization));
		mssql_bind($query, '@passing_year', $model->yop, SQLINT4, false, false, 5);
		mssql_bind($query, '@percentage_marks', $model->grade,  SQLVARCHAR, false, false,strlen($model->grade));
		mssql_bind($query, '@Action', $model->action , SQLINT4, false, false, 5);			
		
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();
	}
	public function getAcadamicinfo($headerCode)
	{
		$query=$this->createCommand('Get_ivc_academic_info');
		mssql_bind($query, '@Header_Code', 	$headerCode, SQLINT4, 	false, 	false, 	5);			
		$result=@mssql_execute($query);
		
		return $result;
	}

	public function getDegree($obj)
	{
		if($obj==1)
			$query = $this->createCommand('SP_Ivc_ug_degree');      
		else
			$query = $this->createCommand('SP_Ivc_pg_degree');      
	    $result = @mssql_execute($query);		     
	    return $result;	
	}
//END--------Acadamic------------

//start----crimainal----------------

	public function saveCriminalDetails($model,$headerCode)
 	{
 
		$query = $this->createCommand('sp_ivc_Criminal_details');
		mssql_bind($query, '@Header_Code', $headerCode, SQLINT4, false, false, 5);
		mssql_bind($query, '@Criminal_Convicted', $model->criminalConvictedDetails, SQLVARCHAR, false, 
		false,strlen($model->criminalConvictedDetails));
		mssql_bind($query, '@Criminal_Proceeding', $model->criminalProceedingDetails, SQLVARCHAR, false, 
		false,strlen($model->criminalProceedingDetails));
		mssql_bind($query, '@Is_Criminal_Convicted', $model-> isCriminalConvicted, SQLVARCHAR, false, 
		false,1);
		mssql_bind($query, '@Is_Criminal_Proceeding', $model-> isCriminalProceeding, SQLVARCHAR, false, 
		false,1);
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();	
	}

	public function getCriminalDetails($head)
	{
		$query = $this->createCommand('sp_Fetch_Criminal_details');
		mssql_bind($query, '@Header_Code',$head,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);	
		
		return $result;	
	}



//Start-----Enclosure---------------
	public function saveEnclosure($model,$headerCode)
	{

		$query =$this->createCommand('sp_IVC_EditEnclosure_yii');	
		mssql_bind($query, '@Header_Code', 		    $headerCode, 			     SQLINT4, 	false, 	false, 	5);			
		mssql_bind($query, '@IVC_Enclosure_Code', 	$model->ivcEnclosureCode, 	 SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Enclosure_Code', 		$model->enClosureName, 		     SQLINT4, 	false, 	false, 	strlen($model->enClosureName));
		mssql_bind($query, '@IYC_Address', 			$model->iycAddress,          SQLVARCHAR, false, 	false, 	strlen($model->iycAddress));
		mssql_bind($query, '@Card_No', 		        $model->cardNo,              SQLVARCHAR, 	false, 	false, strlen($model->cardNo));
		mssql_bind($query, '@Others',		        $model->Others,			 SQLVARCHAR,	false,	false,	strlen($model->Others));
		
		if(strlen($model->cardExpDate)==10) $cardExpDate= date("Y-m-d",strtotime($model->cardExpDate)); 	
		mssql_bind($query, '@Card_Exp_Date', $cardExpDate,  SQLVARCHAR,	false,	false,	50);			
		mssql_bind($query, '@Form_Valid',		    $model->formValid,	SQLVARCHAR,	false,	false,	1);	
		mssql_bind($query, '@Action', 			    $model->action, 		     SQLINT4, 	false,  false, 	
		5);
		mssql_bind($query, '@Ecode', 			    $Ecode, 		     SQLINT4, 	true );
		
			
		$result = @mssql_execute($query);
		
		$this->msg = mssql_get_last_message();	 
		return $Ecode; 
	}

	public function getEnclosureDetails($headerCode)
	{

		$query =$this->createCommand('sp_viewenclosure');
		mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);	
		
		return $result;
	}

	 public function getEnclosure()
     {   
		$query = $this->createCommand('sp_FetchEnclosure');      
    	$result = @mssql_execute($query);       
        return $result;
     }

//End-----Enclosure----------------


//Start-----Language---------------
public function saveLanguage($model,$headerCode,$Omother)
{
	
		$query =$this->createCommand('sp_IVC_EditLanguage');	
		mssql_bind($query, '@Header_Code', 		    $headerCode, 			     SQLINT4, 	false, 	false, 	5);			
		mssql_bind($query, '@IVC_Language_Code', 	$model->ivcLanguageCode, 	 SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Language_Code', 		$model->languageName, 		 SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Speaking_Language', 	$model->speaking,    SQLVARCHAR, false, 	false, 	strlen($model->speaking));
		mssql_bind($query, '@Reading_Language', 	$model->reading,     SQLVARCHAR, false, 	false, 	strlen($model->reading));
		mssql_bind($query, '@Writing_Language', 	$model->writing,     SQLVARCHAR, false, 	false, 	strlen($model->writing));
		mssql_bind($query, '@Mother_Tongue', 		$model->motherTongue, 		 SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Omother',		        $Omother,					 SQLVARCHAR,	false,	false,	strlen($Omother));
		mssql_bind($query, '@Olanguage',		    $model->Olanguage,					 SQLVARCHAR,	false,	false,	strlen($model->Olanguage));
		mssql_bind($query, '@Typing',		        $model->typing,					 SQLVARCHAR,	false,	false,	strlen($model->typing));
		mssql_bind($query, '@Action', 			    $model->action, 		     SQLINT4, 	false,  false, 	5);		
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();	  
}

	public function getLanguageDetails($headerCode)
	{

	$query =$this->createCommand('sp_Fetch_Language_info');
	mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
	$result = @mssql_execute($query);	
	return $result;
	
}
public function getMotherTongueDetails($headerCode)
{
	$query =$this->createCommand('sp_GetMother_Tongue');
	mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
	$result = @mssql_execute($query);	
	if(mssql_num_rows($result)>0)
	{
		$fld=mssql_fetch_array($result);
		return $fld['Mother_Tonuge'];
	}
}


public function getLan_Language()
{
	    $query = $this->createCommand('sp_GetLan_Language');      
    	$result = @mssql_execute($query);		     
 return $result;	
}
public function getmother_Language()
{
	    $query = $this->createCommand('sp_Getmother_Language');      
    	$result = @mssql_execute($query);		     
 return $result;	
}

//END-----Language---------------


//Start-----Department----------------@Group_Name
public function savePastDepartment($model,$headerCode)
{
		$aActive='N';
		$reporting='';	$group = '';	
		$query =$this->createCommand('sp_Department_History');
			
		mssql_bind($query, '@Header_Code', 		        $headerCode, 			SQLINT4, 	false, 	false, 	5);	
		mssql_bind($query, '@Ashram_Official_Code', 	$model->ashramOfficialCode,	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@ADepartment_Code', 		$model->departmentName, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@AOccupation', 				$model->aOccupation, SQLVARCHAR, false, 	false, 	strlen($model->aOccupation));
		mssql_bind($query, '@Reporting', 				$reporting,SQLVARCHAR, false, 	false, 	strlen($reporting));		
		mssql_bind($query, '@From_Date', 		        $model->fromdate,SQLVARCHAR, false, 	false, 	strlen($model->fromdate));
		mssql_bind($query, '@TO_Date', 		            $model->todate,SQLVARCHAR, false, 	false, 	strlen($model->todate));
		mssql_bind($query, '@A_Active', 		        $aActive,SQLVARCHAR, false, 	false, 	1);		
		mssql_bind($query, '@WasReporting', 		    $model->wasReporting,SQLVARCHAR, false, 	false, 	strlen($model->wasReporting));
		mssql_bind($query, '@A_Remarks', 		        $model->aRemarks,SQLVARCHAR, false, 	false, 	strlen($model->aRemarks));
		mssql_bind($query, '@Form_Valid',		        $model->formValid,SQLVARCHAR,	false,	false,	1);		

		mssql_bind($query, '@Action', 			        $model->action, 		SQLINT4, 	false,  false, 	5);		
		mssql_bind($query, '@Group_Name', 			    $group, 		SQLINT4, 	false,  false, 	5);		
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();	  
	}

	public function savepresentDepartment($model1,$headerCode)
	{
		$toDate1='';
		$aRemarks1='';
		$aActive1='Y';		
		$WasReporting1='';
		
		$query =$this->createCommand('sp_Department_History');			
		mssql_bind($query, '@Header_Code', 		        $headerCode, 			SQLINT4, 	false, 	false, 	5);	
		mssql_bind($query, '@Ashram_Official_Code', $model1->ashramOfficialCode1, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@ADepartment_Code', 	$model1->departmentName1, 		SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@AOccupation', 			$model1->aOccupation1, SQLVARCHAR, false, 	false, 	strlen($model1->aOccupation1));
		mssql_bind($query, '@Reporting', 			$model1->reporting1,SQLVARCHAR, false, 	false, 	strlen($model1->reporting1));		
		mssql_bind($query, '@From_Date', 		    $model1->fromDate1,SQLVARCHAR, false, 	false, 	strlen($model1->fromDate1));
		mssql_bind($query, '@TO_Date', 		        $toDate1,SQLVARCHAR, false, 	false, 	strlen($aActive1));
		mssql_bind($query, '@A_Active', 		    $aActive1,SQLVARCHAR, false, 	false, 	1);	
			
		mssql_bind($query, '@WasReporting', 		$wasReporting1,SQLVARCHAR, false, 	false, 	strlen($wasReporting1));
		mssql_bind($query, '@A_Remarks', 		    $aRemarks1,SQLVARCHAR, false, 	false, 	strlen($aRemarks1));
		mssql_bind($query, '@Form_Valid',		    $model1->formValid,SQLVARCHAR,	false,	false,	1);			
		
		mssql_bind($query, '@Action', 			    $model1->action1, 		SQLINT4, 	false,  false, 	5);		
		mssql_bind($query, '@Group_Name', 			        $model1->group, 		SQLINT4, 	false,  false, 	5);				
		
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();	  
	}

	public function getpastDepartmentDetails($headerCode)
	{

		$query =$this->createCommand('sp_viewdepartment_yii');
		mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);	
		return $result;
	
	}
	public function getpresentDepartmentDetails($headerCode)
	{

		$query =$this->createCommand('sp_viewpresentdepartment_YII');
		mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);	
		return $result;
		
	}
   	public function getDepartment()
    {   
		$query = $this->createCommand('sp_FetchDepartment');      
    	$result = @mssql_execute($query);       
        return $result;
    }

//End-----Department-------------

//-------Start Family Form-----------------------------

	public function saveFamily($model,$headerCode)
	{        
		$fCode1=0;           
		$memName1='';                
		$relat1=''; 		             
		$gen1=0; 		              
		$famCont1='';
		$famPhone1='';             
		$famCon1=0;            
		$famEmail1='';        
		$famDepart1='';              
		$famAssoci1='';			 
        $familyResponsibility='';		
		
		$query =$this->createCommand('sp_Familyinformation_yii');
			
		mssql_bind($query, '@Header_Code', 		        $headerCode, 			SQLINT4, 	false, 	false, 	5);	
		mssql_bind($query, '@Family_Code',           	$model->familyCode, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Member_Name', 		        $model->memberName, 	SQLVARCHAR, false, 	false, 	strlen($model->memberName));
		mssql_bind($query, '@Relation', 				$model->relation,       SQLVARCHAR, false, 	false, 	strlen($model->relation));
		mssql_bind($query, '@Fam_Age', 				    $model->famAge,         SQLINT4, 	false, 	false, 	5);	
		mssql_bind($query, '@Gender', 		            $model->gender,         SQLINT4, 	false, 	false, 	5);	
		mssql_bind($query, '@Fam_Occupation', 		    $model->famOccupation,  SQLVARCHAR, false, 	false, 	strlen($model->famOccupation));
		mssql_bind($query, '@Occupation_address', 		$model->occupationAddress, SQLVARCHAR, false, 	false, strlen($model->occupationAddress));
		mssql_bind($query, '@Fam_contact_address', 		$model->famContactAddress, SQLVARCHAR, false, 	false, strlen($model->famContactAddress));
		mssql_bind($query, '@Fam_Phonecode', 		    $model->famPhonecode,SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Fam_contact',		        $model->famContact,SQLVARCHAR, false, 	false, 	strlen($model->famContact));
		mssql_bind($query, '@Fam_Emailid',		        $model->famEmailid,SQLVARCHAR,	false,	false,	strlen($model->famEmailid));
		mssql_bind($query, '@Fam_Department',		    $model->famDepartment,SQLINT4,	false,	false,	5);
		mssql_bind($query, '@Fam_Association',		    $model->famAssociation,SQLVARCHAR,	false,	false,	strlen($model->famAssociation));
	    
     	mssql_bind($query, '@Family_Code1',           	$fCode1, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Member_Name1', 		    $memName1, 	SQLVARCHAR, false, 	false, 	strlen($memName1));
		mssql_bind($query, '@Relation1', 				$relat1,       SQLVARCHAR, false, 	false, 	strlen($relat1));		
		mssql_bind($query, '@Gender1', 		            $gen1,         SQLINT4, 	false, 	false, 	5);		
		mssql_bind($query, '@Fam_contact_address1', 	$famCont1, SQLVARCHAR, false, 	false, 	strlen($famCont1));
		mssql_bind($query, '@Fam_Phonecode1', 		    $famPhone1,SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Fam_contact1',		        $famCon1,SQLVARCHAR, false, 	false, 	strlen($famCon1));
		mssql_bind($query, '@Fam_Emailid1',		        $famEmail1,SQLVARCHAR,	false,	false,	strlen($famEmail1));
		mssql_bind($query, '@Fam_Department1',		    $famDepart1,SQLINT4,	false,	false,	5);
		mssql_bind($query, '@Fam_Association1',		    $famAssoci1,SQLVARCHAR,	false,	false,	strlen($famAssoci1));		
		
		if($model->action==0)$model->action = 1;	
		mssql_bind($query, '@Action', 			        $model->action, SQLINT4, 	false,  false, 	5);	        	
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();
		
	}

	public function saveFamilyRelative($model1,$headerCode)
	{		
	
		$familyResponsibility1='';
		
		if($model1->familyCode1>0)
        	$fCode=$model1->familyCode1;   else $fCode=0;         
		$memName='';                
		$rel='';              
		$age=0;              
		$gen=0;               
		$famOccup='';              
		$oAddress='';              
		$ContactAddress='';
		$famcode='';             
		$famCon=0;            
		$famEmail='';        
		$famDep=0;              
		$famAsso='';
		 
		$query =$this->createCommand('sp_Familyinformation_yii');
					
		mssql_bind($query, '@Header_Code', 		        $headerCode, 			SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Family_Code',           	$fCode, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Member_Name', 		        $memName, 	SQLVARCHAR, false, 	false, 	strlen($memName));
		mssql_bind($query, '@Relation', 				$rel,       SQLVARCHAR, false, 	false, 	strlen($rel));
		mssql_bind($query, '@Fam_Age', 				    $age,         SQLINT4, 	false, 	false, 	5);		
		mssql_bind($query, '@Gender', 		            $gen,         SQLINT4, 	false, 	false, 	5);	
		mssql_bind($query, '@Fam_Occupation', 		    $famOccup,  SQLVARCHAR, false, 	false, 	strlen($famOccup));
		mssql_bind($query, '@Occupation_address', 		$oAddress, SQLVARCHAR, false, 	false, 	strlen($oAddress));
		mssql_bind($query, '@Fam_contact_address', 		$ContactAddress, SQLVARCHAR, false, 	false, 	strlen($ContactAddress));
		mssql_bind($query, '@Fam_Phonecode', 		    $famcode,SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Fam_contact',		        $famCon,SQLVARCHAR, false, 	false, 	strlen($famCon));
		mssql_bind($query, '@Fam_Emailid',		        $famEmail,SQLVARCHAR,	false,	false,	strlen($famEmail));
		mssql_bind($query, '@Fam_Department',		    $famDep,SQLINT4,	false,	false,	5);
		mssql_bind($query, '@Fam_Association',		    $famAsso,SQLVARCHAR,	false,	false,	strlen($famAsso));		
	  
		mssql_bind($query, '@Family_Code1',           	$model1->familyCode1, 	SQLINT4, 	false, 	false, 	5);
     	mssql_bind($query, '@Member_Name1', 		    $model1->memberName1, 	SQLVARCHAR, false, 	false, 	strlen($model1->memberName1));
		mssql_bind($query, '@Relation1', 				$model1->relation1,       SQLVARCHAR, false, 	false, strlen($model1->relation1));
		mssql_bind($query, '@Gender1', 		            $model1->gender1,         SQLINT4, 	false, 	false, 5);	
		mssql_bind($query, '@Fam_contact_address1', 	$model1->famContactAddress1, SQLVARCHAR, false, false, strlen($model1->famContactAddress1));
		mssql_bind($query, '@Fam_Phonecode1', 		    $model1->famPhonecode1,SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Fam_contact1',		        $model1->famContact1,SQLVARCHAR, false, 	false, 	strlen($model1->famContact1));
		mssql_bind($query, '@Fam_Emailid1',		        $model1->famEmailid1,SQLVARCHAR,	false,	false,	strlen($model1->famEmailid1));
		mssql_bind($query, '@Fam_Department1',		    $model1->famDepartment1,SQLINT4,	false,	false,	5);
		mssql_bind($query, '@Fam_Association1',		    $model1->famAssociation1,SQLVARCHAR,	false,	false,strlen($model1->famAssociation1));
						
		if($model1->action1==0)$model1->action1 = 1;		
		
		mssql_bind($query, '@Action', 			        $model1->action1, 		SQLINT4, 	false,  false, 	5);	
		$result = @mssql_execute($query);		
		$this->msg = mssql_get_last_message();
	
	
	}
	public function getFamilyDetails($headerCode)
	{
		$query =$this->createCommand('sp_Getfamilyinformation_yii');
		mssql_bind($query,'@Header_Code',$headerCode,SQLINT4,false,false,5);	
		return $result = @mssql_execute($query);
	
	}
	public function getFamilyRelativesDetails($headerCode)
	{
		$query1 =$this->createCommand('sp_Getfamilyinformation_Relative');
		mssql_bind($query1,'@Header_Code',$headerCode,SQLINT4,false,false,5);
			
		return $result1 = @mssql_execute($query1);	
	}

	public function getFamilyResponsibility($headerCode)
	{

		$query =$this->createCommand('sp_Familyresponsibility');
		mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);	
		return $result;
		
	}

	public function saveFamilyTextAreaDetails($model2,$headerCode)
	{

		$query =$this->createCommand('IVC_FamilyTextArea_Info');
		mssql_bind($query, '@Header_Code', $headerCode, SQLINT4, false, false, 5);
		mssql_bind($query, '@Family_Responsibility', $model2->responsibility, SQLVARCHAR, false, false,
		strlen($model2->responsibility));
		
		if($model2->action2==0)$model2->action2 = 1;
		
		mssql_bind($query, '@Action ',$model2->action2, SQLINT4, false, false, 5);
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();	
	}

	public function getFamilyTextAreaDetails($head)
	{
		$query = $this->createCommand('sp_FetchFamilyTextArea');
		mssql_bind($query, '@Header_Code',$head,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);
		
		return $result ;

	}

//--------Starting Medical--------------------

	public function saveMedical($model,$headerCode)
	{
	
				$mentalAllignment=$mentalMedication=$hospitalHistory=$mentalFrequency=$mentalStatus=$mental	=$majorSurgeries=$pmChallenged='';	$physicalIssue='';	$medicalMentalCode=0;

		$query =$this->createCommand('[SP_Medicalphysicalhistory]');	
		mssql_bind($query, '@Header_Code', 		  $headerCode, 			SQLINT4, 	false, 	false, 	5);	
		mssql_bind($query, '@Medical_Physical_Code', $model->medicalPhysicalCode, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Medical_Mental_Code',   $medicalMentalCode, 	    SQLINT4,    false, 	false, 	5);
	
		mssql_bind($query, '@Physical_Alignment ', 	 $model->physicalAllignment,SQLVARCHAR,false,false,	strlen($model->physicalAllignment));
		mssql_bind($query, '@Physical_Frequency', 	 $model->physicalFrequency,  SQLVARCHAR,false,false, strlen($model->physicalFrequency));
		mssql_bind($query, '@Physical_Status ', 	 $model->physicalStatus, SQLVARCHAR,  false, false, strlen($model->physicalStatus));
		mssql_bind($query, '@Physical', 		     $model->physical, SQLVARCHAR,    false, 	false, 	strlen($model->physical));
		mssql_bind($query, '@Mental_Alignment',		 $mentalAllignment,SQLVARCHAR,	false,	false,	strlen($mentalAllignment));
		mssql_bind($query, '@Mental_Medication',	 $mentalMedication,	 SQLVARCHAR,	false,	false,	strlen($mentalMedication));
		mssql_bind($query, '@Hospital_History',		 $hospitalHistory, SQLVARCHAR,	false,	false,	strlen($hospitalHistory));
		mssql_bind($query, '@Mental_Frequency',		 $mentalFrequency, SQLVARCHAR,	false,	false,	strlen($mentalFrequency));
		mssql_bind($query, '@Mental_Status',		 $mentalStatus,	 SQLVARCHAR,	false,	false,	strlen($mentalStatus));
		mssql_bind($query, '@Mental',		         $mental,		  SQLVARCHAR,	false,	false,	strlen($mental));	
	
		mssql_bind($query, '@Form_Valid',		     $model->formValid,	 SQLVARCHAR,	false,	false,	strlen($model->formValid));
			
		mssql_bind($query, '@Form_Valid1',		     $model->formValid1,SQLVARCHAR,	false,	false,	strlen($model->formValid1));	
		mssql_bind($query, '@Action', 			     $model->action, 	 SQLINT4, 	false,  false, 	5);		
			
		
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();	  
	}
	public function getBlood()
	{
	    $query = $this->createCommand('sp_Getbloodgroup');      
    	$result = @mssql_execute($query);		     
        return $result;	
	}
	public function getPhysicalDetails($headerCode)
	{

		$query =$this->createCommand('sp_viewmedicalhistory_Physical');
		mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);	
		return $result;
	
	}
	public function getBloodDetails($headerCode)
	{
		$query =$this->createCommand('sp_viewajaxblood');
		mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);	
		if(mssql_num_rows($result)>0)
		{
			$fld=mssql_fetch_array($result);
			return $fld['Blood_Group'];
		}
	}

	public function saveMedicalMental($model1,$headerCode)
	{
		$bloodName=$physicalAllignment=$physicalFrequency=$physicalStatus=$physical='';
		$medicalPhysicalCode=0;
		
		$query =$this->createCommand('[SP_Medicalphysicalhistory]');	
		mssql_bind($query, '@Header_Code',  $headerCode,       SQLINT4, 	    false, 	false, 	5);		
		mssql_bind($query, '@Medical_Physical_Code', $medicalPhysicalCode, 	 SQLINT4, 	    false, 	false, 	5);
		mssql_bind($query, '@Medical_Mental_Code',   $model1->medicalMentalCode,  SQLINT4, 	    false, 	false, 	5);
	
		mssql_bind($query, '@Physical_Alignment ', 	 $physicalAllignment,   SQLVARCHAR,    false, 	false, 	strlen($physicalAllignment));
		mssql_bind($query, '@Physical_Frequency', 	 $physicalFrequency,   SQLVARCHAR,    false, 	false, 	strlen($physicalFrequency));
		mssql_bind($query, '@Physical_Status ', 	 $physicalStatus,   SQLVARCHAR,    false, 	false, 	strlen($physicalStatus));
		mssql_bind($query, '@Physical', 		     $physical,        SQLVARCHAR,    false, 	false, 	strlen($physical));
		mssql_bind($query, '@Mental_Alignment',		 $model1->mentalAllignment,	 SQLVARCHAR,false,	false,	strlen($model1->mentalAllignment));
		mssql_bind($query, '@Mental_Medication',	 $model1->mentalMedication, SQLVARCHAR,	false,	false,	strlen($model1->mentalMedication));
		mssql_bind($query, '@Hospital_History',		 $model1->hospitalHistory, SQLVARCHAR,	false,	false,	strlen($model1->hospitalHistory));
		mssql_bind($query, '@Mental_Frequency',		 $model1->mentalFrequency,SQLVARCHAR,	false,	false,	strlen($model1->mentalFrequency));
		mssql_bind($query, '@Mental_Status',		 $model1->mentalStatus,	 SQLVARCHAR,	false,	false,	strlen($model1->mentalStatus));
		mssql_bind($query, '@Mental',		         $model1->mental,	SQLVARCHAR,	false,	false,	strlen($model1->mental));
		
		mssql_bind($query, '@Form_Valid',		     $model1->formValid, SQLVARCHAR,	false,	false,	50);
		mssql_bind($query, '@Form_Valid1',		     $model1->formValid1, SQLVARCHAR,	false,	false,	50);	
		mssql_bind($query, '@Action', 			     $model1->action1, SQLINT4, 	false,  false, 	5);		
		
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();	
	}

	public function getMedicalMentalDetails($headerCode)
	{

		$query =$this->createCommand('sp_viewmedicalhistory_Mental');
		mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);	
		return $result;
		
	}


	public function saveMedicalTextAreaDetails($model2,$headerCode)
	{

		$query =$this->createCommand('IVC_MedicalTextArea_Info');
		mssql_bind($query, '@Header_Code', $headerCode, SQLINT4, false, false, 5);
		mssql_bind($query, '@Major_Surgeries', $model2->majorSurgeries, SQLVARCHAR, false, false,
		strlen($model2->majorSurgeries));
		mssql_bind($query, '@Pm_Challenged', $model2->pmChallenged, SQLVARCHAR, false, false,
		strlen($model2->pmChallenged));
		mssql_bind($query, '@Physical_Issue', $model2->physicalIssue, SQLVARCHAR, false, false,
		strlen($model2->physicalIssue));
		if($model2->action2==0)$model2->action2 = 1;
		
		mssql_bind($query, '@Action ',$model2->action2, SQLINT4, false, false, 5);
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();	
	}

	public function getMedicalTextAreaDetails($head)
	{
		$query = $this->createCommand('sp_FetchMedicalTextArea');
		mssql_bind($query, '@Header_Code',$head,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);
		
		return $result ;
	}

  ////////END MEDICAL///////////////////////////////////>
  
  //Start-----Skills----------------
	public function saveComputerSkill($model,$headerCode)
	{
 	
		$type=1;
		$query =$this->createCommand('Sp_Ivc_Skills_yii');	
		mssql_bind($query, '@Header_Code', 		$headerCode, 			SQLINT4, 	false, 	false, 	5);			
		mssql_bind($query, '@IVC_Skill_Code', 	$model->ivcSkillCode, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Language', 		$model->computer, 		SQLVARCHAR,	false,	false,	strlen($model->computer));
		mssql_bind($query, '@Details', 			$model->details,        SQLVARCHAR,	false,	false,	strlen($model->details));
		mssql_bind($query, '@Form_Valid',		$model->formValid,		SQLVARCHAR,	false,	false,	1);
		mssql_bind($query, '@Form_Valid1',		$model->formValid1,		SQLVARCHAR,	false,	false,	1);
		mssql_bind($query, '@Form_Valid2',		$model->formValid2,			SQLVARCHAR,	false,	false,	1);
		mssql_bind($query, '@Type', 			$type, 		            SQLINT4, 	false,  false, 	5);	
		
		
		if($model->action==0)$model->action = 1;	
		mssql_bind($query, '@Action', 			$model->action, 		SQLINT4, 	false,  false, 	5);	
		
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();	  
	}
	public function getComputerSkillDetails($headerCode)
	{

		$query =$this->createCommand('sp_vieweskills_Computer');
		mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);	
		return $result;
	}
	
	public function saveHobbiesSkill($model3,$headerCode)
	{ 
	
		$type=3;
		$msg7='';
	
		$query =$this->createCommand('Sp_Ivc_Skills_yii');	
		mssql_bind($query, '@Header_Code', 		$headerCode, 			        SQLINT4, 	false, 	false, 	5);	
		mssql_bind($query, '@IVC_Skill_Code', 	$model3->ivcHobbiesCode, 	    SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Language', 		$model3->category, 		SQLVARCHAR,	false,	false,	strlen($model3->category));
		mssql_bind($query, '@Details', 			$model3->details,           SQLVARCHAR,	false,	false,	strlen($model3->details));
		mssql_bind($query, '@Form_Valid',		$model3->formValid,				SQLVARCHAR,	false,	false,1);
		mssql_bind($query, '@Form_Valid1',		$model3->formValid1,			SQLVARCHAR,	false,	false,	1);
		mssql_bind($query, '@Form_Valid2',		$model3->formValid2,			SQLVARCHAR,	false,	false,	1);
		mssql_bind($query, '@Type', 			$type, 	                        SQLINT4, 	false,  false, 	5);	
		
		if($model3->action2==0)$model3->action2 = 1;	
		
		mssql_bind($query, '@Action',$model3->action2,SQLINT4,false,false,5);			
		$result = @mssql_execute($query);		
		$this->msg = mssql_get_last_message();	 	  
	}
	
	public function getHobbiesViewDetails($headerCode)
	{

		$query =$this->createCommand('sp_vieweHobbies_Type');
		mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);	
		return $result;
	}

	public function saveTypingSkill($model1,$headerCode)
	{ 
	
		$type=2;
	
		$query =$this->createCommand('Sp_Ivc_Skills_yii');	
		mssql_bind($query, '@Header_Code', 		$headerCode, 			        SQLINT4, 	false, 	false, 	5);	
		mssql_bind($query, '@IVC_Skill_Code', 	$model1->ivcSkillCode1, 	    SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Language', 		$model1->typingLanguage, 		SQLVARCHAR,	false,	false,	strlen($model1->typingLanguage));
		mssql_bind($query, '@Details', 			$model1->typingSpeed,           SQLVARCHAR,	false,	false,	strlen($model1->typingSpeed));
		mssql_bind($query, '@Form_Valid',		$model1->formValid,				SQLVARCHAR,	false,	false,1);
		mssql_bind($query, '@Form_Valid1',		$model1->formValid1,			SQLVARCHAR,	false,	false,	1);
		mssql_bind($query, '@Form_Valid2',		$model1->formValid2,			SQLVARCHAR,	false,	false,	1);
		mssql_bind($query, '@Type', 			$type, 	                        SQLINT4, 	false,  false, 	5);
			
		
		if($model1->action1==0)$model1->action1 = 1;	
		
		mssql_bind($query, '@Action',$model1->action1,SQLINT4,false,false,5);			
		$result = @mssql_execute($query);		
		$this->msg = mssql_get_last_message();	 	  
	}

	public function getTypingSkillDetails($headerCode)
	{
		$query =$this->createCommand('sp_vieweskills_Type');
		mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);	
		return $result;
}

///START Program Participants////////////////////////////////////

public function saveProgram($model,$headerCode)
{
 $createdby=0;
 
		$query =$this->createCommand('sp_RDS_program_Particiapnt');	
		mssql_bind($query, '@Header_Code', 		$headerCode, 			SQLINT4, 	false, 	false, 	5);				
		mssql_bind($query, '@IVC_Pgmcode', 	    $model->rdsPgmCode, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@pgmcode', 		    $model->pgmName, 		SQLVARCHAR,	false,	false,	50);
		mssql_bind($query, '@pothers', 			$model->Others,                SQLVARCHAR,	false,	false,	strlen($model->Others));
		if($model->pgmMonth>0 and $model->pgmYear>0) $monthYear = $model->pgmYear."-".$model->pgmMonth."-01";
		mssql_bind($query, '@Pgm_Date',$monthYear,SQLVARCHAR,false,false, strlen($monthYear));
		mssql_bind($query, '@PCenter_Code',		$model->pgmCenter,		SQLVARCHAR,	false,	false,	1);
		mssql_bind($query, '@Teacher_Code', 	$model->pgmTeacher, 	SQLVARCHAR,	false,	false,	strlen($model->pgmTeacher));	
		mssql_bind($query, '@Created_by', 		$createdby,  SQLINT4, 	false,  false, 	5);	
		if($model->action==0)$model->action = 1;	
		mssql_bind($query, '@Action', 			$model->action, 		SQLINT4, 	false,  false, 	5);	
		
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();	  
}
public function getProgramDetails($headerCode)
{

	$query =$this->createCommand('sp_RDS_Program_Info');
	mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
	$result = @mssql_execute($query);	
	return $result;
	
}

//fetch completion status
	public function formfillstatus($headerCode)
	{
		$query = $this->createCommand('sp_FetchValidFormData');
		mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
		$result = @mssql_execute($query);
	
		return mssql_fetch_array($result);
	}
		
// Manual check in 

public function savemanualcheckin($uid,$chkstatus)
	{
	
		$query = $this->createCommand('sp_manualcheckin_Out');
		mssql_bind($query, '@uid',$uid,SQLVARCHAR,false,false,strlen($uid));	
		mssql_bind($query, '@chkstatus',$chkstatus,SQLINT4,false,false,5);
		
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();	
	
	
	}
	
	public function saveVolunteeringAtIsha($model,$headerCode)
	{

      $volunteeringCode1=0;$programName1='';$centerName1='';$typeOfVolunteering1='';$Others1='';
	  
		$query =$this->createCommand('sp_VolunteeringHistory1');	
		mssql_bind($query, '@Header_Code', 		    $headerCode, 			SQLINT4, 	false, 	false, 	5);			
		mssql_bind($query, '@Volunteering_Code', 	$model->volunteeringCode, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Program_Name', 		$model->programName, 		SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@TypeOf_Volunteering', 	$model->typeOfVolunteering,SQLVARCHAR,	false,	false,	strlen($model->typeOfVolunteering));		
		
		mssql_bind($query, '@Volunteering_Code1', 	$volunteeringCode1, 	SQLINT4, 	false, 	false, 	5);		
		mssql_bind($query, '@Program_Name1', 		$programName1, 		SQLINT4, 	false, 	false, 	5);		
		mssql_bind($query, '@Center_Name1', 		$centerName1, 		SQLINT4, 	false, 	false, 	5);		
	    mssql_bind($query, '@TypeOf_Volunteering1', $typeOfVolunteering1,SQLVARCHAR,	false,	false,	strlen($typeOfVolunteering1));	
		
		
		if($model->action==0)$model->action = 1;
		mssql_bind($query, '@Action', 			    $model->action, 		SQLINT4, 	false,  false, 	5);	
		
		mssql_bind($query, '@Form_Valid',		     $model->formValid,	 SQLVARCHAR,	false,	false,	strlen($model->formValid));
			
		mssql_bind($query, '@Form_Valid1',		     $model->formValid1,SQLVARCHAR,	false,	false,	strlen($model->formValid1));
		
		mssql_bind($query,	'@Others',	$model->Others,SQLVARCHAR,	false,	false,	strlen($model->Others));
		
		mssql_bind($query,	'@Others1',	$Others1,SQLVARCHAR,	false,	false,	strlen($Others1));		
		$result = @mssql_execute($query);		
		$this->msg = mssql_get_last_message();	  
	}
	
	public function saveVolunteeringAtLocal($model1,$headerCode)
	{
     $volunteeringCode=0;$programName='';$typeOfVolunteering='';$Others='';
	 
	 
	 	$query =$this->createCommand('sp_VolunteeringHistory1');	
		mssql_bind($query, '@Header_Code', 		    $headerCode, 			SQLINT4, 	false, 	false, 	5);			
		mssql_bind($query, '@Volunteering_Code', 	$volunteeringCode, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Program_Name', 		$programName, 		SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@TypeOf_Volunteering', 	$typeOfVolunteering,SQLVARCHAR,	false,	false,	strlen($typeOfVolunteering));	
		
		
		mssql_bind($query, '@Volunteering_Code1', 	$model1->volunteeringCode1, 	SQLINT4, 	false, 	false, 	5);		
		mssql_bind($query, '@Program_Name1', 		$model1->programName1, 		SQLINT4, 	false, 	false, 	5);		
		mssql_bind($query, '@Center_Name1', 		$model1->centerName1, 		SQLINT4, 	false, 	false, 	5);		
	    mssql_bind($query, '@TypeOf_Volunteering1', $model1->typeOfVolunteering1,SQLVARCHAR,	false,	false,	strlen($model1->typeOfVolunteering1));	
		
		
		if($model1->action1==0)$model1->action1 = 1;
		mssql_bind($query, '@Action', 			    $model1->action1, 		SQLINT4, 	false,  false, 	5);		
		
		mssql_bind($query, '@Form_Valid',		     $model1->formValid,	 SQLVARCHAR,	false,	false,	strlen($model1->formValid));
			
		mssql_bind($query, '@Form_Valid1',		     $model1->formValid1,SQLVARCHAR,	false,	false,	strlen($model1->formValid1));		
			
		mssql_bind($query,	'@Others',	$Others,SQLVARCHAR,	false,	false,	strlen($Others));
		
		mssql_bind($query,	'@Others1',	$model1->Others1,SQLVARCHAR,	false,	false,	strlen($model1->Others1));	
			
		$result = @mssql_execute($query);		
		$this->msg = mssql_get_last_message();	  
		
	}
	
	
	public function saveVolunteeringTextAreaDetails($model2,$headerCode)
	{

		$query =$this->createCommand('IVC_VolunteerHistory_TextArea_Info');
		mssql_bind($query, '@Header_Code', $headerCode, SQLINT4, false, false, 5);
		mssql_bind($query, '@StayedAt_Isha', $model2->stayedIsha, SQLVARCHAR, false, false,
		strlen($model2->stayedIsha));
		
		if($model2->action2==0)$model2->action2 = 1;
		
		mssql_bind($query, '@Action ',$model2->action2, SQLINT4, false, false, 5);
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();	
	}
	
	
	public function getVolunteeringTextAreaDetails($headerCode)
	{

	$query =$this->createCommand('sp_GetVolunteerTextArea');
	mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
	$result = @mssql_execute($query);	
	
	return $result;
	
	}

	public function getIshaCenterDetails($headerCode)
	{

	$query =$this->createCommand('sp_GetVolunteer_Isha');
	mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
	$result = @mssql_execute($query);	
	
	return $result;
	
	}
	public function getLocalCenterDetails($headerCode)
	{

	$query =$this->createCommand('sp_GetVolunteer_Local');
	mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
	$result = @mssql_execute($query);	
	
	return $result;
	
	}
	
	//--------------START Reference ---------------------------//
	
	
	public function getProfessionalRefDetails($headerCode)
	{

	$query =$this->createCommand('sp_GetProfessionalRef');
	mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
	$result = @mssql_execute($query);	
	
	return $result;
	
	}
	
	public function getVolunteerRefDetails($headerCode)
	{

	$query =$this->createCommand('sp_GetVolunteerReference');
	mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
	$result = @mssql_execute($query);	
	
	return $result;
	
	}
	
	public function getResidentRefDetails($headerCode)
	{

	$query =$this->createCommand('sp_GetResidentReference');
	mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
	$result = @mssql_execute($query);	
	
	return $result;
	
	}
	
	
	
	public function saveProfessionalReference($model,$headerCode)
	{        
		$referenceIshaCode=0;           
		$rName='';                
		$centerName=''; 		             
		$phoneNo='';		
		$rEmailId='';
		             
		$referenceResidentCode=0;            
		$rRname='';        
		$department='';              
		$cNo='';
		
        		
		$query =$this->createCommand('IVC_ProfessonalReference_Info');
			
		mssql_bind($query, '@Header_Code', 		    $headerCode, 			SQLINT4, 	false, 	false, 	5);	
		mssql_bind($query, '@Reference_Code1',      $model->refernceCode, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Name', 		        $model->name, 	SQLVARCHAR, false, 	false, 	strlen($model->name));
		mssql_bind($query, '@Designation_Address', 	$model->designation,       SQLVARCHAR, false, 	false, 	strlen($model->designation));
		mssql_bind($query, '@Relationship', 		$model->relationship,    SQLVARCHAR, false, 	false, 	strlen($model->relationship));
		mssql_bind($query, '@Contact_No', 		    $model->contactNo,         SQLVARCHAR, false, 	false, 	strlen($model->contactNo));
		mssql_bind($query, '@Pro_EmailId', 		    $model->emailId,  SQLVARCHAR, false, 	false, 	strlen($model->emailId));		
		
	    
     	mssql_bind($query, '@Reference_Code2',      $referenceIshaCode, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Vol_Name', 		    $rName, 	SQLVARCHAR, false, 	false, 	strlen($rName));
		mssql_bind($query, '@Center', 				$centerName,       SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Phone_No', 		    $phoneNo,        SQLVARCHAR, false, 	false, 	strlen($phoneNo));	
		mssql_bind($query, '@Vol_Email', 	        $rEmailId, SQLVARCHAR, false, 	false, 	strlen($rEmailId));
		
		
		mssql_bind($query, '@Reference_Code3', 		$referenceResidentCode,SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Res_Name',		        $rRname,SQLVARCHAR, false, 	false, 	strlen($rRname));
		mssql_bind($query, '@Department',		    $department,SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Res_Contact',		    $cNo,SQLVARCHAR, false, 	false, 	strlen($cNo));
			
		
		if($model->action==0)$model->action = 1;	
		mssql_bind($query, '@Action', 			    $model->action, SQLINT4, 	false,  false, 	5);	        	
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();
		
	}
	
	
	
	public function saveVolunteerReference($model1,$headerCode)
	{        
	
		$refernceCode=0;           
		$name='';                
		$designation=''; 		             
		$relationship='';		
		$contactNo='';
		$emailId='';
		             
		$referenceResidentCode=0;            
		$rRname='';        
		$department='';              
		$cNo='';		
		        		
		$query =$this->createCommand('IVC_ProfessonalReference_Info');
			
		mssql_bind($query, '@Header_Code', 		    $headerCode, 			SQLINT4, 	false, 	false, 	5);	
		mssql_bind($query, '@Reference_Code1',      $refernceCode, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Name', 		        $name, 	SQLVARCHAR, false, 	false, 	strlen($name));
		mssql_bind($query, '@Designation_Address', 	$designation,       SQLVARCHAR, false, 	false, 	strlen($designation));
		mssql_bind($query, '@Relationship', 		$relationship,    SQLVARCHAR, false, 	false, 	strlen($relationship));
		mssql_bind($query, '@Contact_No', 		    $contactNo,         SQLVARCHAR, false, 	false, 	strlen($contactNo));
		mssql_bind($query, '@Pro_EmailId', 		    $emailId,  SQLVARCHAR, false, 	false, 	strlen($emailId));		
		
	    
     	mssql_bind($query, '@Reference_Code2',      $model1->referenceIshaCode, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Vol_Name', 		    $model1->rName, 	SQLVARCHAR, false, 	false, 	strlen($model1->rName));
		mssql_bind($query, '@Center', 				$model1->centerName,       SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Phone_No', 		    $model1->phoneNo,        SQLVARCHAR, false, 	false, 	strlen($model1->phoneNo));	
		mssql_bind($query, '@Vol_Email', 	        $model1->rEmailId, SQLVARCHAR, false, 	false, 	strlen($model1->rEmailId));
		
		
		mssql_bind($query, '@Reference_Code3', 		$referenceResidentCode,SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Res_Name',		        $rRname,SQLVARCHAR, false, 	false, 	strlen($rRname));
		mssql_bind($query, '@Department',		    $department,SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Res_Contact',		    $cNo,SQLVARCHAR, false, 	false, 	strlen($cNo));
			
		
		if($model1->action1==0)$model1->action1 = 1;	
		mssql_bind($query, '@Action', 			    $model1->action1, SQLINT4, 	false,  false, 	5);	        	
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();
		
	}
	
	public function saveResidentReference($model2,$headerCode)
	{     
	
		$refernceCode=0;           
		$name='';                
		$designation=''; 		             
		$relationship='';		
		$contactNo='';
		$emailId='';
		             
		$referenceIshaCode=0;           
		$rName='';                
		$centerName=''; 		             
		$phoneNo='';		
		$rEmailId='';
        		
		$query =$this->createCommand('IVC_ProfessonalReference_Info');
			
		mssql_bind($query, '@Header_Code', 		    $headerCode, 			SQLINT4, 	false, 	false, 	5);	
		mssql_bind($query, '@Reference_Code1',      $refernceCode, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Name', 		        $name, 	SQLVARCHAR, false, 	false, 	strlen($name));
		mssql_bind($query, '@Designation_Address', 	$designation,       SQLVARCHAR, false, 	false, 	strlen($designation));
		mssql_bind($query, '@Relationship', 		$relationship,    SQLVARCHAR, false, 	false, 	strlen($relationship));
		mssql_bind($query, '@Contact_No', 		    $contactNo,         SQLVARCHAR, false, 	false, 	strlen($contactNo));
		mssql_bind($query, '@Pro_EmailId', 		    $emailId,  SQLVARCHAR, false, 	false, 	strlen($emailId));		
		
	    
     	mssql_bind($query, '@Reference_Code2',      $referenceIshaCode, 	SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Vol_Name', 		    $rName, 	SQLVARCHAR, false, 	false, 	strlen($rName));
		mssql_bind($query, '@Center', 				$centerName,       SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Phone_No', 		    $phoneNo,        SQLVARCHAR, false, 	false, 	strlen($phoneNo));	
		mssql_bind($query, '@Vol_Email', 	        $rEmailId, SQLVARCHAR, false, 	false, 	strlen($rEmailId));
		
		
		mssql_bind($query, '@Reference_Code3', 		$model2->referenceResidentCode,SQLINT4, 	false, 	false, 	5);
		mssql_bind($query, '@Res_Name',		        $model2->rRname,SQLVARCHAR, false, 	false, 	strlen($model2->rRname));
		mssql_bind($query, '@Department',		    $model2->department,SQLINT4, 	false, 	false, 	5);
		
		mssql_bind($query, '@Res_Contact',		    $model2->cNo,SQLVARCHAR, false, 	false, 	strlen($model2->cNo));
			
		
		if($model2->action2==0)$model2->action2 = 1;	
		mssql_bind($query, '@Action', 			    $model2->action2, SQLINT4, 	false,  false, 	5);	        	
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();
		
	}
	
	//---------------------------END REFERENCE ----------------------------------//
	
    public function getFamilyProgramDetails($headerCode)
	{

	$query =$this->createCommand('SP_FamilyMembers_ProgramInfo');
	mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
	$result = @mssql_execute($query);	
	
	return $result;
	
	}
	
	
	public function saveFamilyFormValid($model,$headerCode)
	{
		$query =$this->createCommand('SP_IVC_Family_FormValid');	
		mssql_bind($query, '@Header_Code', 			$headerCode, 				SQLINT4, 	false, 	false, 	5);		
		mssql_bind($query, '@Form_Valid',$model->formValid,SQLVARCHAR,	false,	false,	strlen($model->formValid));
		
		$result = @mssql_execute($query);
		
		$this->msg = mssql_get_last_message();	  
	}
	
	
	public function getActiveStatus()
     {   
		$query = $this->createCommand('sp_FetchActive_Status');      
    	$result = @mssql_execute($query);
 		
		return $result;
     }
	 public function getResCategory()
     {   
		$query = $this->createCommand('sp_FetchResident_Category');      
    	$result = @mssql_execute($query);
 		
		return $result;
     }
	  public function getResCardType()
     {   
		$query = $this->createCommand('sp_GetResident_CardType');      
    	$result = @mssql_execute($query);
 		
		return $result;
     }
	 
	  public function getCardStatus()
     {   
		$query = $this->createCommand('sp_FetchResident_CardType');      
    	$result = @mssql_execute($query);
 		
		return $result;
     }
     public function getAlertFlag()
     {   
		$query = $this->createCommand('sp_FetchResident_AlertFlag');      
    	$result = @mssql_execute($query);
 		
		return $result;
     }
	  public function getAlertType()
     {   
		$query = $this->createCommand('sp_FetchSecurity_AlertType');      
    	$result = @mssql_execute($query);
 		
		return $result;
     }
	 public function getAmentiesType()
     {   
		$query = $this->createCommand('sp_FetchAmenties_CardType');      
    	$result = @mssql_execute($query);
 		
		return $result;
     }
	 
	 	 
	 public function saveResidentAdminData($model,$header_code)
	    {
		$query =$this->createCommand('Sp_Resident_Admin_Data');	
		
		mssql_bind($query, '@Header_Code'   ,$header_code,  SQLINT4 ,  false, 	false, 5);
		if(strlen($model->doj)==10) 
		mssql_bind($query, '@Doj'	,date('Y-m-d',strtotime($model->doj)), 	SQLVARCHAR,		 false,     false, strlen($model->doj));
		else
		 mssql_bind($query, '@Doj'	,$model->doj, 	SQLVARCHAR,		 false,     false, strlen($model->doj));
		mssql_bind($query, '@Uid'		,$model->uId, 	   SQLVARCHAR, 	     false,     false, 
		strlen($model->uId));
		mssql_bind($query, '@Active_Status',$model->activeStatus, 		SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Category'	,$model->category, 		SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Card_Type'	,$model->cardType, 		SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Card_Serial_No'	,$model->cardSerialNo, 	SQLVARCHAR,		 false,     false, strlen($model->cardSerialNo));
		if(strlen($model->cardIssueDate)==10)
			mssql_bind($query, '@Card_Issued_Date'	,date('Y-m-d',strtotime($model->cardIssueDate)), 	SQLVARCHAR,		 false,     false, strlen($model->cardIssueDate));
		else	
			mssql_bind($query, '@Card_Issued_Date'	,$model->cardIssueDate, 	SQLVARCHAR,		 false,     false, strlen($model->cardIssueDate));
		if(strlen($model->cardExpiryDate)==10)
				mssql_bind($query, '@Card_ExpiryDate'	,date('Y-m-d',strtotime($model->cardExpiryDate)), 	SQLVARCHAR,		 false,     false, strlen($model->cardExpiryDate));
		else
			mssql_bind($query, '@Card_ExpiryDate'	,$model->cardExpiryDate, 	SQLVARCHAR,		 false,     false, strlen($model->cardExpiryDate));
			
		mssql_bind($query, '@Card_Status'	,$model->cardStatus, 		SQLINT4,   false, 	false, 5);
		if(strlen($model->fTVCdate)==10)
			mssql_bind($query, '@FTVC'	,date('Y-m-d',strtotime($model->fTVCdate)), 	SQLVARCHAR,		 false,     false, 
		strlen($model->fTVCdate));
		else
			mssql_bind($query, '@FTVC'	,$model->fTVCdate, 	SQLVARCHAR,		 false,     false, 
		strlen($model->fTVCdate));
		mssql_bind($query, '@Alert_Flag'	,$model->alertFlag, 		SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Alert_Type'	,$model->alertType, 		SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Honorarium_Amount'	,$model->honorariumAmount, 		SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Acard_Type'	,$model->amentiesCtype, 		SQLINT4,   false, 	false, 5);
		if(strlen($model->amentiesCexpiry)==10)
			mssql_bind($query, '@Acard_Expiry'	,date('Y-m-d',strtotime($model->amentiesCexpiry)), 	SQLVARCHAR,		 false,     false, strlen($model->amentiesCexpiry));
		else
			mssql_bind($query, '@Acard_Expiry'	,$model->amentiesCexpiry, 	SQLVARCHAR,		 false,     false, strlen($model->amentiesCexpiry));
			
		mssql_bind($query, '@Mcard_Issued'	,$model->mCardIssued, 		SQLINT4,   false, 	false, 5);
		if(strlen($model->mCardExpiry)==10)
			mssql_bind($query, '@Mcard_Expiry'	,date('Y-m-d',strtotime($model->mCardExpiry)), 	SQLVARCHAR,		 false,     false, strlen($model->mCardExpiry));
		else
				mssql_bind($query, '@Mcard_Expiry'	,$model->mCardExpiry, 	SQLVARCHAR,		 false,     false, strlen($model->mCardExpiry));
		$result = @mssql_execute($query);				
		$this->msg = mssql_get_last_message(); 
		
		}
		
		public function getResidentAdminDataDetails($headerCode)
     {   
		$query =$this->createCommand('sp_Resident_AdminData_View');	
		mssql_bind($query, '@Header_Code', 		$headerCode, 			SQLINT4, 	false, 	false, 	5);		
    	$result = @mssql_execute($query); 		      
 
 		return $result;
		
     }
	 
	 public function getHobbiesDetails()
     {   
		$query = $this->createCommand('sp_Fetch_Hobbies');      
    	$result = @mssql_execute($query);
 		
		return $result;
     }
	 
	 public function getActiveDepartment($headerCode)
	{	
	    $query = $this->createCommand('sp_Fetch_ActiveDep_info'); 
		mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);     
    	$result = @mssql_execute($query);		     
 
 		return $result;	
	}
	
	
	public function getActiveDepartmentDetails($headerCode)
{
	$query =$this->createCommand('sp_GetActive_Dept');
	mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);	
	$result = @mssql_execute($query);	
	if(mssql_num_rows($result)>0)
	{
		$fld=mssql_fetch_array($result);
		return $fld['Department_Code'];
	}
}
public function saveNewUserCreation($model)
	    {
		$query =$this->createCommand('Sp_NewUserCreation');		
		mssql_bind($query, '@User_Id'	,$model->uId, 	SQLINT4,   false, 	false, 5);
		mssql_bind($query, '@Name'		,$model->fullName, 	   SQLVARCHAR, 	     false,     false, 
		strlen($model->fullName));
		mssql_bind($query, '@User_Name',$model->userName, 		SQLVARCHAR, 	     false,     false, 
		strlen($model->userName));
		mssql_bind($query, '@Password'	,$model->passWord, 		SQLVARCHAR, 	     false,     false, 
		strlen($model->passWord));
		mssql_bind($query, '@EmailId'	,$model->emailId, 		SQLVARCHAR, 	     false,     false, 
		strlen($model->emailId));
		mssql_bind($query, '@NewDepartment'	,$model->department, 	SQLINT4, 	false, 	false, 	5);		
		mssql_bind($query, '@ReferredDept'	,$model->referredFromDept, 	SQLVARCHAR,		 false,     false, 
		strlen($model->referredFromDept));
		mssql_bind($query, '@ReferredVCD'	,$model->referredFromVcd, 	SQLVARCHAR,		 false,     false, 
		strlen($model->referredFromVcd));
		$result = @mssql_execute($query);				
		$this->msg = mssql_get_last_message(); 
		}
		public function getNewUserDetails($headerCode)
     {   
		$query =$this->createCommand('sp_New_UserView');	
		mssql_bind($query, '@Header_Code', 		$headerCode, 			SQLINT4, 	false, 	false, 	5);		
    	$result = @mssql_execute($query); 		      
 		return $result;
     }
	 public function checkForgotPassword($model)
	    {
			$query =$this->createCommand('sp_ForgotPassword');		
		mssql_bind($query, '@Full_Name'		,$model->fullName, 	   SQLVARCHAR, 	     false,     false, 
		strlen($model->fullName));
		mssql_bind($query, '@Email_Id'	,$model->emailId, 		SQLVARCHAR, 	     false,     false, 
		strlen($model->emailId));
		$result = @mssql_execute($query);
		$this->msg = mssql_get_last_message();
				 							
		return $result;
		}
	 public function saveForgotPassword($model,$headerCode)
	    {
	
		$query =$this->createCommand('Sp_Changepassword');		
		mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5);
		mssql_bind($query, '@Currentpwd',$model->currentPassword,SQLVARCHAR,false,false,strlen($model->currentPassword));
		mssql_bind($query,'@Newpwd',$model->newPassword,SQLVARCHAR,false,false, 
		strlen($model->newPassword));
		$result = @mssql_execute($query);				
		$this->msg = mssql_get_last_message(); 
		}
		 public function showNewUsersDetails()
	    {

		$query =$this->createCommand('SP_ShowNewUser');
		
		$result = @mssql_execute($query);	
		
		return $result;
		
		}
		
		public function getChangeSessionDetails($headerCode)
 		{
			$query =$this->createCommand('SP_Login_In_OtherUser');
 			mssql_bind($query, '@Header_Code',$headerCode,SQLINT4,false,false,5); 
 			$result = @mssql_execute($query); 
 		
		return $result;
		}
}	?>