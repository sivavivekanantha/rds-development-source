<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class PastDepartmentForm extends CFormModel
{
	public	$ashramOfficialCode;
	public	$aDepartmentCode;	
	public	$departmentName;	
	public	$aOccupation;	
	public	$fromDate;
	public	$toDate;	
	public	$wasReporting;
	public	$aRemarks;
	public  $action;
	public  $formValid;
    public  $errflag;
	public  $errmsg;
	
	
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// Deptname, occupation, date are required
			
			array('ashramOfficialCode','safe'),		
			array('aDepartmentCode','safe'),
			array('departmentName','required'),		
			array('aOccupation','required'),				
			array('fromDate','safe'),			
			array('toDate','safe'),
			array('aActive','safe'),
			array('wasReporting','safe'),
			array('aRemarks','safe'),
			array('action','safe'),	
			array('formValid','safe'),		
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'departmentName'=>'Department',			
			'aOccupation'=>'Role In Department',			
			'fromDate'=>'From Date',						
			'toDate'=>'To Date',
			'wasReporting'=>'Was Reporting To',			
			'aRemarks'=>'Remarks'	,
			
		);
	}
	public function pastdepartmentValidate($model)
	{
		$cVal = new CommonValidator();
			if( strlen($model->fromDate)==10 and strlen($model->toDate)==10 )
				$dummy = $cVal->DateCheck($model->fromDate,$model->toDate,&$this->errmsg,&$this->errflag,"From Date must less than To Date");
				
			
		    if(strlen($model->fromDate)>0)
		        $dummy = $cVal->dateformatchk($model->fromDate,&$this->errmsg,&$this->errflag,"From Date");
			if(strlen($model->fromDate)==10)
				$dummy = $cVal->DOBCheck($model->fromDate,&$this->errmsg,&$this->errflag,"Work From Date must less than current Date"); 	
	
			
			   if(strlen($model->toDate)>0)
		        $dummy = $cVal->dateformatchk($model->toDate,&$this->errmsg,&$this->errflag,"To Date");
			if(strlen($model->toDate)==10) 	
			$dummy = $cVal->DOBCheck($model->toDate,&$this->errmsg,&$this->errflag,"Work To Date must less than current Date");
			if($model->departmentName<>1000008)	
		$dummy = $cVal->Strcheck($model->aOccupation,&$this->errmsg,&$this->errflag,"Role In Department");
		}

} ?>