<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class OfficialForm extends CFormModel
{
	public  $ivcOfficialInactiveCode;
	public  $action;
	public	$Occupationtype;
	public	$Designation;
	public	$Responsibility;
	public	$CompanyName;
	public	$Address;
	public	$Area;
	public	$country;
	public	$state;
	public  $oState;
	public	$district;
	public	$city;
	public	$oCity;
	
	public	$pincode;
	public	$fromdate;
	public	$todate;	
    public $errmsg;
	public $errflag;
	public $formValid;
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
		
			
				array('ivcOfficialInactiveCode', 'safe'),
				array('action', 'safe'),
				array('Occupationtype', 'required'),
				array('Designation', 'safe'),
				array('Responsibility', 'safe'),
				array('CompanyName', 'safe'),
				array('Address','safe'),
				array('Area', 'safe'),
				array('country', 'safe'),
				array('state', 'safe'),
				array('district', 'safe'),
				array('city', 'safe'),
				array('pincode', 'safe'),
				array('fromdate', 'safe'),
				array('todate', 'safe'),
				array('oState','safe'),
				array('oCity','safe'),
				array('formValid','safe'),
				array('errflag','safe'),
			
				
				);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(			
			'Occupationtype'=>Yii::t('en','Official_label1'),
			'Designation'=>Yii::t('en','Official_label2'),
			'Responsibility'=>Yii::t('en','Official_label3'),
			'CompanyName'=>Yii::t('en','Official_label4'),
			'Address'=>Yii::t('en','Official_label5'),
			'Area'=>Yii::t('en','Official_label6'),
			'country'=>Yii::t('en','Official_label7'),
			'state'=>Yii::t('en','Official_label8'),
			'district'=>Yii::t('en','Official_label9'),
			'city'=>Yii::t('en','Official_label10'),
			'pincode'=>Yii::t('en','Official_label11'),
			'fromdate'=>Yii::t('en','Official_label12'),
			'todate'=>Yii::t('en','Official_label13'),
			'formValid'=>Yii::t('en','Official_label14'),
			'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			'nameAndAddress'=>Yii::t('en','Official_label15'),
			'official'=>Yii::t('en','Header_label11'),
			
		);
	}
	public function officeValidate($model)
	{
		$cVal = new CommonValidator();
		$dummy = $cVal->Strcheck($model->Occupationtype,$this->errflag);
		if($dummy==1) $this->addError('Occupationtype','Field cannot be blank');	
		
		if(strlen($model->fromdate)==10 and strlen($model->todate)== 10)
		{
			$dummy = $cVal->DateCheck($model->fromdate,$model->todate,$this->errflag);
			if($dummy==1) $this->addError('fromdate','From Date must be less than To Date');
		}
			
		if($model->state==9999)
		{
		  $dummy = $cVal->Strcheck($model->oState,$this->errflag);
		  if($dummy==1) $this->addError('oState',Yii::t('en','Official_label8').Yii::t('en','err_label1'));
		}
		  
		if($model->city==99999)
		{
		  $dummy = $cVal->Strcheck($model->oCity,$this->errflag);  
		  if($dummy==1) $this->addError('oCity',Yii::t('en','Official_label10').Yii::t('en','err_label1'));
		}
 	}

}
?>