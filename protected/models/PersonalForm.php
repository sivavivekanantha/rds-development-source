<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class PersonalForm extends CFormModel
{
	
	public	$ivcCode;
	public	$title;
	public	$firstName;
	public	$Aliasname;
	public	$changeName;
	public	$previousName;
	public	$gender;
	public	$dob;
	public	$age;
	public	$nationality;
	public	$maritalStatus;

	public	$phoneCode;
	public	$contact1;
	public	$phoneCode2;
	public	$contact2;
	public	$phoneCode3;
	public	$contact3;
	public	$phoneCode4;
	public	$contact4;
	
	
	public	$emailId;
	public	$languageCode;	
	public	$photo;
	public	$centerCode;
	public	$subCenter;
	
	public	$bloodGroup;
	public	$gazetteChange;
	public	$refNo;
	public	$pageNo;
	public	$dob2;
	public	$isahangaMail;
	public	$aliasName;


	public	$religion;
	public	$caste;
	public	$birthTown;
	public	$birthCity;
	public	$birthCountry;
	public	$receiveIshangaMail;
	public	$FTV_DOJ_Isha;
	
	public	$existingCard;
	public	$otherReligion	;
	public	$reciveMail;
	public	$modifyBy;
	public	$sno;
	public	$oldPID;
	public	$retVal1;
	public	$retIvc;
	public  $errflag;
	public  $errmsg;
	
	
	
		/*Declares the validation rules.*/
	public function rules()
	{
	 return array( 		// fullname, existingcardtype are required

	        array('ivcCode','safe'),	
	        array('title','safe'),		
			array('firstName','required'),	
			array('Aliasname','safe'),	
			array('changeName','safe'),				
			array('gender','safe'),	
			array('dob','safe'),
			array('age','safe'),		
			array('nationality','safe'),	
			array('pageNo','safe'),				
			array('maritalStatus','safe'),	
			
			array('phoneCode','safe'),	
			array('contact1','safe'),	
			array('phoneCode2','safe'),	
			array('contact2','safe'),	
			array('phoneCode3','safe'),	
			array('contact3','safe'),		
			array('phoneCode4','safe'),	
			array('contact4','safe'),			
			
			array('emailId','safe'),	
			array('languageCode','safe'),	
		
			array('photo', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'update'),
			array('centerCode','safe'),	
			array('subCenter','safe'),
			
						
			array('gazetteChange','safe'),	
			array('refNo','safe'),	
			array('pageNo','safe'),	
			array('dob2','safe'),	
			array('isahangaMail','safe'),	
			
			
			array('previousName','safe'),	
			array('religion','safe'),		
			
			array('caste','safe'),					
			array('birthTown','safe'),	
			array('birthCountry','safe'),	
			array('birthCity','safe'),	
			array('receiveIshangaMail','safe'),	
			array('FTV_DOJ_Isha','safe'),	
			array('existingCard','safe'),	
			array('otherReligion','safe'),	
			array('reciveMail','safe'),	
			array('modifyBy','safe'),	
			array('sno','safe'),		
			array('oldPID','safe'),	
			array('retVal1','safe'),	
			array('retIvc','safe'),		
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'title'=>Yii::t('en','Personal_label1'),
			'Aliasname'=>Yii::t('en','Personal_label2'),
			'changeName'=>Yii::t('en','Personal_label3'),
			'previousName'=>Yii::t('en','Personal_label4'),
			'gazetteChange'=>Yii::t('en','Personal_label5'),
			'refNo'=>Yii::t('en','Personal_label6'),
			'dob2'=>Yii::t('en','Personal_label7'),
			'pageNo'=>Yii::t('en','Personal_label8'),
			'gender'=>Yii::t('en','Personal_label9'),			
			'dob'=>Yii::t('en','Personal_label10'),
			'age'=>Yii::t('en','Personal_label11'),						
			'maritalStatus'=>Yii::t('en','Personal_label12'),
			'nationality'=>Yii::t('en','Personal_label13'),
			'birthCountry'=>Yii::t('en','Personal_label14'),
			'birthCity'=>Yii::t('en','Personal_label15'),
			'birthTown'=>Yii::t('en','Personal_label16'),
			'religion'=>Yii::t('en','Personal_label17'),
			'FTV_DOJ_Isha'=>Yii::t('en','Personal_label18'),
			'photo'=>Yii::t('en','Personal_label19'),
			'header'=>Yii::t('en','Header_label1'),
			
		
		);
	}
	public function personalValidate($model)
	{
		$dummy='';	
		$cVal = new CommonValidator();
	
	
			
			if(strlen($model->dob)==0)			
				 $dummy = $cVal->Strcheck($model->dob,$this->errflag);
				 if($dummy==1) $this->addError('dob',Yii::t('en','Personal_label10').Yii::t('en','err_label1'));
					
		     		
				if($model->religion==9999) {
					$dummy = $cVal->Strcheck($model->otherReligion,$this->errflag);
				 	if($dummy==1) $this->addError('otherReligion','otherReligion cannot be blank.');
				}
			
	
	}

	

}
?>