<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class HelpDeskForm extends CFormModel
{
	
	public	$name;
	public	$contactNo;
	public	$emailId;
	public	$description;
	
		
	public  $errflag;
	public  $errmsg;
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
			
			
			array('name','required'),	
			array('contactNo','required'),	
			array('emailId','required'),	
			array('description','required'),
			array('errflag','safe'),	
		
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			
			'name'=>Yii::t('en','Helpdesk_label1'),
			'contactNo'=>Yii::t('en','Helpdesk_label2'),
			'emailId'=>Yii::t('en','Helpdesk_label3'),
			'description'=>Yii::t('en','Helpdesk_label4'),
			
			
		);
	}
	
	
	
	public function HelpdeskValidate($model)
	{
			$cVal = new CommonValidator();
			
			
			$dummy = $cVal->Strcheck($model->name,&$this->errflag);
			if($dummy==1) $this->addError('name',Yii::t('en','Helpdesk_label1').Yii::t('en','err_label1'));
			$dummy = $cVal->Numcheck($model->contactNo,&$this->errflag);
			if($dummy==1) $this->addError('contactNo',Yii::t('en','Helpdesk_label2').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model->emailId,&$this->errflag);
			if($dummy==1) $this->addError('emailId',Yii::t('en','Helpdesk_label3').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model->description,&$this->errflag);
			if($dummy==1) $this->addError('description',Yii::t('en','Helpdesk_label4').Yii::t('en','err_label1'));
						
		    if(strlen($model->emailId)>0)$dummy = $cVal->Emailcheck($model->emailId,$this->errflag);
			if($dummy==1) $this->addError('emailId','Invalid Email ID Format');
			
			
			
   }
   

	

}
?>