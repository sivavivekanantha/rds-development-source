<?php 
	if(Yii::app()->session['Freeze']=="Y") return;
class AjaxModel extends BaseModel
{
	public function getGroup($departmentCode)
{
  		$query = $this->createCommand('sp_Getgroupview_yii');
		mssql_bind($query,'@Department_Code',$departmentCode,SQLINT4,false,false,5);
    	$result =mssql_execute($query);	
		return $result;	
}
	//---------------Ajax Drop Down Area  State-----------------
	public function getState($countrycode)
	{
  		$query = $this->createCommand('sp_Getstateview_yii');
		mssql_bind($query,'@Country_Code',$countrycode,SQLINT4,false,false,5);
    	$result =mssql_execute($query);	
		return $result;	
	}

	//---------------District State-----------------
	public function getdistrict($statecode)
	{
  		$query = $this->createCommand('sp_Getdistrictview');
		mssql_bind($query,'@State_Code',$statecode,SQLINT4,false,false,5);
    	$result =mssql_execute($query);	
		return $result;	
	}

	//---------------city-----------------
	public function getCity($districtcode,$statecode)
	{
  		$query = $this->createCommand('sp_Getcityview_yii');
		mssql_bind($query,'@District_Code',$districtcode,SQLINT4,false,false,5);
		mssql_bind($query,'@State_Code',$statecode,SQLINT4,false,false,5);
		$result =mssql_execute($query);			
		return $result;	
	}

	public function saveTheme($theme,$head)
	{
		   $query = $this->createCommand('sp_Updatetheme');
		   	mssql_bind($query,'@Header_Code',$head,SQLINT4,false,false,5);
			mssql_bind($query,'@theme',$theme,SQLVARCHAR,false,false,100);
	    	return $result = @mssql_execute($query);	
	}
	public function getFloor($cottageno)
	{
		$query = $this->createCommand('sp_Fetchcottage_floor');   
		mssql_bind($query, '@cottage_no', 		$cottageno, 			SQLINT4, 	false, 	false, 	5);   
    	$result = @mssql_execute($query); 
		      
        return $result;
     }
	
	public function saveYatraFormValid($headercode,$formcode)
	{
	 	
	 	$query = $this->createCommand('sp_yatraFormcheck');   
		mssql_bind($query, '@header_Code', $headercode, 			SQLINT4, 	false, 	false, 	5);  
		mssql_bind($query, '@Formvalid', $formcode, 			SQLINT4, 	false, 	false, 	5);    
    	$result = @mssql_execute($query); 
		   
        return $result;
	 }

	public function saveTeacherFormValid($headercode,$formcode)
	{
	 	
	 	$query = $this->createCommand('sp_TeacherFormcheck');   
		mssql_bind($query, '@header_Code', 		$headercode, 			SQLINT4, 	false, 	false, 	5);  
		mssql_bind($query, '@Formvalid', 		$formcode, 			SQLINT4, 	false, 	false, 	5);    
    	$result = @mssql_execute($query); 
		      
        return $result;
	}

	public function saveTypingSkillsFormValid($headercode,$formcode)
	{
	 	$query = $this->createCommand('TypingSkillsFormcheck');   
		mssql_bind($query, '@header_Code', 		$headercode, 			SQLINT4, 	false, 	false, 	5);  
		mssql_bind($query, '@Formvalid', 		$formcode, 			SQLINT4, 	false, 	false, 	5);    
    	$result = @mssql_execute($query); 

        return $result;
	 } 
 public function saveComputerSkillsFormValid($headercode,$formcode)
 {
	 	$query = $this->createCommand('ComputerSkillsFormcheck');   
		mssql_bind($query, '@header_Code', 		$headercode, 			SQLINT4, 	false, 	false, 	5);  
		mssql_bind($query, '@Formvalid', 		$formcode, 			SQLINT4, 	false, 	false, 	5);    
    	$result = @mssql_execute($query); 
		      
        return $result;
	 		  
	 }
	 
	 /*public function Savemother($header_code,$mother,$other)
	{		

		   $query = $this->createCommand('sp_UpdateMothertongue');
		   	mssql_bind($query,'@Header_Code',$header_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Mother_Tonuge',$mother,SQLINT4,false,false,5);
			mssql_bind($query,'@Omother',$other,SQLVARCHAR,false,false,strlen($other));
			
	    	return $result = @mssql_execute($query);	
	}*/
	 public function SaveBlood($header_code,$blood)
	{		
		
		
		    $query = $this->createCommand('sp_UpdateBloodGroup');
		   	mssql_bind($query,'@Header_Code',$header_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Blood_Group',$blood,SQLINT4,false,false,5);			
	    	return $result = @mssql_execute($query);	
	}

public function saveEnclosureFormValid($headercode,$formcode)
{
	 	$query = $this->createCommand('EnclosureFormcheck');   
		mssql_bind($query, '@header_Code',$headercode,SQLINT4, 	false, 	false, 	5);  
		mssql_bind($query, '@Formvalid', $formcode, SQLINT4, 	false, 	false, 	5);    
    	$result = @mssql_execute($query); 
		      
        return $result;
	 		  
	 }	 

public function saveDepartmentFormValid($headercode,$formcode)
{
	 	$query = $this->createCommand('DepartmentFormcheck');   
		mssql_bind($query, '@header_Code',$headercode,SQLINT4, 	false, 	false, 	5);  
		mssql_bind($query, '@Formvalid', $formcode, SQLINT4, 	false, 	false, 	5);
		print_r($headercode)   ;
		print_r($formcode)   ;
		 
    	$result = @mssql_execute($query);		      
        return $result;
	 		  
	 }	 
	 
	 public function saveVolunteerIshaFormValid($headercode,$formcode)
{
	
	 	$query = $this->createCommand('volunteerIshaFormcheck');   
		mssql_bind($query, '@header_Code',$headercode,SQLINT4, 	false, 	false, 	5);  
		mssql_bind($query, '@Formvalid', $formcode, SQLINT4, 	false, 	false, 	5);    
    	$result = @mssql_execute($query);
      
        return $result;
	 		  
	 }		 
 public function saveVolunteerLocalFormValid($headercode,$formcode)
{
	
	 	$query = $this->createCommand('volunteerLocalFormcheck');   
		mssql_bind($query, '@header_Code',$headercode,SQLINT4, 	false, 	false, 	5);  
		mssql_bind($query, '@Formvalid', $formcode, SQLINT4, 	false, 	false, 	5);    
    	$result = @mssql_execute($query);
	      
        return $result;
	 		  
	 }		 
public function savePhysicalFormValid($headercode,$formcode)
{
	
	 	$query = $this->createCommand('physicalFormcheck');   
		mssql_bind($query, '@header_Code',$headercode,SQLINT4, 	false, 	false, 	5);  
		mssql_bind($query, '@Formvalid', $formcode, SQLINT4, 	false, 	false, 	5);    
    	$result = @mssql_execute($query);
		print_r($formcode)	;	      
        return $result;
	 		  
	 }		 
	 
public function savementalFormValid($headercode,$formcode)
{
	 	$query = $this->createCommand('mentalFormcheck');   
		mssql_bind($query, '@header_Code',$headercode,SQLINT4, 	false, 	false, 	5);  
		mssql_bind($query, '@Formvalid', $formcode, SQLINT4, 	false, 	false, 	5);    
    	$result = @mssql_execute($query);
			//print_r($formcode)	;		      
        return $result;
	 		  
	 }	
public function savePassportFormValid($headercode,$formcode)
{
	 	$query = $this->createCommand('passportFormcheck');   
		mssql_bind($query, '@header_Code',$headercode,SQLINT4, 	false, 	false, 	5);  
		mssql_bind($query, '@Formvalid', $formcode, SQLINT4, 	false, 	false, 	5);    
    	$result = @mssql_execute($query);
		      
        return $result;
	 		  
	 }	
	 
public function saveOfficialFormValid($headercode,$formcode)
{
	
	 	$query = $this->createCommand('officialFormcheck');   
		mssql_bind($query, '@header_Code',$headercode,SQLINT4, 	false, 	false, 	5);  
		mssql_bind($query, '@Formvalid', $formcode, SQLINT4, 	false, 	false, 	5);
		  
    	$result = @mssql_execute($query);
		      
        return $result;
	 		  
	 }		 
	  
	 
 	 
	public function ajaxdepartmentDropDownList1()
{

  		$query = $this->createCommand('sp_FetchDepartment');	
    	$result = mssql_execute($query);	
		return $result;	
} 
public function ajaxdepartmentDropDownList()
{

  		$query = $this->createCommand('sp_FetchDepartment');	
    	$result = mssql_execute($query);	
		return $result;	
}
	  	
public function saveFormCompleteStatus($headercode,$filled)
{
	 	$query = $this->createCommand('sp_updateform_filled');   
		mssql_bind($query, '@Header_Code',$headercode,SQLINT4, 	false, 	false, 	5);  
		mssql_bind($query, '@Form_Filled', $filled, SQLVARCHAR, 	false, 	false, 	5);    
    	$result = @mssql_execute($query);
		if($result and $filled=='Y') {

		 echo "<p  align='center' style='color: red;' >Thanks for form completion.Your data has been moved to further process...</p>";  
		
		}
			      
       
	 		  
	 }
	 
	public function saveFeedback($headercode,$msg)
	{
	 	$query = $this->createCommand('sp_userFeedback');   
		mssql_bind($query, '@Header_Code',$headercode,SQLINT4, 	false, 	false, 	5);  
		mssql_bind($query, '@msg', $msg, SQLVARCHAR, 	false, 	false, 	5);    
    	$result = @mssql_execute($query);
	}
	
	
	public function UpdateFamilyProgramInfo($familyCode,$ishaYoga1,$ishaYoga2,$ishaYoga3,$ishaYoga4)
    {
	 	$query = $this->createCommand('sp_Update_Family_Members');  		
		mssql_bind($query, '@IVC_Family_Code',$familyCode,SQLINT4,false,false,5);		
		mssql_bind($query, '@Isha_Yoga1',$ishaYoga1,SQLVARCHAR,false,false,1);		
		mssql_bind($query, '@Isha_Yoga2',$ishaYoga2,SQLVARCHAR,false,false,1);		
		mssql_bind($query, '@Isha_Yoga3',$ishaYoga3,SQLVARCHAR,false,false,1);		
		mssql_bind($query, '@Isha_Yoga4',$ishaYoga4,SQLVARCHAR,false,false,1);		
	  	$result = @mssql_execute($query);		
		return $result;	
		
	 }
	 
	 
	 public function saveFamilyMemberInfoFormValid($headercode,$formcode)
 {    
	  	$query = $this->createCommand('sp_FamilyProgram_Formcheck');   
		mssql_bind($query, '@header_Code', 		$headercode, 			SQLINT4, 	false, 	false, 	5);  
		mssql_bind($query, '@Formvalid', 		$formcode, 			SQLINT4, 	false, 	false, 	5);    
    	$result = @mssql_execute($query); 
		      
        return $result;
	
	 		  
	 }
	  
	  public function SaveHobbiesFormValid($headercode,$formcode)
{
	 	$query = $this->createCommand('HobbiesSkillsFormcheck');   
		mssql_bind($query, '@header_Code', 		$headercode, 			SQLINT4, 	false, 	false, 	5);  
		mssql_bind($query, '@Formvalid', 		$formcode, 			SQLINT4, 	false, 	false, 	5);    
    	$result = @mssql_execute($query); 
		      
        return $result;
	 		  
	 } 

	  
	   public function SaveMother($header_code,$mother)
	{		
		
		
		    $query = $this->createCommand('sp_UpdateLanMother');
		   	mssql_bind($query,'@Header_Code',$header_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Mother',$mother,SQLINT4,false,false,5);			
	    	return $result = @mssql_execute($query);	
	}
	
	 public function SaveActiveDepartment($header_code,$department)
	{		
		
		
		    $query = $this->createCommand('sp_UpdateActiveDepartment');
		   	mssql_bind($query,'@Header_Code',$header_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Department',$department,SQLINT4,false,false,5);			
	    	return $result = @mssql_execute($query);	
	}
	
	 public function showDepartmentlist($group)
	{

		$objdepartment = new AjaxModel();
	
	    $department = $objdepartment->getdepartmentlist($group);	
		
		return $this->retArray($department);

	}
	
	public function UpdateAdminPermission($Header_Code)
	{				
		    $query = $this->createCommand('sp_UpdateAdminPermission');
		   	mssql_bind($query,'@Header_Code',$Header_Code,SQLINT4,false,false,5);
		    return $result = @mssql_execute($query);	
	}
public function getajaxdepartment($group)
	{
	$query = $this->createCommand('sp_Departmentview');   
        mssql_bind($query, '@Group_Code',$group, SQLINT4, 	false, 	false, 	5);			
    	return $result = @mssql_execute($query); 
	
	}
	
	
	//User Type Forms

public function getUserLevel()
{
	$query  = $this->createCommand('SP_User_Level');
	$result =  mssql_execute($query);
    return $result;
}



public function getUserType($utype,$searchtext)
	{
		
	 $query  = $this->createCommand('SP_IVC_User_Type');	
	 mssql_bind($query, '@Utype', $utype, SQLINT4, false,false,5);
	 mssql_bind($query, '@Name', $searchtext, SQLTEXT, false,false,500);
	 $result =  mssql_execute($query);
     
	return $result;
	
	}	
	
	public function getUserAcessMenu()
	{
		$query  = $this->createCommand('SP_IVC_User_Acess_Menu');	
	 	$result = @mssql_execute($query);
     	return $result;
	}
	
	public function getUserMenu()
	{
		$query  = $this->createCommand('SP_IVC_User_Menus');	
	 	$result = @mssql_execute($query);
     	return $result;
	}
	
		
	public function getUserMenu_db($UserType)
	{
		$query  = $this->createCommand('SP_IVC_User_Menus_saved');	
		mssql_bind($query, '@UserType', $UserType, SQLVARCHAR, false,false,200);
	 	$result = @mssql_execute($query);
	   
     	return $result;
	}
	public function checkUserLevel($header_code)
	{
		$check='';
		$query  = $this->createCommand('SP_IVC_CheckUserLevel');	
		mssql_bind($query, '@Header_Code', $header_code, SQLINT4, false,false,5);	
	 	$result = @mssql_execute($query);
		$field = mssql_fetch_array($result);
		if(strlen($field['User_Type'])>0)
		{
			$check = '<span style=color:red>Already Level Assigned:'.$field['User_Type'].'</span>';
			return $check;
		}
		else
     		return $check;
		
	}
	
	public function getAccessMenu()
	{
		$query  = $this->createCommand('SP_Acessing_Menu');	
	 	$result = @mssql_execute($query);
     	return $result;
	}
		
	public function saveUserType($model)
	{	
	 	 $query  = $this->createCommand('SP_IVC_User_Type_Save_Master');
		 mssql_bind($query, '@User_Type_Text', $model->usertype, SQLTEXT, false,false,500);
		 mssql_bind($query, '@View_Menu_Code', $model->viewmenucode, SQLTEXT, false,false,500);
		 mssql_bind($query, '@Edit_Menu_Code', $model->editmenucode, SQLTEXT, false,false,500);
		 mssql_bind($query, '@Access_Menu_Code', $model->accessmenucode, SQLTEXT, false,false,500);
		 $result = @mssql_execute($query);
		 $this->msg = mssql_get_last_message();
     	 return $result;
	}

public function viewUserType($headercode)
	{
	 $query  = $this->createCommand('SP_IVC_View_User_Type');	
	 mssql_bind($query, '@Header_Code', $headercode, SQLINT4, false,false,5);
	 $result = @mssql_execute($query);
      return $result;
	}	
	Public function getUserTypeReport($usertype)
	{
		
	 $query  = $this->createCommand('SP_IVC_User_Type_Report');	
	 mssql_bind($query, '@User_Type', $usertype, SQLINT4, false,false,5);
	 $result = @mssql_execute($query);
     return $result;
		
	}
	public function UpdateFreeze($Header_Code,$Freeze)
	{
		$query = $this->createCommand('sp_updatefreeze_filled');   
	  	mssql_bind($query, '@Header_Code',$Header_Code,SQLINT4,  false,  false,  5);  
	  	mssql_bind($query, '@Freeze', $Freeze, SQLVARCHAR,  false,  false,  5);    
	    $result = @mssql_execute($query);
       
        return $result;
	}	

}?>