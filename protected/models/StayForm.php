<?php 
			class StayForm extends CFormModel
			{
			public $stayPlace;
			public $cottageName;
			public $floorName;
			public $cottageNo;
			public $occupayYear;
			public $occupayMonth;
			public $cottageAllot;
			public $cottageMember;
			public $memberName;
			public $stayProvide;
			public $stayAddress;
			public  $errflag;
			public  $errmsg;
	

			public function rules()
			{
			return array(
			array('stayPlace','safe' ),
			array('cottageName', 'safe'),
			array('floorName', 'safe'),
			array('cottageNo', 'safe'),
			array('occupayYear', 'safe'),
			array('occupayMonth', 'safe'),
			array('cottageAllot', 'safe'),
			array('cottageMember', 'safe'),
			array('memberName', 'safe'),
			array('stayProvide','safe'),
			array('stayAddress','safe')
			);
			}

		    public function attributeLabels()
			{
				return array(
			
			'stayPlace'=>Yii::t('en','Stay_label1'),
			'cottageName'=>Yii::t('en','Stay_label2'),
			'floorName'=>Yii::t('en','Stay_label3'),
			'cottageNo'=>Yii::t('en','Stay_label4'),
			'occupayYear'=>Yii::t('en','Stay_label5'),
			'cottageAllot'=>Yii::t('en','Stay_label6'),
			'cottageMember'=>Yii::t('en','Stay_label7'),
		    'memberName'=>Yii::t('en','Stay_label8'),
			'stayProvide'=>Yii::t('en','Stay_label9'),
			'stayAddress'=>Yii::t('en','Stay_label10'),
			
			'stay'=>Yii::t('en','Header_label19'),
						
		);
	}

	public function stayValidate($model)
		{
		$cVal = new CommonValidator();		
		$dummy = $cVal->Strcheck($model->stayPlace,&$this->errflag);
		if($dummy==1) $this->addError('stayPlace',Yii::t('en','Stay_label1').Yii::t('en','err_label1'));
			
		if($model->stayPlace==3) 
		{			
	    $dummy = $cVal->Strcheck($model->cottageName,&$this->errflag);
		if($dummy==1) $this->addError('cottageName',Yii::t('en','Stay_label2').Yii::t('en','err_label1'));
		$dummy = $cVal->Strcheck($model->floorName,&$this->errflag);
		if($dummy==1) $this->addError('floorName',Yii::t('en','Stay_label3').Yii::t('en','err_label1'));
		$dummy = $cVal->Strcheck($model->cottageNo,&$this->errflag);
		if($dummy==1) $this->addError('cottageNo',Yii::t('en','Stay_label4').Yii::t('en','err_label1'));
		$dummy = $cVal->Strcheck($model->cottageAllot,&$this->errflag);
		if($dummy==1) $this->addError('cottageAllot',Yii::t('en','Stay_label6').Yii::t('en','err_label1'));	
		
		if($model->cottageAllot == 2 or $model->cottageAllot == 3 )
		{
		$dummy = $cVal->Strcheck($model->cottageMember,&$this->errflag);
		if($dummy==1) $this->addError('cottageMember',Yii::t('en','Stay_label7').Yii::t('en','err_label1'));		
		}
		
		if($model->cottageMember==2 or $model->cottageMember==3)
		{
		$dummy = $cVal->Strcheck($model->memberName,&$this->errflag);
		if($dummy==1) $this->addError('memberName',Yii::t('en','Stay_label8').Yii::t('en','err_label1'));		
		}
		
		}			
			
		if($model->stayPlace==1)
	   {    
	    $dummy = $cVal->ZeroCheck($model->stayProvide,&$this->errflag);
		if($dummy==1) $this->addError('stayProvide',Yii::t('en','Stay_label9').Yii::t('en','err_label1'));		
		$dummy = $cVal->Strcheck($model->stayAddress,&$this->errflag);
		if($dummy==1) $this->addError('stayAddress',Yii::t('en','Stay_label10').Yii::t('en','err_label1'));		
	   }
    }
}

?>