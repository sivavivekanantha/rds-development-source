<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{

	public	$username;
	public	$Password;

	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
		
			array('username', 'required'),
			array('Password', 'required'),
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'type'=>'User Name'
		
		);
	}

}

class LoginModel extends BaseModel
{
	public function LoginAuthendication($model)
	{	
    		$query = $this->createCommand("sp_UserAuthenticate_yii");
			mssql_bind($query, '@username'	    	, $model->username , 				SQLVARCHAR ,    	 false, 	false, strlen($model->username));
			mssql_bind($query, '@ipassword'	    	, $model->Password , 				SQLVARCHAR ,    	 false, 	false, strlen($model->Password));
			mssql_bind($query, '@ip'	    		, $ip , 							SQLVARCHAR ,    	 false, 	false, strlen($ip));	
			$result = mssql_execute($query);
			if($result==1)	
				$this->msg=mssql_get_last_message();
			else $this->msg="";
			return $result;
	}
}
?>