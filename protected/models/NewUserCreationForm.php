<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class NewUserCreationForm extends CFormModel
{
	public	$uId;
	public	$fullName;
	public	$userName;
	public	$passWord;
	public	$confirmPassword;
	public	$emailId;
	public  $department;
	public  $referredFromDept;
	public  $referredFromVcd;
	public  $errflag;
	public  $errmsg;
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
			
		
			array('fullName','required'),	
			array('userName','required'),	
			array('passWord','required'),	
			array('confirmPassword','required'),
			array('emailId','required'),	
			array('department','safe'),	
			array('referredFromDept','safe'),		
			array('referredFromVcd','safe'),		
			
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'fullName'=>Yii::t('en','NewUserCreation_label1'),
			'userName'=>Yii::t('en','NewUserCreation_label2'),
			'passWord'=>Yii::t('en','NewUserCreation_label3'),
			'confirmPassword'=>Yii::t('en','NewUserCreation_label4'),
			'emailId'=>Yii::t('en','NewUserCreation_label5'),
			'department'=>Yii::t('en','NewUserCreation_label6'),
			'referredFromDept'=>Yii::t('en','NewUserCreation_label7'),
			'referredFromVcd'=>Yii::t('en','NewUserCreation_label8'),
			'Title'=>Yii::t('en','Header_label54'),
		);
	}
	
	public function NewUserValidate($model)
	{
			$cVal = new CommonValidator();
			$dummy = $cVal->Strcheck($model->fullName,&$this->errflag);
			if($dummy==1) $this->addError('fullName',Yii::t('en','NewUserCreation_label1').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model->userName,&$this->errflag);
			if($dummy==1) $this->addError('userName',Yii::t('en','NewUserCreation_label2').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model->passWord,&$this->errflag);
			if($dummy==1) $this->addError('passWord',Yii::t('en','NewUserCreation_label3').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model->confirmPassword,&$this->errflag);
			if($dummy==1) $this->addError('confirmPassword',Yii::t('en','NewUserCreation_label4').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model->emailId,&$this->errflag);
			if($dummy==1) $this->addError('emailId',Yii::t('en','NewUserCreation_label5').Yii::t('en','err_label1'));
			
			
			if(strlen($model->emailId)>0)$dummy = $cVal->Emailcheck($model->emailId,&$this->errflag);
			if($dummy==1) $this->addError('emailId','Invalid Email ID Format');
			
			
   }
	

	

}
?>