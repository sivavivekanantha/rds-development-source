<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class PersonalEmailForm extends CFormModel
{
	//public	$ivcCode;
	public	$emailId;
	public	$isahangaMail;
	public	$reciveMail;
	public	$receiveIshangaMail;
	public  $errflag;
	public  $errmsg;
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
			
			//array('ivcCode','safe'),	
			array('emailId','required'),	
			array('isahangaMail','safe'),	
			array('reciveMail','safe'),	
			array('receiveIshangaMail','safe'),	
			
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'emailId'=>Yii::t('en','PersonalMail_label1'),
			'isahangaMail'=>Yii::t('en','PersonalMail_label2'),
			'reciveMail'=>Yii::t('en','PersonalMail_label3'),
			'receiveIshangaMail'=>Yii::t('en','PersonalMail_label4'),
			
			'email'=>Yii::t('en','Header_label34'),
			
			
		
		);
	}
	
	public function PersonalEmailValidate($model)
	{
		
		    $cVal = new CommonValidator();
							 
		    $dummy = $cVal->Strcheck($model->emailId,&$this->errflag);
			if($dummy==1) $this->addError('emailId',Yii::t('en','PersonalMail_label1').Yii::t('en','err_label1'));
			
			if(strlen($model->emailId)>0)
			{
			$dummy = $cVal->Emailcheck($model->emailId,&$this->errflag);
			if($dummy==1) $this->addError('emailId','Invalid Isha Entity Mail ID Format');	
				}
				
			if(strlen($model->isahangaMail)>0)
			{
				$dummy = $cVal->Emailcheck($model->isahangaMail,&$this->errflag);
			if($dummy==1) $this->addError('isahangaMail','Invalid Other Email ID Format');
			}
			
			if(strlen($model->receiveIshangaMail)>0)
			{
			$dummy = $cVal->Emailcheck($model->receiveIshangaMail,&$this->errflag);
			if($dummy==1) $this->addError('receiveIshangaMail','Invalid Email ID Format');
			}
			if($model->reciveMail=='2')
    		{
    			$dummy = $cVal->Strcheck($model->receiveIshangaMail,$this->errflag);
    			if($dummy==1) $this->addError('receiveIshangaMail',Yii::t('en','Email ID').Yii::t('en','err_label1')); 
    		}
	}

	

}
?>