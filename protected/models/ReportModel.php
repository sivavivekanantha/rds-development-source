<?php 
class ReportModel extends BaseModel
{

//Start-----Stay----------------
	
	 public function getStay()
     {   
		$query = $this->createCommand('sp_Fetchstay');      
    	$result = @mssql_execute($query); 
		      
        return $result;
     }
	  public function getConsolidateReport()
     {   
		$query = $this->createCommand('sp_rdsqry');      
    	$result = mssql_execute($query); 
		    
        return $result;
     }
	  public function uploadRfidToDb($uid,$rfid,$status)
     {   
		$query = $this->createCommand('sp_Upload_Rfid');
	    mssql_bind($query, '@RFID_No', $rfid,SQLVARCHAR, 	false, 	false, 	strlen($rfid));
		mssql_bind($query, '@RF_UserId',$uid, 	SQLVARCHAR, false, false, strlen($uid));
		mssql_bind($query, '@Status',	$status, 	SQLVARCHAR, false, false, strlen($status));
    	$result = @mssql_execute($query); 
		$this->msg = mssql_get_last_message();

        return $result;
     }
	 
	  public function getResidenceReport()
     {   
		$query = $this->createCommand('SP_res');      
    	$result = mssql_execute($query); 
		    
        return $result;
     }
	 
	   public function getIndividualExcelReport($report)
     {   
		$query = $this->createCommand('SP_IVC_ExcelDetails');    
		mssql_bind($query, '@IVC_Details',$report, 	SQLINT4, 	false, 	false, 	5);		
    	$result = mssql_execute($query); 
        return $result;
     }
	 
	public function Summaryreport($fromdate,$todate,$location,$group,$department,$Name,$Uid,$frmtime,$totime,$Rid)
 	{
  		$fromdate= $fromdate.' '.$frmtime;
  		$todate=$todate.' '.$totime;
		$query = $this->createCommand('sp_detailReport2');   
	  	mssql_bind($query, '@fromdate',$fromdate, SQLVARCHAR, 	false, 	false, 	500);	 
	  	mssql_bind($query, '@todate',$todate, SQLVARCHAR, 	false, 	false, 	500);	
	  	mssql_bind($query, '@location',$location, SQLINT4, 	false, 	false, 	5);	
	  	mssql_bind($query, '@group',$group, SQLINT4, 	false, 	false, 	5);
	  	mssql_bind($query, '@department',$department, SQLINT4, 	false, 	false, 	5);	
	  	mssql_bind($query, '@Name',$Name, SQLVARCHAR, 	false, 	false, 	500);
	  	mssql_bind($query, '@Uid',$Uid, SQLVARCHAR, 	false, 	false, 	500);
		mssql_bind($query, '@Rcode',$Rid, SQLINT4, 	false, 	false, 	5);
		$result = mssql_execute($query); 
	    return $result;

	}
	
	  public function getReportDetails()
     {   
		$query = $this->createCommand('sp_RDS_Report');      
    	$result = mssql_execute($query); 
		    
        return $result;
     }

	

public function DepartmentGrouplist()
     {   
		$query = $this->createCommand('sp_groupList');   
		
    	$result = mssql_execute($query); 
        return $result;
}
}
?>