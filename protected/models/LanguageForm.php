<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class LanguageForm extends CFormModel
{
	public	$ivcLanguageCode;
	public	$languageCode;	
	public	$languageName;
	public	$speaking;
	public	$reading;
	public	$writing;
	public	$typing;
	public	$motherTongue;
	public  $Omother;
	public  $Olanguage;
	public  $action;
	public  $errflag;
	public  $errmsg;
	public  $molanguage;
	public  $sno;
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
			
			array('ivcLanguageCode','safe'),		
			array('languageCode','safe'),
			array('languageName','required'),		
			array('speaking','safe'),
			array('reading','safe'),
			array('writing','safe'),
			array('typing','safe'),
			array('motherTongue','safe'),
			array('Olanguage','safe'),
			array('action','safe'),
			array('molanguage','safe'),
			array('sno','safe'),
			array('errflag','safe'),
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'languageName'=>Yii::t('en','Language_label1'),
			'speaking'=>Yii::t('en','Language_label2'),
			'reading'=>Yii::t('en','Language_label3'),
			'writing'=>Yii::t('en','Language_label4'),
			'typing'=>Yii::t('en','Language_label5'),
			'Olanguage'=>Yii::t('en','Language_label6'),
			'motherTongue'=>Yii::t('en','Language_label7'),
			'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'language'=>Yii::t('en','Header_label7'),
			
		
		);
	}
	public function languageValidate($model)
	{	
		$dummy='';	
		$cVal = new CommonValidator();
			
			$dummy = $cVal->Strcheck($model->languageName,$this->errflag);
		if($dummy==1) $this->addError('languageName',Yii::t('en','Language_label1').Yii::t('en','err_label1'));
		
		if($model->languageName=='8888')
		{
	    $dummy = $cVal->Strcheck($model->Olanguage,$this->errflag);
		if($dummy==1) $this->addError('Olanguage',Yii::t('en','Language_label6').Yii::t('en','err_label1'));	
		}		
	}
	

}
?>