<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ChangePasswordForm extends CFormModel
{
	
	public	$userid;
	public	$currentPassword;
	public	$newPassword;
	public	$confirmPassword;
		
	public  $errflag;
	public  $errmsg;
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
			
			
			array('currentPassword','required'),	
			array('newPassword','required'),	
			array('confirmPassword','required'),	
			
			array('confirmPassword', 'compare', 'compareAttribute'=>'newPassword','message'=>'New & Confirm password not match'),
			
			
		
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			
			'currentPassword'=>Yii::t('en','Forgot_label1'),
			'newPassword'=>Yii::t('en','Forgot_label2'),
			'confirmPassword'=>Yii::t('en','Forgot_label3'),
			
			'password'=>Yii::t('en','Header_label52'),
			
		);
	}
	
	public function NewUserValidate($model)
	{
			$cVal = new CommonValidator();
			$dummy = $cVal->Strcheck($model->currentPassword,&$this->errflag);
			if($dummy==1) $this->addError('currentPassword','Current Password cannot be blank.');
			$dummy = $cVal->Strcheck($model->newPassword,&$this->errflag);
			if($dummy==1) $this->addError('newPassword','New Password cannot be blank.');
			if($model->currentPassword == $model->newPassword){
				$this->addError('newPassword','Choose a password that you haven&#39;t previously used with this account.'); 
				$this->errflag=1;	
			}
			$dummy = $cVal->Strcheck($model->confirmPassword,&$this->errflag);
			if($dummy==1) $this->addError('confirmPassword','Confirm Password cannot be blank.');
			
   }
	

	

}
?>