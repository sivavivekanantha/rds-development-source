<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class PastDepartmentForm_New extends CFormModel
{
	public	$ashramOfficialCode;
	public	$aDepartmentCode;	
	public	$departmentName;	
	public	$aOccupation;	
	public	$fromdate;
	public	$todate;	
	public	$wasReporting;
	public	$aRemarks;
	public  $action;
	public  $formValid;
    public  $errflag;
	public  $errmsg;
	
	
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// Deptname, occupation, date are required
			
			array('ashramOfficialCode','safe'),		
			array('aDepartmentCode','safe'),
			array('departmentName','required'),		
			array('aOccupation','safe'),				
			array('fromdate','safe'),			
			array('todate','safe'),
			array('aActive','safe'),
			array('wasReporting','safe'),
			array('aRemarks','safe'),
			array('action','safe'),	
			array('formValid','safe'),
			array('errflag','safe'),		
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'departmentName'=>Yii::t('en','CurrentDepartment_label1'),
			'aOccupation'=>Yii::t('en','CurrentDepartment_label2'),
			'fromdate'=>Yii::t('en','CurrentDepartment_label4'),
			'todate'=>Yii::t('en','PastDepartment_label1'),
			'wasReporting'=>Yii::t('en','PastDepartment_label2'),
			'aRemarks'=>Yii::t('en','PastDepartment_label3'),
			'sno'=>Yii::t('en','Common_label1'),
		    'action'=>Yii::t('en','Common_label2'),
			
			'departmentHistory'=>Yii::t('en','Header_label17'),
			'departmentHint'=>Yii::t('en','Header_label18'),

			
		);
	}
	public function pastdepartmentValidate($model)
	{
		$cVal = new CommonValidator();
			if( strlen($model->fromdate)==10 and strlen($model->todate)==10 )
			{
				$dummy = $cVal->DateCheck($model->fromdate,$model->todate,&$this->errflag);
				if($dummy==1) $this->addError('fromdate','From Date must less than To Date');
			}
			
		$dummy = $cVal->Strcheck($model->departmentName,$this->errflag);
		if($dummy==1) $this->addError('departmentName',Yii::t('en','CurrentDepartment_label1').Yii::t('en','err_label1'));
		
		if($model->departmentName<>73)
		{
			$dummy = $cVal->Strcheck($model->aOccupation,$this->errflag);
			if($dummy==1) $this->addError('aOccupation',Yii::t('en','CurrentDepartment_label2').Yii::t('en','err_label1'));	
				
		}
		
	}

} ?>