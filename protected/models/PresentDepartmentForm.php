	<?php class PresentDepartmentForm extends CFormModel
{
	public	$ashramOfficialCode1;
	public	$aDepartmentCode1;	
	public	$departmentName1;	
	public	$aOccupation1;	
	public	$fromDate1;	
	public	$reporting1;
	public  $action1;
	public  $formValid;
	public  $errflag;
	public  $errmsg;
	public  $activeDepartment;
	public  $group;
	
	
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// Deptname, occupation, date are required
			
			array('ashramOfficialCode1','safe'),		
			array('aDepartmentCode1','safe'),
			array('departmentName1','required'),		
			array('aOccupation1','safe'),				
			array('fromDate1','safe'),				
			array('reporting1','safe'),		
			array('action1','safe'),
			array('formValid','safe'),		
			array('activeDepartment','safe'),						
			array('group','safe'),	
			array('errflag','safe'),	
										
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'departmentName1'=>Yii::t('en','CurrentDepartment_label1'),
			'aOccupation1'=>Yii::t('en','CurrentDepartment_label2'),
			'fromDate1'=>Yii::t('en','CurrentDepartment_label4'),
			'reporting1'=>Yii::t('en','CurrentDepartment_label5'),
			'formValid'=>Yii::t('en','CurrentDepartment_label6'),
			'activeDepartment'=>Yii::t('en','CurrentDepartment_label7'),
			'group'=>Yii::t('en','CurrentDepartment_label3'),
			'sno'=>Yii::t('en','Common_label1'),
		    'action'=>Yii::t('en','Common_label2'),
			'department'=>Yii::t('en','Header_label15'),
			'departmentPopup'=>Yii::t('en','Header_label16'),
  
		);
	}
	

	public function presentdepartmentValidate($model1)
	{
		$cVal = new CommonValidator();
		$dummy = $cVal->Strcheck($model1->departmentName1,$this->errflag);
		if($dummy==1) $this->addError('departmentName1',Yii::t('en','CurrentDepartment_label1').Yii::t('en','err_label1'));
		
		if($model1->departmentName1<>73)
		{
			$dummy = $cVal->Strcheck($model1->aOccupation1,$this->errflag);
			if($dummy==1) $this->addError('aOccupation1',Yii::t('en','CurrentDepartment_label2').Yii::t('en','err_label1'));	
				
		}
	}
} ?>