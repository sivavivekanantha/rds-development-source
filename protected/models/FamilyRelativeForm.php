<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class FamilyRelativeForm extends CFormModel
{
			public $familyCode1;         
			public $memberName1;               
			public $relation1;         
			public $gender1;          
			public $famContactAddress1;    
			public $famPhonecode1;           
			public $famContact1;            
			public $famEmailid1;           
			public $famDepartment1;            
			public $famAssociation1;
			public $familyResponsibility1;   

            public $fCode;           
			public $memName;                
			public $rel;              
			public $age;              
			public $gen;               
			public $famOccup;              
			public $oAddress;              
			public $ContactAddress;
			public $famcode;             
			public $famCon;            
			public $famEmail;        
			public $famDep;              
			public $famAsso;       			
			public $action1;
			public $errmsg;    
			public $errflag; 
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// Deptname, occupation, date are required
			
			array('familyCode1','safe'),		
			array('memberName1','required'),
			array('relation1','required'),		
			array('gender1','safe'),				
			array('famContactAddress1','safe'),			
			array('famPhonecode1','safe'),
			array('famContact1','safe'),
				array('famContact1', 'length', 'min'=>10, 'max'=>10),
			array('famEmailid1','safe'),
			array('famDepartment1','safe'),
			array('famAssociation1','required'),		
			array('action1','safe'),			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'memberName1'=>Yii::t('en','Family_label1'),
			'relation1'=>Yii::t('en','Family_label2'),
			'gender1'=>Yii::t('en','Family_label3'),
			'famContactAddress1'=>Yii::t('en','Family_label7'),
			'famPhonecode1'=>Yii::t('en','Family_label14'),
			'famContact1'=>Yii::t('en','Family_label8'),
			'famEmailid1'=>Yii::t('en','Family_label9'),
			'famDepartment1'=>Yii::t('en','Family_label11'),
			'famAssociation1'=>Yii::t('en','Family_label12'),
			'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'familyRelative'=>Yii::t('en','Header_label26'),
			'familyRelativeHint'=>Yii::t('en','Header_label27'),
		);
	}
	
	public function familyRelativeValidate($model1)
	{
			$cVal = new CommonValidator();
					
			$dummy = $cVal->Strcheck($model1->memberName1,&$this->errflag);
			if($dummy==1) $this->addError('memberName1',Yii::t('en','Family_label1').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model1->relation1,&$this->errflag);
			if($dummy==1) $this->addError('relation1',Yii::t('en','Family_label2').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model1->famAssociation1,&$this->errflag);
			if($dummy==1) $this->addError('famAssociation1',Yii::t('en','Family_label12').Yii::t('en','err_label1'));
			
			if(strlen($model1->famEmailid1)>0)$dummy = $cVal->Emailcheck($model1->famEmailid1,&$this->errflag);
			if($dummy==1) $this->addError('famEmailid1','Invalid Email ID Format');
				
	}
}



?>