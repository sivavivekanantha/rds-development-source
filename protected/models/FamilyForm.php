<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class FamilyForm extends CFormModel
{    

			public $familyCode;           
			public $memberName;                
			public $relation;              
			public $famAge;              
			public $gender;               
			public $famOccupation;              
			public $occupationAddress;              
			public $famContactAddress;
			public $famPhonecode;             
			public $famContact;            
			public $famEmailid;        
			public $famDepartment;              
			public $famAssociation;  
			public $errmsg;    
			public $errflag; 

            public $fCode1;           
			public $memName1;                
			public $relat1;              
			public $gen1;               
			public $famCont1;
			public $famPhone1;             
			public $famCon1;            
			public $famEmail1;        
			public $famDepart1;              
			public $famAssoci1;  
   			
			public $familyResponsibility;
			public $action;
	
	
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// Deptname, occupation, date are required
			
			array('familyCode','safe'),		
			array('memberName','required'),
			array('relation','required'),		
			array('famAge','required'),	
			array('gender','safe'),			
			array('famOccupation','safe'),
			array('occupationAddress','safe'),
			array('famContactAddress','safe'),
			array('famPhonecode','safe'),
			
			array('famContact','safe'),
			array('famContact', 'length', 'min'=>10, 'max'=>10),
			array('famEmailid','safe'),
			array('famDepartment','safe'),
			array('famAssociation','safe'),
			array('familyResponsibility','safe'),			
			array('action','safe'),			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'memberName'=>Yii::t('en','Family_label1'),
			'relation'=>Yii::t('en','Family_label2'),
			'gender'=>Yii::t('en','Family_label3'),
			'famAge'=>Yii::t('en','Family_label4'),
			'famOccupation'=>Yii::t('en','Family_label5'),
			'occupationAddress'=>Yii::t('en','Family_label6'),
			'famContactAddress'=>Yii::t('en','Family_label7'),
			'famContact'=>Yii::t('en','Family_label8'),
			'famEmailid'=>Yii::t('en','Family_label9'),
			'famAssociation'=>Yii::t('en','Family_label10'),
			'famDepartment'=>Yii::t('en','Family_label11'),
			'famPhonecode'=>Yii::t('en','Family_label14'),						
			'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'family'=>Yii::t('en','Header_label24'),
			'familyHint'=>Yii::t('en','Header_label25'),
			'familyDetails'=>Yii::t('en','Header_label60'),
			
			
		);
	}
	public function familyValidate($model)
	{
		    $cVal = new CommonValidator();
			
			$dummy = $cVal->Strcheck($model->memberName,&$this->errflag);
			if($dummy==1) $this->addError('memberName',Yii::t('en','Family_label1').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model->relation,&$this->errflag);
			if($dummy==1) $this->addError('relation',Yii::t('en','Family_label2').Yii::t('en','err_label1'));
			
			if(strlen($model->famEmailid)>0)$dummy = $cVal->Emailcheck($model->famEmailid,&$this->errflag);
			if($dummy==1) $this->addError('famEmailid','Invalid Email ID Format');

	}

}



?>