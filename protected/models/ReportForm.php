<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ReportForm extends CFormModel
{
	public	$name;
	public	$userName;	
	public	$photo;
	public	$competion;
	public	$freeze;
	public  $personal;
	public  $passport;
	public  $acadamic;
	public  $language;
	public  $skills;
	public  $work;
	public  $program;
	public  $teacher;
	public  $yatra;
	public  $department;
	public  $stay;
	public  $medical;
	public  $emergengy;
	public  $family;
	public  $enclosure ;
	public  $criminal; 
	 
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
			
			array('familyPgmCode','safe'),		
			array('name','safe'),
			array('ishaYoga','safe'),		
			array('bsp','safe'),
			array('hataYoga','safe'),
			array('samayama','safe'),
			
			array('action','safe'),
			array('formValid','safe'),
			
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'name'=>Yii::t('en','FamilyProgram_label1'),
			'ishaYoga'=>Yii::t('en','FamilyProgram_label2'),
				'bsp'=>Yii::t('en','FamilyProgram_label3'),
					'hataYoga'=>Yii::t('en','FamilyProgram_label4'),
					'samayama'=>Yii::t('en','FamilyProgram_label5'),
					'formValid'=>Yii::t('en','FamilyProgram_label6'),
		
		);
	}
	
		

}
?>