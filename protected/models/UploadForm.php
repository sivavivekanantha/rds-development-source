<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class UploadForm extends CFormModel
{

	public	$doc_file;
	public  $errflag;
	public  $errmsg;
	//public  $msg;
		
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
			array('doc_file','required'),					
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'doc_file'=>'Upload File:',	
		);
	}
	

}
?>