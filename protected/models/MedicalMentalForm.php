<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class MedicalMentalForm extends CFormModel
{       
       
		public $medicalMentalCode;
		public $mentalAllignment;
		public $mentalMedication;
		public $hospitalHistory;
		public $mentalFrequency;
		public $mentalStatus;
		public $mental;
		public  $formValid;
	    public  $formValid1;
	    public  $action1;	
	
	    public $errmsg;    
	    public $errflag; 
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
			
			array('medicalMentalCode','safe'),		
			array('mentalAllignment','required'),
			array('mentalMedication','safe'),		
			array('hospitalHistory','safe'),
			array('mentalFrequency','safe'),
			array('mentalStatus','safe'),	
			array('mental','safe'),			
			array('action1','safe'),
			array('formValid','safe'),
			array('formValid1','safe'),
			
			array('errflag','safe'),
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'mentalAllignment'=>Yii::t('en','Medical_label8'),
			'mentalMedication'=>Yii::t('en','Medical_label9'),
			'hospitalHistory'=>Yii::t('en','Medical_label10'),
			'mentalFrequency'=>Yii::t('en','Medical_label11'),
			'mentalStatus'=>Yii::t('en','Medical_label12'),
			'mental'=>Yii::t('en','Medical_label13'),
			'formValid1'=>Yii::t('en','Medical_label7'),	
			'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'Mental'=>Yii::t('en','Header_label22'),
		
		);
	}
	
	public function medicalMendalValidate($model1)
	{
		$cVal = new CommonValidator();
			
		$dummy = $cVal->Strcheck($model1->mentalAllignment,&$this->errflag);
			if($dummy==1) $this->addError('mentalAllignment',Yii::t('en','Medical_label8').Yii::t('en','err_label1'));
		
	}
	

}
?>