<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ReferenceResidentForm extends CFormModel
{    

			
			public $refernceCode;           
			public $name;                
			public $designation;              
			public $relationship;              
			public $contactNo;               
			public $emailId;              
			public $action;
			
			
			public $referenceIshaCode;           
			public $rName;                
			public $centerName;              
			public $phoneNo;              
			public $rEmailId;               
			public $action1;
			
			public $referenceResidentCode;           
			public $rRname;                
			public $department;              
			public $cNo;              
			public $action2;
			
			public  $errflag;
	                public  $errmsg;
			
			

	
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// Deptname, occupation, date are required
			
			array('referenceIshaCode','safe'),		
			array('rName','safe'),
			array('centerName','safe'),		
			array('phoneNo','safe'),				
			array('rEmailId','safe'),			
			array('action1','safe'),	
			
			array('refernceCode','safe'),		
			array('name','safe'),
			array('designation','safe'),		
			array('relationship','safe'),				
			array('contactNo','safe'),			
			array('emailId','safe'),
			array('action','safe'),	
			
			array('referenceResidentCode','safe'),		
			array('rRname','required'),
			array('department','required'),		
			array('cNo','safe'),				
			array('action2','safe'),
			
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'rRname'=>Yii::t('en','Reference_label1'),
			'department'=>Yii::t('en','Family_label11'),
			'cNo'=>Yii::t('en','Reference_label4'),		
            'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'referenceD'=>Yii::t('en','Header_label48'),
			'referenceG'=>Yii::t('en','Header_label51'),
		);
	}
	public function referenceResidentEmailValidate($model2)
	{
			$cVal = new CommonValidator();
			
			$dummy = $cVal->Strcheck($model2->rRname,&$this->errflag);
			if($dummy==1) $this->addError('rRname',Yii::t('en','Reference_label1').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model2->department,&$this->errflag);
			if($dummy==1) $this->addError('department',Yii::t('en','Family_label11').Yii::t('en','err_label1'));
						
		
		
				
	}

}



?>