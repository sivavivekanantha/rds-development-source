<?php 
class Menu extends BaseModel
{

	public function showMenu($headercode)
	{
		
		$sql = "sp_showmodule";	
        $query = $this->createCommand($sql);
		mssql_bind($query, '@headerCode', $headercode,SQLINT4, 	false, 	false, 	5); 	
		$result = @mssql_execute($query);	
		return $result;
				

	}
	
	public function showGroup($headercode,$oheadercode)
	{
	    $sql = "sp_showgroup";
	    $query = $this->createCommand($sql);
		mssql_bind($query, '@headerCode', $headercode,SQLINT4, 	false, 	false, 	5); 
		mssql_bind($query, '@oheaderCode', $oheadercode,SQLINT4, 	false, 	false, 	5); 
	    $result = @mssql_execute($query);
	    return $result;
	}
	
	
}?>