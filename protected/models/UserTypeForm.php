<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class UserTypeForm extends CFormModel
{
	public	$utype;
	public	$searchtext;
	public  $ulevel;	
	public  $viewmenucode;	
	public  $editmenucode;
	public  $accessmenucode;	
	public  $headercode;
	public  $levelcheck;
	public  $usertype;
	public  $errflag;
	public  $errmsg;
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array(
					
			array('utype','safe'),
			array('searchtext','safe'),
			array('ulevel','safe'),
			array('viewmenucode','safe'),
			array('editmenucode','safe'),
			array('headercode','safe'),
			array('levelcheck','safe'),			
			array('usertype','safe'),
			array('accessmenucode','safe'),
				  );
			
	}
	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
						
			'Assignusertype'=>Yii::t('en','Header_label55'),
			'utype'=>Yii::t('en','Usertype_label1'),
			'as'=>Yii::t('en','Usertype_label2'),
			'users'=>Yii::t('en','Usertype_label3'),
			'viewmenus'=>Yii::t('en','Usertype_label4'),
			'editmenus'=>Yii::t('en','Usertype_label5'),
			'userlevel'=>Yii::t('en','Usertype_label6'),
			'accessmenus'=>Yii::t('en','Usertype_label7'),
			'accesspermission'=>Yii::t('en','Header_label62'),
		
		);
	}
	
	

	

}
?>