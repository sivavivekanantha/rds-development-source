<?php class DepartmentForm extends CFormModel
{
	public	$ashramOfficialCode1;
	public	$aDepartmentCode1;	
	public	$departmentName1;	
	public	$aOccupation1;	
	public	$fromDate1;	
	public	$reporting1;
	public  $action1;
	public  $formValid;
	public  $errflag;
	public  $errmsg;
	
	
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// Deptname, occupation, date are required
			
			array('ashramOfficialCode1','safe'),		
			array('aDepartmentCode1','safe'),
			array('departmentName1','required'),		
			array('aOccupation1','required'),				
			array('fromDate1','safe'),				
			array('reporting1','safe'),		
			array('action1','safe'),
			array('formValid','safe'),				
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'departmentName1'=>'Department',			
			'aOccupation1'=>'Role In Department',			
			'fromDate1'=>'From Date',			
			'reporting1'=>'Reporting To',			
			'formValid'=>'Are you currently part of any department in Isha ?'			
		);
	}
	

public function departmentValidate($model1)
	{
		$cVal = new CommonValidator();
			
		    if(strlen($model1->fromDate1)>0)
		        $dummy = $cVal->dateformatchk($model1->fromDate1,&$this->errmsg,&$this->errflag,"From Date");
			if(strlen($model1->fromDate1)==10)
				$dummy = $cVal->DOBCheck($model1->fromDate1,&$this->errmsg,&$this->errflag,"Work From Date must less than current Date"); 	
				if($model1->departmentName1<>1000008)	
		$dummy = $cVal->Strcheck($model1->aOccupation1,&$this->errmsg,&$this->errflag,"Role In Department");
				
				}
} ?>
