<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class TeacherTrainingForm extends CFormModel
{
	public	$ivcTrainingCode;
	public	$program;	
	public	$trainingLanguage;
	public	$trainingCompleted;
	public	$handledSession;
	public  $action;
	public  $formValid; 
	public  $Others;
	public  $errflag;
	public  $errmsg;
	public  $sno;
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
			
			array('ivcTrainingCode','safe'),		
			array('program','required'),
			array('trainingLanguage','required'),		
			array('trainingCompleted','required'),
			array('handledSession','safe'),
			array('action','safe'),
			array('formValid','safe'),
			array('Others','safe'),
			array('sno','safe'),
			array('errflag','safe'),
			
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(

		'program'=>Yii::t('en','Teacher_label1'),
		'trainingLanguage'=>Yii::t('en','Teacher_label2'),
		'trainingCompleted'=>Yii::t('en','Teacher_label3'),
		'handledSession'=>Yii::t('en','Teacher_label4'),
		'formValid'=>Yii::t('en','Teacher_label5'),
		'Others'=>Yii::t('en','Program_label6'),
		'sno'=>Yii::t('en','Common_label1'),
		'action'=>Yii::t('en','Common_label2'),
		
		'teacher'=>Yii::t('en','Header_label13'),
		'teacherTraining'=>Yii::t('en','Header_label29'),

		);
	}
	public function teacherTrainingValidate($model)
	{		
		$cVal = new CommonValidator();
	
			$dummy = $cVal->Strcheck($model->program,$this->errflag);
		if($dummy==1) $this->addError('program',Yii::t('en','Teacher_label1').Yii::t('en','err_label1'));
		$dummy = $cVal->Strcheck($model->trainingLanguage,$this->errflag);
		if($dummy==1) $this->addError('trainingLanguage',Yii::t('en','Teacher_label2').Yii::t('en','err_label1'));
	
		
			if($model->trainingCompleted=='Y')	
			{
					$dummy = $cVal->Strcheck($model->handledSession,$this->errflag);
		if($dummy==1) $this->addError('handledSession',Yii::t('en','Teacher_label4').Yii::t('en','err_label1'));
			}
			
		
		if($model->program=='8888')
		{
		$dummy = $cVal->Strcheck($model->Others,$this->errflag);
		if($dummy==1) $this->addError('Others',Yii::t('en','Program_label6').Yii::t('en','err_label1'));
		}
			
	
		
	}

}
?>