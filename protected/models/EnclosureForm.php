<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class EnclosureForm extends CFormModel
{
	public	$ivcEnclosureCode;
	public	$enclosureCode;	
	public	$enClosureName;
	public	$iycAddress;
	public	$Others;
	public	$documents;
	public	$cardNo;
	public	$cardExpDate;
	public  $action;
	public  $errflag;
	public  $errmsg;
	public  $formValid;
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, cardno, date are required
			
			array('ivcEnclosureCode','safe'),		
			array('enclosureCode','safe'),
			array('enClosureName','required'),
			array('iycAddress','safe'),		
			array('cardNo','required'),
			array('cardExpDate','safe'),
			array('Others','safe'),
			array('documents','safe'),
			array('action','safe'),
			array('formValid','safe'),
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'enClosureName'=>Yii::t('en','Enclosure_label1'),
			'iycAddress'=>Yii::t('en','Enclosure_label2'),
			'cardNo'=>Yii::t('en','Enclosure_label3'),
			'documents'=>Yii::t('en','Enclosure_label5'),
			'cardExpDate'=>Yii::t('en','Enclosure_label4'),
			'formValid'=>Yii::t('en','Enclosure_label6'),
			'Others'=>Yii::t('en','Enclosure_label7'),			
			'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'enclosure'=>Yii::t('en','Header_label31'),
			'enclosurePopup'=>Yii::t('en','Header_label32'),
			
			
		
		);
	}
	
	public function enclosureValidate($model)
	{		
		$cVal = new CommonValidator();
		
		    $dummy = $cVal->Strcheck($model->enClosureName,&$this->errflag);
			if($dummy==1) $this->addError('enClosureName',Yii::t('en','Enclosure_label1').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model->cardNo,&$this->errflag);
			if($dummy==1) $this->addError('cardNo',Yii::t('en','Enclosure_label3').Yii::t('en','err_label1'));
			
				if($model->enClosureName=='1')
				{
				$dummy = $cVal->Strcheck($model->Others,$this->errflag);
				if($dummy==1) $this->addError('Others',Yii::t('en','Enclosure_label7').Yii::t('en','err_label1')); 
				}
			
			
			
				
	}
	

}
?>