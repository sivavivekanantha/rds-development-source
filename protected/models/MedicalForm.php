<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class MedicalForm extends CFormModel
{       
        public $medicalPhysicalCode;		
		public $bloodName;
		public $physicalAllignment;
		public $physicalFrequency;
		public $physicalStatus;
		public $physical;
		public  $formValid;
	    public  $formValid1;	   
	    public  $action;	
	    public $errmsg;    
		public $errflag; 
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
			
			array('medicalPhysicalCode','safe'),		
			array('bloodName','required'),
			array('physicalAllignment','required'),		
			array('physicalFrequency','safe'),
			array('physicalStatus','safe'),
			array('physical','safe'),			
			array('action','safe'),
			array('formValid','safe'),
			array('formValid1','safe'),
			array('errflag','safe'),
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'bloodName'=>Yii::t('en','Medical_label1'),
			'physicalAllignment'=>Yii::t('en','Medical_label2'),
			'physicalFrequency'=>Yii::t('en','Medical_label3'),
			'physicalStatus'=>Yii::t('en','Medical_label4'),
			'physical'=>Yii::t('en','Medical_label5'),
			'formValid'=>Yii::t('en','Medical_label6'),
			'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'Medical'=>Yii::t('en','Header_label20'),
			'MedicalPopup'=>Yii::t('en','Header_label21'),
		
		);
	}
	
	
	public function medicalValidate($model)
	{
		$cVal = new CommonValidator();
				
		$dummy = $cVal->Strcheck($model->physicalAllignment,&$this->errflag);
		if($dummy==1) $this->addError('physicalAllignment',Yii::t('en','Medical_label2').Yii::t('en','err_label1'));
		
	}
	
	
	
	

}
?>