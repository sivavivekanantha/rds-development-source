<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class CriminalForm extends CFormModel
{
	public	$isCriminalConvicted;
	public	$criminalConvictedDetails;	
	public	$isCriminalProceeding;
	public	$criminalProceedingDetails;
	public  $errflag;
	public  $errmsg;

	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
			
			array('isCriminalConvicted','safe'),		
			array('criminalConvictedDetails','safe'),
			array('isCriminalProceeding','safe'),		
			array('criminalProceedingDetails','safe'),
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'isCriminalConvicted'=>Yii::t('en','Criminal_label1'),
			'isCriminalProceeding'=>Yii::t('en','Criminal_label2'),
			
			'criminal'=>Yii::t('en','Header_label33'),
			
			
		
		);
	}
	
	
	public function criminalValidate($model)
	{
		$cVal = new CommonValidator();
		
		if($model->isCriminalConvicted=='Y')
		{
		$dummy = $cVal->Strcheck($model->criminalConvictedDetails,&$this->errflag);
		if($dummy==1) $this->addError('criminalConvictedDetails',Yii::t('en','Criminal convicted details').Yii::t('en','err_label1'));		
		}
        if($model->isCriminalProceeding=='Y')
		{
		$dummy = $cVal->Strcheck($model->criminalProceedingDetails,&$this->errflag);
		if($dummy==1) $this->addError('criminalProceedingDetails',Yii::t('en','Criminal proceeding details').Yii::t('en','err_label1'));		
		}
		
		
	}
	

}
?>