<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class SearchForm extends CFormModel
{
	public	$uid;
	public	$name;	
	public	$dob;
	public	$gender;
	public	$emailid;	
	public	$anytext;
	public	$mobile;
	public	$program;
	public	$center;
	public	$teacher;
	public	$fromdate;
	public	$todate;
	
	
	/*public	$yearLastAttend;
	public  $action;
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
			
			array('uid','safe'),		
			array('name','safe'),
			array('dob','safe'),		
			array('gender','safe'),
			array('emailid','safe'),
			array('anytext','safe'),		
			array('mobile','safe'),
			array('program','safe'),
			array('center','safe'),		
			array('teacher','safe'),
				array('fromdate','safe'),
					array('todate','safe'),
				
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'uid'=>'UID',
			'name'=>'Name',
			'dob'=>'DOB',
			'gender'=>'Gender',
			'email'=>'Email',
			'anytext'=>'AnyText',
			'mobile'=>'Mobile',
			'program'=>'Program',
			'center'=>'Center',
			'teacher'=>'Teacher',
			'teacher'=>'Teacher',
			'fromdate'=>'From Date',
			'todate'=>'To Date',
			
			
			
		
		);
	}
	

}
?>