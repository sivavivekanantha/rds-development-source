<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class MobileDataForm extends CFormModel
{   
    public	$mobileDetails;
	public	$IVC_MobileCode;
	public	$phoneCode;
	public	$mobileNumber;
	public	$serviceProvider;
	public	$issuedByIsha;
	public	$paidByIsha;
	public	$paln;
	public  $action;
	public  $errflag;
	public  $errmsg;
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, cardno, date are required
			
			array('mobileDetails','required'),
			array('IVC_MobileCode','safe'),		
			array('phoneCode','safe'),		
			array('mobileNumber','required'),
			array('mobileNumber', 'length', 'min'=>10, 'max'=>10),
			array('serviceProvider','required'),
			array('issuedByIsha','safe'),		
			array('paidByIsha','safe'),
			array('paln','safe'),
			array('action','safe'),
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'mobileDetails'=>Yii::t('en','Mobile_label1'),
            'mobileNumber'=>Yii::t('en','Mobile_label2'),
			'serviceProvider'=>Yii::t('en','Mobile_label3'),
			'issuedByIsha'=>Yii::t('en','Mobile_label4'),
			'paidByIsha'=>Yii::t('en','Mobile_label5'),
			'paln'=>Yii::t('en','Mobile_label6'),
			'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'mobile'=>Yii::t('en','Header_label38'),
			'mobileA'=>Yii::t('en','Header_label39'),

			
		);
	}
	
	public function mobileDataValidate($model)
	{		
		    $cVal = new CommonValidator();
		    $dummy = $cVal->Strcheck($model->mobileDetails,&$this->errflag);
			if($dummy==1) $this->addError('mobileDetails',Yii::t('en','Mobile_label1').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model->mobileNumber,&$this->errflag);
			if($dummy==1) $this->addError('mobileNumber',Yii::t('en','Mobile_label2').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model->serviceProvider,&$this->errflag);
			if($dummy==1) $this->addError('serviceProvider',Yii::t('en','Mobile_label3').Yii::t('en','err_label1'));
			
	}
	

}
?>