<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ComputerSkillForm extends CFormModel
{
	public	$ivcSkillCode;
	public	$computer;	
	public	$details;		
	public  $action;
	public  $formValid;
	public  $formValid1;
	public  $formValid2;
	public  $errflag;
	public  $errmsg;
	public  $sno;
	
	
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// Computer are required
			
			array('ivcSkillCode','safe'),		
			array('computer','required'),
			array('details','safe'),		
			array('action','safe'),	
			array('formValid','safe'),		
			array('sno','safe'),	
			array('errflag','safe'),	
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'computer'=>Yii::t('en','Skill_label1'),
			'details'=>Yii::t('en','Skill_label2'),
			'formValid'=>Yii::t('en','Skill_label4'),
			'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'computerHead'=>Yii::t('en','Header_label8'),
			'computerHint'=>Yii::t('en','Header_label9'),
			
			'computerPopup'=>Yii::t('en','Header_label56'),
		);
	}
	
  public function ComplterOtherValidate($model)
	{
		
			$cVal = new CommonValidator();
	
			$dummy = $cVal->Strcheck($model->computer,$this->errflag);		
			if($dummy==1) $this->addError('computer',Yii::t('en','Skill_label1').Yii::t('en','err_label1'));
			if($model->computer=='Others')
			{
		    $dummy = $cVal->Strcheck($model->details,$this->errflag);
			if($dummy==1) $this->addError('details',Yii::t('en','Skill_label2').Yii::t('en','err_label1'));	
			}
			
		

		
	}
}


?>