<?php 
class searchModel extends BaseModel
{
public function retArray($recordSet)
	{

		$lstArray=array();
		while($field=mssql_fetch_object($recordSet))
		{	
			$lstArray[] = $field;
		} 
		
	return $lstArray;
		
	}
public function getResidentDetails($model)
{

	$query  = $this->createCommand('show_ResidentInformation');
	mssql_bind($query, '@ivc_Code', $model->uid, SQLVARCHAR, false,false,50);
	mssql_bind($query, '@Name', $model->name, SQLVARCHAR, false,false,50);
	if(strlen($model->dob)==10)	$dob=date('Y-m-d',strtotime($model->dob));
	mssql_bind($query, '@DOB', $dob,SQLVARCHAR, false,false,50); 
	mssql_bind($query, '@Gender', $model->gender, SQLINT4, false,false,5); 
	mssql_bind($query, '@Email_Id', $model->emailid, SQLVARCHAR, false,false,50); 
	mssql_bind($query, '@Anytext', $model->anytext, SQLVARCHAR, false,false,500);   	mssql_bind($query, '@Email_Id', $model->emailid, SQLVARCHAR, false,false,50);
	mssql_bind($query, '@Mobile', $model->mobile, SQLVARCHAR, false,false,1);
	mssql_bind($query, '@Program', $model->program, SQLINT4, false,false,5);	
	mssql_bind($query, '@Center', $model->center, SQLINT4, false,false,5);	
	mssql_bind($query, '@pgmdate', $model->fromdate, SQLINT4, false,false,5);			
       $result = @mssql_execute($query);		     
       return $result;
			
	
}
public function getcenter()
{
	    $query = $this->createCommand('sp_FetchCenter');      
    	$result = @mssql_execute($query);		     
        return $result;	
}

    public function dateDropDownList($program,$center)
	{
	
	  
		$query = $this->createCommand('showdateList');  
		mssql_bind($query, '@program', $program, SQLINT4, false,false,5); 	
		mssql_bind($query, '@center', $center, SQLINT4, false,false,5); 
    	$result = @mssql_execute($query);       
     
    	// print_r($this->retArray($result));
		return $this->retArray($result); 
		/*$query = $this->createCommand('showdateList');  
		mssql_bind($query, '@program', $program, SQLINT4, false,false,5); 	
		mssql_bind($query, '@center', $center, SQLINT4, false,false,5); 
    	$result = @mssql_execute($query);  
		$programDateLists = array();
		$programDateLists[0]= 'Select';
		while($row = mssql_fetch_array($result))
		{
				$value = $row['Event_Id'];
				$text = $row['Pgm_Date'];
				$programDateLists[$value]= $text;
		}
		
		return   $programDateLists;	*/
		
	}
	
	public function GetRFSearchDetails($monitor)
	{
		$query  = $this->createCommand('sp_RFIDSearch');
		mssql_bind($query, '@Monitor', $monitor, SQLINT4, false,false,5);
		$result = @mssql_execute($query);		     
        return $result;	
	}
	
	public function GetPersonViewDetails($Header_Code)
	{
	  $query  = $this->createCommand('SP_IVC_Same_Dept_Persons');	
	  mssql_bind($query, '@Header_Code',$Header_Code, SQLINT4, false,false,5);
	  $result = @mssql_execute($query);
      return $result;
	}
	public function GetPersonCountDetails()
	{
	  $query  = $this->createCommand('SP_IVC_Resident_Count');	
	  $result = @mssql_execute($query);
      return $result;
	}
	
	
	public function GetResidentAttDetails()
	{
	  $query1  = $this->createCommand('SP_IVC_Get_Resident_Attendance');	
	  $result1 = @mssql_execute($query1);

      return $result1;
	}
	
	public function saveResidentAttendance($cgrp,$ugrp,$dcnt)
	{
	 $query  = $this->createCommand('SP_IVC_Resident_Attendace');	
	 mssql_bind($query, '@cgrp',$cgrp, SQLTEXT, false,false,strlen($cgrp));
	 mssql_bind($query, '@ugrp',$ugrp, SQLTEXT, false,false,strlen($ugrp));
	 mssql_bind($query, '@dcnt',$dcnt, SQLINT4, false,false,5);
	 $result = @mssql_execute($query);
     
	 return $result;
	}	

}

?>