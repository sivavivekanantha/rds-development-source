<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
	class FamilyProgramForm extends CFormModel
	{
		public	$Header_Code;
		public	$familyPgmCode;
		public	$ishaYoga1;
		public	$ishaYoga2;
		public	$ishaYoga3;
		public	$ishaYoga4;
		public  $formValid; 
		public  $errflag; 
	 
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
			array('Header_Code','safe'),		
			array('familyPgmCode','safe'),		
			array('ishaYoga1','safe'),		
			array('ishaYoga2','safe'),		
			array('ishaYoga3','safe'),		
			array('ishaYoga4','safe'),		
			
			array('formValid','safe'),
			array('errflag','safe'),
			
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'name'=>Yii::t('en','FamilyProgram_label1'),
			'ishaYoga'=>Yii::t('en','FamilyProgram_label2'),
				'bsp'=>Yii::t('en','FamilyProgram_label3'),
					'hataYoga'=>Yii::t('en','FamilyProgram_label4'),
					'samayama'=>Yii::t('en','FamilyProgram_label5'),
					'formValid'=>Yii::t('en','FamilyProgram_label6'),
					'sno'=>Yii::t('en','Common_label1'),
					
					'FamilyPgmA'=>Yii::t('en','Header_label43'),
					'FamilyPgmB'=>Yii::t('en','Header_label44'),
		
		);
	}
	
		

}
?>