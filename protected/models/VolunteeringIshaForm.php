<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class VolunteeringIshaForm extends CFormModel
{ 
   

			public $volunteeringCode;           
			public $programName;                
			public $typeOfVolunteering;  
			public $action;
			
			public $volunteeringCode1;         
			public $programName1;               
			public $centerName1;         
			public $typeOfVolunteering1; 
			public $action1;
			
			public  $formValid;
	        public  $formValid1;	   
			
			public  $Others;	   
			public  $Others1;	   
			
			public  $errflag;
			public  $errmsg;
	 		
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// Deptname, occupation, date are required
			
			array('volunteeringCode','safe'),		
			array('programName','required'),
			array('typeOfVolunteering','safe'),		
			array('action','safe'),
			
			array('volunteeringCode1','safe'),		
			array('programName1','safe'),
			array('centerName1','safe'),		
			array('typeOfVolunteering1','safe'),
			array('action1','safe'),
			
			array('formValid','safe'),
			array('formValid1','safe'),
			
			array('Others','safe'),
			array('Others1','safe'),
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'programName'=>Yii::t('en','Volunteering_label1'),
			'typeOfVolunteering'=>Yii::t('en','Volunteering_label2'),
			'Others'=>Yii::t('en','Volunteering_label3'),
			'formValid'=>Yii::t('en','Volunteering_label7'),			
            'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'VolunteeringA'=>Yii::t('en','Header_label40'),
			'VolunteeringB'=>Yii::t('en','Header_label41'),
			
			'VolunteeringPopup'=>Yii::t('en','Header_label58'),
			
			
		);
	}
	
	
	public function volunteeringIshaValidate($model)
	{
			$cVal = new CommonValidator();
			
			$dummy = $cVal->Strcheck($model->programName,&$this->errflag);
			if($dummy==1) $this->addError('programName',Yii::t('en','Volunteering_label1').Yii::t('en','err_label1'));
			
			if($model->programName=='8888')
				{
				$dummy = $cVal->Strcheck($model->Others,$this->errflag);
				if($dummy==1) $this->addError('Others',Yii::t('en','Volunteering_label3').Yii::t('en','err_label1')); 
				}
						
	}
	

}



?>