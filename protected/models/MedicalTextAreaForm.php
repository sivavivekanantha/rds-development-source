<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class MedicalTextAreaForm extends CFormModel
{
	
		public $majorSurgeries;
		public $pmChallenged;
	    public $physicalIssue;    
		public $action2;    

			

		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
		
			
				array('majorSurgeries', 'safe'),
				array('pmChallenged', 'safe'),
				array('physicalIssue', 'safe'),	
				array('action2','safe'),
			
			
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'majorSurgeries'=>Yii::t('en','Medical_label14'),
			'pmChallenged'=>Yii::t('en','Medical_label15'),
			'physicalIssue'=>Yii::t('en','Medical_label16'),
			'action2'=>Yii::t('en','Common_label2'),
			
					
		);
	}
	

}
?>