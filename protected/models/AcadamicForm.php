<?php
class AcadamicForm extends CFormModel
{
	public	$ivcAcadamicCode;
	public	$category;
	public	$ugDegree;
	public	$pgDegree;
	public	$categoryOthers;
	public	$degreeOthers;
	public	$institutionName;
	public	$institutionCity;
	public	$specialization;
	public	$yop;
	public	$grade;
	public	$degree;
	public	$action;
	public	$sno;
	public  $errmsg;    
	public  $errflag; 
	
	/**
	 * Declares the validation rules.
	 */
	
	public function rules()
	{
		return array(
			array('ivcAcadamicCode','safe'),
			array('category','required'),
			array('ugDegree','safe'),
			array('pgDegree','safe'),
			array('categoryOthers','safe'),
			array('degreeOthers','safe'),
			array('institutionName','safe'),
			array('institutionCity','safe'),
			array('specialization','safe'),
			array('yop','safe'),
			array('grade','safe'),
			array('action','safe'),
			
		);	
	}

	public function attributeLabels()
	{
		return array(
			'category'=>Yii::t('en','Acadamic_label1'),
			'ugDegree'=>Yii::t('en','Acadamic_label2'),
			'pgDegree'=>Yii::t('en','Acadamic_label2'),
			'categoryOthers'=>Yii::t('en','Acadamic_label2'),
			'degreeOthers'=>Yii::t('en','Acadamic_label2'),
			'institutionName'=>Yii::t('en','Acadamic_label3'),
			'institutionCity'=>Yii::t('en','Acadamic_label4'),
			'specialization'=>Yii::t('en','Acadamic_label5'),
			'yop'=>Yii::t('en','Acadamic_label6'),
			'grade'=>Yii::t('en','Acadamic_label7'),
			'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'academic'=>Yii::t('en','Header_label5'),
			'academicHint'=>Yii::t('en','Header_label6'),
		);
		
	}
	
	
	public function academicValidate($model)
	{
			$cVal = new CommonValidator();
			$dummy = $cVal->Strcheck($model->category,&$this->errflag);
			if($dummy==1) $this->addError('category',Yii::t('en','Acadamic_label1').Yii::t('en','err_label1'));		
			if($model->category=='4')
			{
				$dummy = $cVal->Strcheck($model->ugDegree,$this->errflag);
				if($dummy==1) $this->addError('ugDegree',Yii::t('en','Acadamic_label2').Yii::t('en','err_label1')); 
				}
			if($model->category=='5')
				{
				$dummy = $cVal->Strcheck($model->pgDegree,$this->errflag);
				if($dummy==1) $this->addError('pgDegree',Yii::t('en','Acadamic_label2').Yii::t('en','err_label1')); 
				}	
			if($model->category=='7')
			{
				$dummy = $cVal->Strcheck($model->categoryOthers,$this->errflag);
				if($dummy==1) $this->addError('categoryOthers',Yii::t('en','Acadamic_label2').Yii::t('en','err_label1')); 
			}	
				
			if($model->category=='4' and $model->ugDegree=='1')
			{
				$dummy = $cVal->Strcheck($model->degreeOthers,$this->errflag);
				if($dummy==1) $this->addError('degreeOthers',Yii::t('en','Acadamic_label2').Yii::t('en','err_label1')); 
			}
			
			if($model->category=='5' and $model->pgDegree=='2')
			{
				$dummy = $cVal->Strcheck($model->degreeOthers,$this->errflag);
				if($dummy==1) $this->addError('degreeOthers',Yii::t('en','Acadamic_label2').Yii::t('en','err_label1')); 
			}
				
	}

}
?>