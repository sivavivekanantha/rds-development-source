<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class PassportForm extends CFormModel
{
	
	public	$nationality;
	public	$passportNo;	
	public	$passportExpiryDate;
	public	$placeOfIssue;
	public	$nameInPassport;
	public	$dualCitizenship;
	public	$dualDetails;
	public	$visaCategory;
	public	$visaType;
	public	$documentNumber;
	public	$visaIssueDate;
	public	$visaExpiryDate;
	public  $errflag;
	public  $errmsg;
	public $formValid;
	 private $_identity;
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// name, age, city and mobileno are required
		
			array('nationality','safe'),
			array('passportNo', 'required'),
			array('passportExpiryDate', 'safe'),
			
			array('placeOfIssue', 'safe'),
			array('nameInPassport', 'safe'),
			array('dualCitizenship', 'safe'),
			array('dualDetails', 'safe'),
			array('visaCategory', 'safe'),
			
			array('visaType', 'safe'),
			array('documentNumber', 'safe'),
			array('visaIssueDate', 'safe'),
			array('visaExpiryDate', 'safe'),
			array('formValid','safe'),
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'nationality'=>Yii::t('en','Passport_label1'),
			'passportNo'=>Yii::t('en','Passport_label2'),
			'passportExpiryDate'=>Yii::t('en','Passport_label3'),
			'placeOfIssue'=>Yii::t('en','Passport_label4'),
			'nameInPassport'=>Yii::t('en','Passport_label5'),
			'dualCitizenship'=>Yii::t('en','Passport_label6'),
			'dualDetails'=>Yii::t('en','Passport_label7'),
			'visaCategory'=>Yii::t('en','Passport_label8'),
			'visaType'=>Yii::t('en','Passport_label9'),
			'documentNumber'=>Yii::t('en','Passport_label10'),
			'visaIssueDate'=>Yii::t('en','Passport_label11'),
			'visaExpiryDate'=>Yii::t('en','Passport_label12'),
			'formValid'=>Yii::t('en','Passport_label13'),
			
			'passportDetails'=>Yii::t('en','Header_label2'),
			'visaDetails'=>Yii::t('en','Header_label3'),
			//'visaHint'=>Yii::t('en','Header_label4'),
			
			
		
		);
	}
	
	public function passportValidate($model)
	{
		$cVal = new CommonValidator();
	
		$dummy = $cVal->Strcheck($model->passportNo,$this->errflag);
		if($dummy==1) $this->addError('passportNo',Yii::t('en','Passport_label2').Yii::t('en','err_label1'));
		
		if($model->visaCategory == 1)
		{	
		$dummy = $cVal->Zerocheck($model->visaType,$this->errflag);
		if($dummy==1) $this->addError('visaType',Yii::t('en','Passport_label9').Yii::t('en','err_label1'));
			
		$dummy = $cVal->Strcheck($model->documentNumber,$this->errflag);
		if($dummy==1) $this->addError('documentNumber',Yii::t('en','Passport_label10').Yii::t('en','err_label1'));
	
		
	  
	  	$dummy = $cVal->Strcheck($model->visaIssueDate,$this->errflag);
		if($dummy==1) $this->addError('visaIssueDate',Yii::t('en','Passport_label11').Yii::t('en','err_label1'));
	
		$dummy = $cVal->Strcheck($model->visaExpiryDate,$this->errflag);
		if($dummy==1) $this->addError('visaExpiryDate',Yii::t('en','Passport_label12').Yii::t('en','err_label1'));
				}
	
				
			}
		
	
}
?>
