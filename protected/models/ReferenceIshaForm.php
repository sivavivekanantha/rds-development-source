<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ReferenceIshaForm extends CFormModel
{    

			public $refernceCode;           
			public $name;                
			public $designation;              
			public $relationship;              
			public $contactNo;               
			public $emailId;              
			public $action;
			
			
			public $referenceIshaCode;           
			public $rName;                
			public $centerName;              
			public $phoneNo;              
			public $rEmailId;               
			public $action1;
			
			public $referenceResidentCode;           
			public $rRname;                
			public $department;              
			public $cNo;              
			public $action2;
			
			public  $errflag;
	        public  $errmsg;
			
			


	
	
	
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// Deptname, occupation, date are required
			
			array('referenceIshaCode','safe'),		
			array('rName','required'),
			array('centerName','required'),		
			array('phoneNo','safe'),				
			array('rEmailId','safe'),			
			array('action1','safe'),	
			
			array('refernceCode','safe'),		
			array('name','safe'),
			array('designation','safe'),		
			array('relationship','safe'),				
			array('contactNo','safe'),			
			array('emailId','safe'),
			array('action','safe'),	
			
			array('referenceResidentCode','safe'),		
			array('rRname','safe'),
			array('department','safe'),		
			array('cNo','safe'),				
			array('action2','safe'),	
			
													
					
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'rName'=>Yii::t('en','Reference_label1'),
			'centerName'=>Yii::t('en','Reference_label6'),
			'phoneNo'=>Yii::t('en','Reference_label4'),
			'rEmailId'=>Yii::t('en','Reference_label5'),
			'cNo'=>Yii::t('en','Reference_label4'),
			'rRname'=>Yii::t('en','Reference_label1'),		
            'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'referenceC'=>Yii::t('en','Header_label47'),
			'referenceF'=>Yii::t('en','Header_label50'),
			
		);
	}
	
	public function referenceIshaEmailValidate($model1)
	{
			$cVal = new CommonValidator();
			
			$dummy = $cVal->Strcheck($model1->rName,&$this->errflag);
			if($dummy==1) $this->addError('rName',Yii::t('en','Reference_label1').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model1->centerName,&$this->errflag);
			if($dummy==1) $this->addError('centerName',Yii::t('en','Reference_label6').Yii::t('en','err_label1'));
						
		    if(strlen($model1->rEmailId)>0)$dummy = $cVal->Emailcheck($model1->rEmailId,&$this->errflag);
			if($dummy==1) $this->addError('rEmailId','Invalid Email ID Format');
		
				
	}

	
}



?>