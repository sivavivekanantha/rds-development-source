<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ReferenceForm extends CFormModel
{    

			public $refernceCode;           
			public $name;                
			public $designation;              
			public $relationship;              
			public $contactNo;               
			public $emailId;              
			public $action;
			
			
			public $referenceIshaCode;           
			public $rName;                
			public $centerName;              
			public $phoneNo;              
			public $rEmailId;               
			public $action1;
			
			public $referenceResidentCode;           
			public $rRname;                
			public $department;              
			public $cNo;              
			public $action2;
			
			public  $errflag;
	                public  $errmsg;
			
			
	
		/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	return array( 		// Deptname, occupation, date are required
			
			array('refernceCode','safe'),		
			array('name','required'),
			array('designation','required'),		
			array('relationship','safe'),				
			array('contactNo','safe'),			
			array('emailId','safe'),
			array('action','safe'),			
			
			
			array('referenceIshaCode','safe'),		
			array('rName','safe'),
			array('centerName','safe'),		
			array('phoneNo','safe'),				
			array('rEmailId','safe'),			
			array('action1','safe'),	
			
			array('referenceResidentCode','safe'),		
			array('rRname','safe'),
			array('department','safe'),		
			array('cNo','safe'),				
			array('action2','safe'),	
			
																	
			);
			
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			
			'name'=>Yii::t('en','Reference_label1'),
			'designation'=>Yii::t('en','Reference_label2'),
			'relationship'=>Yii::t('en','Reference_label3'),
			'contactNo'=>Yii::t('en','Reference_label4'),
			'emailId'=>Yii::t('en','Reference_label5'),			
             'sno'=>Yii::t('en','Common_label1'),
			'action'=>Yii::t('en','Common_label2'),
			
			'referenceA'=>Yii::t('en','Header_label45'),
			'referenceB'=>Yii::t('en','Header_label46'),
			'referenceE'=>Yii::t('en','Header_label49'),
			
			
		);
	}
	public function referenceEmailValidate($model)
	{
			$cVal = new CommonValidator();
			
			$dummy = $cVal->Strcheck($model->name,&$this->errflag);
			if($dummy==1) $this->addError('name',Yii::t('en','Reference_label1').Yii::t('en','err_label1'));
			$dummy = $cVal->Strcheck($model->designation,&$this->errflag);
			if($dummy==1) $this->addError('designation',Yii::t('en','Reference_label2').Yii::t('en','err_label1'));
			
			if(strlen($model->emailId)>0)$dummy = $cVal->Emailcheck($model->emailId,&$this->errflag);
			if($dummy==1) $this->addError('emailId','Invalid Email ID Format');
						
		
		
		
				
	}


}



?>