<?php
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Isha - Resident Database System',
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),
	   'language'=>'en',


	'modules'=>array(
		// uncomment the following to enable the Gii tool
	
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123',
		 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
			//'ipFilters'=>array('127.0.0.1','::1'),
			 'ipFilters'=>array('192.168.104.26', '192.168.104.76'),
             'newFileMode'=>0666,
             'newDirMode'=>0777,
		),

	),
	
	// application components
	'components'=>array(
		'user'=>array(
			'class' => 'WebUser',
    		'loginUrl' => array('site/index'),
    		'allowAutoLogin' => false, 			// enable cookie-based authentication
    		'authTimeout' => 60000

		),
		'Smtpmail'=>array(
            'class'=>'application.extensions.smtpmail.PHPMailer',
            'Host'=>"smtp.gmail.com",
            'Username'=>'rfidrelated@gmail.com',
            'Password'=>'IshaRfid',
            'Mailer'=>'smtp',
            'Port'=>587,
            'SMTPAuth'=>true, 
        ),
        
	// SESSIOn TIMED Out
	  'session' => array(
		'class'=>'CDbHttpSession',
		'useTransparentSessionID'   =>($_POST['PHPSESSID']) ? true : false,
		'autoStart' => 'false',
		'cookieMode' => 'only',
		//'savePath' => '/path/to/new/directory',
		'timeout' => 60000
   	  ),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
			'showScriptName'=>true,
		),
		
	/*	'pusher'=>array(
       'class'=>'Pusher',
       'key'=>'df55cae3d661e7a25656',
       'secret'=>'03c81c28c7c48da6979c',
       'appId'=>'67175',
     ), */
	 
		/*'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database

		'db'=>array(
            'class'=>'CDbConnection',
			'connectionString' => 'mssql:host=192.168.104.221;dbname=RDS_Beta', // 'mysql:host=localhost;dbname=Customer',
			//'emulatePrepare' => true,
			'username' => 'db_testing', // 'root',
			'password' => 'Isha123', // 'ishadb',
			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				
				array(
					'class'=>'CWebLogRoute',
				),
				
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
	//'image'=>array(     
//'class'=>'application.extensions.image.CImageComponent',            
//'driver'=>'GD', 
 // 'params'=>array(),
//) ,
	
);