$(document).ready(function() {

    $('#datepicker-example1').Zebra_DatePicker();

    $('#FTV_DOJ_Isha').Zebra_DatePicker({
        direction: -1 ,   // boolean true would've made the date picker future only
             format: 'd-m-Y',
			 show_hint :  'If you dont remember exact date, pls choose approximate date closest to the actual date in the date fields'              // but starting from today, rather than tomorrow
    });
	
	
	 $('#passportExpiryDate').Zebra_DatePicker({
          // boolean true would've made the date picker future only
             format: 'd-m-Y'              // but starting from today, rather than tomorrow
    });
	
	
	$('#visaIssueDate').Zebra_DatePicker({
		 direction: -1,
    pair: $('#visaExpiryDate'),
	format: 'd-m-Y' 
});
 
$('#visaExpiryDate').Zebra_DatePicker({
    direction: 1,
	format: 'd-m-Y' 
});

$('#fromdate_start').Zebra_DatePicker({
	direction: -1,  
	format: 'd-m-Y',
	show_hint :'If you dont remember exact date, pls choose approximate date closest to the actual date in the date fields' 
});
 
$('#todate_end').Zebra_DatePicker({
    direction: -1,
	format: 'd-m-Y',
	show_hint :  'If you dont remember exact date, pls choose approximate date closest to the actual date in the date fields'
});



	
	$('#cardExpDate').Zebra_DatePicker({
			  format: 'd-m-Y'  
		});
 $('#fromDate1').Zebra_DatePicker({
        direction: -1 ,   // boolean true would've made the date picker future only
             format: 'd-m-Y' ,
			 show_hint :  'If you dont remember exact date, pls choose approximate date closest to the actual date in the date fields'             // but starting from today, rather than tomorrow
    });		
		
	
	
	

    $('#datepicker-example3').Zebra_DatePicker({
        direction: true,
        disabled_dates: ['* * * 0,6']   // all days, all monts, all years as long
                                        // as the weekday is 0 or 6 (Sunday or Saturday)
    });

    $('#datepicker-example4').Zebra_DatePicker({
        direction: [1, 10]
    });

    $('#datepicker-example5').Zebra_DatePicker({
        // remember that the way you write down dates
        // depends on the value of the "format" property!
        direction: ['2012-08-01', '2012-08-12']
    });

    $('#datepicker-example6').Zebra_DatePicker({
        // remember that the way you write down dates
        // depends on the value of the "format" property!
        direction: ['2012-08-01', false]
    });

    $('#datepicker-example7-start').Zebra_DatePicker({
        direction: true,
        pair: $('#datepicker-example7-end')
    });

    $('#datepicker-example7-end').Zebra_DatePicker({
        direction: 1
    });

    $('#datepicker-example8').Zebra_DatePicker({
        format: 'M d, Y'
    });

    $('#datepicker-example9').Zebra_DatePicker({
        show_week_number: 'Wk'
    });

    $('#dob').Zebra_DatePicker({
        view: 'years',
		direction: -1, 
		format: 'd-m-Y',
		show_hint :  'If you dont remember exact date, pls choose approximate date closest to the actual date in the date fields'
		
    });

    $('#datepicker-example11').Zebra_DatePicker({
        format: 'm Y'
    });

    $('#datepicker-example12').Zebra_DatePicker({
        onChange: function(view, elements) {
            if (view == 'days') {
                elements.each(function() {
                    if ($(this).data('date').match(/\-24$/))
                        $(this).css({
                            background: '#C40000',
                            color:      '#FFF'
                        });
                });
            }
        }
    });

    $('#datepicker-example13').Zebra_DatePicker({
        always_visible: $('#container')
    });

    $('#datepicker-example14').Zebra_DatePicker();

$('#datepicker-example15').Zebra_DatePicker({
        format: 'd-m-Y'
    });

$('#dob2').Zebra_DatePicker({
	direction: -1,
        format: 'd-m-Y'
    });
	
	$('#doj').Zebra_DatePicker({
		direction: -1,
        format: 'd-m-Y'
    });
	$('#cardIssueDate').Zebra_DatePicker({
		direction: -1,
        format: 'd-m-Y'
    });
	$('#cardExpiryDate').Zebra_DatePicker({
		direction: 1,
        format: 'd-m-Y'
    });
	$('#fTVCdate').Zebra_DatePicker({
		direction: 1,
        format: 'd-m-Y'
    });
	$('#amentiesCexpiry').Zebra_DatePicker({
	    direction: 1,
        format: 'd-m-Y'
    });
	$('#mCardExpiry').Zebra_DatePicker({
		direction: 1,
        format: 'd-m-Y'
    });
	
	 $('#checkdate_from').Zebra_DatePicker({
          direction: -1,
    pair: $('#checkdate_To'),
             format: 'd-m-Y'              // but starting from today, rather than tomorrow
    });
 
    $('#checkdate_To').Zebra_DatePicker({
          direction: 1,
             format: 'd-m-Y'              // but starting from today, rather than tomorrow
    });


	
});