<?php
ini_set('error_reporting', E_ALL);
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
// change the following paths if necessary
$yii=realpath(__DIR__ . '/../..').'/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
//defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
//defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
date_default_timezone_set("UTC");

require_once($yii);
Yii::createWebApplication($config)->run();
