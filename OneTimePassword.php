<?php ?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <title>Welcome to the Ashram Visit Registration webpage</title>
  <link rel="stylesheet" type="text/css" media="all" href="css/style.css">
  <link rel="stylesheet" type="text/css" media="all" href="css/responsive.css">

	<link rel="stylesheet" href="css/otpstyle/classic.css" id="theme_base">
	<link rel="stylesheet" href="css/otpstyle/classic.date.css" id="theme_date">
	<link rel="stylesheet" href="css/otpstyle/classic.time.css" id="theme_time">

	<script type="text/javascript" src="js/jquery-1.5.min.js"></script>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.filter_input.js"></script>

	<script src="js/picker.js"></script>
	<script src="js/picker.date.js"></script>
	<script src="js/picker.time.js"></script>
	<script src="js/legacy.js"></script>
	<script src="js/js_function.js"></script>
  
  <script type="text/javascript">
	$("#new_req").hide();  
  	function showdate()
	{
		$("#Arrival_date").pickadate({
		format: 'dd-mm-yyyy',
    // An integer (positive/negative) sets it relative to today.
    min: 3,
    // `true` sets it to today. `false` removes any limits.
    max: 63
})
	}
	
  	function showtime()
	{
		$("#Arrival_time").pickatime();
	}
  		
	
  	function randomnumber(obj1)
	{

		var obj = $('#telephone').val();	
    	if ( obj.length == 10 )
		{
			url ="ajax.php?listcode=1&telephone="+obj+"&frm="+obj1;	
			$("#test").load(url);
		}
	}

	function blockmobile()
	{
		$('#mainscreen').html('<div class="error">Namaskaram. Your Mobile No. is blocked today. Please try tomorrow. Pranam</div>');
	}
	function blockotp()
	{
		$("#otp").hide();
		$("#verifymobile").show();			
	}
		
	function Update_req()
	{	
		$("#mainContent").show();	
		$("#modify_req").hide();
	}
		function Cancel_req()
	{	
	
		var obj = $('#telephone').val();	
    	if ( obj.length == 10 )
		{
			url ="ajax.php?listcode=4&telephone="+obj;	
			//$("#test").load(url);
			$.ajax({url:url,success:function(result){
    			$("#mainscreen").html(result);
				$("#modify_req").hide();
				$("#new_req").show();
  				}});
		}
		
		$("#mainContent").show();	
		$("#cancel_req").hide();
	}
	

	function verify()
	{
			var obj = $('#onetimepass').val();
			var obj1 = $('#RandomNumber').val();
			var obj2 = $('#otpCount').val();
			var tobj = $('#telephone').val();
			var errorcode = $('#errorcode').val();
		
			obj2++;		
			if(obj2<4) {
				if(obj==obj1 ){
					
					if(errorcode==1)
				{
					
					$("#otp").hide();
					$("#verifymobile").hide();	
					$("#modify_req").show();	
				}
				else{
				
						$("#mainContent").show();
						$("#verifymobile").hide();	
							
					}
				}
					
				else{
						$("#otpError").show();
						$('#onetimepass').val('');
					    $('#otpCount').val(obj2);
					}
				
					
			} 
			else {
					url ="ajax.php?listcode=2&telephone="+tobj;
					$("#mainscreen").load(url);	
			}
		}
		function RegisterData()
		{
			var obj = $('#telephone').val();
			var obj1 = $('#No_of_Visitors').val();
			var obj2 = $('#No_of_Days').val();
			var obj3 = $('#Arrival_date').val();
			var obj4 = $('#Arrival_time').val();
			if(obj1>0 && obj2>0 && obj3.length == 10)
			{
				url ="ajax.php?listcode=3&telephone="+obj+"&No_of_Visitors="+obj1+"&No_of_Days="+obj2+"&Arrival_date="+obj3+"&Arrival_time="+obj4;		
				$.ajax({url:url,success:function(result){
    			$("#mainscreen").html(result);
				$("#new_req").show();
  				}});
			}
		}
	function offline_update(obj)
	{
		$("#otp").hide();
		if(obj==1)
			$("#modify_req").show();
		else $("#mainContent").show();
	}
  </script>

</head>
<body>
<div id="test"></div>
	<section id="container">
	<?php if($_GET['frm']==1) {	?>
	<span class="chyron"><em><a href="http://192.168.104.220/vro/main.php">&laquo; back to the site</a></em></span>	
	<?php } else {?>
	<span class="chyron"><em><a href="http://www.ishafoundation.org/">&laquo; back to the site</a></em></span>
	<?php } ?>
			<h2>Welcome to the Ashram Visit Registration webpage</h2>
			<div id="mainscreen">
        	<div id="otp">
				<form name="hongkiat" id="hongkiat-form" method="post" action="#">
	        	<div id="wrapping" class="clearfix">
	        	<section id="aligned">
		           	<input type="tel" name="telephone" id="telephone" onkeydown="return numberonly('telephone')" placeholder="Enter Phone number" maxlength="10" class="txtinput">
		            <section id="buttons">
		            	<input type="button" name="Enter" id="submitbtn" class="submitbtn"  value="Enter" onClick="return randomnumber(<?php echo $_GET['frm']?>);">
		      		</section>
		        </section>
	            </div>
	            </form>
	        </div>
            
			<div id="verifymobile" style="display:none;">
			 <form name="hongkiat" id="hongkiat-form" method="post" action="#">
				<input type="hidden" id="otpCount" name="otpCount" value="" />
				<div id="wrapping" class="clearfix">
					<section id="aligned">
						<input type="text" name="onetimepass" id="onetimepass" placeholder="Enter One Time Password" autocomplete="off" tabindex="1" maxlength="4" class="txtinput"><div id="otpError" style="display: none;" class="errorno" > Please Enter Valid OTP</div>
                        <section id="buttons">
            			<input type="button" name="Enter" id="submitbtn" class="submitbtn"  value="Enter" onClick="return verify();">
      				 </section>
					</section>
					
					 
				</div>
			</form>
        </div>
		
		 <div id="mainContent" style="display:none;">	
         <form name="hongkiat" id="hongkiat-form" method="post" action="#">
			<div id="wrapping" class="clearfix">
				<p style="text-align: right">Fields with red border are mandatory</p>
				<section id="aligned">
				
			<!--<input type="text" name="name" id="name" placeholder="Your name" autocomplete="off" tabindex="1" class="txtinput">
			<input type="email" name="email" id="email" placeholder="Your e-mail address" autocomplete="off" tabindex="2" class="txtinput">-->
				<input type="text" name="No_of_Visitors" id="No_of_Visitors" onkeydown="return numberonly('No_of_Visitors')" placeholder="No.of Visitors" autocomplete="off" maxlength="3" tabindex="3" class="txtinput" style="border-color: #ff4242">
				<input type="text" name="No_of_Days" id="No_of_Days" onkeydown="return numberonly('No_of_Days')" placeholder="No.of Days" autocomplete="off" maxlength="3" tabindex="4" class="txtinput" style="border-color: #ff4242">
				<input type="text" name="Arrival_date" id="Arrival_date" placeholder="Arrival Date" autocomplete="off" tabindex="5" class="txtinput" onClick="showdate();" style="border-color: #ff4242">
				<input type="text" name="Arrival_time" id="Arrival_time" placeholder="Arrival Time" autocomplete="off" tabindex="6" class="txtinput" onClick="showtime();">			
				</section>
			</div>
		<section id="buttons">
			<input type="reset" name="reset" id="resetbtn" class="resetbtn" value="Reset" tabindex="7">
			<input type="button" name="submit" id="submitbtn" class="submitbtn" value="Submit this!" onClick="RegisterData();" tabindex="8">
			<br style="clear:both;">
		</section>
</form>
        </div>
   </div>
			<div id="new_req" style="display: none; font-size:14px; text-align:center; padding-top:15px;">
			<?php if($_GET['frm']<>1) { ?>
			<a href="OneTimePassword.php">Goto Registration Page</a></div>	
			<?php } else {?>
			<a href="OneTimePassword.php?frm=1">Goto Registration Page</a></div>	
			<?php } ?>
			<div id="modify_req" style="display: none;">
			<div id="wrapping" class="" style="width:30%; margin:0px auto;">
				<section id="aligned">
			<section id="buttons">
				<div><input type="button" name="submit" id="submitbtn" class="submitbtn" value="Update Request!" onClick="Update_req();"></div><br><br><br><br><br>
				<div><input type="button" name="submit" id="submitbtn" class="submitbtn" value="Cancel Request!" onClick="Cancel_req();"></div>
			</section>
			</section>
			</div>
            <p>&nbsp;</p>
			</div>	
			
	</section>

</body>
</html>
<?php ?>